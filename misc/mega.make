#!/bin/bash

hn=`hostname`

if [ $hn == hpclogin1 ]; then
  echo On Minerva;

  module load ompi/1.6.4/gnu/4.3.4
  export PYTHONPATH=/storage/maths/masnaj/cython

  module load cmake/3.0.2;
  module load gcc/4.9.2;
  module load boost/1.57.0/gcc/4.9.2/python;

elif [ $hn == login1 ]; then
  echo On Tinis;

  module load cmake/3.0.2;
  module load gcc/4.9.2;
  module load boost/1.57.0/gcc/4.9.2/python;
else
  echo Not on Minerva or Tinis;

  module load ompi/1.6.4/gnu/4.3.4
  export PYTHONPATH=/storage/maths/masnaj/cython
fi;


if [ $hn == radcliffe-XPS13-9333 ]; then
  echo On Laptop;
else
  echo Not on Laptop;
fi;


echo Setting-up dune config file ...

cd ../..
COUPLING=$PWD
cd ..

pwd | sed 's/\//\\\//g' > temp.tmp

loc=`more temp.tmp`

echo 
echo Using root location of "$loc"
echo 

if [ $1 == nobem ]; then
  cd $COUPLING
  sed -e "s/LOKATION/$loc/g" -e "/bempp-build/d" dune-fem-fem-coupling/misc/config.opts > config.opts
  dunemods="istl common grid localfunctions geometry grid-glue alugrid fem fem-howto";
else
  
  if [ ! -d mod ]; then mkdir mod; fi;
  cd mod;
  echo Entering $PWD;

  if [ ! -d bempp ]; then 
    echo Cloning bempp;
    git clone https://github.com/bempp/bempp.git;
  fi;

  echo Check using good version of bempp ...;
  cd bempp;
  echo Entering $PWD;
  bempploc=`pwd`;
  echo Bempp location is $bempploc;

  if [ $1 == bembutnobembuild ]; then
    echo Skipping bempp build step;
  else

  git checkout a50f417fe00f16228ed7bd98cdd91f443c374ac2;

  mv lib/grid/concrete_domain_index.hpp aaa; sed -e "s/LevelGridView/LeafGridView/g" -e "s/levelIndexSet(0)/leafIndexSet()/g" aaa > lib/grid/concrete_domain_index.hpp;
  mv lib/grid/concrete_grid.hpp bbb; sed -e "s/LevelGridView/LeafGridView/g" -e "s/levelView(level)/leafGridView()/g" bbb > lib/grid/concrete_grid.hpp;
  mv lib/grid/concrete_grid_view.hpp ccc; sed -e "s/LevelGridView/LeafGridView/g" -e "s/DuneLevelGridView/DuneLeafGridView/g" ccc > lib/grid/concrete_grid_view.hpp;

  if [ $hn == hpclogin1 ]; then
    sed "s/.*tbb43_20150209oss_lin.tgz/\/home\/maths\/masnaj\/tbb43_20150209oss_lin.tgz/g" cmake/TBB/lookup.cmake > bob.bob;
    sed "/URL_MD5 \${TBB_URL_MD5}/d" bob.bob > cmake/TBB/lookup.cmake;
  fi;

  cd ..;

  if [ ! -d bempp-build ]; then 
     echo Making directory bempp-build; 
     mkdir bempp-build; 
   else 
     echo Existing bempp-build directory found; 
  fi;

  cd bempp-build;
  echo Entering $PWD;
  
  if [ $hn == hpclogin1 ]; then
    CC=gcc CXX=g++ cmake -DBOOST_ROOT=$EBROOTBOOST -DCMAKE_BUILD_TYPE=Development -DWITH_FENICS=ON  $bempploc;
  else
  CC=gcc-4.9 CXX=g++-4.9 cmake -DCMAKE_BUILD_TYPE=Development -DWITH_FENICS=ON  $bempploc;
  fi;

  if [ $hn == radcliffe-XPS13-9333 ]; then
    make;
  else
    make -j6;
  fi;

  sed -e 's/::LevelGridView/::LeafGridView/g' -e 's/levelView(level)/leafGridView()/g' include/bempp/grid/concrete_grid.hpp > bob
  sed -e 's/::GlobalIdSet/::LocalIdSet/g' -e 's/->globalIdSet/->localIdSet/g' bob > include/bempp/grid/concrete_grid.hpp
  rm bob

  mv external/src/dune-grid/dune/grid/io/file/dgfparser/entitykey.hh bib
  sed '/topology/d' bib > external/src/dune-grid/dune/grid/io/file/dgfparser/entitykey.hh
  rm bib


  mv external/src/dune-grid/dune/grid/geometrygrid/gridview.hh bub
  sed 's/\(.*!indexSet.*\)/        return grid().leafIndexSet();\n\1/g' bub > external/src/dune-grid/dune/grid/geometrygrid/gridview.hh
  rm bub

  fi;

  cd $COUPLING

  if [ $hn == hpclogin1 ]; then
    sed -e "s/LOKATION/$loc/g" -e "s/LOCATION/$loc/g" -e "s/gcc-4.9/gcc/g" -e "s/g++-4.9/g++/g" dune-fem-fem-coupling/misc/config.opts > config.opts;
  else
  sed -e "s/LOKATION/$loc/g" -e "s/LOCATION/$loc/g" dune-fem-fem-coupling/misc/config.opts > config.opts
  fi;

  dunemods="istl grid-glue alugrid fem";
fi

echo ==========================================================

echo Cloning DUNE modules into $PWD

for i in $dunemods; 
do
  repo=https://gitlab.dune-project.org;
  vers=releases/2.4;
  extra=".git";
  repoplus=core;
  if [ $i == alugrid ]; then
    repoplus=extensions;
  elif [ $i == fem ]; then
    repoplus=dune-fem;
  elif [ $i == grid-glue ]; then
    repoplus=extensions;
    vers=8e4267dca232ab09cdcb66b0806a5a5c85622ce6;
  elif [ $i == fem-howto ]; then
    repo=https://users.dune-project.org;
    repoplus=repositories/projects;
  fi;
  if [ ! -d dune-$i ]; then 
    git clone $repo/$repoplus/dune-$i$extra;
    # mkdir dune-$i;
  fi;
  cd dune-$i;
  echo Inside $PWD;
  git checkout $vers;
  if [ $i == iistl ]; then
    cp $COUPLING/dune-fem-fem-coupling/misc/dune.mudule ./dune.module;
  elif [ $i == feem ]; then

    if [ $1 == bembutnobembuild ]; then
      echo Skipping complexify patch;
    else

    git apply $COUPLING/dune-fem-fem-coupling/misc/complexify.patch;
    fi;
  fi;
  cd ..;
done

echo ==========================================================

echo Inside $PWD;

if [ $1 == nobem ]; then
  ./dune-common/bin/dunecontrol --opts=config.opts all;
else
  ../mod/bempp-build/external/bin/dunecontrol --opts=config.opts all;
fi
