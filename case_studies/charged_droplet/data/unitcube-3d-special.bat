c=0; 
rm vertex.tmp;
rm simplex.tmp;
     for x in -50 50; do 
     for y in -50 50; do 
     for z in -50 50; do 
        xm=`bc <<< "$x-50"`; 
        xp=`bc <<< "$x+50"`; 
        ym=`bc <<< "$y-50"`; 
        yp=`bc <<< "$y+50"`; 
        zm=`bc <<< "$z-50"`; 
        zp=`bc <<< "$z+50"`; 
        c=`bc <<< "$c+1"`; 
        sed -e "s/c/$c/g" -e "s/iiiim/$xm/g" -e "s/iiiip/$xp/g" -e "s/iiiii/$x/g" -e "s/jjjjm/$ym/g" -e "s/jjjjp/$yp/g" -e "s/jjjjj/$y/g" -e "s/kkkkm/$zm/g" -e "s/kkkkp/$zp/g" -e "s/kkkkk/$z/g" unitcube-3d-special.ver >> vertex.tmp;
        sed "s/c/pre$c/g" unitcube-3d-special.sim >> simplex.tmp;
        echo "  " >> simplex.tmp;
     done; 
     done; 
     done;

sed "s/\ *ID.*//g" vertex.tmp | sort -nk 1 | uniq | sed 's/\-/\\\-/g' > ordered.tmp;



if [ -f sed.map ]; then rm sed.map; fi;

if [ -f sed.diag ]; then rm sed.diag; fi;

q=0;
     for i in `more ordered.tmp`; do 
        echo "===================================" >> sed.diag;
        echo "i = $i" >> sed.diag;
        w=`grep -n " $i" vertex.tmp | sed "s/.*ID//g"`;
        echo "w = $w" >> sed.diag;
        echo "q = $q" >> sed.diag;
        for j in $w; do
           echo "     j = $j" >> sed.diag;
           echo "s/ pre$j / $q /g" >> sed.map; 
        done;
        q=`bc <<< "$q+1"`; 
     done


more unitcube-3d-special.one > unitcube-3d-special.new;

sed -e 's/___/\.000e\-02  /g' -e 's/\\\-/\-/g' ordered.tmp >> unitcube-3d-special.new;

more unitcube-3d-special.two >> unitcube-3d-special.new;

sed -f sed.map simplex.tmp >> unitcube-3d-special.new;

more unitcube-3d-special.thr >> unitcube-3d-special.new;
