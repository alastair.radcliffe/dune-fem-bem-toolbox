#ifndef MESHVELOCITY_HH
#define MESHVELOCITY_HH


template < class outDFT , class DFT >
struct MeshVelocity
{
  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;

  typedef Dune::Fem::FunctionSpace<double,double,DFT::DiscreteFunctionSpaceType::dimDomain,
                              outDFT::DiscreteFunctionSpaceType::dimRange> FunctionSpaceType;
  // range type that is required
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  // constructor
  MeshVelocity( const double &ts, const outDFT &f0, const DFT &f1, const DFT &f2 )
    : lf0_( f0 ),
      lf1_( f1 ),
      lf2_( f2 ),
      ts_( ts )
  {}

  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    typename outDFT::RangeType u;
    typename DFT::RangeType v1;
    typename DFT::RangeType v2;
    lf0_.evaluate( x, u  );
    lf1_.evaluate( x, v1 );
    lf2_.evaluate( x, v2 );

    for( unsigned int i = 0; i < dimensionworld; ++i )
      ret[ i ] =  - ( v1[ i ] - v2[ i ] ) / ts_;
  }

  template< class Point >
  void jacobian ( const Point &x, JacobianRangeType &ret ) const
  {
    ret = 0;
    std::cout << "Should not be in Mesh Velocity Jacobian !!!" << std::endl;
    abort();
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf0_.init( entity );
    lf1_.init( entity );
    lf2_.init( entity );
  }

  private:
    double ts_;
    typename outDFT::LocalFunctionType lf0_;
    typename DFT::LocalFunctionType lf1_;
    typename DFT::LocalFunctionType lf2_;
};


#endif // end #if MESHVELOCITY_HH
