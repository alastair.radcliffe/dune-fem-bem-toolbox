#ifndef MCF_FEMSCHEME_HH
#define MCF_FEMSCHEME_HH

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parallel/mpicollectivecommunication.hh>
#include <dune/fem/operator/projection/vtxprojection.hh>

// local includes
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include "addpressure.hh"
#include "addchargeforcing.hh"

// MCFScheme
//-----------

template < class ImplicitModel, class ExplicitModel >
struct MCFScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef ExplicitModel ExplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  typedef typename BaseType::LinearInverseOperatorType LinearInverseOperatorType;
  MCFScheme( GridPartType &gridPart,
              const ImplicitModelType& implicitModel,
              const ExplicitModelType& explicitModel )
  : BaseType(gridPart, implicitModel),
    explicitModel_(explicitModel),
    Rhs_( "forcing", discreteSpace_ ),
    Oh_(  Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
    chi_( Dune::Fem::Parameter::getValue< double >( "fiss.ility", 0.0   ) ),
    surfTensCoeff_( 1.0 / Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
    explicitOperator_( explicitModel_, discreteSpace_ )
  {
    // set all DoF to zero
    Rhs_.clear();
  }

  DiscreteFunctionType &forcing()
  {
    return polution_;
  }
  const DiscreteFunctionType &forcing() const
  {
    return polution_;
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void prepare(OtherDiscreteFunctionType &otherSolution_)
  {
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = otherSolution_.space().gridPart();

    typedef AddPressure< OtherDiscreteFunctionType, DiscreteFunctionType > AddPressureType;
    typedef Dune::Fem::LocalFunctionAdapter< AddPressureType > AddPressureFunctionType;
    AddPressureType addpressure( otherSolution_ );
    AddPressureFunctionType addPressureFunction( "addpressure", addpressure, otherGridPart_ );

    // apply explicit operator and also setup right hand side
    explicitOperator_( solution_, rhs_ );

    // apply coupling constraints using new expanded solution
    implicitOperator_.coupling( polution_, addPressureFunction, Rhs_ );
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void setup(OtherDiscreteFunctionType &otherSolution_)
  {
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = otherSolution_.space().gridPart();

    typedef AddPressure< OtherDiscreteFunctionType, DiscreteFunctionType > AddPressureType;
    typedef Dune::Fem::LocalFunctionAdapter< AddPressureType > AddPressureFunctionType;
    AddPressureType addpressure( otherSolution_ );
    AddPressureFunctionType addPressureFunction( "addpressure", addpressure, otherGridPart_ );

    // apply coupling constraints using new expanded solution
    implicitOperator_.coupleSetup( polution_, addPressureFunction, Rhs_ );
  }

  void initialize ()
  {
     Dune::Fem::LagrangeInterpolation
          < typename ExplicitModelType::InitialFunctionType, DiscreteFunctionType > interpolation;
     interpolation( explicitModel_.initialFunction(), solution_ );
  }

  //! solve the system
  void solve ( bool assemble )
  {
    // make a record of the original positions to be used to see the differences later
    polution_.assign( solution_ );
    polution_ *= -1.0;
    // solve as per the base type
    BaseType::solve( assemble );
  }

  //! prepare the forcing from the MCF solution
  void prepforce ()
  {
    // the forcing is proportional to the distance moved
    // if "H" is the mean curvature, then the velocity for mean curvature flow
    // is = 2 H, and thus the distance moved in one timestep "k" is = 2 H k
    polution_ += solution_;

    // apply any scaling that might be necessary
    // divide by 2 to remove the 2 from the 2 H above
    polution_ *= surfTensCoeff_ / 2.0;

#if !BEMPP
    // the coupling should have set the Rhs to contain the fluid velocites on the boundary as Dirichlet boundary conditions
    // put these into the solution, which should then remain untouched to be used by the nudge after the return
    solution_.assign( Rhs_ );
    // NB: now the solution holds fluid velocities
#endif
  }

#if BEMPP
  void explode()
  {
    // reset fissility after explosion
    chi_ = Dune::Fem::Parameter::getValue< double >( "fiss.ulity", 0.0   );
    std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << "BOOM !!!" << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl;
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void charge(OtherDiscreteFunctionType &otherSolution_)
  {
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = otherSolution_.space().gridPart();


    // determine the total charge on the surface from using a constant potential of unity

    typedef typename OtherGridPartType::template Codim<0>::IteratorType OtherIteratorType;

    typedef typename OtherDiscreteFunctionType::RangeType OtherRangeType;

    double totArea = 0.0;
    double totQ = 0.0;
    const OtherIteratorType end = otherGridPart_.template end<0>();
    for( OtherIteratorType it = otherGridPart_.template begin<0>(); it != end; ++it )
    {
      double area = it->geometry().volume();
      totArea += area;
      OtherRangeType phi;
      otherSolution_.evaluate( it->geometry().center() , phi );
      totQ += area * phi[ 0 ];
    }
    std::cout << "Area = " << totArea << "  Q = " << totQ;

    Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> sumComm(Dune::MPIHelper::getCommunicator());
    double totalQ = sumComm.sum(totQ);

    // total charge correction factor
    double corr = 8.0 * M_PI * std::sqrt( chi_ / Oh_ ) / totalQ;

    std::cout << "  QQ = " << totalQ << "  corr = " << corr << std::endl;


    typedef AddChargeForcing< OtherDiscreteFunctionType, DiscreteFunctionType > AddChargeForcingType;
    typedef Dune::Fem::LocalFunctionAdapter< AddChargeForcingType > AddChargeForcingFunctionType;
    AddChargeForcingType addForcing( corr, polution_, otherSolution_ );
    AddChargeForcingFunctionType addChargeForcingFunction( "addchargeforcing", addForcing, otherGridPart_ );

    Dune::Fem::VtxProjection
          < AddChargeForcingFunctionType, DiscreteFunctionType > interpolation;
    interpolation( addChargeForcingFunction, solution_ );

    // Note: polution now holds the SQUARE of the surface charge density



    typedef typename GridPartType::template Codim<0>::IteratorType IteratorType;

    typedef typename DiscreteFunctionType::RangeType RangeType;

    /*
    const OtherIteratorType eend = otherGridPart_.template end<0>();
    for( IteratorType it = gridPart_.template begin<0>(); it != eend; ++it )
    {
      RangeType phi;
      polution_.evaluate( it->geometry().center() , phi );
      std::cout << "Before " << phi[ 0 ] << "  " << phi[ 1 ] << "  " << phi[ 2 ] << "  " << std::endl;
    }
    */
    // add the forcings due to the surface charge
    polution_ += solution_;
    /*
    for( IteratorType it = gridPart_.template begin<0>(); it != eend; ++it )
    {
      RangeType phi;
      polution_.evaluate( it->geometry().center() , phi );
      std::cout << "After " << phi[ 0 ] << "  " << phi[ 1 ] << "  " << phi[ 2 ] << "  " << std::endl;
    }
    */
    solution_.assign( Rhs_ );
  }
#endif

private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::solution_;
  using BaseType::polution_;
  DiscreteFunctionType Rhs_;
  using BaseType::implicitModel_;
  using BaseType::rhs_;
  using BaseType::implicitOperator_;
  using BaseType::linearOperator_;
  using BaseType::solverEps_;
  const ExplicitModelType &explicitModel_;
  typename BaseType::EllipticOperatorType explicitOperator_; // the operator for the rhs
  // Ohnesorge number
  double Oh_;
  // Fissility
  double chi_;
  // Surface tension coefficient
  double surfTensCoeff_;
};

#endif // end #if MCF_FEMSCHEME_HH
