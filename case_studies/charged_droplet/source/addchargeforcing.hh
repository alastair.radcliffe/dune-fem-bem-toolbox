#ifndef ADDCHARGEFORCING_HH
#define ADDCHARGEFORCING_HH


template < class DFT , class outDFT >
struct AddChargeForcing
{
  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;

  typedef Dune::Fem::FunctionSpace<double,double,DFT::DiscreteFunctionSpaceType::dimDomain,
                              outDFT::DiscreteFunctionSpaceType::dimRange> FunctionSpaceType;
  // range type that is required
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  // constructor
  AddChargeForcing( const double &correction , const outDFT &f0  , const DFT &f1 )
    : ts_( Dune::Fem::Parameter::getValue< double >( "mcf.timestep", 0.125 ) ),
      lf0_( f0 ),
      lf1_( f1 ),
      fact_( correction )
  {}

  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    typename DFT::RangeType phi;
    typename outDFT::RangeType psi;
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    ret = 0;
    lf0_.evaluate( x, psi );
    double len = psi.two_norm();
    lf1_.evaluate( x, phi );
    double mag = ts_ * fact_ * fact_ * phi[ 0 ] * phi[ 0 ] / 4.0;
    // std::cout << "Check " << len << "  " << phi[ 0 ] << "  " << mag << std::endl;
    for( int i = 0; i < dimRange; ++i )
      ret[ i ] = - mag * psi[ i ] / len;
  }
  template< class Point >
  void jacobian ( const Point &x, JacobianRangeType &ret ) const
  {
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    typename DFT::JacobianRangeType phi;
    lf1_.jacobian( x, phi );
    int i = 0;
    for( ; i < dimensionworld; ++i )
      ret[ i ] = phi[ i ];
    for( ; i < dimRange; ++i )
      ret[ i ] = 0;
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf0_.init( entity );
    lf1_.init( entity );
  }

  private:
    typename outDFT::LocalFunctionType lf0_;
    typename DFT::LocalFunctionType lf1_;
    double fact_;
    double ts_;
};


#endif // end #if ADDCHARGEFORCING_HH
