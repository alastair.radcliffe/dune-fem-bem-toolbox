#ifndef VOLUME_DEFORMATION_HH
#define VOLUME_DEFORMATION_HH

#include <dune/grid/geometrygrid/coordfunction.hh>
#include <dune/fem/space/common/functionspace.hh>

// VolumeDeformationCoordFunction
// ------------------------

template < class VolumeEvolution >
class VolumeDeformationCoordFunction
  : public Dune::AnalyticalCoordFunction< double, 3, 3, VolumeDeformationCoordFunction< VolumeEvolution > >
{
  typedef Dune::AnalyticalCoordFunction< double, 3, 3, VolumeDeformationCoordFunction< VolumeEvolution > > BaseType;

public:
  typedef VolumeEvolution VolumeEvolutionType;
  typedef Dune::Fem::FunctionSpace< double, double, 3, 3 > FunctionSpaceType;

  typedef typename VolumeEvolutionType::DomainType DomainVector;
  typedef typename VolumeEvolutionType::RangeType RangeVector;
  typedef double RangeFieldType;

  VolumeDeformationCoordFunction ( VolumeEvolution &volumeEvolution )
    : volumeEvolution_( volumeEvolution )
  {}

  void evaluate ( const DomainVector &x, RangeVector &y ) const
  {
    volumeEvolution_.evaluate( x, y );
  }

  void setTime( const double time )
  {
    volumeEvolution_.setTime( time );
  }

private:
  VolumeEvolution& volumeEvolution_;
};

template <class DiscreteFunctionType>
class VolumeDeformationDiscreteFunction
: public Dune::DiscreteCoordFunction< double, 3, VolumeDeformationDiscreteFunction< DiscreteFunctionType > >
{
  typedef Dune::DiscreteCoordFunction< double, 3, VolumeDeformationDiscreteFunction< DiscreteFunctionType > > BaseType;

  typedef typename DiscreteFunctionType :: GridType GridType ;
  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType ;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;

  typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType ;
  typedef typename DiscreteFunctionType :: RangeType  RangeType ;

protected:
  typedef typename DiscreteFunctionSpaceType::BlockMapperType BlockMapperType;
  static const unsigned int blockSize = DiscreteFunctionSpaceType::localBlockSize;

  typedef typename DiscreteFunctionType::DofBlockPtrType DofBlockPtrType;

public:
  VolumeDeformationDiscreteFunction ( GridType &grid)
  : gridPart_( grid ),
    space_( gridPart_ )
  {
    vertices_ = new DiscreteFunctionType( "deformation", space_ );
  }

  template < class RangeType, class DFunctionType >
  struct CoordFunctor
  {
    RangeType& y_;
    DFunctionType * function_;

    CoordFunctor( RangeType& y, DFunctionType* function )
      : y_( y )
      , function_( function )
    {
    }

    template <class GlobalKey>
    void operator () ( const size_t local, const GlobalKey& globalKey )
    {
      DofBlockPtrType blockPtr = function_->block( globalKey );
      for( unsigned int j = 0; j < blockSize; ++j )
      {
        y_[ j ] = (*blockPtr)[ j ];
      }
    }

    void fill(RangeType& y)
    {
      for( unsigned int j = 0; j < blockSize; ++j )
      {
        y[ j ] = y_[ j ];
      }
    }
  };

  template< class HostEntity , class RangeVector >
  void evaluate ( const HostEntity &hostEntity, unsigned int corner,
                  RangeVector &y ) const
  {
    CoordFunctor< RangeVector, DiscreteFunctionType > coord ( y, vertices_);

    vertices_->space().blockMapper().mapEachEntityDof( hostEntity, coord );
    coord.fill ( y );
  }

  template <class RangeVector>
  void evaluate ( const typename GridType :: template Codim<0>::Entity &entity,
		              unsigned int corner,
                  RangeVector &y ) const
  {
    typedef typename GridType::ctype  ctype;
    enum { dim = GridType::dimension };

    const Dune::ReferenceElement< ctype, dim > &refElement
      = Dune::ReferenceElements< ctype, dim >::general( entity.type() );

    LocalFunctionType localVertices = vertices_->localFunction( entity );

    localVertices.evaluate( refElement.position( corner, dim ), y );
  }

  template <class DF>
  void setTime( const double time, DF &solution )
  {
    const typename DiscreteFunctionType::DofIteratorType end = vertices_->dend();
    typename DF::ConstDofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::DofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
      *it = *git;
  }

  template <class GF, class DF>
  void initialize(const GF& gf, DF &solution)
  {
    typedef Dune::Fem::LagrangeInterpolation< GF, DiscreteFunctionType > VertexInterpolationType;
    VertexInterpolationType interpolation;
    interpolation( gf, *vertices_ );

    const typename DiscreteFunctionType::ConstDofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::ConstDofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
      *git = *it;
  }

  template <class GF>
  void initializeVerticesOnly(const GF& gf)
  {
    typedef Dune::Fem::LagrangeInterpolation< GF, DiscreteFunctionType > VertexInterpolationType;
    VertexInterpolationType interpolation;
    interpolation( gf, *vertices_ );
  }

  double radius(double &max, double &min)
  {
    max = 0.0;
    min = 1.0e6;
    double average = 0.;
    double count = 0;
    const DiscreteFunctionSpaceType &dfSpace = vertices_->space();

    const IteratorType end = dfSpace.end();
    for( IteratorType it = dfSpace.begin(); it != end; ++it )
      {
	const EntityType &entity = *it;

	for( unsigned int corner = 0; corner < 3; ++corner )
	  {
	    RangeType y;
	    evaluate( entity, corner, y );

	    double r = y.two_norm();
	    average += r;
	    ++count;

	    if( r > max )
	      max = r;
	    if( r < min )
	      min = r;
	  }
      }
    average /= count;

    if( max - min > 1.0e-8 )
      {
	std::cout << "not circular! (max: " << max << " min: " << min << " )" << std::endl;
      }

    return average;
  }

protected:
  GridPartType gridPart_;
  DiscreteFunctionSpaceType space_;
  DiscreteFunctionType* vertices_;
};

#endif // #ifndef VOLUME_DEFORMATION_HH
