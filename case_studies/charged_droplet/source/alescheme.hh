#ifndef ALE_FEMSCHEME_HH
#define ALE_FEMSCHEME_HH

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>

// local includes
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include "meshvelocity.hh"

// ALEScheme
//-----------

template < class ImplicitModel >
struct ALEScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  ALEScheme( GridPartType &gridPart,
              const ImplicitModelType& implicitModel )
  : BaseType(gridPart, implicitModel)
  {
  }

  template <class OtherDiscreteFunctionType>
  void advective(double &ts, OtherDiscreteFunctionType &otherSolution_)
  {
    // use the difference between the old and new ALE solutions to compute the mesh velocity
    typedef MeshVelocity< OtherDiscreteFunctionType, DiscreteFunctionType > MeshVelocityType;
    typedef Dune::Fem::LocalFunctionAdapter< MeshVelocityType > MeshVelocityFunctionType;
    MeshVelocityType meshVelocity( ts, otherSolution_, solution_, polution_ );
    MeshVelocityFunctionType meshVelocityFunction( "mesh velocity", meshVelocity, gridPart_ );

    Dune::Fem::LagrangeInterpolation
          < MeshVelocityFunctionType, OtherDiscreteFunctionType > interpolation;
    interpolation( meshVelocityFunction, otherSolution_ );

    // set the old ALE solution to the new one
    polution_.assign( solution_ );
  }

private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::solution_;
  using BaseType::polution_;
  using BaseType::implicitModel_;
  using BaseType::rhs_;
};

#endif // end #if ALE_FEMSCHEME_HH
