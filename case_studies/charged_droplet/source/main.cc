#include <config.h>

// iostream includes
#include <iostream>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

// include GeometryGrid
#include <dune/grid/geometrygrid.hh>

// include grid part
#if BEMPP
#include <dune/fem/gridpart/leafgridpart.hh>
#else
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#endif

// include grid width
#include <dune/fem/misc/gridwidth.hh>

// include discrete function space
#include <dune/fem/space/lagrange.hh>

// include discrete function
#include <dune/fem/function/adaptivefunction.hh>

// include solvers
#include <dune/fem/solver/cginverseoperator.hh>
#include <dune/fem/solver/oemsolver.hh>

// include Lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>

// include norms
#include <dune/fem/misc/l2norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

// include eoc calc
#include <dune/fem/misc/femeoc.hh>
// include discrete function
#include <dune/fem/function/blockvectordiscretefunction/blockvectordiscretefunction.hh>

// include linear operators
//#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/operator/matrix/blockmatrix.hh>
#include <dune/fem/operator/2order/dgmatrixsetup.hh>
#include <dune/fem/operator/2order/lagrangematrixsetup.hh>

#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>

// local includes
#include <dune/fem_bem_toolbox/fem_objects/elliptic.hh>
#include "deformation.hh"
#include "volumedeformation.hh"
#include "mcf.hh"
#include "mcfmodel.hh"
#include <dune/fem_bem_toolbox/fem_objects/model.hh>
#include "navierstokesmodel.hh"
#include "mcfscheme.hh"
#include "alescheme.hh"
#include "nsscheme.hh"
#include "divergence.hh"
#include "curvature.hh"
#include <dune/fem_bem_toolbox/fem_objects/rhs.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_zoltan.hh>

#if BEMPP
#include <dune/fem_bem_toolbox/bem_objects/fembem.hh>
#include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/common/parallel/mpihelper.hh>

// algorithm
// ---------

template <class HGridType>
void algorithm ( HGridType &grid, const double timeStep, int step, const int eocId )
{
  // choose type of discrete function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::InsideDeformationType  InsideDeformationType;

  //DeformationCoordFunction deformation;
  InsideDeformationType insideDeformation( grid );

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GridType GridType;
  GridType insideGeoGrid( grid, insideDeformation );

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GridPartType GridPartType;

  GridPartType gridPart( insideGeoGrid );

  // setup time provider
  const double endTime = Dune::Fem::Parameter::getValue< double >( "mcf.endtime", 2.0 );
  const double dtreducefactor = Dune::Fem::Parameter::getValue< double >("mcf.reducetimestepfactor", 1 );
  double timeStup = Dune::Fem::Parameter::getValue< double >( "mcf.timestep", 0.125 );

  timeStup *= pow(dtreducefactor,step);

  Dune::Fem::GridTimeProvider< GridType > insideTimeProvider( insideGeoGrid );

  // initialize with fixed time step
  insideTimeProvider.init( timeStup ) ;

  // type of the mathematical model used
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceType FunctionSpaceInsideType;
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceOutsideType FunctionSpaceOutsideType;
  typedef AleNiceMesh< FunctionSpaceInsideType > InsideProblemType;

  typedef FunctionSpaceOutsideType ALEBoundaryDataType;

  InsideProblemType insideProblem;

  typedef DiffusionModel< FunctionSpaceInsideType, GridPartType, ALEBoundaryDataType > ALEModelType;

  // implicit model for left hand side
  ALEModelType insideImplicitModel( insideProblem, gridPart );

  // create ALE scheme
  typedef ALEScheme< ALEModelType > ALESchemeType;
  ALESchemeType scheme( gridPart, insideImplicitModel );

  // initial data for volume
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::DiscreteFunctionSpaceInsideType DiscreteInsideHostFunctionSpaceType;
  typedef Problem::VolumeEvolution< DiscreteInsideHostFunctionSpaceType > VolumeEvolutionType;
  typedef VolumeDeformationCoordFunction< VolumeEvolutionType > VolumeDeformationCoordFunctionType;

  // all the discrete vertex stuff
  // typedef typename Dune::FemFemCoupling::Glue<HGridType>::HostGridPartType HostGridPartType;
  // HostGridPartType hGridPart( grid );

  // find initial condition
  VolumeEvolutionType insideInitialCondition(1000.0);

  // interpolate initial condition
  VolumeDeformationCoordFunctionType insideDeform( insideInitialCondition );
  insideDeformation.initialize( insideDeform, scheme.solution() );

  // setup data output
  typedef Dune::Fem::GridFunctionAdapter< InsideProblemType, GridPartType > InsideGridExactSolutionType;
  InsideGridExactSolutionType insideGridExactSolution("inside exact solution", insideProblem, gridPart, 5 );
  typedef Dune::tuple< const typename ALESchemeType::DiscreteFunctionType *, const typename ALESchemeType::DiscreteFunctionType *, InsideGridExactSolutionType * > InsideIOTupleType;
  InsideIOTupleType insideIoTuple( &(scheme.solution()), &(scheme.rhs()), &insideGridExactSolution );
  typedef Dune::Fem::DataOutput< GridType, InsideIOTupleType > InsideDataOutputType;
  InsideDataOutputType insideDataOutput( insideGeoGrid, insideIoTuple, DataOutputParameters( step, "volume" ) );

  // flag for whether or not to output vtu files
  const unsigned int vtuout = Dune::Fem::Parameter::getValue< unsigned int >( "vtu.output", 0 );

  // special vtu flag to output volume data just to check the partitioning (via Zoltan or otherwise ...)
  if( vtuout == -99 )
  {
    insideDataOutput.write(   insideTimeProvider );
    return;
  }

  typedef typename Dune::FemFemCoupling::Glue< HGridType,false >::OutsideHostGridType OutsideHostGridType;

  // create dune grid file with surface mesh
  OutsideHostGridType* grydd = 0;
  OutsideHostGridType* fullGrydd = 0;

  // if requested, use a previously created surface grid
  if( ( Dune::Fem::Parameter::getValue< int >( "surface.use", 0 ) > 0 ) || ( Dune::Fem::MPIManager::size() != 1 ) )
  {
    const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", OutsideHostGridType::dimension );
    const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );
    Dune::GridPtr< OutsideHostGridType > grydPtr( grydfile );
    grydd = grydPtr.release();
    // duplicate surface grid for BEM operators which will NOT be partitioned
    Dune::GridPtr< OutsideHostGridType > fullGrydPtr( grydfile, Dune::MPIHelper::getLocalCommunicator() );
    fullGrydd = fullGrydPtr.release();
  }
  else
  {
    grydd = scheme.extractSurface();
    fullGrydd = grydd;
    return;
  }
  OutsideHostGridType& gryd = *grydd ;

#if BEMPP
  // duplicate surface grid for BEM operators which will NOT be partitioned
  OutsideHostGridType& fullGryd = *fullGrydd ;
#endif

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<OutsideHostGridType> SurfLoadBalancer;
  SurfLoadBalancer sldb(gryd);
#if BEMPP
  SurfLoadBalancer fsldb(fullGryd,true);
#endif

  // do initial load balance
  if ( sldb.repartition() )
    gryd.repartition( sldb );
  else
  gryd.loadBalance();
#if BEMPP
  // dummey load balance that puts whole grid on node zero
  if ( fsldb.repartition() )
    fullGryd.repartition( fsldb );
  else
  fullGryd.loadBalance();
#endif

  // choose type of discrete function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::OutsideHostDiscreteFunctionType OutsideHostDiscreteFunctionType;
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::OutsideDeformationType  OutsideDeformationType;

  //DeformationCoordFunction deformation;
  OutsideDeformationType outsideDeformation( gryd );
#if BEMPP
  OutsideDeformationType fullOutsideDeformation( fullGryd );
#endif

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GrydType GrydType;
  GrydType outsideGeoGrid( gryd, outsideDeformation );
#if BEMPP
  GrydType fullOutsideGeoGrid( fullGryd, fullOutsideDeformation );
#endif

  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::GrydPartType GrydPartType;

  GrydPartType grydPart( outsideGeoGrid );
#if BEMPP
  GrydPartType fullGrydPart( fullOutsideGeoGrid );
#endif

  Dune::Fem::GridTimeProvider< GrydType > outsideTimeProvider( outsideGeoGrid );

  // initialize with fixed time step
  outsideTimeProvider.init( timeStup ) ;

  // type of the mathematical model used
  typedef TimeDependentCosinusProduct< FunctionSpaceOutsideType > OutsideProblemType;

  typedef FunctionSpaceOutsideType MCFBoundaryDataType;

  OutsideProblemType outsideProblem( outsideTimeProvider ) ;

  typedef MCFModel< FunctionSpaceOutsideType, GrydPartType, MCFBoundaryDataType > MCFModelType;

  // implicit model for left hand side
  MCFModelType outsideImplicitModel( outsideProblem, grydPart, true );

  // explicit model for right hand side
  MCFModelType outsideExplicitModel( outsideProblem, grydPart, false );

  // initial data for surface
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::DiscreteFunctionSpaceOutsideType DiscreteOutsideHostFunctionSpaceType;
  typedef Problem::SurfaceEvolution< DiscreteOutsideHostFunctionSpaceType > SurfaceEvolutionType;
  typedef SurfaceDeformationCoordFunction< SurfaceEvolutionType > SurfaceDeformationCoordFunctionType;

  // all the discrete vertex stuff
  // typedef typename Dune::FemFemCoupling::Glue<HGridType>::HostGridPartType HostGridPartType;
  // HostGridPartType hGridPart( grid );

  // find initial condition
  SurfaceEvolutionType outsideInitialCondition(1000.0);

  // interpolate initial condition
  SurfaceDeformationCoordFunctionType outsideDeform( outsideInitialCondition );
  outsideDeformation.initialize( outsideDeform );
#if BEMPP
  fullOutsideDeformation.initialize( outsideDeform );
#endif

  // create mcf scheme
  typedef MCFScheme< MCFModelType, MCFModelType > MCFSchemeType;
  MCFSchemeType schyme( grydPart, outsideImplicitModel, outsideExplicitModel );

  // initialize solution
  outsideDeformation.initializeSolution( schyme.solution() );

  // setup data output
  typedef Dune::Fem::GridFunctionAdapter< OutsideProblemType, GrydPartType > OutsideGridExactSolutionType;
  OutsideGridExactSolutionType outsideGridExactSolution("outside exact tension solution", outsideProblem, grydPart, 5 );

  // local function adapter just to get size of forcing movements
  typedef typename MCFSchemeType::DiscreteFunctionType MCFDFT;
  typedef Curvature< MCFDFT > CurvatureType;
  typedef Dune::Fem::LocalFunctionAdapter< CurvatureType > CurvFunctionType;
  CurvatureType curvature( schyme.forcing(), timeStup );
  CurvFunctionType curvFunction( "curvature", curvature, grydPart );

  typedef Dune::tuple< const typename MCFSchemeType::DiscreteFunctionType *, OutsideGridExactSolutionType * > OutsideIOTupleType;
  typedef Dune::tuple< const typename MCFSchemeType::DiscreteFunctionType *, CurvFunctionType *, OutsideGridExactSolutionType * > ForcingOutsideIOTupleType;
  OutsideIOTupleType outsideIoTuple( &(schyme.solution()), &outsideGridExactSolution );
  ForcingOutsideIOTupleType forcingIoTuple( &(schyme.forcing()), &curvFunction, &outsideGridExactSolution );
  typedef Dune::Fem::DataOutput< GrydType, OutsideIOTupleType > OutsideDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, ForcingOutsideIOTupleType > ForcingOutsideDataOutputType;
  OutsideDataOutputType outsideDataOutput( outsideGeoGrid, outsideIoTuple, DataOutputParameters( step, "surface" ) );
  ForcingOutsideDataOutputType forcingDataOutput( outsideGeoGrid, forcingIoTuple, DataOutputParameters( step, "forcing" ) );

  // find grid width at start time
  // const double h = Dune::Fem::GridWidth::calcGridWidth( hGridPart );

  double minR,maxR;

  // set-up the coupling
  scheme.setup(schyme.solution());
  schyme.setup(scheme.solution());




  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::AugmentedFunctionSpaceInsideType AugmentedFunctionSpaceInsideType;

  // type of the mathematical model used
  typedef Fluid< AugmentedFunctionSpaceInsideType > NSInsideProblemType;

  typedef typename MCFSchemeType::DiscreteFunctionType NavierStokesBoundaryDataType;

  NSInsideProblemType nsInsideProblem( insideTimeProvider ) ;

  typedef NavierStokesModel< AugmentedFunctionSpaceInsideType, GridPartType, NavierStokesBoundaryDataType > NavierStokesModelType;

  // implicit model for left hand side
  NavierStokesModelType nsInsideImplicitModel( nsInsideProblem, gridPart, true );

  // explicit model for right hand side
  NavierStokesModelType nsInsideExplicitModel( nsInsideProblem, gridPart, false );

  // create NS scheme
  typedef NSScheme< NavierStokesModelType, NavierStokesModelType > NSSchemeType;
  NSSchemeType nsScheme( gridPart, nsInsideImplicitModel, nsInsideExplicitModel );


  // set-up Navier-Stokes data output
  typedef Dune::Fem::GridFunctionAdapter< NSInsideProblemType, GridPartType > NSInsideGridExactSolutionType;
  NSInsideGridExactSolutionType nsInsideGridExactSolution("NS inside exact solution", nsInsideProblem, gridPart, 5 );

  typedef typename NSSchemeType::DiscreteFunctionType DFT;
  typedef Divergence< DFT > DivergenceType;
  typedef Dune::Fem::LocalFunctionAdapter< DivergenceType > DivFunctionType;
  DivergenceType divergence( nsScheme.solution() );
  DivFunctionType divFunction( "divergence", divergence, gridPart );


  typedef Dune::tuple< const typename NSSchemeType::DiscreteFunctionType *, const typename NSSchemeType::DiscreteFunctionType *, DivFunctionType *, InsideGridExactSolutionType * > NsInsideIOTupleType;

  NsInsideIOTupleType nsInsideIoTuple( &(nsScheme.solution()), &(nsScheme.rhs()), &divFunction, &insideGridExactSolution );
  typedef Dune::Fem::DataOutput< GridType, NsInsideIOTupleType > NsInsideDataOutputType;
  NsInsideDataOutputType nsInsideDataOutput( insideGeoGrid, nsInsideIoTuple, DataOutputParameters( step, "fluid" ) );

  nsScheme.setup(schyme.forcing());

  std::cout << "Just before BEM stuff" << std::endl;


#if BEMPP
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::ChargeFunctionSpaceOutsideType ChargeFunctionSpaceOutsideType;

  typedef DiffusionModel< ChargeFunctionSpaceOutsideType, GrydPartType, ChargeFunctionSpaceOutsideType > SurfaceChargeModelType;

  typedef typename SurfaceChargeModelType::ProblemType SurfaceChargeProblemType ;

  SurfaceChargeProblemType* qProbPtr = new CoulombProblem< ChargeFunctionSpaceOutsideType > ();

  assert( qProbPtr );
  SurfaceChargeProblemType& qProb = *qProbPtr ;

  SurfaceChargeModelType qModel( qProb, grydPart );

  typedef BemScheme< SurfaceChargeModelType > SurfaceChargeSchemeType;

  std::cout << "Just before BEM scheme" << std::endl;

  SurfaceChargeSchemeType qSchyme( grydPart, fullGrydPart, qModel,"surface charge" );

  std::cout << "Just after BEM scheme" << std::endl;

  double bob = qSchyme.setDofRank( sldb );

  std::cout << "Full surface area = " << bob << std::endl;

  // setup data output
  typedef Dune::Fem::GridFunctionAdapter< SurfaceChargeProblemType, GrydPartType > SurfaceChargeExactSolutionType;
  SurfaceChargeExactSolutionType qExactSolution("outside exact charge solution", qProb, grydPart, 5 );
  SurfaceChargeExactSolutionType qFullExactSolution("full outside exact charge solution", qProb, fullGrydPart, 5 );

  typedef Dune::tuple< const typename SurfaceChargeSchemeType::DiscreteFunctionType *, SurfaceChargeExactSolutionType * > ChargeIOTupleType;
  typedef Dune::tuple< const typename SurfaceChargeSchemeType::P1DiscreteFunctionType *, const typename SurfaceChargeSchemeType::NudgeVectorP1DiscreteFunctionType * > FullDirDataIOTupleType;
  typedef Dune::tuple< const typename SurfaceChargeSchemeType::DiscreteFunctionType *, SurfaceChargeExactSolutionType * > FullChargeIOTupleType;
  typedef Dune::tuple< const typename SurfaceChargeSchemeType::P1DiscreteFunctionType * > DofRankIOTupleType;
  ChargeIOTupleType chargeIoTuple( &(qSchyme.solution()), &qExactSolution );
  FullDirDataIOTupleType fullDirDataIoTuple( &(qSchyme.fullDirData()), &(qSchyme.fullVectorDirData()) );
  FullChargeIOTupleType fullChargeIoTuple( &(qSchyme.fullSolution()), &qFullExactSolution );
  DofRankIOTupleType dofRankIoTuple( &(qSchyme.dofRank()) );
  typedef Dune::Fem::DataOutput< GrydType, ChargeIOTupleType > ChargeDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, FullDirDataIOTupleType > FullDirDataDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, FullChargeIOTupleType > FullChargeDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, DofRankIOTupleType > DofRankDataOutputType;

  ChargeDataOutputType qDataOutput( outsideGeoGrid, chargeIoTuple, DataOutputParameters( step, "charge" ) );
  FullDirDataDataOutputType fullDirDataDataOutput( fullOutsideGeoGrid, fullDirDataIoTuple, DataOutputParameters( step, "full-dir-data" ) );
  FullChargeDataOutputType qFullDataOutput( fullOutsideGeoGrid, fullChargeIoTuple, DataOutputParameters( step, "full-charge" ) );
  DofRankDataOutputType dofRankDataOutput( fullOutsideGeoGrid, dofRankIoTuple, DataOutputParameters( step, "dofrank" ) );

  dofRankDataOutput.write( outsideTimeProvider );

  // set-up the coupling
  qSchyme.setup(scheme.solution());
#endif

  // initialize fluid velocities with quadrupole
  nsScheme.initialize();

  // set-up a more interesting initial condition
  insideInitialCondition.ReDe(Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ));
  outsideInitialCondition.ReDe(Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ));

  // interpolate the new more interesting initial condition
  insideDeformation.initialize( insideDeform, scheme.solution() );
  outsideDeformation.initialize( outsideDeform );
#if BEMPP
  fullOutsideDeformation.initialize( outsideDeform );
#endif

  // initialize solution
  outsideDeformation.initializeSolution( schyme.solution() );

#if BEMPP
    qSchyme.reset();
#endif

  bool firsttime = true;

  // setup the right hand side
  // schyme.prepare();
  // scheme.prepare();
  // nsScheme.prepare(firsttime);

  // time loop, increment with fixed time step
  for( ; outsideTimeProvider.time() < endTime; outsideTimeProvider.next( timeStup ) )
    {                                     // Surface Forcings
      // keep track of the droplet volume
      double vol = scheme.volume();
      std::cout << "Volume = " << vol << "  " << 0.75*vol/M_PI << "  " << std::pow(0.75*vol/M_PI,1.0/3.0) << std::endl;

#if BEMPP
    // setup the right hand side
    qSchyme.prepare(nsScheme.solution());

    // solve once (we need to assemble the system the first time the method is called)
    qSchyme.solve( true );
#endif

    // setup the right hand side
    schyme.prepare(nsScheme.solution());

      // std::cout << "=============================================================== tension" << std::endl;

    // solve once (we need to assemble the system the first time the method is called)
    schyme.solve( true );

    // use the MCF solution to prepare the surface forcing
    schyme.prepforce();

#if BEMPP
    // apply any charge forcing
    schyme.charge(qSchyme.solution());
#endif

    // turn the fluid velocities now held in schyme solution into positions
    outsideDeformation.nudge( timeStup, schyme.solution() );

#if BEMPP
    // copy the surface velocities on the ranked surface mesh to the full one
    qSchyme.collate(schyme.solution());
#endif

    // calculate the ellipsoid fit
    bool boom = outsideDeformation.abnfit();

#if BEMPP
    // reset fissility after "explosion"
    if(boom) schyme.explode();
#endif

                                         // Node Distribution

    // setup the right hand side
    scheme.prepare(schyme.solution());

      // std::cout << "=============================================================== ale" << std::endl;

    // ALE solve to get nice distribution of interior nodes
    scheme.solve( true );

    // ALE scheme sets-up the NS scheme with the mesh velocity
    if( ! firsttime )
    scheme.advective( timeStup, nsScheme.polution() );

                                         // Fluid Calculation
    // setup the right hand side
    nsScheme.prepare(firsttime,schyme.forcing());

      // std::cout << "=============================================================== navier" << std::endl;

    // Calculate new fluid velocities
    nsScheme.solve( true );

    // set NS polution to hold the current solution
    nsScheme.setPolution( false ); // outsideDeformation.peaked() );

    if( false ) // outsideDeformation.peaked() )
    {
      insideInitialCondition.ReDe(outsideDeformation.ratio());
      outsideInitialCondition.ReDe(outsideDeformation.ratio());
      insideDeformation.initialize( insideDeform, scheme.solution() );
      outsideDeformation.initialize( outsideDeform );
      outsideDeformation.initializeSolution( schyme.solution() );
    }
    // write data
#if BEMPP
    if( vtuout > 0 )
    qDataOutput.write(       outsideTimeProvider );
    if( vtuout > 3 )
    qFullDataOutput.write( outsideTimeProvider );
#endif
    if( vtuout > 2 )
    {
    insideDataOutput.write(   insideTimeProvider );
    outsideDataOutput.write( outsideTimeProvider );
    }
#if BEMPP
    if( vtuout > 3 )
    fullDirDataDataOutput.write( outsideTimeProvider );
#endif
    if( vtuout > 1 )
    forcingDataOutput.write( outsideTimeProvider );
    if( vtuout > 2 )
    nsInsideDataOutput.write( insideTimeProvider );

  // output error information
  // const double radius = outsideDeformation.radius(minR,maxR);
  // const double time = outsideTimeProvider.time();
  // const double error = std::sqrt( 1.0 - 4.0 * time );
  // std::cout << "MCF: " << time << "  " << radius << "  " << error << std::endl;

    // update vertex positions of deforming volume + surface grid using the new surface solution
    insideDeformation.setTime(   insideTimeProvider.time(), scheme.solution() );
    outsideDeformation.setTime( outsideTimeProvider.time(), schyme.solution() );
#if BEMPP
    fullOutsideDeformation.setTime( outsideTimeProvider.time(), qSchyme.fullVectorDirData() );
#endif

    // reset the problem
    scheme.reset();
    schyme.reset();
#if BEMPP
    qSchyme.reset();
#endif

    // keep the time providers in sync
    insideTimeProvider.next( timeStup );

    if( firsttime ) firsttime = false;
    // if(!firsttime) break;
  }

  // write data at end time
  insideDeformation.setTime(   insideTimeProvider.time(), scheme.solution() );
  outsideDeformation.setTime( outsideTimeProvider.time(), schyme.solution() );
#if BEMPP
  fullOutsideDeformation.setTime( outsideTimeProvider.time(), qSchyme.fullVectorDirData() );
#endif

    // write final data
#if BEMPP
    if( vtuout > 0 )
    qDataOutput.write(       outsideTimeProvider );
    if( vtuout > 3 )
    qFullDataOutput.write( outsideTimeProvider );
#endif
    if( vtuout > 2 )
    {
    insideDataOutput.write(   insideTimeProvider );
    outsideDataOutput.write( outsideTimeProvider );
    }
#if BEMPP
    if( vtuout > 3 )
    fullDirDataDataOutput.write( outsideTimeProvider );
#endif
    if( vtuout > 1 )
    forcingDataOutput.write( outsideTimeProvider );
    if( vtuout > 2 )
    nsInsideDataOutput.write( insideTimeProvider );

  // output error information
  // const double radius = outsideDeformation.radius(minR,maxR);
  // const double time = outsideTimeProvider.time();
  // const double error = std::abs( radius -
    // 			 std::sqrt( 1.0 - 4.0 * time ) );
    // std::cout << "error: " << error << std::endl;

    std::cout << "End of algorithm." << std::endl;

  // write eoc data to cout
  // Dune::Fem::FemEoc::setErrors( eocId, error );
  // Dune::FemFemEoc::write( h, dfSpace.size(), 0, timeProvider.timeStep(), std::cout );

  return;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // initialise eoc file
  // Dune::FemEoc::initialize( "../output/mcf-eoc", "eoc", "mean curvature flow" );

  // add headers
  std::vector< std::string > femEocHeaders;
  femEocHeaders.push_back("radius error");

  // get eoc id
  // const int eocId = Dune::FemEoc::addEntry( femEocHeaders );
  const int eocId = 0;


  Dune::GridFactory<HGridType> factory;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );
#if 0
  // determine type of grid file
  bool dgfFile = gridfile.find("dgf") != std::string::npos;
  bool gmshFile = gridfile.find("msh") != std::string::npos;

  // check file type is meaningful
  if( !dgfFile && !gmshFile )
  {
    std::cout << "Unknown file type " << dgfFile << "  " << gmshFile << std::endl;
    std::abort();
  }
  // if a gmsh file, then just create a dgf file from it and quit
  else if( gmshFile )
  {
#endif
  if( Dune::Fem::MPIManager::rank() == 0 )
  {
#if 0
  std::string dgfFileName( gridfile );
  dgfFileName.resize( dgfFileName.find_last_of( "." ) );
  dgfFileName += "_.dgf";
#endif
    std::cout << "Loading macro grid: " << gridfile << std::endl;
    Dune::GmshReader<HGridType>::read(factory, gridfile, true, true );
#if 0
  HGridType *grid = Dune::GmshReader< HGridType >::read( gridfile );

  typedef HGridType::LeafGridView GridView;
  GridView gridView = grid->leafView();

  Dune::DGFWriter< GridView > dgfWriter( gridView );
  dgfWriter.write( dgfFileName );

  const GridView::IndexSet &indexSet = gridView.indexSet();
  std::cerr << "Grid successfully written: "
            << indexSet.size( HGridType::dimension ) << " vertices, "
            << indexSet.size( 0 ) << " elements."
            << std::endl;

  // need to remove the termination "#" at the end of the new dgf file
  std::string more("more ");
  std::string command = more + dgfFileName;
  command += " | tac | sed '0,/#/s/#//' | tac > zob; mv zob ";
  command += dgfFileName;

  std::cout << "Executing the following command on the system:\n\n" << std::endl;
  std::cout << command << std::endl;

  int sys = system ( command.c_str() );

  std::ofstream extra;
  extra.open(dgfFileName, ios::app );
  extra << "\nPROJECTION" << std::endl;
  extra << "function p(x) = x / |x|" << std::endl;
  extra << "default p" << std::endl;
  extra << "#\n\n\n#" << std::endl;
  extra.close();

  std::cout << "Loading macro grid: " << dgfFileName << std::endl;

  // now read in the newly created dgf file
  Dune::GridPtr< HGridType > gridPtrTwo( dgfFileName );
  HGridType& gridTwo = *gridPtrTwo ;

  // refine the grid
  const float level = Dune::Fem::Parameter::getValue< float >( "mcf.level" );
  Dune::Fem::GlobalRefine::apply( gridTwo, std::ceil( level*Dune::DGFGridInfo< HGridType >::refineStepsForHalf() ) );

  // set-up the final dgf file name to use
  dgfFileName.resize( dgfFileName.find_last_of( "_" ) );
  dgfFileName += ".dgf";

  // and write out another dgf file
  GridView gridViewTwo = gridTwo.leafView();

  Dune::DGFWriter< GridView > dgfWriterTwo( gridViewTwo );
  dgfWriterTwo.write( dgfFileName );

  const GridView::IndexSet &indexSetTwo = gridViewTwo.indexSet();
  std::cerr << "Second grid successfully written: "
            << indexSetTwo.size( HGridType::dimension ) << " vertices, "
            << indexSetTwo.size( 0 ) << " elements."
            << std::endl;
  }

    // std::abort();
   return 0;
#endif
  }

  // otherwise dgf file gets read-in as normal


  // construct macro using the DGF Parser
  // std::unique_ptr<HGridType> gridPtr( Dune::GmshReader<HGridType>::read( gridfile, true, false ) );

  // Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType *gridPtr = factory.createGrid();

  HGridType& grid = *gridPtr ;

  // create special load balancer for 12 node case
#if HAVE_ZOLTAN
  typedef ZoltanLoadBalanceHandle<HGridType> BulkLoadBalancer;
#else
  typedef SimpleLoadBalanceHandle<HGridType> BulkLoadBalancer;
#endif
  BulkLoadBalancer bldb(grid);

  // do initial load balance
  if ( bldb.repartition() )
    grid.repartition( bldb );
  else
  grid.loadBalance();

  // initial grid refinement
  const float level = Dune::Fem::Parameter::getValue< float >( "mcf.level" );
  Dune::Fem::GlobalRefine::apply( grid, std::ceil( level*Dune::DGFGridInfo< HGridType >::refineStepsForHalf() ) );

  double timeStep = Dune::Fem::Parameter::getValue< double >( "mcf.timestep", 0.125 );
  algorithm( grid, timeStep, 0, eocId );
  /*
  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "mcf.repeats", 0 );
  for( int step = 1; step <= repeats; ++step )
    {
      timeStep /= 4.0;
      Dune::Fem::GlobalRefine::apply( grid, Dune::DGFGridInfo< HGridType >::refineStepsForHalf() );

      algorithm( grid, timeStep, step, eocId );
    }
  */
  std::cout << std::endl;
  std::cout << "Don't worry about the following abort, it is just to avoid bempp mega-dump at end of execution." << std::endl;
  std::abort();
  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
