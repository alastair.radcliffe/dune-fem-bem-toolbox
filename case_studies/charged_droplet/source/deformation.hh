#ifndef SURFACE_DEFORMATION_HH
#define SURFACE_DEFORMATION_HH

#include <dune/grid/geometrygrid/coordfunction.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parallel/mpicollectivecommunication.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <algorithm>    // std::sort
#include <vector>       // std::vector
#include <dune/fem/function/blockvectorfunction.hh>
#include "nr.h"

using namespace std;

struct myclass {
  template < class FitPointType >
  bool operator() (FitPointType i,FitPointType j) { return (i[2]<j[2]);}
} myobject;

  void superellips(const DP x, Vec_I_DP &aa, DP &y, Vec_O_DP &dyda)
  {
    y=0;
    if(aa.size()!=3) cerr << "superellips: wrong # of parameters\n";

    DP a=aa[0]; DP b=aa[1]; DP n=aa[2];
    DP c1=x*x/(a*a); DP c2=(1.0-x*x)/(b*b);
    DP f = std::pow(c1,0.5*n) + std::pow(c2,0.5*n);
    y = std::pow(f,-1/n);

    dyda[0]=(std::pow(c1,0.5*n)*std::pow(f,-(1+n)/n))/a;
    dyda[1]=(std::pow(c2,0.5*n)*std::pow(f,-(1+n)/n))/b;
    DP dfdn = 0.5*std::pow(c1,0.5*n)*std::log(c1)+0.5*std::pow(c2,0.5*n)*std::log(c2);
    dyda[2]=std::pow(f,-1/n)*(std::log(f)/(n*n) -dfdn/(n*f));
  }

// SurfaceDeformationCoordFunction
// ------------------------

template < class SurfaceEvolution >
class SurfaceDeformationCoordFunction
  : public Dune::AnalyticalCoordFunction< double, 3, 3, SurfaceDeformationCoordFunction< SurfaceEvolution > >
{
  typedef Dune::AnalyticalCoordFunction< double, 3, 3, SurfaceDeformationCoordFunction< SurfaceEvolution > > BaseType;

public:
  typedef SurfaceEvolution SurfaceEvolutionType;
  typedef Dune::Fem::FunctionSpace< double, double, 3, 3 > FunctionSpaceType;

  typedef typename SurfaceEvolutionType::DomainType DomainVector;
  typedef typename SurfaceEvolutionType::RangeType RangeVector;
  typedef double RangeFieldType;

  SurfaceDeformationCoordFunction ( SurfaceEvolution &surfaceEvolution )
    : surfaceEvolution_( surfaceEvolution )
  {}

  void evaluate ( const DomainVector &x, RangeVector &y ) const
  {
    surfaceEvolution_.evaluate( x, y );
  }

  void setTime( const double time )
  {
    surfaceEvolution_.setTime( time );
  }

private:
  SurfaceEvolution& surfaceEvolution_;
};

template <class DiscreteFunctionType>
class SurfaceDeformationDiscreteFunction
: public Dune::DiscreteCoordFunction< double, 3, SurfaceDeformationDiscreteFunction< DiscreteFunctionType > >
{
  typedef Dune::DiscreteCoordFunction< double, 3, SurfaceDeformationDiscreteFunction< DiscreteFunctionType > > BaseType;

  typedef typename DiscreteFunctionType :: GridType GridType ;
  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType ;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;

  typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType ;
  typedef typename DiscreteFunctionType :: RangeType  RangeType ;

  typedef typename Dune::FieldVector<double,GridPartType::dimensionworld> FitPointType;

protected:
  typedef typename DiscreteFunctionSpaceType::BlockMapperType BlockMapperType;
  static const unsigned int blockSize = DiscreteFunctionSpaceType::localBlockSize;

  typedef typename DiscreteFunctionType::DofBlockPtrType DofBlockPtrType;

public:
  SurfaceDeformationDiscreteFunction ( GridType &grid)
  : gridPart_( grid ),
    space_( gridPart_ ),
    fittol_( Dune::Fem::Parameter::getValue< double >( "fit.tol", 1.0e-4 ) ),
    timeStore_( 0 ),
    fitCount_( 0 ),
    track_( 0 ),
    de_( Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ) ),
    critRatio_( Dune::Fem::Parameter::getValue< double >( "critical.ratio", 2.5   ) ),
    boomed_( false ),
    retVal_( 0 ),
    ejectQ_( 0.71 ),
    ejectP_( 0.005 ),
    ejectG_( -0.5 ),
    ejectK_( 1.0 ),
    ejectC_( 0.0 ),
    small_( false ),
    snall_( false ),
    initiate_( Dune::Fem::Parameter::getValue< double >( "initi.ate", 4   ) ),
    wonk_( Dune::Fem::Parameter::getValue< double >( "initi.wonk", 1.1   ) ),
    fitres_( std::pow( 10.0, Dune::Fem::Parameter::getValue< double >( "fit.res", 3.0   ) ) ),
    top_( 1.0e5 ),
    prevTop_( 0 ),
    bot_( -1.0e5 ),
    prevBot_( 0 ),
    point_( 0 )
  {
    vertices_ = new DiscreteFunctionType( "deformation", space_ );
    // open file to hold the fit data
    if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_.open("fit.dat");
    ratioStore_.assign(3,0);
  }

  ~SurfaceDeformationDiscreteFunction()
  {
    if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_.close();
  }

  template < class RangeType, class DFunctionType >
  struct CoordFunctor
  {
    RangeType& y_;
    DFunctionType * function_;

    CoordFunctor( RangeType& y, DFunctionType* function )
      : y_( y )
      , function_( function )
    {
    }

    template <class GlobalKey>
    void operator () ( const size_t local, const GlobalKey& globalKey )
    {
      DofBlockPtrType blockPtr = function_->block( globalKey );
      for( unsigned int j = 0; j < blockSize; ++j )
      {
        y_[ j ] = (*blockPtr)[ j ];
      }
    }

    void fill(RangeType& y)
    {
      for( unsigned int j = 0; j < blockSize; ++j )
      {
        y[ j ] = y_[ j ];
      }
    }
  };

  template< class HostEntity , class RangeVector >
  void evaluate ( const HostEntity &hostEntity, unsigned int corner,
                  RangeVector &y ) const
  {
    CoordFunctor< RangeVector, DiscreteFunctionType > coord ( y, vertices_);

    vertices_->space().blockMapper().mapEachEntityDof( hostEntity, coord );
    coord.fill ( y );
  }

  template <class RangeVector>
  void evaluate ( const typename GridType :: template Codim<0>::Entity &entity,
		              unsigned int corner,
                  RangeVector &y ) const
  {
    typedef typename GridType::ctype  ctype;
    enum { dim = GridType::dimension };

    const Dune::ReferenceElement< ctype, dim > &refElement
      = Dune::ReferenceElements< ctype, dim >::general( entity.type() );

    LocalFunctionType localVertices = vertices_->localFunction( entity );

    localVertices.evaluate( refElement.position( corner, dim ), y );
  }

  template <class DF>
  void setTime( const double time, DF &solution )
  {
    const typename DiscreteFunctionType::DofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    timeStore_ = time;
    for( typename DiscreteFunctionType::DofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
      *it = *git;
  }

  template <class GF>
  void initialize(const GF& gf)
  {
    typedef Dune::Fem::LagrangeInterpolation< GF, DiscreteFunctionType > VertexInterpolationType;
    VertexInterpolationType interpolation;
    interpolation( gf, *vertices_ );
  }

  template <class DF>
  void initializeSolution(DF &solution)
  {
    const typename DiscreteFunctionType::ConstDofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::ConstDofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
      *git = *it;
  }

  template <class DF>
  void nudge(double & ts, DF &solution)
  {
    Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> maxMinComm(Dune::MPIHelper::getCommunicator());
    prevTop_ = maxMinComm.max(top_); prevBot_ = maxMinComm.min(bot_);
    // will need an average gradient of nodes around the tip
    double gradientAccumulator = 0;
    // gradient accumulator count
    int gac = 0;
    top_ = 0.0; bot_ = 0.0;
    int count = 0;
    initiate_--;

    // int rank = Dune::Fem::MPIManager::rank();

    const typename DiscreteFunctionType::ConstDofIteratorType end = vertices_->dend();
    typename DF::DofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::ConstDofIteratorType it = vertices_->dbegin();
	       it != end; ++it, ++git )
    // const typename DF::DofIteratorType end = solution.dend();
    // typename DiscreteFunctionType::DofIteratorType it = vertices_->dbegin();
    // for( typename DF::DofIteratorType git = solution.dbegin();
    //         git != end; ++it, ++git )
    {
      // parallel runs can come in here with *apparently* unset vertex positions
      if( *it > 1.0e9 ) continue;

      // std::cout << "ABC  " << *git << "  " << *it;

      if( ( ( *it >= prevTop_ ) || ( *it <= prevBot_ ) ) && ( initiate_ > 0 ) && small_ && snall_ )
      {
        *git = *it + wonk_ * ts * *git;
        count++;
      }
      else
      *git = *it + ts * *git;

      bool bob = *git > 1.0e+05;

      // std::cout << "  " << *git << "  " << ts << "  " << bob << "  " << rank << std::endl;

      // keep track of the biggest and smallest (new) raw coordinate values
      if( *git > top_ ) top_ = *git;
      if( *git < bot_ ) bot_ = *git;

      // pick-up the coordinates for the (potential) fit points
      track_++;
      if( track_ == 1 )
        // x-coordinate
        point_[ 0 ] = *git;
      else if( track_ == 2 )
        // y-coordinate
        point_[ 1 ]  = *git;
      else
      { // z-coordinate

	// std::cout << "QWQ " << point_[ 0 ] << "  " << point_[ 1 ] << "  " << *git << "  " << snall_ << "  " << small_ << std::endl;

        // all points are used in the fitting
	point_[ 1 ] = std::sqrt( point_[1] * point_[1] + point_[0] * point_[0] );
        point_[ 1 ] = std::round( fitres_ * point_[ 1 ] ) / fitres_;

        // following line makes removing duplicates easier
        point_[ 0 ] = 0;

        // build up average of tip gradients
        if( ( *git > 0.75 * prevTop_ ) && ( *git < prevTop_ ) )
	{
          gradientAccumulator += ( *git - prevTop_ ) / point_[ 1 ];
          gac++;
        }
        else if( ( *git < 0.75 * prevBot_ ) && ( *git > prevBot_ ) )
	{
          gradientAccumulator -= ( *git - prevBot_ ) / point_[ 1 ];
          gac++;
        }

        // Accomodate tip shape change at explosion
        if( retVal_ )
	{
          if( std::abs(*git) > ejectC_ / ejectK_ )
          {
            double r = point_[ 1 ];

            // Compute the scaled displacement to use
            double d = 4.0 * ejectG_ * ejectK_ * r + 2.0 * ejectG_ * ejectG_;
            d = ( 4.0 * ejectK_ * ejectK_ * r * r - ejectG_ * ejectG_ ) / d;
            d = 1.0 + d;
            // Apply it to the z coordinate
            if( *git > 0 )
              *git = *git - d * ( *git - ejectC_ / ejectK_ );
            else
              *git = *git - d * ( *git + ejectC_ / ejectK_ );
          }
        }
        point_[ 2 ]  = *git;
        point_[ 2 ] = std::round( fitres_ * point_[ 2 ] ) / fitres_;

        {
         // add point to list of fitpoints
	 if( !bob && ( point_.two_norm() > 0.1 ) )
	 {
         fitPoints_.push_back(point_);
         // std::cout << "Scat  " << point_[ 2 ] << "  " << point_[ 1 ] << "  " << gridPart_.comm().rank() << std::endl;
         fitCount_++;
         }
        }
        track_ = 0;
      } // z-coordinate

      // ... and whether the previous two values looked at were zeroish
      snall_ = small_;
      small_ = std::abs(*git) < 3.0e-2;

    } // end of loop over vertices

    // determine estimate of tip gradient
    double totalGradientAccumulator = maxMinComm.sum(gradientAccumulator);
    int totalGac = maxMinComm.sum(gac);
    if( totalGac > 0 )
      ejectG_ = totalGradientAccumulator / double( totalGac );
    else
      ejectG_ = -0.005;

    totalFitCount_ = maxMinComm.sum(fitCount_);

#if 0
    if( ( count != 2 ) && ( prevTop_ < 5.0e4 ) && ( initiate_ > 0 ) )
    {
      std::cout << "count = " << count << std::endl;
      DUNE_THROW(Dune::Exception, "Bad nudge count.");
    }
#endif
#if 1
    // Send the locally collected fit points to the root process
    // std::vector<int> sizes(fitPoints_.size()); // gridPart_.comm().size());

    int share = fitPoints_.size();
    // gridPart_.comm().template allgather<int>(&share, 1, sizes.data());
    int globSize = maxMinComm.sum(share);

    // std::cout << "WWW " << rank << "  " << gridPart_.comm().rank() << "  " << share << "  " << globSize << std::endl;

    // std::vector<int> offsets(sizes.size());

    // for (size_t k = 0; k < offsets.size(); ++k)
    // offsets[k] = std::accumulate(sizes.begin(), sizes.begin() + k, 0);

    if (gridPart_.comm().rank() == 0)
      fitPointsGlobal_.resize(globSize); // std::accumulate(sizes.begin(), sizes.end(), 0));

    maxMinComm.gather(fitPoints_.data(),
                          fitPointsGlobal_.data(),
                          fitPoints_.size(),
                          0);

    // std::cout << "\n\nZZZZZZZZZZZZZZZZZZZ\n\n" << std::endl;
#endif
  }

  // indicates whether a max or min of a / b has been reached
  bool peaked()
  {
    bool one = ( ratioStore_[ 1 ] > ratioStore_[ 0 ] ) && ( ratioStore_[ 1 ] > ratioStore_[ 2 ] );
    bool two = ( ratioStore_[ 1 ] < ratioStore_[ 0 ] ) && ( ratioStore_[ 1 ] < ratioStore_[ 2 ] );

    if( one || two ) std::cout << "PEAKED !!!" << std::endl;
    return one || two;
  }

  double ratio()
  {
    return ratioStore_[ 1 ];
  }

  bool abnfit()
  {
    Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> comm(Dune::MPIHelper::getCommunicator());
    short int localRetVal = 0;
#if 1
    if( Dune::Fem::MPIManager::rank() == 0 )
    {

    // for (int i=0;i<fitPointsGlobal_.size();i++)
    // {
    //    std::cout << "Before " << fitPointsGlobal_[ i ][ 2 ] << "  " << fitPointsGlobal_[ i ][ 1 ] << std::endl;
    // }

    // std::cout << "\n\n\n Before " << fitPointsGlobal_.size() << std::endl;

    // remove the duplicates
    std::sort(fitPointsGlobal_.begin(), fitPointsGlobal_.end(), myobject);

    // for (int i=0;i<fitPointsGlobal_.size();i++)
    // {
    //    std::cout << "Middle " << fitPointsGlobal_[ i ][ 2 ] << "  " << fitPointsGlobal_[ i ][ 1 ] << std::endl;
    // }

    // std::cout << "\n\n\n Middle " << fitPointsGlobal_.size() << std::endl;

    auto last = std::unique(fitPointsGlobal_.begin(), fitPointsGlobal_.end());
    fitPointsGlobal_.erase(last, fitPointsGlobal_.end());

    // for (int i=0;i<fitPointsGlobal_.size();i++)
    // {
    //    std::cout << "After  " << fitPointsGlobal_[ i ][ 2 ] << "  " << fitPointsGlobal_[ i ][ 1 ] << std::endl;
    // }

    // std::cout << "\n\n\n After  " << fitPointsGlobal_.size() << std::endl;

    int NPT = fitPointsGlobal_.size(), MA = 3;
#else
    int NPT = fitPoints_.size(), MA = 3;
#endif
    Vec_DP x(NPT),y(NPT),sig(NPT);
    Vec_BOOL ia(MA);
    Vec_DP a(MA);
    Mat_DP covar(MA,MA),alpha(MA,MA);
    DP alamda,chisq=0,ochisq=1000;
    double xm = 0, ym = 0;

    // set-up the radius and angle info needed for the fitting
    for (int i=0;i<NPT;i++)
    {
#if 1
       double xx = fitPointsGlobal_[ i ][ 2 ];
       double yy = fitPointsGlobal_[ i ][ 1 ];
#else
       double xx = fitPoints_[ i ][ 2 ];
       double yy = fitPoints_[ i ][ 1 ];
#endif
       // std::cout << "Gath  " << xx << "  " << yy << "  " << gridPart_.comm().rank() << std::endl;
       double rr = std::sqrt( xx*xx + yy*yy );
       double ang = std::atan2( yy , xx );

       x[i]=std::cos( M_PI / 2.0 - ang );
       y[i]=rr;
       sig[i]=1;

       // keep track of biggest x and y values for initializing the fitting
       if( ( abs(yy) < 1e-2 ) || ( de_ > 0 ) ) xm = std::max( xm , std::abs(xx) );
       if( ( abs(xx) < 1e-2 ) || ( de_ > 0 ) ) ym = std::max( ym , std::abs(yy) );
    }

    // initial guesses for the "a", "b" and "n" parameters
    a[0] = ym;
    a[1] = xm;
    a[2] = 2.0;

    for (int i=0;i<MA;i++) ia[i]=true;

    alamda = -1;

    if( de_ > 0 )
    {
    // perform the least-squared fitting
    for( unsigned int i = 0; i< 220; ++i )
    {
      // std::cout << "CHI " << a[0] << "  " << a[1] << "  " <<  a[2] << "  " <<  chisq << std::endl;
      if( std::abs( ochisq - chisq ) < 1e-13 ) break;
      ochisq = chisq;
      NR::mrqmin(x,y,sig,a,ia,covar,alpha,chisq,superellips,alamda);
    }
    }
    // write fit data to output file
    if( Dune::Fem::MPIManager::rank() == 0 ) fitOutput_ << timeStore_ << "  " << a[0] << "  " << a[1] << "  " <<  a[2] << "  " << NPT << "  " << totalFitCount_ << "  " << chisq << "  " << top_ << "  " << bot_ << "  " << ejectG_ << std::endl;

    fitPointsGlobal_.clear();

    // keep a short history of the fit ratio
    for( unsigned int i = 2; i > 0; --i )
      ratioStore_[ i ] = ratioStore_[ i-1 ];
    ratioStore_[ 0 ] = a[ 1 ] / a[ 0 ];

    bool boom = a[1] / a[0] > critRatio_;

    if( boom && ( ! boomed_ ) ) localRetVal = 1;

    if( boom ) boomed_ = true;

    } // end of rank 0 stuff

    retVal_ = comm.sum(localRetVal);

    // reset fit points array
    track_ = 0;
    fitPoints_.clear();
    fitCount_ = 0;

    if( retVal_ )
    {
      // Set-up the transformation parameters to the canonical tip geometry
      ejectK_ = 0.25 * std::pow( ejectG_ * ejectG_ * ejectG_ * ejectG_ / ejectP_ , 1.0/3.0 );
      ejectC_ = ( prevTop_ - prevBot_ ) * ejectK_ / 2.0 - ejectG_ * ejectG_ / 2.0;

      std::cout << "Eject parameters on rank " << Dune::Fem::MPIManager::rank() << " are: Top = " << prevTop_
                << " Bot = " << prevBot_ << " P = " << ejectP_ << " G = " << ejectG_
                << " K = " << ejectK_ << " C = " << ejectC_ << " Q = " << ejectQ_ << std::endl;
    }

    return retVal_ != 0;
  }

  double radius(double &max, double &min)
  {
    max = 0.0;
    min = 1.0e6;
    double average = 0.;
    double count = 0;
    const DiscreteFunctionSpaceType &dfSpace = vertices_->space();

    const IteratorType end = dfSpace.end();
    for( IteratorType it = dfSpace.begin(); it != end; ++it )
      {
	const EntityType &entity = *it;

	for( unsigned int corner = 0; corner < 3; ++corner )
	  {
	    RangeType y;
	    evaluate( entity, corner, y );

	    double r = y.two_norm();
	    average += r;
	    ++count;

	    if( r > max )
	      max = r;
	    if( r < min )
	      min = r;
	  }
      }
    average /= count;

    if( max - min > 1.0e-8 )
      {
	std::cout << "not circular! (max: " << max << " min: " << min << " )" << std::endl;
      }

    return average;
  }

protected:
  GridPartType gridPart_;
  DiscreteFunctionSpaceType space_;
  DiscreteFunctionType* vertices_;
private:
  // tolerance for deciding whether a potential fit point is on the appropriate coordinate plane
  const double fittol_;
  // index to keep track of which (potential) fit point coordinate is being picked-up
  unsigned int track_;
  // stores to pick up full 3-D (potential) fit point coordinates from the solution vector
  FitPointType point_;
  // store of points to be used by the fitting
  std::vector<FitPointType> fitPoints_;
  std::vector<FitPointType> fitPointsGlobal_;
  // output stream
  std::ofstream fitOutput_;
  // keep a record of the time for outputing with the fit data
  double timeStore_;
  // record how many fit points were found
  unsigned int fitCount_;
  unsigned int totalFitCount_;
  // history of the ratios
  std::vector<double> ratioStore_;
  double de_;
  double top_;
  double prevTop_;
  double bot_;
  double prevBot_;
  double critRatio_;
  bool boomed_;
  short int retVal_;

  // Multiplier to reduce charge on prolate explosion
  double ejectQ_;

  // Percentage of initial fluid volume to be lost at explosion
  double ejectP_;

  // Gradient of droplet surface at tip just before explosion
  double ejectG_;

  // Scale factor to transform to canonical tip geometry
  double ejectK_;

  // Vertical displacement to transform to canonical tip geometry
  double ejectC_;

  bool small_;
  bool snall_;
  int initiate_;
  double wonk_;
  double fitres_;
};

#endif // #ifndef SURFACE_DEFORMATION_HH
