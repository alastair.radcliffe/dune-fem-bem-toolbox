#ifndef NAVIER_STOKES_MODEL_HH
#define NAVIER_STOKES_MODEL_HH

#include <dune/fem/solver/timeprovider.hh>

#include <dune/fem_bem_toolbox/fem_objects/temporalprobleminterface.hh>
#include <dune/fem_bem_toolbox/fluid_models/stokesmodel.hh>

template< class FunctionSpace, class GridPart, class BoundaryDataFunctionSpace >
struct NavierStokesModel : public StokesModel<FunctionSpace,GridPart,BoundaryDataFunctionSpace>
{
  typedef StokesModel<FunctionSpace,GridPart,BoundaryDataFunctionSpace> BaseType;
  typedef FunctionSpace FunctionSpaceType;
  typedef BoundaryDataFunctionSpace BoundaryDataFunctionSpaceType;
  typedef GridPart GridPartType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef TemporalProblemInterface< FunctionSpaceType > ProblemType ;

  typedef typename BaseType::ProblemType InitialFunctionType;

  typedef Dune::Fem::TimeProviderBase TimeProviderType;

  //! constructor taking problem reference, time provider,
  //! time step factor( either theta or -(1-theta) ),
  //! flag for the right hand side
  NavierStokesModel( const ProblemType& problem,
             const GridPart &gridPart,
             const bool implicit )
    : BaseType(problem,gridPart),
      timeProvider_(problem.timeProvider()),
      implicit_( implicit ),
      timeStepFactor_( 0 )
  {
    // get theta for theta scheme
    const double theta = Dune::Fem::Parameter::getValue< double >( "ns.theta", 0.5 );
    if (implicit)
    {
      timeStepFactor_ = theta ;
    }
    else
    {
      timeStepFactor_ = -( 1.0 - theta ) ;
    }
  }

  // the linearization of the source function
  template< class Entity, class Point >
  void linSource ( const RangeType& uBar,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    const DomainType xGlobal = entity.geometry().global( coordinate( x ) );
    RangeType m;
    problem_.m(xGlobal,m);

    BaseType::linSource(uBar,entity,x,value,gradient,flux);

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      flux[ localRow ] *= 2.0 * timeStepFactor_;
    }

    if( ! implicit_ )
      BaseType::advectionSource(uBar,entity,x,value,gradient,flux);

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      flux[ localRow ] *= timeProvider_.deltaT();
      flux[ localRow ] += m[ localRow ] * value[ localRow ];
    }

    flux[ dimDomain ] *= 2.0 * timeStepFactor_ * timeProvider_.deltaT();
  }

  // linearization of diffusiveFlux
  template< class Entity, class Point >
  void linDiffusiveFlux ( const RangeType& uBar,
                          const JacobianRangeType& gradientBar,
                          const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          const JacobianRangeType &gradient,
                          JacobianRangeType &flux ) const
  {
    BaseType::linDiffusiveFlux(uBar,gradientBar,entity,x,value,gradient,flux);

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      flux[ localRow ] *= 2.0 * timeStepFactor_ * timeProvider_.deltaT();
    }
    flux[ dimDomain ] *= 2.0 * timeStepFactor_ * timeProvider_.deltaT();
  }

  //! return reference to Problem's time provider
  const TimeProviderType & timeProvider() const
  {
    return timeProvider_;
  }

  const InitialFunctionType &initialFunction() const
  {
    return problem_;
  }

protected:
  using BaseType::problem_;
  const TimeProviderType &timeProvider_;
  bool implicit_;
  double timeStepFactor_;
public:
  static const bool applyBoundaryForcing = true;
};
#endif // #ifndef NAVIER_STOKES_MODEL_HH
