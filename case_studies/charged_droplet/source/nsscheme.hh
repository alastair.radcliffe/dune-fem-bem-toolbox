#ifndef NS_FEMSCHEME_HH
#define NS_FEMSCHEME_HH

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>

// local includes
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include "addpressure.hh"

// NSScheme
//-----------

template < class ImplicitModel, class ExplicitModel >
struct NSScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef ExplicitModel ExplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;

  NSScheme( GridPartType &gridPart,
              const ImplicitModelType& implicitModel,
              const ExplicitModelType& explicitModel )
  : BaseType(gridPart, implicitModel),
    explicitModel_(explicitModel),
    explicitOperator_( explicitModel_, discreteSpace_ )
  {
  }

  DiscreteFunctionType &polution()
  {
    return polution_;
  }
  const DiscreteFunctionType &polution() const
  {
    return polution_;
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void prepare(const bool &firsttime,OtherDiscreteFunctionType &otherSolution_)
  {
    if( firsttime )
    {
    // apply constraints, e.g. Dirichlet contraints, to the solution
    BaseType::preparePartOne();

    // apply explicit operator and also setup right hand side
    explicitOperator_( solution_, otherSolution_, rhs_ );

    // apply constraints, e.g. Dirichlet contraints, to the result
    BaseType::preparePartThree();
    }
    else
    {
      // add solution (velocities) to (minus) mesh velocities held in polution
      polution_ += solution_;
      explicitOperator_( solution_, otherSolution_, polution_, rhs_ );
    }
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void setup(OtherDiscreteFunctionType &otherSolution_)
  {
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = otherSolution_.space().gridPart();

    typedef AddPressure< OtherDiscreteFunctionType, DiscreteFunctionType > AddPressureType;
    typedef Dune::Fem::LocalFunctionAdapter< AddPressureType > AddPressureFunctionType;
    AddPressureType addpressure( otherSolution_ );
    AddPressureFunctionType addPressureFunction( "addpressure", addpressure, otherGridPart_ );

    // apply coupling constraints using new expanded solution
    implicitOperator_.coupleSetup( solution_, addPressureFunction, rhs_ );
  }

  void initialize ()
  {
     Dune::Fem::LagrangeInterpolation
          < typename ExplicitModelType::InitialFunctionType, DiscreteFunctionType > interpolation;
     interpolation( explicitModel_.initialFunction(), solution_ );
  }

  void setPolution(const bool &peaked)
  {
    if( peaked ) solution_.clear();
    polution_.assign( solution_ );
  }

private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::solution_;
  using BaseType::polution_;
  using BaseType::implicitModel_;
  using BaseType::implicitOperator_;
  using BaseType::linearOperator_;
  using BaseType::rhs_;
  const ExplicitModelType &explicitModel_;
  typename BaseType::EllipticOperatorType explicitOperator_; // the operator for the rhs
};

#endif // end #if NS_FEMSCHEME_HH
