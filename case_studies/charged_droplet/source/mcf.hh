#ifndef MCF_HH
#define MCF_HH

#include <cassert>
#include <cmath>

#include <dune/fem/function/common/function.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/quadrature/quadrature.hh>

#include <dune/fem_bem_toolbox/fem_objects/temporalprobleminterface.hh>

// EvolutionFunction
// -----------------

template< class FunctionSpace, class Impl >
struct EvolutionFunction
: public Dune::Fem::Function< FunctionSpace, Impl >
{
  explicit EvolutionFunction ( const double time = 0.0 )
  : time_( time )
  {}

  const double &time () const
  {
    return time_;
  }

  void setTime ( const double time )
  {
    time_ = time;
  }

private:
  double time_;
};

namespace Problem
{
  // SurfaceEvolution
  // ----------------
  template< class FunctionSpace >
  struct SurfaceEvolution
    : public EvolutionFunction< FunctionSpace, SurfaceEvolution< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    SurfaceEvolution(const double &deform)
    : de_( deform )
    {}

    void evaluate( const DomainType &z, RangeType &y ) const
    {
      assert( dimRange == 3 );
      assert( time() < 1.0e-10 );
      DomainType x = z;

      if( de_ < 0 )
      {
        // scaling parameter
        double gamma = 1; // 4 * M_PI / 3;

        // base hypoteneuse (axi-sym about x axis)
        double baseHyp = std::sqrt( x[1]*x[1]+x[2]*x[2] );

        // angle phi about axisym axis
        double cosPhi = baseHyp > 1.0e-10 ? x[1] / baseHyp : 0;
        double sinPhi = baseHyp > 1.0e-10 ? x[2] / baseHyp : 0;

        // angle alpha over z-coordinate
        double cosAlpha = x[0] / x.two_norm();
        double sinAlpha = baseHyp / x.two_norm();

        // second spherical harmonic
        double P2 = 0.5 * ( 3 * cosAlpha * cosAlpha - 1 );

        // required radius
        double radius = x.two_norm() * gamma * ( - de_ + 0.9 * P2 );

        // the projected positions
        y[0] = radius * cosAlpha;
        y[2] = radius * sinAlpha * cosPhi;
        y[1] = radius * sinAlpha * sinPhi;
      }
      else
      {
        y = x;
        if( de_ < 900.0 )
        {
          // now apply the deformation
          y /= std::pow( de_ , 1.0/3.0 );
          y[2] *= de_;
        }
      }
      return;
    }

    void ReDe(const double &deform)
    {
      de_ = deform;
    }

    using EvolutionFunction< FunctionSpace, SurfaceEvolution< FunctionSpace > >::time;

private:
    double de_;
  };
  // VolumeEvolution
  // ----------------
  template< class FunctionSpace >
  struct VolumeEvolution
    : public EvolutionFunction< FunctionSpace, VolumeEvolution< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    VolumeEvolution(const double &deform)
    : de_( deform )
    {}

    void evaluate( const DomainType &z, RangeType &y ) const
    {
      assert( dimRange == 3 );
      assert( time() < 1.0e-10 );
      DomainType x = z;

      if( de_ < 0 )
      {
        // scaling parameter
        double gamma = 1; // 4 * M_PI / 3;

        // base hypoteneuse (axi-sym about x axis)
        double baseHyp = std::sqrt( x[1]*x[1]+x[2]*x[2] );

        // angle phi about axisym axis
        double cosPhi = baseHyp > 1.0e-10 ? x[1] / baseHyp : 0;
        double sinPhi = baseHyp > 1.0e-10 ? x[2] / baseHyp : 0;

        // angle alpha over z-coordinate
        double cosAlpha = x[0] / x.two_norm();
        double sinAlpha = baseHyp / x.two_norm();

        // second spherical harmonic
        double P2 = 0.5 * ( 3 * cosAlpha * cosAlpha - 1 );

        // required radius
        double radius = x.two_norm() * gamma * ( - de_ + 0.9 * P2 );

        // the projected positions
        y[0] = radius * cosAlpha;
        y[2] = radius * sinAlpha * cosPhi;
        y[1] = radius * sinAlpha * sinPhi;
      }
      else
      {
        y = x;
        if( de_ < 900.0 )
        {
          // now apply the deformation
          y /= std::pow( de_ , 1.0/3.0 );
          y[2] *= de_;
        }
      }
      return;
    }

    void ReDe(const double &deform)
    {
      de_ = deform;
    }

    using EvolutionFunction< FunctionSpace, VolumeEvolution< FunctionSpace > >::time;

private:
    double de_;
  };
#if 0
  // RHSFunction
  // -----------

  template< class FunctionSpace >
  struct RHSFunction
  : public EvolutionFunction< FunctionSpace, RHSFunction< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    void evaluate( const DomainType &x, RangeType &phi ) const
    {
      phi = RangeType( 0 );
    }

    using EvolutionFunction< FunctionSpace, RHSFunction< FunctionSpace > >::time;
  };
#endif

}


template <class FunctionSpace>
class AleNiceMesh : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    /*
    phi  = M_PI*(4*dimDomain*M_PI*std::cos( M_PI*time() ) - std::sin( M_PI*time() ));
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    */
    phi = 0;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true ;
  }

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    phi = std::cos( M_PI );
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::cos( M_PI )*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }
};


template <class FunctionSpace>
class Fluid : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  Fluid( const Dune::Fem::TimeProviderBase &timeProvider )
    : BaseType( timeProvider ),
      m_( 1.0 / Dune::Fem::Parameter::getValue< double >( "ohne.sorge", 1.0e9 ) ),
      n_( Dune::Fem::Parameter::getValue< double >( "non.linear", 0 ) ),
      k_( Dune::Fem::Parameter::getValue< double >( "drop.kick", 0 ) )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    /*
    phi  = M_PI*(4*dimDomain*M_PI*std::cos( M_PI*time() ) - std::sin( M_PI*time() ));
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    */
    phi = 0;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return false ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return false ;
  }

  //! mass coefficient (default = 0)
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(m_);
  }

  //! non-linear coefficient (default = 0)
  virtual void n(const DomainType& x, RangeType &n) const
  {
    n = RangeType(2.0*n_*m_);
  }

  //! the exact solution
  // corresponds to the first (quadrupole) mode of oscillation
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    // vertical velocity component - along z-axis
    phi[ 2 ] = 2 * k_ * x[2];
    // radial velocity component
    phi[ 1 ] =  - k_ * x[1];
    phi[ 0 ] =  - k_ * x[0];
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::cos( M_PI*time() )*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }

private:
  double m_;
  double n_;
  double k_;
};


template <class FunctionSpace>
class TimeDependentCosinusProduct : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  TimeDependentCosinusProduct( const Dune::Fem::TimeProviderBase &timeProvider )
    : BaseType( timeProvider ),
      def_( Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ) ),
      // assume ellipsoid alligned along x-axis
      fita_( def_ / std::pow( def_ , 1.0/3.0 ) ),
      fitb_( 1.0  / std::pow( def_ , 1.0/3.0 ) )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi  = M_PI*(4*dimDomain*M_PI*std::cos( M_PI*time() ) - std::sin( M_PI*time() ));
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
  }

  //! the exact solution
  virtual void u(const DomainType& xx,
                 RangeType& phi) const
  {
    // radial distance
    double x = std::sqrt( xx[ 1 ]*xx[ 1 ] + xx[ 0 ]*xx[ 0 ] );

    // The y to go with the given x
    double y = x / fitb_;
    y = y * y;
    y = fita_ * sqrt( 1.0 - y );

    // std::cout << "Mean " << x << "  " << y << "  " << xx[ 0 ] << std::endl;

    // Constructions
    double r1 = fitb_ * fitb_; double r2 = fita_ * fita_;

    double H = r1 * r2; double K = r1;

    r1 = x * x / ( r1 * r1 ); r2 = y * y / ( r2 * r2 );

    r2 = sqrt( r1 + r2 );

    // Principle radii of Curvature
    r1 = H * r2 * r2 * r2;
    r2 = K * r2;

    // Mean Curvature - the Algebraic Mean
    phi = ( r1 + r2 ) / ( 2.0 * r1 * r2 );

    // Gaussian Curvature - the Geometric Mean
    // K = 1.0d0 / sqrt( r1 * r2 );

    /*
            ! Use spheroid analytic form when close to a sphere
            else if(sphereish) then
              ! Get spheroid Mean and Gaussian curvatures
              call fitHK(X_g(3),ek,Ex(j))
              ek = 2.0d0 * ek; Ex(j) = 2.0d0 * Ex(j) * ( omeg / fourpi ) ** 2


    phi = 5.6;
    phi = std::cos( M_PI*time() );
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    */
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::cos( M_PI*time() )*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }
private:
  double def_;
  double fita_;
  double fitb_;
};


#endif // #ifndef MCF_HH
