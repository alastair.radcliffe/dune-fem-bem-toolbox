#ifndef WAVESCAT_PROBLEMS_HH
#define WAVESCAT_PROBLEMS_HH

#include <cassert>
#include <cmath>
#include <complex>

#include <dune/fem_bem_toolbox/fem_objects/probleminterface.hh>


// -laplace u + omega * omega u = 0 with Dirichlet boundary conditions on spherical annulus
// Exact solution is u = exp( i omega r ) / ( 4 pi r )
template <class FunctionSpace>
class WaveScatter : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  WaveScatter(const double omega)
  : omega_( omega )
  , shift_( Dune::Fem::Parameter::getValue< double >( "helmholtz.shift", 0 ) )
  , amplitude_( Dune::Fem::Parameter::getValue< double >( "helmholtz.amplitude", 0 ) )
  , boxwaveno_( Dune::Fem::Parameter::getValue< double >( "helmholtz.boxwave", 0 ) )
  , direction_( DomainType(0) )
  , c_(1.)
  {
    direction_[0] = 1.0;
    direction_[1] = 2.0;
    double len = direction_.two_norm();
    direction_ /= len;
  }

  //! the right hand side data for an incident wave
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    if( assembleRHSwithOperator() )
    {
    double r = x.dot(direction_);
    // real part of solution
    double real = std::cos( omega_ * r );
    // imaginary part of solutionn
    double imag = std::sin( omega_ * r );
#if COMPLEX
    phi = std::complex<double>(real,imag);
#else
    // real part of solution
    phi[0] = real;
    // imaginary part of solutionn
    phi[1] = imag;
#endif
    // the minus in the following is because this phi will have the operator applied to it to form the rhs
    // and it should be *minus* the operator applied to it

      phi *= -amplitude_;
    }
    else
      phi = 0;
  }

  //! the exact solution is not possible for this problem, so use the u function to
  // hold the incident plane wave on which the operator is applied to set-up the rhs
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    if( !assembleRHSwithOperator() )
    {
    double r = x.dot(direction_);
    // real part of solution
    double real = std::cos( omega_ * r );
    // imaginary part of solutionn
    double imag = std::sin( omega_ * r );
#if COMPLEX
    phi = std::complex<double>(real,imag);
#else
    // real part of solution
    phi[0] = real;
    // imaginary part of solutionn
    phi[1] = imag;
#endif
    // the minus in the following is because this phi will have the operator applied to it to form the rhs
    // and it should be *minus* the operator applied to it

      phi *= amplitude_;
    }
    else
      phi = 0;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret = 0;
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    double n = 1.0;
    double p = 1.0;
    if( boxwaveno_ < -1.0e-8  ) p *= -boxwaveno_;
    // if( nn < 0.25 ) n = boxwaveno_;
    if (x.infinity_norm()<0.5)
    {
      n = std::max( std::max( x[0]*x[0],  x[1]*x[1] ),  x[2]*x[2] );
      n = 1 - 0.5 * p * std::exp( - n );
      n /= 1 - 0.5 * p * std::exp( -0.25 );
    }
    if( boxwaveno_ > 1.0e-8  ) n = boxwaveno_;
    m = -n*n*omega_*omega_/c_*c_;
  }

  //! return true if there is an incident field (default is false)
  virtual bool hasIncidentField () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true;
  }
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  //! return true if the right hand side is to be assembled using the operator
  virtual bool assembleRHSwithOperator() const
  {
    // false implies computation of total fields by fem and bem schemes
    // true implies calculation of only scattered fields by fem and bem
    return false ;
  }

private:
  const double omega_;
  const double shift_;
  const double amplitude_;
  const double boxwaveno_;
  DomainType direction_;
  const double c_;
};

// -laplace u + omega * omega u = 0 with Dirichlet boundary conditions on spherical annulus
// Exact solution is u = exp( i omega r ) / ( 4 pi r )
template <class FunctionSpace>
class SurfaceWaveScatter : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  SurfaceWaveScatter(const double omega)
  : omega_( omega )
  , amplitude_( Dune::Fem::Parameter::getValue< double >( "helmholtz.amplitude", 0 ) )
  , direction_( DomainType(0) )
  , c_(1.)
  {
    direction_[0] = 1.0;
    direction_[1] = 2.0;
    double len = direction_.two_norm();
    direction_ /= len;
  }

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi = RangeType(0);
  }

  //! the exact normal derivative of the solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    // real part of solution
    double real = 0;
    // imaginary part of solution
    double imag = 0;

      // normal direction
      DomainType n = DomainType(0);
      if     ( x[0] < 0.0001 ) n[0] = -1;
      else if( x[0] > 0.9999 ) n[0] = +1;
      else if( x[1] < 0.0001 ) n[1] = -1;
      else if( x[1] > 0.9999 ) n[1] = +1;
      else if( x[2] < 0.0001 ) n[2] = -1;
      else if( x[2] > 0.9999 ) n[2] = +1;

      // normal for a sphere geometry
      n = x;
      n /= x.two_norm();

      double r = x.dot(direction_);
      double d = n.dot(direction_);
      // real part of solution
      real = 0; // - omega_ * d * std::sin( omega_ * r );
      // imaginary part of solutionn
      imag = 0; //   omega_ * d * std::cos( omega_ * r );

#if COMPLEX
    phi = std::complex<double>(real,imag);
#else
    // real part of solution
    phi[0] = real;
    // imaginary part of solutionn
    phi[1] = imag;
#endif

      phi *= amplitude_;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret = 0;
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = -omega_*omega_/c_*c_;
  }

  //! return true if there is an incident field (default is false)
  virtual bool hasIncidentField () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true;
  }
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

private:
  const double omega_;
  const double amplitude_;
  DomainType direction_;
  const double c_;
};

#endif // #ifndef WAVESCAT_HH
