#include <config.h>

// iostream includes
#include <iostream>
#include <complex>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

// include grid part
#include <dune/fem/gridpart/filteredgridpart.hh>
#include <dune/fem_bem_toolbox/fem_objects/radialfilter.hh>
#if FEM_FEM
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#else
#include <dune/fem/gridpart/leafgridpart.hh>
#endif

#include <dune/fem_fem_coupling/fem_fem_coupling.hh>
#include <dune/fem_fem_coupling/surfacegridclass.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_simple.hh>

// include header of adaptive scheme
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>

#include <dune/fem_bem_toolbox/bem_objects/fembem.hh>
#include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>
// #include "../../common/source/erfscheme.hh"
#include <dune/fem_bem_toolbox/coupling_schemes/gmresscheme.hh>

#include <dune/grid/io/file/gmshreader.hh>

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType> //  , class PlotGridType>
double algorithm ( HGridType &grid, int step ) // , PlotGridType &plotGryd
{
  // we want to solve the problem on the leaf elements of the grid
  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GridPartType GridPartType;
  GridPartType gridPart( grid );
  // GridPartType GridPart( hostGridPart, Filter );

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType, false>::FunctionSpaceType FunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< FunctionSpaceType, GridPartType, FunctionSpaceType > ModelType;

  typedef typename ModelType::ProblemType ProblemType ;
  ProblemType* problemPtr = 0 ;
  const std::string problemNames [] = { "bulkprob", "surfprob", "bulkwave", "surfwave" };
  const int problemNumber = Dune::Fem::Parameter::getEnum("volume.problem", problemNames, 0 );
  switch ( problemNumber )
  {
    case 0:
#if COMPLEX
      problemPtr = new HelmholtzFundamental< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
#else
      problemPtr = new BulkBemProblem< FunctionSpaceType > ();
#endif
      break ;
    case 1:
#if COMPLEX
      problemPtr = new SurfaceHelmholtzFundamental< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
      break ;
    case 2:
      problemPtr = new WaveScatter< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
      break ;
    case 3:
      problemPtr = new SurfaceWaveScatter< FunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
#else
      problemPtr = new SurfaceBemProblem< FunctionSpaceType > ();
#endif
      break ;
  }
  assert( problemPtr );
  ProblemType& problem = *problemPtr ;

  // implicit model for left hand side
  ModelType implicitModel( problem, gridPart );
  // ModelType ImplicitModel( problem, GridPart );

  // create adaptive scheme
  typedef FemScheme< ModelType, istl > SchemeType;
  SchemeType scheme( gridPart, implicitModel,"bulk" );

  // create dune grid file with surface mesh
  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydType GrydType;
  // create dune grid file with surface mesh
  GrydType* grydd = 0;
  GrydType* fullGrydd = 0;

  // if requested, use a previously created surface grid
  if( Dune::Fem::Parameter::getValue< int >( "surface.use", 0 ) > 0 )
  {
    const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", GrydType::dimension );
    const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );
    Dune::GridPtr< GrydType > grydPtr( grydfile );
    grydd = grydPtr.release();
    // duplicate surface grid for BEM operators which will NOT be partitioned
    Dune::GridPtr< GrydType > fullGrydPtr( grydfile, Dune::MPIHelper::getLocalCommunicator() );
    fullGrydd = fullGrydPtr.release();
  }
  else
  {
    grydd = scheme.extractSurface();
    fullGrydd = grydd;
    return 0;
  }
  GrydType& gryd = *grydd ;
  GrydType& fullGryd = *fullGrydd ;

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<GrydType> SurfLoadBalancer;
  SurfLoadBalancer sldb(gryd);
  SurfLoadBalancer fsldb(fullGryd,true);

  // do initial load balance
  if ( sldb.repartition() )
    gryd.repartition( sldb );
  else
  gryd.loadBalance();

  // dummey load balance that puts whole grid on node zero
  if ( fsldb.repartition() )
    fullGryd.repartition( fsldb );
  else
    fullGryd.loadBalance();

  typedef typename Dune::FemFemCoupling::Glue< HGridType, false >::GrydPartType GrydPartType;
  GrydPartType grydPart(gryd);
  GrydPartType fullGrydPart(fullGryd);

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType, false>::FunctionSpaceOutsideType ExtractedSurfaceFunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< ExtractedSurfaceFunctionSpaceType, GrydPartType, ExtractedSurfaceFunctionSpaceType > ExtractedSurfaceModelType;

  typedef typename ExtractedSurfaceModelType::ProblemType ExtractedSurfaceProblemType ;
  ExtractedSurfaceProblemType* problimPtr = 0 ;

  const int problimNumber = Dune::Fem::Parameter::getEnum("surface.problem", problemNames, 0 );

  switch ( problimNumber )
  {
    case 0:
#if COMPLEX
      problimPtr = new HelmholtzFundamental< ExtractedSurfaceFunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
#else
      problimPtr = new BulkBemProblem< ExtractedSurfaceFunctionSpaceType > ();
#endif
      break ;
    case 1:
#if COMPLEX
      problimPtr = new SurfaceHelmholtzFundamental< ExtractedSurfaceFunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
      break ;
    case 2:
      problimPtr = new WaveScatter< ExtractedSurfaceFunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
      break ;
    case 3:
      problimPtr = new SurfaceWaveScatter< ExtractedSurfaceFunctionSpaceType > (Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ));
#else
      problimPtr = new SurfaceBemProblem< ExtractedSurfaceFunctionSpaceType > ();
#endif
      break ;
  }
  assert( problimPtr );
  ExtractedSurfaceProblemType& problim = *problimPtr ;

  ExtractedSurfaceModelType implycitModel( problim, grydPart );

  typedef BemScheme< ExtractedSurfaceModelType > ExtractedSurfaceSchemeType;
  ExtractedSurfaceSchemeType schyme( grydPart, fullGrydPart, implycitModel,"surface" );

  // SchemeType scheme( gridPart, implicitModel );
  // int ghg = 0;
  // std::cin >> ghg;

  // typedef typename Dune::Fem::LeafGridPart< PlotGridType > PlotGridPartType;

  // PlotGridPartType plotGrydPart(plotGryd);

  // typedef typename Dune::FemFemCoupling::Glue<HGridType>::PlotFunctionSpaceOutsideType PlotSurfaceFunctionSpaceType;

  // typedef DiffusionModel< PlotSurfaceFunctionSpaceType, PlotGridPartType, PlotSurfaceFunctionSpaceType > PlotSurfaceModelType;
  // typedef DiffusionModel< ExtractedSurfaceFunctionSpaceType, PlotGridPartType, ExtractedSurfaceFunctionSpaceType > PlotSurfaceModelType;

  // PlotSurfaceModelType plotImplycitModel( problim, plotGrydPart );

  // typedef ErfScheme< PlotSurfaceModelType > PlotSurfaceErfSchemeType;

  // PlotSurfaceErfSchemeType plotSchyme( plotGrydPart, plotImplycitModel,"erf surface" );

  // set-up the ext rep formulae potential operators
  // schyme.erfSetup( plotSchyme.rhs() );


  double bob = schyme.setDofRank( sldb );

  std::cout << "Full surface area = " << bob << std::endl;


  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GridPartType > GridExactSolutionType;
  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GrydPartType > GrydExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, gridPart, 5 );
  GrydExactSolutionType grydExactSolution("exact solution", problim, grydPart, 5 );
  GrydExactSolutionType fullGrydExactSolution("exact solution", problim, fullGrydPart, 5 );
  // GrydExactSolutionType plotGrydExactSolution("erf exact solution", problim, plotGrydPart, 5 );
  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *,
                       const typename SchemeType::DiscreteFunctionType *,
                       const typename SchemeType::DiscreteFunctionType *,
                       GridExactSolutionType * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &(scheme.incident()), &(scheme.rhs()), &gridExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step ) );

  typedef Dune::tuple< const typename ExtractedSurfaceSchemeType::DiscreteFunctionType *,
                       const typename ExtractedSurfaceSchemeType::P1DiscreteFunctionType *,
                       GrydExactSolutionType *  > SurfaceIOTupleType;
  // typedef Dune::tuple< const typename PlotSurfaceErfSchemeType::P1DiscreteFunctionType * > PlotSurfaceIOTupleType;
  typedef Dune::tuple< const typename ExtractedSurfaceSchemeType::P1DiscreteFunctionType * > DofRankIOTupleType;
  typedef Dune::Fem::DataOutput< GrydType, SurfaceIOTupleType > SurfaceDataOutputType;
  typedef Dune::Fem::DataOutput< GrydType, DofRankIOTupleType > DofRankDataOutputType;
  // typedef Dune::Fem::DataOutput< PlotGridType, PlotSurfaceIOTupleType > PlotSurfaceDataOutputType;
  SurfaceIOTupleType surfaceioTuple( &(schyme.solution()),
                                     &(schyme.rhs()),
                                     &grydExactSolution ) ; // tuple with pointers
  // PlotSurfaceIOTupleType plotSurfaceioTuple( &(plotSchyme.rhs()) ) ; // tuple with pointers
  DofRankIOTupleType dofRankIoTuple( &(schyme.dofRank()) );
  SurfaceDataOutputType surfacedataOutput( gryd, surfaceioTuple, DataOutputParameters( step, "surface" ) );
  // PlotSurfaceDataOutputType erfDataOutput( plotGryd, plotSurfaceioTuple, DataOutputParameters( step, "erf surface" ) );

  DofRankDataOutputType dofRankDataOutput( fullGryd, dofRankIoTuple, DataOutputParameters( step, "dofrank" ) );
  dofRankDataOutput.write( );

  GMResCouplingScheme<decltype(scheme), decltype(schyme)> fbScheme( scheme, schyme );

  int limit = Dune::Fem::Parameter::getValue< int >( "helmholtz.limit", 14 );

  // set-up the coupling
  fbScheme.setup();

  // the following will flip the Dirichlet-Neumann flag for the coupling style to Neumann for the fem scheme
  scheme.reset();

  // std::abort();

  // set up a timer for the looping
  time_t before;
  time(&before);

  // setup the right hand side
  fbScheme.prepare( true );

  // solve once
  fbScheme.solve();

  // determine how long it took to do the loop
  time_t after;
  time(&after);

  double seconds = difftime(after,before);

  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Converged at iteration " << fbScheme.n() << " to an error of " << fbScheme.errorOne() << " after a loop time " << seconds << " secs." << std::endl; // +errer

  // write final output ((float)timer)/CLOCKS_PER_SEC
  dataOutput.write();
  surfacedataOutput.write();

  // output bem solution on an extended grid
  const std::string externalName = Dune::Fem::Parameter::getValue< std::string >( "external_grid" );
  const int externalLevel = Dune::Fem::Parameter::getValue< int >( "external_grid.level" );
  if( externalLevel < 0 )  return fbScheme.errorOne();
  try
  {
    // first try a 2,3 grid
    typedef Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> ExternalGrid;
    Dune::GridPtr< ExternalGrid > gridPtr( externalName, Dune::MPIHelper::getLocalCommunicator() );
    ExternalGrid &grid = *gridPtr;
    grid.globalRefine( externalLevel );
    schyme.outputExternal( grid );
  }
  catch (...)
  {
    // output bem solution on an extended grid
    typedef Dune::ALUGrid<3,3,Dune::cube,Dune::nonconforming> ExternalGrid;
    Dune::GridPtr< ExternalGrid > gridPtr( externalName, Dune::MPIHelper::getLocalCommunicator() );
    ExternalGrid &grid = *gridPtr;
    grid.globalRefine( externalLevel );
    schyme.outputExternal( grid );
  }
  return fbScheme.errorOne();
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // ensure OMP eigen uses the same number of threads as the MPI routines
  int numThreads = Dune::Fem::MPIManager::size();
  Eigen::setNbThreads(numThreads);

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // create plot grid from DGF file
  // const std::string plotkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroPlotGridFile", HGridType::dimension );
  // const std::string plotGridFile = Dune::Fem::Parameter::getValue< std::string >( plotkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << "  " << std::endl; // < plotGridFile <

  // determine type of grid file
  bool dgfFile = gridfile.find("dgf") != std::string::npos;
  bool gmshFile = gridfile.find("msh") != std::string::npos;

  // check file type is meaningful
  if( !dgfFile && !gmshFile )
  {
    std::cout << "Unknown file type " << dgfFile << "  " << gmshFile << std::endl;
    std::abort();
  }

  // typedef typename Dune::FemFemCoupling::Glue< HGridType >::GrydType PlotGridType;
  // typedef Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> PlotGridType;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  // Dune::GridPtr< PlotGridType > plotGridPtr( plotGridFile );
  // std::unique_ptr<HGridType> gridPtr( Dune::GmshReader<HGridType>::read( gridfile, true, false ) );
  HGridType& grid = *gridPtr ;
  // PlotGridType& plotGrid = *plotGridPtr ;

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<HGridType> BulkLoadBalancer;
  BulkLoadBalancer bldb(grid);

  // do initial load balance
  if ( bldb.repartition() )
    grid.repartition( bldb );
  else
  grid.loadBalance();

  // plotGrid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "poisson.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );
  // Dune::Fem::GlobalRefine::apply( plotGrid, level * refineStepsForHalf );

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "poisson.repeats", 0 );

  // calculate first step
  double oldError = algorithm( grid, (repeats > 0) ? 0 : -1 ); // , plotGrid

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );
    // Dune::Fem::GlobalRefine::apply( plotGrid, refineStepsForHalf );

    const double newError = algorithm( grid, step ); // , plotGrid
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }
  std::cout << std::endl;
  std::cout << "Don't worry about the following abort, it is just to avoid bempp mega-dump at end of execution." << std::endl;
  std::abort();
  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
