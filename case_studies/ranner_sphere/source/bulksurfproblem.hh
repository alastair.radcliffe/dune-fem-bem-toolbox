#ifndef BULKSURF_PROBLEM_HH
#define BULKSURF_PROBLEM_HH

#include <cassert>
#include <cmath>

#include <dune/fem_bem_toolbox/fem_objects/probleminterface.hh>

// a reentrant corner problem with a 270 degree corner
template <class FunctionSpace>
class BulkProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;

public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  BulkProblem() {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    const double Xx = x[0], Yy = x[1], Zz = x[2];
    phi = std::pow(std::exp(1.),-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(3. + 4.*Xx - 4.*std::pow(Xx,2) + 4.*Yy - 4.*std::pow(Yy,2));
    //phi = -6;
  /*
    // nudge the argument onto the perfect sphere
    DomainType y = x;
    y /= x.two_norm();

    double xx = y[0];
    double yy = y[1];
    double zz = y[2];
    phi =

2*std::pow(xx,3)*std::pow(1 - 2*yy,2) - std::pow(xx,4)*std::pow(1 - 2*yy,2) + yy*(-2 + yy + 2*std::pow(yy,2) - std::pow(yy,3)) + std::pow(xx,2)*(1 + 4*yy - 8*std::pow(yy,2) + 8*std::pow(yy,3) - 4*std::pow(yy,4)) + xx*(-2 + 4*std::pow(yy,2) - 8*std::pow(yy,3) + 4*std::pow(yy,4))

    ;
    RangeType uVal;
    u( x, uVal );
    // multiply everything through by bulk solution removed in calculation of Mathematica expressions used for the above
    // phi *= uVal;
    // add in the undifferentiated interior bulk solution
    phi += uVal;
  */
  }

  //! the exact bulk solution
  virtual void u(const DomainType& x,
                 RangeType& ret) const
  {
    ret = std::exp( - x[0] * (x[0] - 1.0) - x[1] * (x[1] - 1.0)  );
    // ret = x[0]*x[0]+x[1]*x[1]+x[2]*x[2]+x[0]+x[1]+x[2];
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(1);
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret[0][0] = 2.*x[0];
    ret[0][1] = 2.*x[1];
    ret[0][2] = 2.*x[2];
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true ;
  }
};

template <class FunctionSpace>
class SurfaceProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  typedef typename FunctionSpace::HessianRangeType HessianRangeType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& ret) const
  {
    const double Xx = x[0], Yy = x[1], Zz = x[2];
    ret = std::pow(std::exp(1),-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(1.*Xx - 2.*std::pow(Xx,2) + 1.*Yy - 2.*std::pow(Yy,2)) + std::pow(std::exp(1),-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(3. + 4.*std::pow(Xx,5) - 4.*std::pow(Xx,6) + 4.*std::pow(Yy,5) - 4.*std::pow(Yy,6) + std::pow(Yy,4)*(13. - 4.*std::pow(Zz,2)) + std::pow(Xx,4)*(13. + 4.*Yy - 12.*std::pow(Yy,2) - 4.*std::pow(Zz,2)) + Yy*(8. - 2.*std::pow(Zz,2)) + std::pow(Yy,3)*(-10. + 4.*std::pow(Zz,2)) + std::pow(Xx,3)*(-10. - 2.*Yy + 8.*std::pow(Yy,2) + 4.*std::pow(Zz,2)) + std::pow(Yy,2)*(-14. + 5.*std::pow(Zz,2)) + std::pow(Xx,2)*(-14. + 8.*std::pow(Yy,3) - 12.*std::pow(Yy,4) + 5.*std::pow(Zz,2) + std::pow(Yy,2)*(26. - 8.*std::pow(Zz,2)) + Yy*(-10. + 4.*std::pow(Zz,2))) + Xx*(8. - 2.*std::pow(Yy,3) + 4.*std::pow(Yy,4) - 2.*std::pow(Zz,2) + Yy*(4. - 2.*std::pow(Zz,2)) + std::pow(Yy,2)*(-10. + 4.*std::pow(Zz,2)))) + std::pow(std::exp(1),-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(4. + 19.*Xx - 34.*std::pow(Xx,2) - 12.*std::pow(Xx,7) + 8.*std::pow(Xx,8) + 19.*Yy - 34.*std::pow(Yy,2) - 56.*std::pow(Yy,3) + 60.*std::pow(Yy,4) + 45.*std::pow(Yy,5) + 12.*Xx*std::pow(Yy,5) - 36.*std::pow(Xx,2)*std::pow(Yy,5) - 38.*std::pow(Yy,6) - 12.*Xx*std::pow(Yy,6) + 32.*std::pow(Xx,2)*std::pow(Yy,6) - 12.*std::pow(Yy,7) + 8.*std::pow(Yy,8) - 2.*Xx*std::pow(Zz,2) + 8.*std::pow(Xx,2)*std::pow(Zz,2) - 2.*Yy*std::pow(Zz,2) + 8.*std::pow(Yy,2)*std::pow(Zz,2) + 21.*std::pow(Yy,3)*std::pow(Zz,2) - 22.*std::pow(Yy,4)*std::pow(Zz,2) - 12.*std::pow(Yy,5)*std::pow(Zz,2) + 8.*std::pow(Yy,6)*std::pow(Zz,2) + std::pow(Xx,2)*std::pow(Yy,2)*(120. - 44.*std::pow(Zz,2)) + std::pow(Xx,2)*std::pow(Yy,3)*(88. - 24.*std::pow(Zz,2)) + Xx*std::pow(Yy,4)*(43. - 12.*std::pow(Zz,2)) + std::pow(Xx,5)*(45. + 12.*Yy - 36.*std::pow(Yy,2) - 12.*std::pow(Zz,2)) + Xx*Yy*(24. - 8.*std::pow(Zz,2)) + std::pow(Xx,6)*(-38. - 12.*Yy + 32.*std::pow(Yy,2) + 8.*std::pow(Zz,2)) + Xx*std::pow(Yy,3)*(-32. + 12.*std::pow(Zz,2)) + std::pow(Xx,2)*Yy*(-52. + 19.*std::pow(Zz,2)) + Xx*std::pow(Yy,2)*(-52. + 19.*std::pow(Zz,2)) + std::pow(Xx,2)*std::pow(Yy,4)*(-114. + 24.*std::pow(Zz,2)) + std::pow(Xx,3)*(-56. + 24.*std::pow(Yy,3) - 36.*std::pow(Yy,4) + 21.*std::pow(Zz,2) + std::pow(Yy,2)*(88. - 24.*std::pow(Zz,2)) + Yy*(-32. + 12.*std::pow(Zz,2))) + std::pow(Xx,4)*(60. - 36.*std::pow(Yy,3) + 48.*std::pow(Yy,4) - 22.*std::pow(Zz,2) + Yy*(43. - 12.*std::pow(Zz,2)) + std::pow(Yy,2)*(-114. + 24.*std::pow(Zz,2))));
    /*
    DomainType y = x;
    y /= x.two_norm();

    // surface tensor contribution
    JacobianRangeType Ju;
    uJacobian( y, Ju );
    Ju.mv( y, ret );
    ret *= double( dimDomain - 1 );

    // Hessian contribution
    HessianRangeType Hu;
    hessian( y, Hu );

    for( int i = 0; i < dimDomain; ++i )
    {
      // normal vector to the surface
      DomainType ds = y;
      for( int j = 0; j < dimRange; ++j )
      {
        DomainType Hds;
        Hu[ j ].mv( ds, Hds );
        ret[ j ] += ds * Hds;
      }
    }

    // regular Laplacian contribution
    ret -= 0;

    // add in (twice) the undifferentiated surface solution
    RangeType uVal;
    u( x, uVal );
    ret += 2.0*uVal;

    // subtract out (once) the interior undifferentiated bulk solution
    RangeType wVal;
    w( x, wVal );
    ret -= wVal;
    */
  }

  //! the exact surface solution derives from the chosen bulk interior solution
  virtual void u(const DomainType& p, RangeType& phi) const
  {
    const double Xx = p[0], Yy = p[1], Zz = p[2];
    phi = std::exp(-1.*(-1. + Xx)*Xx - 1.*(-1. + Yy)*Yy)*(1. + 1.*Xx - 2.*Xx*Xx + (1. - 2.*Yy)*Yy);
  /*
    // just add the bulk solution ...
    RangeType wVal;
    w( p, wVal );
    phi = wVal;
    // ... to its normal derivative
    RangeType dwdnVal;
    dwdn( p, dwdnVal );
    phi += dwdnVal;
  */
  }

  //! the exact bulk solution
  virtual void w(const DomainType& x, RangeType& ret) const
  {
    // choose freely the function to use
    ret = x[0]*x[0]+x[1]*x[1]+x[2]*x[2]+x[0]+x[1]+x[2];
    // ret = std::exp(-xx*(xx-1)*yy*(yy-1));
  }

  //! the exact bulk solution's normal derivative on the surface
  virtual void dwdn(const DomainType& x, RangeType& ret) const
  {
    // must be compatible with the function chosen above
    ret = 2+x[0]+x[1]+x[2];
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(1);
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x, JacobianRangeType& ret) const
  {
    ret[0][0] = 2.;
    ret[0][1] = 2.;
    ret[0][2] = 2.;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return false ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return false ;
  }
private:
  void hessian ( const DomainType &x, HessianRangeType &ret ) const
  {
    assert( dimRange == 1 );

    double xx = x[0];
    double yy = x[1];

    ret = 0;
    /*
    // ( d2u/dxdx ) / w
    ret[ 0 ][ 0 ][ 0 ] = 2;
    // ( d2u/dxdy ) / w
    ret[ 0 ][ 0 ][ 1 ] = 2;
    // ( d2u/dydy ) / w
    ret[ 0 ][ 1 ][ 1 ] = 2;
    // d2u/dydx = d2u/dxdy
    ret[ 0 ][ 1 ][ 0 ] = ret[ 0 ][ 0 ][ 1 ];
    */
  }
};

#endif
