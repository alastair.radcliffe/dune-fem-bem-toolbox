#include <config.h>

// iostream includes
#include <iostream>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

// include grid part
#include <dune/fem/gridpart/filteredgridpart.hh>
#include <dune/fem_bem_toolbox/fem_objects/radialfilter.hh>
#include <dune/fem/gridpart/leafgridpart.hh>

#include <dune/fem_fem_coupling/surfacegridclass.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_simple.hh>

// include header of adaptive scheme
#include "bulksurfproblem.hh"
#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>

#include <dune/fem_bem_toolbox/coupling_schemes/jacobischeme.hh>

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( HGridType &grid, int step )
{
  // create host grid part consisting of leaf level elements
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::HostGridPartType HostGridPartType;
  HostGridPartType hostGridPart( grid );

  // create filters
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FilterType FilterType;
  typename FilterType::GlobalCoordinateType center( 0.5 );
  // dummey filter just captures whole of mesh given in
  typename FilterType::ctype radias( 10.0 );
  FilterType filter( hostGridPart, center, radias, true );
  // true filter to generate mesh to be used in glue object generation for overlapping case
  // typename FilterType::ctype radius( -SliceStore::sliceHome() );
  // FilterType Filter( hostGridPart, center, radius, true );

  // we want to solve the problem on the leaf elements of the grid
  typedef typename Dune::FemFemCoupling::Glue< HGridType,false >::GridPartType GridPartType;
  GridPartType gridPart( hostGridPart, filter );
  // GridPartType GridPart( hostGridPart, Filter );

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceType FunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< FunctionSpaceType, GridPartType, FunctionSpaceType> ModelType;

  typedef typename ModelType::ProblemType ProblemType ;
  const ProblemType& problem = BulkProblem< FunctionSpaceType > ();

  // implicit model for left hand side
  ModelType implicitModel( problem, gridPart );
  // ModelType ImplicitModel( problem, GridPart );

  // create adaptive scheme
  typedef FemScheme< ModelType, istl > SchemeType;
  SchemeType scheme( gridPart, implicitModel,"bulk" );

  // create dune grid file with surface mesh
  typedef typename Dune::FemFemCoupling::Glue< HGridType,false >::GrydType GrydType;
  // create dune grid file with surface mesh
  GrydType* grydPtr = scheme.extractSurface();
  GrydType& gryd = *grydPtr ;

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<GrydType> SurfLoadBalancer;
  SurfLoadBalancer sldb(gryd);

  // do initial load balance
  if ( sldb.repartition() )
    gryd.repartition( sldb );
  else
  gryd.loadBalance();

  typedef typename Dune::FemFemCoupling::Glue< HGridType,false >::GrydPartType GrydPartType;
  GrydPartType grydPart(gryd);

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType,false>::FunctionSpaceOutsideType ExtractedSurfaceFunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< ExtractedSurfaceFunctionSpaceType, GrydPartType, ExtractedSurfaceFunctionSpaceType > ExtractedSurfaceModelType;

  typedef typename ExtractedSurfaceModelType::ProblemType ExtractedSurfaceProblemType ;
  const ExtractedSurfaceProblemType& problim = SurfaceProblem< ExtractedSurfaceFunctionSpaceType > ();

  ExtractedSurfaceModelType implycitModel( problim, grydPart );

  typedef FemScheme< ExtractedSurfaceModelType, istl > ExtractedSurfaceSchemeType;
  ExtractedSurfaceSchemeType schyme( grydPart, implycitModel,"surface" );

  // SchemeType scheme( gridPart, implicitModel );
  // int ghg = 0;
  // std::cin >> ghg;

  typedef typename ModelType::ExactSolutionType GridExactSolutionType;
  typedef typename ExtractedSurfaceModelType::ExactSolutionType GrydExactSolutionType;
  GridExactSolutionType gridExactSolution = implicitModel.exactSolution();
  GrydExactSolutionType grydExactSolution = implycitModel.exactSolution();
  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *,
                          decltype(gridExactSolution) * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step ) );

  typedef Dune::tuple< const typename ExtractedSurfaceSchemeType::DiscreteFunctionType *, GrydExactSolutionType *  > SurfaceIOTupleType;
  typedef Dune::Fem::DataOutput< GrydType, SurfaceIOTupleType > SurfaceDataOutputType;
  SurfaceIOTupleType surfaceioTuple( &(schyme.solution()), &grydExactSolution ) ; // tuple with pointers
  SurfaceDataOutputType surfacedataOutput( gryd, surfaceioTuple, DataOutputParameters( step, "surface" ) );

  JacobiCouplingScheme<decltype(scheme), decltype(schyme)> ffScheme( scheme, schyme );

  // set up a timer for the looping
  time_t before;
  time(&before);

  // solve once
  ffScheme.solve();

  // determine how long it took to do the loop
  time_t after;
  time(&after);

  // write initial solve
  dataOutput.write();
  surfacedataOutput.write();

  double seconds = difftime(after,before);

  std::cout << "Converged at iteration " << ffScheme.n() << " to an error of " << ffScheme.error() << " after a loop time " << seconds << " secs." << std::endl;

  return ffScheme.error();
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType& grid = *gridPtr ;

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<HGridType> BulkLoadBalancer;
  BulkLoadBalancer bldb(grid);

  // do initial load balance
  if ( bldb.repartition() )
    grid.repartition( bldb );
  else
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "poisson.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "poisson.repeats", 0 );

  // calculate first step
  double oldError = algorithm( grid, (repeats > 0) ? 0 : -1 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    const double newError = algorithm( grid, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }
  std::cout << std::endl;
  std::cout << "Don't worry about the following abort, it is just to avoid bempp mega-dump at end of execution." << std::endl;
  std::abort();
  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
