/**************************************************************************

  The dune-fem module is a module of DUNE (see www.dune-project.org).
  It is based on the dune-grid interface library
  extending the grid interface by a number of discretization algorithms
  for solving non-linear systems of partial differential equations.

  Copyright (C) 2003 - 2014 Robert Kloefkorn
  Copyright (C) 2003 - 2010 Mario Ohlberger
  Copyright (C) 2004 - 2014 Andreas Dedner
  Copyright (C) 2005        Adrian Burri
  Copyright (C) 2005 - 2014 Mirko Kraenkel
  Copyright (C) 2006 - 2014 Christoph Gersbacher
  Copyright (C) 2006 - 2014 Martin Nolte
  Copyright (C) 2011 - 2014 Tobias Malkmus
  Copyright (C) 2012 - 2014 Stefan Girke
  Copyright (C) 2013 - 2014 Claus-Justus Heine
  Copyright (C) 2013 - 2014 Janick Gerstenberger
  Copyright (C) 2013        Sven Kaulman
  Copyright (C) 2013        Tom Ranner


  The dune-fem module is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of
  the License, or (at your option) any later version.

  The dune-fem module is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

**************************************************************************/
#include <config.h>

// iostream includes
#include <iostream>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

/** loadbalancing scheme **/
#include <dune/fem_bem_toolbox/load_balancers/loadbalance_simple.hh>

// include header of elliptic solver
#include "femscheme.hh"
#include "poisson.hh"

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( HGridType &grid, int step )
{
  // we want to solve the problem on the leaf elements of the grid
  typedef Dune::Fem::AdaptiveLeafGridPart< HGridType, Dune::InteriorBorder_Partition > GridPartType;
  GridPartType gridPart(grid);

  // use a scalar function space
  typedef Dune::Fem::FunctionSpace< double, double,
              HGridType::dimensionworld, 1 > FunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< FunctionSpaceType, GridPartType > ModelType;

  typedef typename ModelType::ProblemType ProblemType ;
  ProblemType* problemPtr = 0 ;
  const std::string problemNames [] = { "cos", "sphere", "sin", "corner", "curvedridges" };
  const int problemNumber = Dune::Fem::Parameter::getEnum("poisson.problem", problemNames, 0 );
  switch ( problemNumber )
  {
    case 0:
      problemPtr = new CosinusProduct< FunctionSpaceType > ();
      break ;
    case 1:
      problemPtr = new SphereProblem< FunctionSpaceType > ();
      break ;
    case 2:
      problemPtr = new SinusProduct< FunctionSpaceType > ();
      break ;
    case 3:
      problemPtr = new ReentrantCorner< FunctionSpaceType > ();
      break ;
    case 4:
      problemPtr = new CurvedRidges< FunctionSpaceType > ();
      break ;
    default:
      problemPtr = new CosinusProduct< FunctionSpaceType > ();
  }
  assert( problemPtr );
  ProblemType& problem = *problemPtr ;

  // implicit model for left hand side
  ModelType implicitModel( problem, gridPart );

  // poisson solver
  typedef FemScheme< ModelType > SchemeType;
  SchemeType scheme( gridPart, implicitModel );

  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GridPartType > GridExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, gridPart, 5 );
  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *, GridExactSolutionType * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step ) );

  // set up a timer for the looping
  time_t before;
  time(&before);
  // clock_t timer = clock();

  // setup the right hand side
  scheme.prepare();
  // solve once
  scheme.solve( true );

      // determine how long it took to do the loop
      time_t after;
      time(&after);
      // timer = clock() - timer;

      double seconds = difftime(after,before);

  // write initial solve
  dataOutput.write();

  // calculate error
  // double error = 0 ;


    // calculate standard error
    // select norm for error computation
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    NormType norm( gridPart );
    double error = norm.distance( gridExactSolution, scheme.solution() );


      if( Dune::Fem::MPIManager::rank() == 0 )
  std::cout << "Converged at iteration " << 0 << " to an error of " << error << " after a loop time " << seconds << " secs." << std::endl;

  return error ;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType& grid = *gridPtr ;

  // create special load balancer for 12 node case
  typedef SimpleLoadBalanceHandle<HGridType> BulkLoadBalancer;
  BulkLoadBalancer bldb(grid);

  // do initial load balance
  if ( bldb.repartition() )
    grid.repartition( bldb );
  else
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "poisson.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "poisson.repeats", 0 );

  // calculate first step
  double oldError = algorithm( grid, (repeats > 0) ? 0 : -1 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    const double newError = algorithm( grid, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }
  std::cout << std::endl;
  std::cout << "Don't worry about the following abort, it is just to avoid bempp mega-dump at end of execution." << std::endl;
  std::abort();
  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
