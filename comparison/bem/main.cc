// Copyright (C) 2011 by the BEM++ Authors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// #define USE_SMP_PARALLEL
#define DUNE_DEVEL_MODE

#include <config.h>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#include <dune/foamgrid/foamgrid.hh>

#include "bempp/assembly/assembly_options.hpp"
#include "bempp/assembly/boundary_operator.hpp"
#include "bempp/assembly/context.hpp"
#include "bempp/assembly/evaluation_options.hpp"
#include "bempp/assembly/grid_function.hpp"
#include "bempp/assembly/l2_norm.hpp"
#include "bempp/assembly/numerical_quadrature_strategy.hpp"
#include "bempp/assembly/surface_normal_and_domain_index_dependent_function.hpp"
#include "bempp/assembly/discrete_boundary_operator.hpp"
#include "bempp/common/global_parameters.hpp"

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "bempp/assembly/identity_operator.hpp"
#include "bempp/assembly/laplace_3d_single_layer_boundary_operator.hpp"
#include "bempp/assembly/laplace_3d_double_layer_boundary_operator.hpp"
#include "bempp/assembly/laplace_3d_single_layer_potential_operator.hpp"
#include "bempp/assembly/laplace_3d_double_layer_potential_operator.hpp"

#include "bempp/common/boost_make_shared_fwd.hpp"

#include "bempp/grid/grid.hpp"
#include "bempp/grid/concrete_grid.hpp"
#include "bempp/grid/grid_factory.hpp"

#include "bempp/space/piecewise_linear_continuous_scalar_space.hpp"
#include "bempp/space/piecewise_constant_scalar_space.hpp"


#include "bempp/common/eigen_support.hpp"

#include <dune/fem_bem_toolbox/shared_memory_vectors/eigenvector.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <iostream>
#include <fstream>

typedef double BFT; // basis function type
typedef std::complex<double> RT; // result type (type used to represent discrete operators)
// typedef double RT; // result type (type used to represent discrete operators)
typedef double CT; // coordinate type

class DirichletData
{
public:
    // Type of the function's values (e.g. float or std::complex<double>)
    typedef RT ValueType;
    // Type of coordinates (must be the "real part" of ValueType)
    typedef CT CoordinateType;

    // Number of components of the function's argument
    int argumentDimension() const { return 3; }
    // Number of components of the function's value
    int resultDimension() const { return 1; }

    DirichletData()
      : doit_( Dune::Fem::MPIManager::rank() == 0 )
    {}

    // Evaluate the function at the point "point" and store result in
    // the array "result"
    inline void evaluate(const Eigen::Ref<Bempp::Vector<CoordinateType>>& point,
                         const Eigen::Ref<Bempp::Vector<CoordinateType>>& normal,
                         int domainIndex,
                         Eigen::Ref<Bempp::Vector<ValueType>> result) const {
        CoordinateType x = point(0), y = point(1), z = point(2);
        CoordinateType r = sqrt(point(0) * point(0) +
                point(1) * point(1) +
                point(2) * point(2));
        if( doit_ )
        result(0) = 2 * x * z / (r * r * r * r * r) - y / (r * r * r);
        else
	  result(0) = 0;
    }
private:
  bool doit_;
};

class ExactNeumannData
{
public:
    // Type of the function's values (e.g. float or std::complex<double>)
    typedef RT ValueType;
    // Type of coordinates (must be the "real part" of ValueType)
    typedef CT CoordinateType;

    // Number of components of the function's argument
    int argumentDimension() const { return 3; }
    // Number of components of the function's value
    int resultDimension() const { return 1; }

    // Evaluate the function at the point "point" and store result in
    // the array "result"
    inline void evaluate(const Eigen::Ref<Bempp::Vector<CoordinateType>>& point,
                         const Eigen::Ref<Bempp::Vector<CoordinateType>>& normal,
                         int domainIndex,
                         Eigen::Ref<Bempp::Vector<ValueType>> result) const {
        CoordinateType x = point(0), y = point(1), z = point(2);
        CoordinateType r = sqrt(point(0) * point(0) +
                point(1) * point(1) +
                point(2) * point(2));
        result(0) = -6 * x * z / (r * r * r * r * r * r) + 2 * y / (r * r * r * r);
    }
};

int main( int argc, char **argv )
{
  try
  {
#if 0
        int provided;
        int is_initialized = MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided );
        if( provided != MPI_THREAD_FUNNELED )
        {
          if( provided == MPI_THREAD_SINGLE )
            std::cout << "MPI thread support = single (instead of funneled)!" << std::endl;
          else
            std::cout << "WARNING: MPI thread support = " << provided << " != MPI_THREAD_FUNNELED " << MPI_THREAD_FUNNELED << std::endl;
       } else
         std::cout << "is funneled" << std::endl;
#endif
    // initialize MPI, if necessary
    Dune::Fem::MPIManager::initialize( argc, argv );

    // Import symbols from namespace Bempp to the global namespace

    using namespace Bempp;

    // Load mesh

    GridParameters params;
    params.topology = GridParameters::TRIANGULAR;
    typedef Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> GridType;
    // typedef Dune::FoamGrid<2,3> GridType;
    // const char* meshFile = "meshes/sphere-h-0.2.msh";
    const char* meshFile = "meshes/sphere-h-0.2.msh";
#if 0 // ORIGINAL
    shared_ptr<Grid> grid = GridFactory::importGmshGrid(params, meshFile);
#else
    std::unique_ptr< GridType > dunegridptr( Dune::GmshReader<GridType>::read( meshFile, false, false ) );
    // Dune::GridPtr<GridType> dunegridptr("../droplet/source/surface.dgf", Dune::MPIHelper::getLocalCommunicator());
    GridType &dunegrid = *dunegridptr;
    typedef typename GridType::LeafGridView GV;
    const GV &gv = dunegrid.leafGridView();
    Dune::GmshWriter<GV> writer( gv );
    writer.write("surface");
    shared_ptr<Grid> grid ( new ConcreteGrid<GridType>(&dunegrid,GridParameters::TRIANGULAR,false) );
#endif

    // Initialize the spaces

    PiecewiseLinearContinuousScalarSpace<BFT> pwiseLinears(grid);
    PiecewiseConstantScalarSpace<BFT> pwiseConstants(grid);

    // Define the Context object from default parameters

    Context<BFT, RT> context(GlobalParameters::parameterList());

    // create a shared memory variable
    typedef Dune::Fem::EigenVector<RT> DofVectorType;
    // get number of grid elements
    unsigned int size = gv.indexSet().size(0); // GridType::dimension);
    std::cout << "Number of elements is " << size << std::endl;
    DofVectorType test(size,true);

  // set up a timer for the looping
    time_t before;
    time(&before);

    // calculate the solution only on process zero
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Process zero fills the memory..." << std::endl;

    // Construct elementary operators

    BoundaryOperator<BFT, RT> slpOp =
            laplace3dSingleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants));
    BoundaryOperator<BFT, RT> dlpOp =
            laplace3dDoubleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants));
    BoundaryOperator<BFT, RT> idOp =
            identityOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants));

    // Form the right-hand side sum

    BoundaryOperator<BFT, RT> rhsOp = -0.5 * idOp + dlpOp;

    // Construct the grid function representing the (input) Dirichlet data

    GridFunction<BFT, RT> dirichletData(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                surfaceNormalAndDomainIndexDependentFunction(DirichletData()));

    // Construct the right-hand-side grid function

    GridFunction<BFT, RT> rhs = rhsOp * dirichletData;

    // Solve the dense system via Eigen

    // First get the projections onto the space of piecewise constant functions.
    Vector<RT> rhsVector = rhs.projections(make_shared_from_ref(pwiseConstants));

    // Now get the matrix

    Matrix<RT> mat = slpOp.weakForm()->asMatrix();

    // Solve the system via Eigen. We can use the LLT solver since the matrix
    // in this case is symmetric and positive definite.

    Vector<RT> solVector = mat.llt().solve(rhsVector);

    // Create a grid function from the result

    GridFunction<BFT,RT> solFun(make_shared_from_ref(context),
                                make_shared_from_ref(pwiseConstants),
                                solVector);
#if 0
      // pass the solution to the shared memory variable
      test.coefficients() = solFun.coefficients();
    }

    // sync processes
    std::cout << "Rank " << Dune::Fem::MPIManager::rank() << " is waiting ..." << std::endl;
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "Rank " << Dune::Fem::MPIManager::rank() << " is waiting ..." << std::endl;

    // output the solution only on not process zero
    if( Dune::Fem::MPIManager::rank() == 1 )
    {
      // return the solution from the shared memory variable
      auto& retrievedDofs = test.coefficients();

      GridFunction<BFT,RT> solFun(make_shared_from_ref(context),
                                  make_shared_from_ref(pwiseConstants),
                                  retrievedDofs);
#endif
      // determine how long it took to do the loop
      // timer = clock() - timer;

      time_t after;
      time(&after);

      double seconds = difftime(after,before);

    // Export solution to VTK

    // exportToVtk(solFun, VtkWriter::CELL_DATA, "Neumann_data", "solution");
    // Compare the numerical and analytical solution on the grid

    GridFunction<BFT, RT> exactSolFun(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseConstants),
                surfaceNormalAndDomainIndexDependentFunction(ExactNeumannData()));
    CT absoluteError, relativeError;
    estimateL2Error(solFun, surfaceNormalAndDomainIndexDependentFunction(ExactNeumannData()),
                *context.quadStrategy(), absoluteError, relativeError);
    std::cout << "Relative L^2 error: " << relativeError << " after a loop time " << seconds << " secs." << std::endl;
    }
  } // try  ((float)timer)/CLOCKS_PER_SEC
  catch( const Dune::Exception &exception )
  {
    std::cerr << "Error: " << exception << std::endl;
    return 1;
  }

  MPI_Barrier(MPI_COMM_WORLD);

  std::cout << std::endl;
  std::cout << "Don't worry about the following abort, it is just to avoid bempp mega-dump at end of execution." << std::endl;
  std::abort();

    /*
    // GridFunction<BFT, RT> diff = solFun - exactSolFun;
    // double relativeError = diff.L2Norm() / exactSolFun.L2Norm();
    // std::cout << "Relative L^2 error: " << relativeError << std::endl;

    // Prepare to evaluate the solution on an annulus outside the sphere

    // Create potential operators

    Laplace3dSingleLayerPotentialOperator<BFT, RT> slPotOp;
    Laplace3dDoubleLayerPotentialOperator<BFT, RT> dlPotOp;

    // Construct the array 'evaluationPoints' containing the coordinates
    // of points where the solution should be evaluated

    const int rCount = 51;
    const int thetaCount = 361;
    const CT minTheta = 0., maxTheta = 2. * M_PI;
    const CT minR = 1., maxR = 2.;
    const int dimWorld = 3;
    Bempp::Matrix<CT> evaluationPoints(dimWorld, rCount * thetaCount);
    for (int iTheta = 0; iTheta < thetaCount; ++iTheta) {
        CT theta = minTheta + (maxTheta - minTheta) *
            iTheta / (thetaCount - 1);
        for (int iR = 0; iR < rCount; ++iR) {
            CT r = minR + (maxR - minR) * iR / (rCount - 1);
            evaluationPoints(0, iR + iTheta * rCount) = r * cos(theta); // x
            evaluationPoints(1, iR + iTheta * rCount) = r * sin(theta); // y
            evaluationPoints(2, iR + iTheta * rCount) = 0.;             // z
        }
    }

    // Use the Green's representation formula to evaluate the solution

    EvaluationOptions evaluationOptions;

#if 0
    arma::Mat<RT> field =
        -slPotOp.evaluateAtPoints(solFun, evaluationPoints,
                                  quadStrategy, evaluationOptions) +
         dlPotOp.evaluateAtPoints(dirichletData, evaluationPoints,
                                  quadStrategy, evaluationOptions);
#else
    AssembledPotentialOperator<BFT, RT> slPotOpAss =
      slPotOp.assemble( make_shared_from_ref(pwiseConstants),
                        make_shared_from_ref(evaluationPoints),
                        quadStrategy, evaluationOptions );
    AssembledPotentialOperator<BFT, RT> dlPotOpAss =
      dlPotOp.assemble( make_shared_from_ref(pwiseLinears),
                        make_shared_from_ref(evaluationPoints),
                        quadStrategy, evaluationOptions );
    arma::Mat<RT> field =
        -slPotOpAss.apply( solFun ) +
         dlPotOpAss.apply( dirichletData );
#endif

    // Export the solution into text file

    auto weak_form = slpOp.weakForm();
    */
}

    /*

  // bempp(w.gridPart().grid(),u,w);
  auto& dirichletDataDofs = u.dofVector().coefficients();
  auto& neumannDataDofs = w.dofVector().coefficients();

  GridFunction<BFT, RT> dirichletDataNummeric(
              make_shared_from_ref(context),
              make_shared_from_ref(pwiseLinears),
              dirichletDataDofs);

  // First get the projections onto the space of piecewise constant functions.
  Vector<RT> rhsVector = dirichletDataNummeric.projections(make_shared_from_ref(pwiseConstants));

  // Solve the dense system via Eigen
  Vector<RT> solVector = decomp_.solve(rhsVector);

  GridFunction<BFT,RT> solution(make_shared_from_ref(context),
                                make_shared_from_ref(pwiseConstants),
                                solVector);

  // const GridFunction<BFT, RT>& solFun = solution.gridFunction();
  neumannDataDofs = solution.coefficients();

    */
