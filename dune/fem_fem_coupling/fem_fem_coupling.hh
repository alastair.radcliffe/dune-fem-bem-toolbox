#ifndef DUNE_fem_fem_coupling_hh
#define DUNE_fem_fem_coupling_hh

// add your classes here

#include <dune/fem/function/common/scalarproducts.hh>
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
#include <dune/grid-glue/merging/overlappingmerge.hh>
#else
#include <dune/grid-glue/merging/conformingmerge.hh>
#endif
#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/test/couplingtest.hh>
#include <dune/grid-glue/test/communicationtest.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <doc/grids/gridfactory/hybridtestgrids.hh>
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
#include <dune/fem/gridpart/common/gridpart2gridview.hh>
#else
#include <dune/fem/gridpart/common/gridpartview.hh>
#endif
#include <dune/grid/common/gridenums.hh>
#include <dune/common/exceptions.hh>
#include <dune/fem_fem_coupling/gridgluevtkwriter.hh>

#include <dune/fem_fem_coupling/surfacegridclass.hh>
#if BEMPP
#include <dune/fem_bem_toolbox/bem_objects/bempp.hh>
#endif
#include <fstream>
#include <iomanip>
#include <sstream>
#if BEMPP
  using namespace Bempp;
#endif

namespace Dune {

template<bool>
struct couplingDofNeumannTreatment
{
  template< class EntityType, class RangeType >
  static void meshQualForcing(const EntityType &entity, std::vector<RangeType> &qual)
  {
    // mesh quality forcing factor
    double factor = Dune::Fem::Parameter::getValue< double >( "quality.factor", 0 );
    double offset = Dune::Fem::Parameter::getValue< double >( "quality.offset", 0 );

    // determine number of corners of element
    int numcorn = entity.geometry().corners();

    // should be three
    if( numcorn != 3 )
    {
      std::cout << "numcorn = " << numcorn << std::endl;
      DUNE_THROW(Dune::Exception, "Bad number of corners for mesh quality forcing.");
    }

    // typedef typename EntityType::Geometry

    // corner-to-centroid and edge vectors and their lengths
    std::vector<RangeType> troi(3), edge(3);
    std::vector<double> lin(3), len(3);

    auto centroid = entity.geometry().center();

    for (int i = 0; i < numcorn; ++i)
    {
      // coordinates of corner
      auto coordcor = entity.geometry().corner(i);

      // coordinates of end-points of corresponding edge
      auto coordone = entity.geometry().corner((i+1)%3);
      auto coordtwo = entity.geometry().corner((i+2)%3);

      // edge labelled by opposite corner
      for (int j = 0; j < 3; ++j)
      {
        troi[i][j] = coordcor[j] - centroid[j];
        edge[i][j] = coordtwo[j] - coordone[j];
      }
      lin[i] = troi[i].two_norm();
      len[i] = edge[i].two_norm();
    }
    // maximum troi + edge lengths
    double maxlin = std::max(lin[0],std::max(lin[1],lin[2]));
    double maxlen = std::max(len[0],std::max(len[1],len[2]));

    // loop the three corners of the element
    for (int i = 0; i < numcorn; ++i)
    {
      qual[i] = 0;
      // loop the troi (j=0) and then the two edges (j=1,2) attached to each corner
      for (int j = 0; j < 3; ++j)
      {
        int k = (i+j)%3;
        // mesh quality force magnitude for that edge
        double mag = -1.0;
        if( j == 0 )
          mag += std::max( 1.0, offset * maxlin / lin[i] );
        else
          mag += std::max( 1.0, offset * maxlen / len[k] );
        // for normalization of the direction vector used next
        if( j == 0 )
          mag *= factor / lin[i];
        else
          mag *= factor / len[k];
        // add this force directed in the edge direction
        RangeType bit = RangeType( 0 );
        if( j == 0 )
          bit += troi[i];
        else if( j == 1 )
          bit += edge[k];
        else
          bit -= edge[k];
        bit *= mag;
        qual[i] += bit;
      }
    }
    /*
    std::cout << "===================================================================================" << std::endl;
    for (int i = 0; i < numcorn; ++i)
    {
      std::cout << len[i] << "    |    " << edge[i] << std::endl;
    }
    std::cout << "  " << std::endl;
    for (int i = 0; i < numcorn; ++i)
    {
      std::cout << qual[i] << std::endl;
    }
    */
  }

  //! set the rhs values to include the boundary integral of the Neumann condition for the coupling
  template< class IntersectionIt, class GridFunctionType, class OtherGridFunctionType, class DiscreteFunctionType >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              const OtherGridFunctionType& v,
                              DiscreteFunctionType &w,
                              bool robin, bool flip )
  {
#if 1 // !DROP
    auto entityPointer = rIIt->inside();
    auto entytyPointer = rIIt->outside();

    auto &entity = *entityPointer;
    auto &entyty = *entytyPointer;

    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteSpaceType;
    typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename DiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    // get local functions of result
    LocalFunctionType wLocal = w.localFunction( entity );

    // get local functions of argument
    auto uLocal = u.localFunction( entity );
    auto vLocal = v.localFunction( entyty );

    const LagrangePointSetType &lagrangePointSet = w.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    typedef typename GridFunctionType :: RangeType RangeAType;
    typedef typename OtherGridFunctionType :: RangeType RangeBType;

    // Schwarz Neumann parameter
    const RangeAType alpha = RangeAType( Dune::Fem::Parameter::getValue< double >( "coupling.alpha" ) );
    const RangeAType beta  = RangeAType( Dune::Fem::Parameter::getValue< double >( "coupling.beta" ) );
    const RangeAType gamma = RangeAType( Dune::Fem::Parameter::getValue< double >( "coupling.gamma" ) );
    const RangeAType unity = RangeAType( 1.0 );
    const RangeAType maxbeta = RangeAType( std::max( Dune::Fem::Parameter::getValue< double >( "coupling.beta" ) , 0.0 ) );
    const RangeAType absbeta = RangeAType( std::abs( Dune::Fem::Parameter::getValue< double >( "coupling.beta" )       ) );
    const RangeAType maxgamma = RangeAType( std::max( Dune::Fem::Parameter::getValue< double >( "coupling.gamma" ) , 0.0 ) );
    const RangeAType absgamma = RangeAType( std::abs( Dune::Fem::Parameter::getValue< double >( "coupling.gamma" )       ) );

    int localDof = 0;
    const unsigned int localBlockSize = w.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    w.space().blockMapper().map(entity,globalBlockDofs);

    typedef typename IntersectionIt::value_type Intersection;

    // Dimension of the intersection
    const int dim = Intersection::mydim;

    // Dimension of world coordinates
    const int coorddim = Intersection::coorddim;

    // Create a set of quadrature points
    const Dune::QuadratureRule<double, dim>& quad = Dune::QuadratureRules<double, dim>::rule(rIIt->type(), w.space().order());

    const int phidim = RangeAType::dimension;
    const int Psidim = RangeBType::dimension;
    RangeAType phi( 0 ), Phi( 0 );
    RangeBType psi( 0 ), Psi( 0 );

    typedef typename GridFunctionType :: JacobianRangeType JacobianRangeAType;
    typedef typename OtherGridFunctionType :: JacobianRangeType JacobianRangeBType;
    JacobianRangeAType dphi;
    JacobianRangeBType dpsi;

    // iterate over quadrature points
    for (unsigned int l=0; l<quad.size(); l++)
    {
      // quadrature node position
      Dune::FieldVector<double, dim> quadPos = quad[l].position();

      // home and away solutions (just for debugging)
      uLocal.evaluate( rIIt->geometryInInside().global(quadPos), Phi );
      vLocal.evaluate( rIIt->geometryInOutside().global(quadPos), Psi );

      // home and away derivatives at the quadrature point
      uLocal.jacobian( rIIt->geometryInInside().global(quadPos), dphi );
      vLocal.jacobian( rIIt->geometryInOutside().global(quadPos), dpsi );

      // check normal points as expected (for debugging only)
      auto chknorm = rIIt->centerUnitOuterNormal();

      assert( std::abs( rIIt->centerUnitOuterNormal().two_norm() - 1.) < 1e-8 );

      // dot product of jacobian with unit normal to coupling (glue) surface
      dphi.mv(rIIt->centerUnitOuterNormal(), phi);
      dpsi.mv(rIIt->centerUnitOuterNormal(), psi);

      // combine the two home and away solutions via the Schwarz Neumann parameter to form the new rhs value
      if ( robin )
      {
#if !DROP
	/*
        auto bob = entity.geometry().global( rIIt->geometryInInside().center() );
        auto bub = entyty.geometry().global( rIIt->geometryInOutside().center() );

        if(std::abs(bob[0])<1.0e-6) bob[0]=0;
        if(std::abs(bob[1])<1.0e-6) bob[1]=0;
        if(std::abs(bob[2])<1.0e-6) bob[2]=0;

        if(std::abs(bub[0])<1.0e-6) bub[0]=0;
        if(std::abs(bub[1])<1.0e-6) bub[1]=0;
        if(std::abs(bub[2])<1.0e-6) bub[2]=0;

	std::cout << "NNN " << flip << " (  " << bob[0] << "  " << bob[1] << "  " << bob[2] << " ) " << " (  " << bub[0] << "  " << bub[1] << "  " << bub[2] << " ) " << phi << "  " << psi << "  " << Phi << "  " << Psi << std::endl;
	*/
        // if Robin coupling, beta always pulls in the "away" solution
        if ( flip )
        {
          phi = - ( maxbeta * psi + ( unity - absbeta ) * phi ) + ( unity - maxgamma ) * ( alpha * Psi + ( unity - alpha ) * Phi );
        }
        else
        {
          phi =  ( maxbeta * psi + ( unity - absbeta ) * phi ) +  absgamma * ( alpha * Psi + ( unity - alpha ) * Phi );
        }
#endif
      }
      else
      {
#if BEMPP && !DROP
        if ( !flip )
        {
          // Bempp returns the Neumann derivative in the (away) solution vector directly - no need to calculate jacobian derivatives to get the Neumann info.
          // std::cout << "hererere   " << beta << " # " << phi << " # " << Psi << std::endl;
          phi = beta*phi + (unity-beta)*Psi;
        }
        else
        {
          std::cout << "Shouldn't be in couplingDofNeumannTreatment for flip side of bempp example" << std::endl;
          abort();
        }
#elif DROP
	/*
        // (Neumann) forcing in rhs of drop system held in undifferentiated away solution
        double meancurv = 0.0;
        double direction = 0.0;
        // determine estimate of mean curvature from size of forcing
        for( int i = 0; i < Psidim; ++i )
	{
          meancurv += Psi[ i ] * Psi[ i ];
          direction += Psi[ i ] * chknorm[ i ];
        }
        double sign = 1.0;
        if( direction < 0.0 ) sign = -1.0;
        meancurv = std::sqrt( meancurv );

        auto bob = entity.geometry().global( rIIt->geometryInInside().center() );
        auto bub = entyty.geometry().global( rIIt->geometryInOutside().center() );

        if(std::abs(bob[0])<1.0e-6) bob[0]=0;
        if(std::abs(bob[1])<1.0e-6) bob[1]=0;
        if(std::abs(bob[2])<1.0e-6) bob[2]=0;

        if(std::abs(bub[0])<1.0e-6) bub[0]=0;
        if(std::abs(bub[1])<1.0e-6) bub[1]=0;
        if(std::abs(bub[2])<1.0e-6) bub[2]=0;

	std::cout << "NNN " << flip << " (  " << bob[0] << "  " << bob[1] << "  " << bob[2] << " ) " << " (  " << bub[0] << "  " << bub[1] << "  " << bub[2] << " ) " << Psi << std::endl;
	*/
#if 0
        // set-up vector to hold grid quality forcing
	std::vector<RangeBType> qualityForcing(3);

	// determine the mesh quality diffusion forcing
        meshQualForcing(entyty,qualityForcing);

	/*
        RangeAType phi( 0 );
        RangeBType psi( 0 );

        // home solution at the Lagrange point
        uLocal.evaluate( lagrangePointSet[ localBlock ], phi );
        // away solution at the corresponding away Lagrange point
        auto awayPoint =
          rIIt->geometryInOutside().global(
            rIIt->geometryInInside().local(
	      Dune::Fem::coordinate(lagrangePointSet[ localBlock ])
            )
          );
        vLocal.evaluate( awayPoint, psi );
	*/

        //  loop the three corners of the surface (away) entyty
        for( int i = 0; i < 3; ++i )
	{
          // determine local coordinate of corresponding corner of volume (home) entity
          auto homeCorner =
            rIIt->geometryInInside().global(
              rIIt->geometryInOutside().local(
	        entyty.geometry().local(
	          entyty.geometry().corner(i)
		)
              )
            );
          /*
          Dune::FieldVector<double, 3> test;
          for( int j = 0; j < 3; ++j ) test[j] = qualityForcing[i][j];

	  std::cout << "MQF " << chknorm.dot( test ) << "    |    " << qualityForcing[i] << "    |    " << test << std::endl;
          */
          // put result into rhs
          wLocal.axpy( homeCorner, qualityForcing[i] );
        }
#endif
        // now direct this force in the normal direction
        for( int i = 0; i < phidim; ++i )
          phi[ i ] = Psi[ i ];
#else
        if ( flip )
        {
          phi = - ( beta * psi + ( unity - beta ) * phi );
        }
        else
        {
          phi =  ( beta * phi + ( unity - beta ) * psi );
        }
#endif
      }
#if FEM_FEM
      Dune::FieldVector<double, Intersection::InsideGridView::dimensionworld> localGrid0Pos =
      rIIt->inside()->geometry().global(rIIt->geometryInInside().center());

      Dune::FieldVector<double, Intersection::OutsideGridView::dimensionworld> localGrid1Pos =
      rIIt->outside()->geometry().global(rIIt->geometryInOutside().center());

      if ( (localGrid0Pos-localGrid1Pos).two_norm() >= 1e-6 )
      {
        std::cout << localGrid0Pos[0] << "  " << localGrid0Pos[1] << "  " << localGrid0Pos[2] << std::endl;
        std::cout << localGrid1Pos[0] << "  " << localGrid1Pos[1] << "  " << localGrid1Pos[2] << std::endl;
        DUNE_THROW(Dune::Exception, "Bad center matching (3).");
      }
      // std::cout << flip_ << "=== " << " " << localGrid0Pos[0] << "  " << localGrid0Pos[1] << "  " << localGrid0Pos[2] << "  " << phi << std::endl;
#endif
      // std::cout << "AAA  " << phi << "     " <<  rIIt->geometry().volume() << "     " <<  (rIIt->geometry().center()).two_norm() << std::endl;
      // multiply through with "volume" of glue object on which the surface integration was performed
      phi *= rIIt->geometry().integrationElement(quadPos) * quad[l].weight();

      // store result in rhs
      wLocal.axpy( rIIt->geometryInInside().global(quadPos), phi );
    }
#endif
  }
};

template<bool>
struct couplingDofIncidentTreatment
{
  //! set the rhs values to include the boundary integral of the Neumann condition for the coupling
  template< class IntersectionIt, class GridFunctionType, class DiscreteFunctionType >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              DiscreteFunctionType &w )
  {
    auto entityPointer = rIIt->inside();

    auto &entity = *entityPointer;

    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteSpaceType;
    typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename DiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    // get local functions of result
    LocalFunctionType wLocal = w.localFunction( entity );

    // get local functions of argument
    auto uLocal = u.localFunction( entity );

    const LagrangePointSetType &lagrangePointSet = w.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    typedef typename GridFunctionType :: RangeType RangeType;

    int localDof = 0;
    const unsigned int localBlockSize = w.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    w.space().blockMapper().map(entity,globalBlockDofs);

    typedef typename IntersectionIt::value_type Intersection;

    // Dimension of the intersection
    const int dim = Intersection::mydim;

    // Dimension of world coordinates
    const int coorddim = Intersection::coorddim;

    // Create a set of quadrature points
    // const Dune::QuadratureRule<double, dim>& quad = Dune::QuadratureRules<double, dim>::rule(rIIt->type(), 3);

    const int phidim = RangeType::dimension;

    RangeType phi( 0 );

    typedef typename GridFunctionType :: JacobianRangeType JacobianRangeType;

    JacobianRangeType dphi;

    // iterate over quadrature points
    // for (unsigned int l=0; l<quad.size(); l++)
    {
      // home and away derivatives at the quadrature point
      uLocal.jacobian( rIIt->geometryInInside().center(), dphi );

      // check normal points as expected (for debugging only)
      auto chknorm = rIIt->centerUnitOuterNormal();

      assert( std::abs( rIIt->centerUnitOuterNormal().two_norm() - 1.) < 1e-8 );

      // dot product of jacobian with unit normal to coupling (glue) surface
      dphi.mv(rIIt->centerUnitOuterNormal(), phi);

      // multiply through with "volume" of glue object on which the surface integration was performed
      phi *= -rIIt->geometry().volume();

      // store result in rhs
      wLocal.axpy( rIIt->geometryInInside().center(), phi );
    }
  }
};

template<bool>
struct updateDirichletDofs
{


template <class DiscreteFunctionSpaceType>
class DirichletBuilder
    : public CommDataHandleIF< DirichletBuilder<DiscreteFunctionSpaceType>, int >
{
public:
  typedef DiscreteFunctionSpaceType SpaceType;
  typedef typename SpaceType::BlockMapperType MapperType;

  enum { nCodim = SpaceType :: GridType :: dimension + 1 };

public:
  typedef int DataType;

  const int myRank_;
  const int mySize_;

  typedef std::vector< short int > DirichletBlocksType;
  DirichletBlocksType &dirichletBlocks_;

  const SpaceType &space_;
  const MapperType &mapper_;

public:
  DirichletBuilder( DirichletBlocksType &dirichletBlocks,
                    const SpaceType &space,
                    const MapperType &mapper,
                    const bool &binaryBlocks )
  : myRank_( space.gridPart().comm().rank() ),
    mySize_( space.gridPart().comm().size() ),
    dirichletBlocks_( dirichletBlocks ),
    space_( space ),
    binaryBlocks_( binaryBlocks ),
    mapper_( mapper )
  {
  }
  bool contains ( int dim, int codim ) const
  {
    return mapper_.contains( codim );
  }

  bool fixedsize ( int dim, int codim ) const
  {
    return false;
  }

  //! read buffer and apply operation
  template< class MessageBuffer, class Entity >
  inline void gather ( MessageBuffer &buffer,
                       const Entity &entity ) const
  {
    unsigned int localBlocks = mapper_.numEntityDofs( entity );
    std::vector<std::size_t> globalBlockDofs(localBlocks);
    mapper_.mapEntityDofs( entity, globalBlockDofs );
    for( unsigned int localBlockDof = 0 ; localBlockDof < localBlocks; ++ localBlockDof )
    {
      if ( dirichletBlocks_[ globalBlockDofs[localBlockDof] ] != 0 )
        buffer.write( 1 );
      else
        buffer.write( 0 );
    }
  }

  //! read buffer and apply operation
  //! scatter is called for one every entity
  //! several times depending on how much data
  //! was gathered
  template< class MessageBuffer, class EntityType >
  inline void scatter ( MessageBuffer &buffer,
                        const EntityType &entity,
                        size_t n )
  {
    unsigned int localBlocks = mapper_.numEntityDofs( entity );
    std::vector<std::size_t> globalBlockDofs(localBlocks);
    mapper_.mapEntityDofs( entity, globalBlockDofs );
    assert( n == globalBlockDofs.size() );
    assert( n == size(entity) );
    for( unsigned int localBlock = 0 ; localBlock < localBlocks; ++ localBlock )
    {
      int val;
      buffer.read(val);
      if (val == 1)
      {
        if( binaryBlocks_ )
	{
          dirichletBlocks_[ globalBlockDofs[localBlock] ] = 1;
        }
        else if ( dirichletBlocks_[ globalBlockDofs[localBlock] ] > 0)
	{
          // flag original Dirichlet status
          dirichletBlocks_[ globalBlockDofs[localBlock] ] += 1;
        }
        else
	{
          // flag acquired Dirichlet status
          dirichletBlocks_[ globalBlockDofs[localBlock] ] -= 1;
        }
      }
    }
  }

  //! return local dof size to be communicated
  template< class Entity >
  size_t size ( const Entity &entity ) const
  {
    return mapper_.numEntityDofs( entity );
  }
private:
  bool binaryBlocks_;
};



  // detect all DoFs on the Dirichlet boundary
  template< class ModelType, class DiscreteFunctionSpaceType >
  static void apply( const ModelType &model,
		     DiscreteFunctionSpaceType &wspace, std::vector< short int > &dirichletBlocks, std::vector< short int > &couplingBlocks, int &sequence, bool &hasDirichletDofs, bool &dirneu )
  {
#if 1 // FEM_FEM || DROP || BEMPP
    if( sequence != wspace.sequence() )
    {
      // only start search if Dirichlet boundary is present
      if( ! model.hasDirichletBoundary() )
      {
        hasDirichletDofs = false ;
        return ;
      }

      // resize flag vector with number of blocks and reset flags
      const int blocks = wspace.blockMapper().size() ;
      dirichletBlocks.resize( blocks );
      for( int i=0; i<blocks; ++i )
      {
        dirichletBlocks[ i ] = 0 ;
      }

      typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
      typedef typename IteratorType :: Entity EntityType;

      typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;

      bool hasDirichletBoundary = false;
      const IteratorType end = wspace.end();
      for( IteratorType it = wspace.begin(); it != end; ++it )
      {
        const EntityType &entity = *it;

        bool  hasBoundaryFace = false;

        const GridPartType &gridPart = wspace.gridPart();

        typedef typename GridPartType :: IntersectionIteratorType IntersectionIteratorType;

        IntersectionIteratorType itt = gridPart.ibegin( entity );
        const IntersectionIteratorType enditt = gridPart.iend( entity );
        for( ; itt != enditt; ++itt )
        {
          typedef typename IntersectionIteratorType :: Intersection IntersectionType;
          const IntersectionType& intersection = *itt;

          if( intersection.boundary() ) hasBoundaryFace = true;
	}

        // if entity has boundary intersections
#if FILTERED
        if(( entity.hasBoundaryIntersections() )||hasBoundaryFace)
#else
        if( entity.hasBoundaryIntersections() )
#endif
        {
          hasDirichletBoundary |= searchEntityDirichletDofs( entity, model, wspace, dirichletBlocks, couplingBlocks, dirneu );
        }
      }

      // update sequence number
      sequence = wspace.sequence();
      if( wspace.gridPart().comm().size() > 1 )
      {
        try
        {
#if DROP
          static const bool flip = GridPartType::dimension != GridPartType::dimensionworld;
#else
          static const bool flip = false;
#endif
	  DirichletBuilder<DiscreteFunctionSpaceType> handle( dirichletBlocks, wspace , wspace.blockMapper(), (!dirneu) || flip );
          wspace.gridPart().communicate
            ( handle, GridPartType::indexSetInterfaceType, Dune::ForwardCommunication );
        }
        // catch possible exceptions here to have a clue where it happend
        catch( const Dune::Exception &e )
        {
          std::cerr << e << std::endl;
          std::cerr << "Exception thrown in: " << __FILE__ << " line:" << __LINE__ << std::endl;
          abort();
        }
        hasDirichletDofs = wspace.gridPart().grid().comm().max( hasDirichletBoundary );
      }
      else
      {
        hasDirichletDofs = hasDirichletBoundary;
      }
    }
#endif
  }
};


  // detect all DoFs on the Dirichlet boundary of the given entity
template< class ModelType, class DiscreteFunctionSpaceType, class EntityType >
  bool searchEntityDirichletDofs( const EntityType &entity, const ModelType& model,
				  DiscreteFunctionSpaceType &wspace, std::vector< short int > &dirichletBlocks, std::vector< short int > &couplingBlocks, bool &dirneu )
  {
    typedef typename DiscreteFunctionSpaceType :: LagrangePointSetType
      LagrangePointSetType;

    typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;

    const int faceCodim = 1;
    typedef typename GridPartType :: IntersectionIteratorType
      IntersectionIteratorType;

    typedef typename LagrangePointSetType
      :: template Codim< faceCodim > :: SubEntityIteratorType
      FaceDofIteratorType;

    typedef typename DiscreteFunctionSpaceType :: DomainType DomainType ;

    const GridPartType &gridPart = wspace.gridPart();

    // default is false
    bool hasDirichletBoundary = false;

    typedef typename EntityType :: Geometry Geometry;
    const Geometry& geo = entity.geometry();

    // get Lagrange pionts from space
    const LagrangePointSetType &lagrangePointSet = wspace.lagrangePointSet( entity );

    // get number of Lagrange Points
    const unsigned int localBlocks = lagrangePointSet.size();

    //map local to global BlockDofs
    std::vector<size_t> globalBlockDofs(localBlocks);
    // space_.blockMapper().mapEntityDofs(entity,globalBlockDofs);
    wspace.blockMapper().map(entity,globalBlockDofs);

    IntersectionIteratorType it = gridPart.ibegin( entity );
    const IntersectionIteratorType endit = gridPart.iend( entity );
    for( ; it != endit; ++it )
    {
      typedef typename IntersectionIteratorType :: Intersection IntersectionType;
      const IntersectionType& intersection = *it;

      // if intersection is with boundary, adjust data
      if( intersection.boundary() )
      {
        // get face number of boundary intersection
        const int face = intersection.indexInInside();

        // get dof iterators
        FaceDofIteratorType faceIt
          = lagrangePointSet.template beginSubEntity< faceCodim >( face );
        const FaceDofIteratorType faceEndIt
          = lagrangePointSet.template endSubEntity< faceCodim >( face );
        for( ; faceIt != faceEndIt; ++faceIt )
        {
          // get local dof number (expensive operation, therefore cache result)
          const unsigned int localBlock = *faceIt;

          // get global coordinate of point on boundary
          DomainType global = geo.global( lagrangePointSet.point( localBlock ) );
	  /*
          if(std::abs(global[0])<1.0e-6) global[0]=0;
          if(std::abs(global[1])<1.0e-6) global[1]=0;
          if(std::abs(global[2])<1.0e-6) global[2]=0;
	  */
          // get dirichlet information from model
          const bool isDirichletDof = model.isDirichletPoint( global );

          // mark dof
          if( isDirichletDof )
          {
            // mark global DoF number if not a coupling dof in a Neumann iteration
            if ( dirneu || ( couplingBlocks[globalBlockDofs[ localBlock ] ] == 0 ) )
            {
              assert( globalBlockDofs[ localBlock ] < dirichletBlocks.size() );
              dirichletBlocks[globalBlockDofs[ localBlock ] ] = 1 ;

              // std::cout << "DDD " << " (  " << global[0] << "  " << global[1] << "  " << global[2] << " ) " << std::endl;

              // std::cout << "Genuine dir node   " << dirneu << "  " << global[0] << " , " << global[1] << " , " << global[2] << endl;

              // we have Dirichlet values
              hasDirichletBoundary = true ;
            }
            else if ( couplingBlocks[globalBlockDofs[ localBlock ] ] != 0 )
            {
              // std::cout << "CCC " << " (  " << global[0] << "  " << global[1] << "  " << global[2] << " ) " << std::endl;

              // std::cout << "Dir node knocked-out by couple node at  " << dirneu << "  " << global[0] << " , " << global[1] << " , " << global[2] << endl;
            }
          }
        }
      }
    }

    return hasDirichletBoundary;
  }


template<bool>
struct couplingDofDirichletTreatment
{
  //! set the rhs dirichlet point values to those required for the coupling
  template< class IntersectionIt, class GridFunctionType, class GrydFunctionType, class DiscreteFunctionType >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              const GrydFunctionType& v,
		     DiscreteFunctionType &w, std::vector< short int > &dirichletBlocks, std::vector< short int > &couplingBlocks, bool robin, bool flip, bool special )
  {
#if FEM_FEM || DROP || BEMPP
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteSpaceType;
    typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename DiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename DiscreteSpaceType :: RangeType RangeType;

    auto entityPointer = rIIt->inside();
    auto entytyPointer = rIIt->outside();

    auto &entity = *entityPointer;
    auto &entyty = *entytyPointer;

    // get local functions of result
    LocalFunctionType wLocal = w.localFunction( entity );

    // get local functions of argument
    auto uLocal = u.localFunction( entity );
    auto vLocal = v.localFunction( entyty );

    const LagrangePointSetType &lagrangePointSet = w.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    typedef typename GridFunctionType :: RangeType RangeAType;
    typedef typename GrydFunctionType :: RangeType RangeBType;

    // Schwarz Dirichlet parameter
    const RangeAType alpha = RangeType( Dune::Fem::Parameter::getValue< double >( "coupling.alpha" ) );
    const RangeAType unity = RangeType( 1.0 );

    int localDof = 0;
    const unsigned int localBlockSize = w.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    w.space().blockMapper().map(entity,globalBlockDofs);

    // home, away and glue geometries
    auto glueGeometry = rIIt->geometry();

    // iterate over face dofs and set unit row
    for( unsigned int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
      int global = globalBlockDofs[ localBlock ];
#if 0 // DROP
      if( dirichletBlocks[ global ] &&  !couplingBlocks[ global ] )
      {
	std::cout << "Bad  " << global << std::endl;
        abort();
      }
      if( couplingBlocks [ global ] && !dirichletBlocks[ global ] )
      {
	std::cout << "Bud  " << global << std::endl;
        abort();
      }
#endif
#if BEMPP || DROP
      if( ( couplingBlocks[ global ] != 0 ) || special )
#else
      if( ( dirichletBlocks[ global ] != 0 ) && ( couplingBlocks[ global ] != 0 ) )
#endif
      {
        RangeAType phi( 0 );
        RangeBType psi( 0 );

        // home solution at the Lagrange point
        uLocal.evaluate( lagrangePointSet[ localBlock ], phi );

        // avoid shared memory clashes (phi should contain values from dofrank)
        if( special && (!flip) )
	{
          int rank = Dune::Fem::MPIManager::rank();
          RangeAType ranky( rank );
          // process can only set value if it has rank equal to the corresponding dofrank
          if( (phi - ranky).two_norm() > 1.0e-6 )
          {
            // increase localDofs by block size
            localDof += localBlockSize;
            continue;
          }
        }

        // away solution at the corresponding away Lagrange point
        auto awayPoint =
          rIIt->geometryInOutside().global(
            rIIt->geometryInInside().local(
	      Dune::Fem::coordinate(lagrangePointSet[ localBlock ])
            )
          );
        vLocal.evaluate( awayPoint, psi );

        // double radius = entity.geometry().global( Dune::Fem::coordinate( lagrangePointSet[ localBlock ] ) ).two_norm();

        // combine the two solutions via the Schwarz parameter to form the new Dirichlet boundary value
        if ( robin )
	{
          // for Robin coupling alpha always pulls in the "away" solution
#if !DROP
          phi = alpha * psi + ( unity - alpha ) * phi;
#endif
	}
	else
	{
        if ( flip )
        {
#if DROP
          phi = psi;
#else
	  // std::cout << "bobobobob " << alpha << " # " << radius << "  " << psi << " # " << phi << std::endl;
          phi = alpha * psi + ( unity - alpha ) * phi;
#endif
        }
        else
        {
#if DROP
          phi = psi;
#elif BEMPP
	  // std::cout << "bobobobobo " << alpha << " # " << radius << "  " << psi << " # " << phi << std::endl;
          phi = alpha * psi + ( unity - alpha ) * phi;
#else
          phi = alpha * phi + ( unity - alpha ) * psi;
#endif
        }
	}

/*
        typedef typename IntersectionIt::value_type Intersection;

        Dune::FieldVector<double, Intersection::InsideGridView::dimensionworld> localGrid0Pos =
        rIIt->inside()->geometry().global(rIIt->geometryInInside().center());

        Dune::FieldVector<double, Intersection::OutsideGridView::dimensionworld> localGrid1Pos =
        rIIt->outside()->geometry().global(rIIt->geometryInOutside().center());
        if ( (localGrid0Pos-localGrid1Pos).two_norm() >= 1e-6 )
        {
#if !DROP
          std::cout << localGrid0Pos[0] << "  " << localGrid0Pos[1] << "  " << localGrid0Pos[2] << "  " << std::endl;
          std::cout << localGrid1Pos[0] << "  " << localGrid1Pos[1] << "  " << localGrid1Pos[2] << "  " << std::endl;
          DUNE_THROW(Dune::Exception, "Bad center matching (4).");
#endif
        }
*/
        // std::cout << flip_ << "+++ " << " " << localGrid0Pos[0] << "  " << localGrid0Pos[1] << "  " << localGrid0Pos[2] << "  " << phi << std::endl;
	/*
	auto bob = entity.geometry().global( Dune::Fem::coordinate( lagrangePointSet[ localBlock ] ) );

        if(std::abs(bob[0])<1.0e-6) bob[0]=0;
        if(std::abs(bob[1])<1.0e-6) bob[1]=0;
        if(std::abs(bob[2])<1.0e-6) bob[2]=0;

        // if(std::abs(phi[0])<1.0e-6) phi[0]=0;
        // if(std::abs(phi[1])<1.0e-6) phi[1]=0;
        // if(std::abs(phi[2])<1.0e-6) phi[2]=0;

	std::cout <<"ZZZ " << " (  " << bob[0] << "  " << bob[1] << "  " << bob[2] << " ) " << bob.two_norm() << "  " << phi << std::endl;
	*/
        // store result to dof vector
        for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
        {
          if( special )
	  {
	  // for the special case, the gluing is strictly between surfaces
          wLocal[ localDof ] = phi[ l ];
          }
          else
          {
	  // if not special, then global dofs are those pointed at by the glue iterators, which will all be
          // original coupling nodes, ie: with positive couplingBlock number
          // assert( couplingBlocks[ global ] > 0 );
          wLocal[ localDof ] = phi[ l ];
          wLocal[ localDof ] /= couplingBlocks[ global ];
          }
        }
      }
      else
      {
        // increase localDofs by block size
        localDof += localBlockSize ;
      }
    }
#endif
  }
};



template<>
struct couplingDofNeumannTreatment<false>
{
  //! set the rhs values to include the boundary integral of the Neumann condition for the coupling
  template< class IntersectionIt, class GridFunctionType, class OtherGridFunctionType, class DiscreteFunctionType >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              const OtherGridFunctionType& v,
                              DiscreteFunctionType &w,
		              bool robin, bool flip )
  {}
};

template<>
struct couplingDofIncidentTreatment<false>
{
  //! set the rhs values to include the boundary integral of the Neumann condition for the coupling
  template< class IntersectionIt, class GridFunctionType, class DiscreteFunctionType >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              DiscreteFunctionType &w )
  {}
};

template<>
struct updateDirichletDofs<false>
{
  // detect all DoFs on the Dirichlet boundary
  template< class ModelType, class DiscreteFunctionSpaceType >
  static void apply( const ModelType &model,
		     DiscreteFunctionSpaceType &wspace, std::vector< short int > &dirichletBlocks, std::vector< short int > &couplingBlocks, int &sequence, bool &hasDirichletDofs, bool &dirneu )
  {}
  class DirichletBuilder;
};

template<>
struct couplingDofDirichletTreatment<false>
{
  //! set the rhs dirichlet point values to those required for the coupling
  template< class IntersectionIt, class GridFunctionType, class GrydFunctionType, class DiscreteFunctionType >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              const GrydFunctionType& v,
                              DiscreteFunctionType &w, std::vector< short int > &dirichletBlocks, std::vector< short int > &couplingBlocks, bool robin, bool flip, bool special )
  {}
};


template<bool>
struct couplingDofRobinTreatment
{
  //! set the rhs values to include the boundary integral of the Neumann condition for the coupling
  template< class IntersectionIt, class LinearOperator >
  static void apply( const IntersectionIt &rIIt,
                     LinearOperator &linearOperator,
                     bool robin, bool flip,
                     double gamma )
  {
#if DROP
    abort();
#else
    auto entityPointer = rIIt->inside();
    auto &entity = *entityPointer;

    // lets start with a one point quadrature
    auto homeCenter = rIIt->geometryInInside().center();

    // multiply Robin parameter through with "volume" of glue object on which the surface integration will be performed
    double value = rIIt->geometry().volume();
    if ( flip )
      value *= ( 1.0 - std::max(gamma,0.0) );
    else
      value *= std::abs(gamma);

    typedef typename LinearOperator :: LocalMatrixType LocalMatrixType;

    // get local matrix from linear operator
    LocalMatrixType localMatrix = linearOperator.localMatrix( entity, entity );

    const unsigned int numDofs = localMatrix.domainSpace().blockMapper().maxNumDofs();

    typedef typename LocalMatrixType :: DomainSpaceType :: RangeType RangeType;
    std::vector< RangeType > phi( numDofs );

    auto &baseSet = localMatrix.domainBasisFunctionSet();
    const unsigned int numBaseFunctions = baseSet.size();
    baseSet.evaluateAll( homeCenter, phi );

    // add the value to the system matrix
    for( unsigned int localCol = 0; localCol < numBaseFunctions; ++localCol )
    {
      // add phi_j phi_i   for i=0,...,N-1, j=localCol
      localMatrix.column( localCol ).axpy( phi, phi[localCol], value );
    }
#endif
  }
};
template<>
struct couplingDofRobinTreatment<false>
{
  //! set the rhs values to include the boundary integral of the Neumann condition for the coupling
  template< class IntersectionIt, class LinearOperator >
  static void apply( const IntersectionIt &rIIt,
                     LinearOperator &linearOperator,
                     bool robin, bool flip,
                     double gamma )
  {
  }
};

// ***************************************************************

template <class GridView>
class VerticalFaceDescriptor
  : public Dune::GridGlue::ExtractorPredicate<GridView,1>
{
public:

  virtual bool contains(const typename GridView::Traits::template Codim<0>::Entity& eptr,
                        unsigned int face) const
  {
    return true;
  }
};

/** \brief trafo used for yaspgrids */
template<int dim, typename ctype>
class ShiftTrafo
  : public Dune::AnalyticalCoordFunction< ctype, dim, dim, ShiftTrafo<dim,ctype> >
{
  double shift;
public:
  ShiftTrafo(double x) : shift(x) {};

  //! evaluate method for global mapping
  void evaluate ( const Dune::FieldVector<ctype, dim> &x, Dune::FieldVector<ctype, dim> &y ) const
  {
    y = x;
    y[0] += shift;
  }
};

/** \brief Returns always true */
template <class GridView>
class AllElementsDescriptor
  : public Dune::GridGlue::ExtractorPredicate<GridView,0>
{
public:
  virtual bool contains(const typename GridView::Traits::template Codim<0>::Entity& element, unsigned int subentity) const
  {
    return true;
  }
};

template <class GridView, int codim>
class SurfaceCouplingDescriptor
  : public Dune::GridGlue::ExtractorPredicate<GridView,codim>
{
public:
  SurfaceCouplingDescriptor(const GridView &gridView)
    : gridView_(gridView)
  {}

  // the method for the bulk + surface grids
  virtual bool contains(const typename GridView::Traits::template Codim<0>::Entity& e,
                        unsigned int face) const
  {
    const int dim = GridView::dimension;
    const int wdim = GridView::dimensionworld;

    // surface grid elements always selected
    if (dim != wdim) return true;

      // bulk grid elements
    if ( !e.hasBoundaryIntersections() )
      return false;

    auto iter = gridView_.ibegin( e );
    auto end = gridView_.iend( e );
    for ( ; iter != end; ++iter )
      if ( iter->indexInInside() == face && iter->boundary() )
#if HOLE_HACK
        if (iter->geometry().center() * iter->centerUnitOuterNormal() > 0. )
#endif
          return true;

    return false;
  }
private:
  const GridView gridView_;
};

// a store for the CouplingConstraints Class
template < bool special >
struct CouplingConstraintsStore
{
  // get various types needed for gluing
  typedef typename Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue<GT,special> TraitsType;
  typedef typename TraitsType::GrydPartType GrydPartType;
  typedef typename TraitsType::GridPartType VolGridPartType;
  typedef typename std::conditional<special,GrydPartType,VolGridPartType>::type GridPartType;
  typedef typename TraitsType::GlueType GlueType;

  typedef typename std::conditional<special,int,GridPartType>::type JunkGridPartType;
  typedef typename std::conditional<special,int,GrydPartType>::type JunkGrydPartType;

  // doing second glue flag
  static bool doingSecondGlueValue(bool change, bool newval = false)
  {
    static bool doingSecondGlue = false;
    if ( change ) doingSecondGlue = newval;
    return doingSecondGlue;
  }

  // stores glue object's address
  static GlueType* glueHome(GlueType* storeThis = 0, bool glueTwo = false)
  {
    // initialize to no address
    static GlueType* glueAddress = 0;
    static GlueType* glu2Address = 0;
    // receiving a glue address to be stored
    if ( glueTwo )
    {
      if (storeThis != 0) glu2Address = storeThis;
      if ( glu2Address != 0 )
        return glu2Address;
      else
        return glueAddress;
    }
    else
    {
    if (storeThis != 0) glueAddress = storeThis;
    return glueAddress;
    }
  }
  // stores the address for the inside space
  static const GridPartType* gridpartInsideHome(const GridPartType* storeThis = 0)
  {
    static const GridPartType* leftAddress = 0;
    if ((storeThis != 0) && (storeThis == leftAddress)) leftAddress = 0;
    else if ((storeThis != 0) && (leftAddress == 0)) {leftAddress = storeThis; return 0;}
    return leftAddress;
  }
#if !FEM_FEM
  // dummey method needed when inside and outside spaces are different in type
  static const JunkGrydPartType* gridpartInsideHome(const JunkGrydPartType* storeThis)
  {
    return 0;
  }
#endif
  // stores the address for the outside space
#if FEM_FEM
  static const GridPartType* gridPartOutsideHome(const GridPartType* storeThis = 0)
  {
    static const GridPartType* rightAddress = 0;
    if ((storeThis != 0) && (storeThis == rightAddress)) rightAddress = 0;
    else if ((storeThis != 0) && (rightAddress == 0)) {rightAddress = storeThis; return 0;}
    return rightAddress;
  }
#else
  static const GrydPartType* gridPartOutsideHome(const GrydPartType* storeThis = 0)
  {
    static const GrydPartType* rightAddress = 0;
    if ((storeThis != 0) && (storeThis == rightAddress)) rightAddress = 0;
    else if (storeThis != 0) rightAddress = storeThis;
    return rightAddress;
  }
  // dummey
  static const JunkGridPartType* gridPartOutsideHome(const JunkGridPartType* storeThis)
  {
    return 0;
  }
#endif
  // collects gridParts for construction of glue object
  template <class GridPartType>
  static bool glueSetup(const GridPartType* gridpart, const GridPartType* grydpart )
  {
    // reset gridpart homes to zero for new refinements
    if ( gridpart == 0 )
    {
      if(grydpart==(GridPartType*) gridpartInsideHome()) gridpartInsideHome( grydpart );
      if(grydpart==(GridPartType*) gridPartOutsideHome()) gridPartOutsideHome( grydpart );
      CouplingConstraintsStore<special>::doingSecondGlueValue(true,false);
#if !DEFORM
      // delete the glue objects
      GlueStruct::makeGlue1(0,0);
      GlueStruct::makeGlue2(0,0);
#endif
    }
#if FEM_FEM
    // if first home empty, then fill it with the address of the gridpart received
    else if (gridpartInsideHome() == 0)
    {
      gridpartInsideHome(gridpart);
    }
#endif
    // otherwise, must now have the two gridparts needed for the construction of the glue object
    else
    {
#if !FEM_FEM
      auto temp = gridpartInsideHome( gridpart );

      // for surface-surface gluing, only pass the gridpart address to the
      // outside home for storage if inside home hasn't already stored it
      if((!special)||(temp!=0))
#endif
      gridPartOutsideHome(gridpart);

      // give the address of the (new) glue object to the glueHome to store
      if ( ( gridpartInsideHome() != 0 ) && ( gridPartOutsideHome() != 0) )
      {
        glueHome( makeGlue( *gridpartInsideHome(), *gridPartOutsideHome(), CouplingConstraintsStore<special>::doingSecondGlueValue(false) ), CouplingConstraintsStore<special>::doingSecondGlueValue(false) );
        // writeGlue(CouplingConstraintsStore<special>::doingSecondGlueValue(false));
        checkGlue<GridPartType>(CouplingConstraintsStore<special>::doingSecondGlueValue(false));
        CouplingConstraintsStore<special>::doingSecondGlueValue(true,true);
        // reset gridpart homes to zero for next glue construction
        grydpart = (GridPartType*) gridpartInsideHome();
        gridpartInsideHome( grydpart );
        grydpart = (GridPartType*) gridPartOutsideHome();
        gridPartOutsideHome( grydpart );
        return true;
      }
    }
    return false;
  }
  // make the glue object required for the coupling
  static GlueType* makeGlue(const GridPartType& gridPartPtr, const GrydPartType& grydPartPtr, bool doingSecondGlue)
  {
    GlueType *glue;
#if DROP || !DEFORM
    if (doingSecondGlue)
      glue = GlueStruct::makeGlue2(&gridPartPtr,&grydPartPtr);
    else
      glue = GlueStruct::makeGlue1(&gridPartPtr,&grydPartPtr);
#endif

    std::cout << "Gluing successful, " << glue->size() << " remote intersections found!" << std::endl;
    assert(glue->size() > 0);

    // testCoupling(*glue);
    // testCommunication(*glue);

    return glue;
  }

  struct GlueStruct
  {
    // set-up the glue object
    typedef typename Dune::FemFemCoupling::Glue<GT,special>::DomGridView DomGridView;
    typedef typename Dune::FemFemCoupling::Glue<GT,special>::TarGridView TarGridView;
    typedef typename Dune::FemFemCoupling::Glue<GT,special>::DomExtractor DomExtractor;
    typedef typename Dune::FemFemCoupling::Glue<GT,special>::TarExtractor TarExtractor;

    DomGridView gridleafView;
    TarGridView grydleafView;

#if FEM_FEM
    typedef VerticalFaceDescriptor< DomGridView > DomFaceDes;
    typedef VerticalFaceDescriptor< TarGridView > TarFaceDes;
#else
    typedef SurfaceCouplingDescriptor< DomGridView,!special > DomFaceDes;
    typedef SurfaceCouplingDescriptor< TarGridView,0 > TarFaceDes;
#endif
    DomFaceDes domdesc;
    TarFaceDes tardesc;
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
    typedef Dune::GridGlue::OverlappingMerge< GridPartType::dimensionworld-1, GridPartType::dimensionworld-1, GridPartType::dimensionworld, double > SurfaceMergeImpl;
#else
    typedef Dune::GridGlue::ConformingMerge< GridPartType::dimensionworld-1, GridPartType::dimensionworld, double > SurfaceMergeImpl;
#endif

    DomExtractor domEx;
    TarExtractor tarEx;

    SurfaceMergeImpl merger;

    GlueType glue;

#if DROP || !DEFORM
    GlueStruct(const GridPartType& gridPart, const GrydPartType& grydPart)
      : gridleafView(gridPart)
      , grydleafView(grydPart)
#if FEM_FEM
      , domdesc()
      , tardesc()
#else
      , domdesc(gridleafView)
      , tardesc(grydleafView)
#endif
      , domEx(gridleafView, domdesc)
      , tarEx(grydleafView, tardesc)
      , merger()
      , glue(domEx,tarEx,&merger)
    {
      glue.build();
    }
    static GlueType* makeGlue1(const GridPartType* gridPartPtr, const GrydPartType* grydPartPtr)
    {
      static GlueStruct* object = 0;
      if ( !gridPartPtr || !grydPartPtr)
      {
        delete object;
        object = 0;
        return 0;
      }
      else
      {
        assert( object == 0);
        object = new GlueStruct(*gridPartPtr,*grydPartPtr);
        return &(object->glue);
      }
    }
    static GlueType* makeGlue2(const GridPartType* gridPartPtr, const GrydPartType* grydPartPtr)
    {
      static GlueStruct* object = 0;
      if ( !gridPartPtr || !grydPartPtr)
      {
        delete object;
        object = 0;
        return 0;
      }
      else
      {
        assert( object == 0);
        object = new GlueStruct(*gridPartPtr,*grydPartPtr);
        return &(object->glue);
      }
    }
#endif
  };

  static std::string ZeroPadNumber(int num)
  {
    std::stringstream ss;

	// the number is converted to string with the help of stringstream
	ss << num;
	std::string ret;
	ss >> ret;

	// Append zero chars
	int str_length = ret.length();
	for (int i = 0; i < 3 - str_length; i++)
		ret = "0" + ret;
	return ret;
  }

  // static glue checker
  template <class GridPartType>
  static void checkGlue(bool glu2 = false)
  {
    typedef Dune::MultipleCodimMultipleGeomTypeMapper< typename GlueType::Grid0View, Dune::MCMGElementLayout > View0Mapper;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper< typename GlueType::Grid1View, Dune::MCMGElementLayout > View1Mapper;
    GlueType* newglue = glueHome(0,glu2);

    View0Mapper view0mapper(newglue->template gridView<0>());
    View1Mapper view1mapper(newglue->template gridView<1>());

    const int  inRank =  gridpartInsideHome()->comm().rank();
    const int outRank = gridPartOutsideHome()->comm().rank();

    const std::string  inRankStr = ZeroPadNumber(  inRank );
    const std::string outRankStr = ZeroPadNumber( outRank );

    int entCount = 0; double totVol = 0;
    // left geometry glued to right geometry

    std::ofstream gmsh;

    if((!glu2)&&(!special))
    {
      gmsh.open("inner-"+inRankStr+"-"+outRankStr+"-left.pos");
      gmsh << "View \"Inner left glue geometry \" {" << std::endl;
    }
    else
    {
      gmsh.open("outer-"+inRankStr+"-"+outRankStr+"-left.pos");
      gmsh << "View \"Outer left glue geometry \" {" << std::endl;
    }
    {
      typename GlueType::Grid0IntersectionIterator rIIt    = newglue->template ibegin<0>();
      typename GlueType::Grid0IntersectionIterator rIEndIt = newglue->template iend<0>();
      typedef typename GlueType::Grid0IntersectionIterator::value_type Intersection;
      std::ofstream inter;
      if((!glu2)&&(!special))
      {
        inter.open("inner-"+inRankStr+"-"+outRankStr+"-glue.pos");
        inter << "View \"Inner Glue geometry \" {" << std::endl;
      }
      else
      {
        inter.open("outer-"+inRankStr+"-"+outRankStr+"-glue.pos");
        inter << "View \"Outer Glue geometry \" {" << std::endl;
      }
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
          // couplingEntChecker( rIIt, gridpartInsideHome(), gridPartOutsideHome(), false, glu2, gmsh );
          entCount++;
          totVol += rIIt->geometry().volume();
#if !BEMPP
          Dune::FieldVector<double, Intersection::InsideGridView::dimensionworld> localGrid0Pos =
          rIIt->inside()->geometry().global(rIIt->geometryInInside().center());

          Dune::FieldVector<double, Intersection::OutsideGridView::dimensionworld> localGrid1Pos =
          rIIt->outside()->geometry().global(rIIt->geometryInOutside().center());

          if ( (localGrid0Pos-localGrid1Pos).two_norm() >= 1e-6 )
          {
            DUNE_THROW(Dune::Exception, "Bad center matching (1).");
          }
#endif
          int numcorn = rIIt->geometry().corners();

          // declare the gmsh element type
          if( numcorn == 3 )
            inter << "ST" << std::endl;
          else if( numcorn == 4 )
            inter << "SQ" << std::endl;
          else
	  {
            std::cout << "numcorn = " << numcorn << std::endl;
            DUNE_THROW(Dune::Exception, "Bad number of corners for gmsh output.");
	  }
          inter << "(" << std::endl;

          double centerX = 0.0; double centerY = 0.0; double centerZ = 0.0; double count = 0.0;

          for (int i = 0; i < numcorn; ++i)
	  {
            // coordinate of one of the intersections corners
            auto intergeo = rIIt->geometry().corner(i);
            // output global coordinates of corner to gmsh file
            inter <<   intergeo[0] << "  ,  " <<  intergeo[1]  << "  ,  " <<    intergeo[2] << std::endl;
            count += 1.0; centerX += intergeo[0]; centerY += intergeo[1]; centerZ += intergeo[2];
            if( i < numcorn - 1 )
              inter << "," << std::endl;
	  }
          // flag to gmsh output which (if any) error was encountered
          inter << ")" << std::endl;
          inter << "{" << std::endl;
          for( int i = 0 ; i < numcorn; ++i )
          {
            inter <<  0.0 << std::endl;
            if( i < numcorn - 1 )
              inter << "," << std::endl;
          }
          inter << "};" << std::endl;

          Dune::FieldVector<double, Intersection::OutsideGridView::dimensionworld> localNormal = rIIt->centerUnitOuterNormal();

          centerX=centerX/count; centerY=centerY/count; centerZ=centerZ/count;

          // gmsh plot of local normal
          inter << "VP" << std::endl;
          inter << "(" << std::endl;
          inter << centerX << " , " << centerY << " ,  " << centerZ << std::endl;
          inter << ")" << std::endl;
          inter << "{" << std::endl;
          inter << localNormal[0] << " , " << localNormal[1] << " ,  " << localNormal[2] << std::endl;
          inter << "};" << std::endl;

        }
      }
      inter << "};" << std::endl;
      inter.close();
    }
    gmsh << "};" << std::endl;
    gmsh.close();
    int intCount = 0; double totVul = 0;
    // right geometry glued to left geometry
    std::ofstream gnsh;
    if((!glu2)&&(!special))
    {
      gnsh.open("inner-"+inRankStr+"-"+outRankStr+"-right.pos");
      gnsh << "View \"Inner right glue geometry \" {" << std::endl;
    }
    else
    {
      gnsh.open("outer-"+inRankStr+"-"+outRankStr+"-right.pos");
      gnsh << "View \"Outer right glue geometry \" {" << std::endl;
    }
    {
      typename GlueType::Grid1IntersectionIterator rIIt    = newglue->template ibegin<1>();
      typename GlueType::Grid1IntersectionIterator rIEndIt = newglue->template iend<1>();
      typedef typename GlueType::Grid0IntersectionIterator::value_type Intersection;
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
          // couplingEntChecker( rIIt, gridPartOutsideHome(), gridpartInsideHome(), true, glu2, gnsh );
          intCount++;
          totVul += rIIt->geometry().volume();
#if !BEMPP
          Dune::FieldVector<double, Intersection::InsideGridView::dimensionworld> localGrid0Pos =
          rIIt->inside()->geometry().global(rIIt->geometryInInside().center());

          Dune::FieldVector<double, Intersection::OutsideGridView::dimensionworld> localGrid1Pos =
          rIIt->outside()->geometry().global(rIIt->geometryInOutside().center());

          if ( (localGrid0Pos-localGrid1Pos).two_norm() >= 1e-6 )
          {
            DUNE_THROW(Dune::Exception, "Bad center matching (2).");
          }
#endif
        }
      }
    }
    gnsh << "};" << std::endl;
    gnsh.close();
    std::cout << inRank << "  " << outRank << " Checkglue has been called successfully ( " << entCount << " , " << intCount << " ) [ " << totVol << " , " << totVul << " ]." << std::endl;

  }

  // static glue writer
  static void writeGlue(bool glu2 = false)
  {
    GlueType* newglue = glueHome(0,glu2);
    // Extracted Parts
    const std::string fileleft = "glueleftpart.vtk";
    GridGlueVtkWriter::writeExtractedPart<GlueType, 0>(*newglue, fileleft);
    const std::string fileright = "gluerightpart.vtk";
    GridGlueVtkWriter::writeExtractedPart<GlueType, 1>(*newglue, fileright);
    std::cout << "Glue vtk part output has been written to files " << fileleft << " and " << fileright << std::endl;
    // Intersections
    const std::string fyleleft = "glueleftis.vtk";
    GridGlueVtkWriter::writeIntersections<GlueType, 0>(*newglue, fyleleft);
    const std::string fyleright = "gluerightis.vtk";
    GridGlueVtkWriter::writeIntersections<GlueType, 1>(*newglue, fyleright);
    std::cout << "Glue vtk is output has been written to files " << fyleleft << " and " << fyleright << std::endl;
  }
};

template <bool>
struct searchEntityCouplingDofs
{
  // detect all DoFs on the Coupling boundary of the given entity
  template< class IntersectionIt, class GridFunctionType, class OtherGridFunctionType, class DiscreteFunctionType, class ShortIntVector >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              const OtherGridFunctionType& v,
                              DiscreteFunctionType &w, ShortIntVector &couplingBlocks )
  {
    auto entityPointer = rIIt->inside();
    auto &entity = *entityPointer;

    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteSpaceType;
    typedef typename DiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    const LagrangePointSetType &lagrangePointSet = w.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    int localDof = 0;
    const unsigned int localBlockSize = w.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    w.space().blockMapper().map(entity,globalBlockDofs);

    // home and away geometries
    auto homeGeometry = entity.geometry();

    // loop over home Lagrange points
    for( unsigned int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
      // alternate global away coordinate for this Lagrange point
      auto testCoordinate =
        rIIt->geometryInInside().global(
          rIIt->geometryInInside().local(
            Dune::Fem::coordinate(lagrangePointSet[ localBlock ])
        )
      );
      double dist = ( testCoordinate-Dune::Fem::coordinate(lagrangePointSet[ localBlock ]) ).two_norm2();
      // check if home and away global coordinates match
      if ( dist < 1e-6 )
      {
        // mark global DoF number
        assert( globalBlockDofs[ localBlock ] < couplingBlocks.size() );
	/*
        auto bob = homeGeometry.global( testCoordinate );

        if(std::abs(bob[0])<1.0e-6) bob[0]=0;
        if(std::abs(bob[1])<1.0e-6) bob[1]=0;
        if(std::abs(bob[2])<1.0e-6) bob[2]=0;

	std::cout << "LLL " << " (  " << bob[0] << "  " << bob[1] << "  " << bob[2] << " ) " << std::endl;
	*/
        couplingBlocks[globalBlockDofs[ localBlock ] ] = 1;
      }
    }
  }
};

template <>
struct searchEntityCouplingDofs<false>
{
  // detect all DoFs on the Coupling boundary of the given entity
  template< class IntersectionIt, class GridFunctionType, class OtherGridFunctionType, class DiscreteFunctionType, class ShortIntVector >
  static void apply( const IntersectionIt &rIIt,
                              const GridFunctionType& u,
                              const OtherGridFunctionType& v,
                              DiscreteFunctionType &w, ShortIntVector &couplingBlocks )
  {}
};

template < class Model, class DiscreteFunctionSpace, bool special >
class CouplingConstraints
{
public:
  typedef Model ModelType;
  typedef DiscreteFunctionSpace DiscreteFunctionSpaceType;

  // type of grid partition
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;

  //! type of grid
  typedef typename DiscreteFunctionSpaceType :: GridType GridType;

  // types for boundary treatment
  // ----------------------------
  typedef typename DiscreteFunctionSpaceType :: BlockMapperType BlockMapperType;

  typedef Fem::SlaveDofs< DiscreteFunctionSpaceType, BlockMapperType > SlaveDofsType;
  typedef typename SlaveDofsType :: SingletonKey SlaveDofsKeyType;
  typedef Fem::SingletonList< SlaveDofsKeyType, SlaveDofsType >
      SlaveDofsProviderType;

  // extra typedefs for the gluing
  typedef typename Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue<GT,special> TraitsType;
  typedef typename TraitsType::GlueType GlueType;

  // typedef for the CouplingConstraints store
  typedef CouplingConstraintsStore<special> CStore;

  CouplingConstraints( const ModelType &model, const DiscreteFunctionSpaceType& space )
    : model_(model),
      space_( space ),
      gridPart_( space.gridPart() ),
      slaveDofs_( getSlaveDofs( space_ ) ),
      dirichletBlocks_(),
      couplingBlocks_(),
      // mark DoFs on the Dirichlet boundary
      hasDirichletDofs_( false ),
      // initialize Dirichlet/Neumann flipper flag (true->Dirichlet condition will be applied to coupling boundary, false->Neumann condition will be applied)
      dirneu_( true ),
      robin_( false ),
      flip_( false ),
      sequence_( -1 )
  {
    typedef typename DiscreteFunctionSpaceType :: GridPartType MyGridPartType;
    static const bool flip = MyGridPartType::dimension != MyGridPartType::dimensionworld;

    gamma_ = Dune::Fem::Parameter::getValue< double >( "coupling.gamma" );

    robin_ = ( gamma_ != 0 );

    if ( robin_ )
    {
      dirneu_ = false;
    }
    else
    {
#if DROP
      dirneu_ = model_.hasDirichletBoundary();
#endif
      return;
    }
  }
  // destructor resets associated statics
  ~CouplingConstraints()
  {
    CStore::glueSetup( (GridPartType*) 0,&gridPart_);
  }

  /*! treatment of Dirichlet-DoFs for given discrete function
   *
   *   \note A LagrangeDiscreteFunctionSpace is implicitly assumed.
   *
   *   \param[in]  u   discrete function providing the constraints
   *   \param[out] w   discrete function the constraints are applied to
   */
  template < class DiscreteFunctionType >
  void operator ()( const DiscreteFunctionType& u, DiscreteFunctionType& w ) const
  {
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType MyDiscreteFunctionSpaceType;
    typedef typename MyDiscreteFunctionSpaceType :: GridPartType MyGridPartType;
    static const bool flip = MyGridPartType::dimension != MyGridPartType::dimensionworld;

#if FEM_FEM
    updateDirichletDofs<true>::apply( model_, w.space(), dirichletBlocks_, couplingBlocks_, sequence_, hasDirichletDofs_, dirneu_ );
#else
    updateDirichletDofs<flip>::apply( model_, w.space(), dirichletBlocks_, couplingBlocks_, sequence_, hasDirichletDofs_, dirneu_ );
#endif

    // if Dirichlet Dofs have been found, treat them
    if( hasDirichletDofs_ )
    {
      typedef typename DiscreteFunctionType :: DofIteratorType DofIteratorType ;
      typedef typename DiscreteFunctionType :: ConstDofIteratorType ConstDofIteratorType ;

      ConstDofIteratorType uIt = u.dbegin();
      DofIteratorType wIt = w.dbegin();

      const unsigned int localBlockSize = w.space().localBlockSize;
      // loop over all blocks
      const unsigned int blocks = u.space().blockMapper().size();
      for( unsigned int blockDof = 0; blockDof < blocks ; ++ blockDof )
      {
        if( dirichletBlocks_[ blockDof ] )
        {
	  //  std::cout << "VVV ";
          // copy dofs of the block
          for( unsigned int l = 0; l < localBlockSize ; ++ l, ++ wIt, ++ uIt )
          {
            assert( uIt != u.dend() );
            assert( wIt != w.dend() );
	    //  std::cout << (*uIt) << "  ";
            (*wIt) = (*uIt);
          }
	  //  std::cout << std::endl;
        }
        else
        {
          // increase dof iterators anyway
          for( unsigned int l = 0; l < localBlockSize ; ++ l, ++ wIt, ++ uIt )
          {}
        }
      }
      // w.communicate();
    }
  }

  /*! treatment of Dirichlet-DoFs for given discrete function
   *
   *   \note A LagrangeDiscreteFunctionSpace is implicitly assumed.
   *
   *   \param[in]  u   discrete function providing the constraints
   *   \param[out] w   discrete function the constraints are applied to
   */
  template < class GridFunctionType, class DiscreteFunctionType >
  void operator ()( const GridFunctionType& u, DiscreteFunctionType& w ) const
  {
    apply( u, w );
  }

  void operator ()( ) const
  {
    flip_ = CStore::glueSetup(&gridPart_, (GridPartType*) 0);
  }

  template < class DiscreteFunctionType >
  void initializeInitialSolution( const DiscreteFunctionType& u, DiscreteFunctionType& w) const
  {
    // Coupling Dofs will have been found, so treat them
    typedef typename DiscreteFunctionType :: DofIteratorType DofIteratorType ;
    typedef typename DiscreteFunctionType :: ConstDofIteratorType ConstDofIteratorType ;

    ConstDofIteratorType uIt = u.dbegin();
    DofIteratorType wIt = w.dbegin();

    const unsigned int localBlockSize = w.space().localBlockSize;
    // loop over all blocks
    const unsigned int blocks = u.space().blockMapper().size();
    for( unsigned int blockDof = 0; blockDof < blocks ; ++ blockDof )
    {
#if DROP || !DEFORM
      if( couplingBlocks_[ blockDof ] != 0 )
#endif
      {
        // copy dofs of the block
        for( unsigned int l = 0; l < localBlockSize ; ++ l, ++ wIt, ++ uIt )
        {
          assert( uIt != u.dend() );
          assert( wIt != w.dend() );
          (*wIt) = (*uIt);
        }
      }
#if DROP || !DEFORM
      else
      {
        // increase dof iterators anyway
        for( unsigned int l = 0; l < localBlockSize ; ++ l, ++ wIt, ++ uIt )
        {}
      }
#endif
    }
  }

  template < class GridFunctionType, class OtherGridFunctionType, class DiscreteFunctionType >
  void operator ()( const GridFunctionType& u, const OtherGridFunctionType& v, DiscreteFunctionType& w ) const
  {
    couple( u, v, w );
  }

  void reset() const
  {
    // flip Dirichlet / Neumann flipper
#if BEMPP && FILTERED
#elif !DROP
    if ( ! robin_ ) dirneu_ = ! dirneu_;
#endif

    sequence_ = -1;
  }

  /*! treatment of Dirichlet-DoFs for solution and right-hand-side
   *
   *   delete rows for dirichlet-DoFs, setting diagonal element to 1.
   *
   *   \note A LagrangeDiscreteFunctionSpace is implicitly assumed.
   *
   *   \param[out] linearOperator  linear operator to be adjusted
   */
  template <class LinearOperator>
  void applyToOperator( LinearOperator& linearOperator ) const
  {
    typedef typename DiscreteFunctionSpaceType :: GridPartType MyGridPartType;
    static const bool flip = MyGridPartType::dimension != MyGridPartType::dimensionworld;

#if FEM_FEM
    updateDirichletDofs<true>::apply( model_, linearOperator.rangeSpace(), dirichletBlocks_, couplingBlocks_, sequence_, hasDirichletDofs_, dirneu_ );
#else
    updateDirichletDofs<!flip>::apply( model_, linearOperator.rangeSpace(), dirichletBlocks_, couplingBlocks_, sequence_, hasDirichletDofs_, dirneu_ );
#endif
    typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
    typedef typename IteratorType :: Entity EntityType;

    // if Dirichlet Dofs have been found, treat them
    if( hasDirichletDofs_ )
    {
      const IteratorType end = linearOperator.rangeSpace().end();
      for( IteratorType it =linearOperator.rangeSpace() .begin(); it != end; ++it )
      {
        const EntityType &entity = *it;
        // adjust linear operator
        dirichletDofsCorrectOnEntity( linearOperator, entity );
      }
    }

  /*! treatment of Robin-DoFs for solution and right-hand-side
   *
   *  add a diagonal element of beta to rows for Robin-DoFs
   *
   *   \note A LagrangeDiscreteFunctionSpace is implicitly assumed.
   *
   *   \param[out] linearOperator  linear operator to be adjusted
   */

    if ( robin_ )
    {
    }
    else
    {
      return;
    }

    // updateCouplingDofs();

    GlueType* newglue = CStore::glueHome( 0, !flip_ );

    // left geometry glued to right geometry
    if ( ! flip_ )
    {
      typename GlueType::Grid0IntersectionIterator rIIt    = newglue->template ibegin<0>();
      typename GlueType::Grid0IntersectionIterator rIEndIt = newglue->template iend<0>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
          // adjust linear operator
#if FEM_FEM
          couplingDofRobinTreatment<true>::apply( rIIt, linearOperator, robin_, flip_, gamma_ );
#else
          couplingDofRobinTreatment<!flip>::apply( rIIt, linearOperator, robin_, flip_, gamma_ );
#endif
        }
      }
    }
    // right geometry glued to left geometry
    else
    {
      typename GlueType::Grid1IntersectionIterator rIIt    = newglue->template ibegin<1>();
      typename GlueType::Grid1IntersectionIterator rIEndIt = newglue->template iend<1>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
          // adjust linear operator
#if FEM_FEM
          couplingDofRobinTreatment<true>::apply( rIIt, linearOperator, robin_, flip_, gamma_ );
#else
          couplingDofRobinTreatment<flip>::apply( rIIt, linearOperator, robin_, flip_, gamma_ );
#endif
        }
      }
    }
  }

protected:
  template < class GridFunctionType, class DiscreteFunctionType >
  void apply( const GridFunctionType& u, DiscreteFunctionType& w ) const
  {
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType MyDiscreteFunctionSpaceType;
    typedef typename MyDiscreteFunctionSpaceType :: GridPartType MyGridPartType;
    static const bool flip = MyGridPartType::dimension != MyGridPartType::dimensionworld;

#if FEM_FEM
    updateDirichletDofs<true>::apply( model_, w.space(), dirichletBlocks_, couplingBlocks_, sequence_, hasDirichletDofs_, dirneu_ );
#else
    updateDirichletDofs<!flip>::apply( model_, w.space(), dirichletBlocks_, couplingBlocks_, sequence_, hasDirichletDofs_, dirneu_ );
#endif

    typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
    typedef typename IteratorType :: Entity EntityType;

    // if Dirichlet Dofs have been found, treat them
    if( hasDirichletDofs_ )
    {
      const IteratorType end = space_.end();
      for( IteratorType it = space_.begin(); it != end; ++it )
      {
        const EntityType &entity = *it;
        dirichletDofTreatment( entity, u, w );
      }
      // w.communicate();
    }
  }

protected:
  template < class GridFunctionType, class OtherGridFunctionType, class DiscreteFunctionType >
  void couple( const GridFunctionType& u, const OtherGridFunctionType& v, DiscreteFunctionType& w ) const
  {
    typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
    typedef typename IteratorType :: Entity EntityType;

    typedef typename DiscreteFunctionSpaceType :: GridPartType MyGridPartType;
    static const bool flip = MyGridPartType::dimension != MyGridPartType::dimensionworld;

#if DROP
    dirneu_ = model_.hasDirichletBoundary();
#endif

    GlueType* newglue = CStore::glueHome( 0, !flip_ );

    // accumulate the size of the coupling surface (for debugging)
    double couplingSurfaceVolume = 0.0;

    // left geometry glued to right geometry
    if ( ! flip_ )
    {
      typename GlueType::Grid0IntersectionIterator rIIt    = newglue->template ibegin<0>();
      typename GlueType::Grid0IntersectionIterator rIEndIt = newglue->template iend<0>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
 	  if ( dirneu_ )
 	  {
#if FEM_FEM
            couplingDofDirichletTreatment<true>::apply( rIIt, u, v, w, dirichletBlocks_, couplingBlocks_, robin_, flip, special_ );
#else
            couplingDofDirichletTreatment<((!flip)&&(!special))||special>::apply( rIIt, u, v, w, dirichletBlocks_, couplingBlocks_, robin_, flip_, special );
#endif
	  }
	  else
	  {
#if FEM_FEM
            couplingDofNeumannTreatment<true>::apply( rIIt, u, v, w, robin_, flip_ );
#else
            couplingDofNeumannTreatment<!flip>::apply( rIIt, u, v, w, robin_, flip_ );
#endif
	  }
        }
      }
    }
    // right geometry glued to left geometry
    else
    {
      typename GlueType::Grid1IntersectionIterator rIIt    = newglue->template ibegin<1>();
      typename GlueType::Grid1IntersectionIterator rIEndIt = newglue->template iend<1>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
	  if ( dirneu_ )
	  {
#if FEM_FEM
            couplingDofDirichletTreatment<true>::apply( rIIt, u, v, w, dirichletBlocks_, couplingBlocks_, robin_, flip_, special );
#else
            couplingDofDirichletTreatment<((flip)&&(!special))||special>::apply( rIIt, u, v, w, dirichletBlocks_, couplingBlocks_, robin_, flip_, special );
#endif
	  }
	  else
          {
#if FEM_FEM
            couplingDofNeumannTreatment<true>::apply( rIIt, u, v, w, robin_, flip_ );
#else
            couplingDofNeumannTreatment<flip>::apply( rIIt, u, v, w, robin_, flip_ );
#endif
	  }
        }
      }
    }
    // if ( ! dirneu_ ) std::cout << "Coupling surface has volume =  " << couplingSurfaceVolume << std::endl;
  }

public:
  template < class GridFunctionType, class DiscreteFunctionType >
  void incident( const GridFunctionType& u, DiscreteFunctionType& w ) const
  {
    typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
    typedef typename IteratorType :: Entity EntityType;

    typedef typename DiscreteFunctionSpaceType :: GridPartType MyGridPartType;
    static const bool flip = MyGridPartType::dimension != MyGridPartType::dimensionworld;

    // only add in the normal derivative of the incident field on the boundary if there is one
    if( !model_.hasIncidentField() ) return;

    GlueType* newglue = CStore::glueHome( 0, !flip_ );

    // left geometry glued to right geometry
    if ( ! flip_ )
    {
      typename GlueType::Grid0IntersectionIterator rIIt    = newglue->template ibegin<0>();
      typename GlueType::Grid0IntersectionIterator rIEndIt = newglue->template iend<0>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
            couplingDofIncidentTreatment<!flip>::apply( rIIt, u, w );
        }
      }
    }
    // right geometry glued to left geometry
    else
    {
      typename GlueType::Grid1IntersectionIterator rIIt    = newglue->template ibegin<1>();
      typename GlueType::Grid1IntersectionIterator rIEndIt = newglue->template iend<1>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
            couplingDofIncidentTreatment<flip>::apply( rIIt, u, w );
        }
      }
    }
  }


protected:
  /*! treatment of Dirichlet-DoFs for one entity
   *
   *   delete rows for dirichlet-DoFs, setting diagonal element to 1.
   *
   *   \note A LagrangeDiscreteFunctionSpace is implicitly assumed.
   *
   *   \param[in]  entity  entity to perform Dirichlet treatment on
   */
  template< class LinearOperator, class EntityType >
  void dirichletDofsCorrectOnEntity ( LinearOperator& linearOperator,
                                      const EntityType &entity ) const
  {
    // get slave dof structure (for parallel runs)   /*@LST0S@*/
    SlaveDofsType &slaveDofs = this->slaveDofs();

    /*
    // home geometry
    typedef typename EntityType::Geometry GeometryType;
    const GeometryType homeGeometry = entity.geometry();
    */

    typedef typename DiscreteFunctionSpaceType :: LagrangePointSetType
      LagrangePointSetType;
    const LagrangePointSetType &lagrangePointSet = space_.lagrangePointSet( entity );

    typedef typename LinearOperator :: LocalMatrixType LocalMatrixType;

    // get local matrix from linear operator
    LocalMatrixType localMatrix = linearOperator.localMatrix( entity, entity );

    // get number of basis functions
    const unsigned int localBlocks = lagrangePointSet.size();
    const unsigned int localBlockSize = space_.localBlockSize;

    // map local to global dofs
    std::vector<std::size_t> globalBlockDofs(localBlocks);
    // obtain all DofBlocks for this element
    space_.blockMapper().map( entity, globalBlockDofs );

    // counter for all local dofs (i.e. localBlockDof * localBlockSize + ... )
    int localDof = 0;
    // iterate over face dofs and set unit row
    for( unsigned int localBlockDof = 0 ; localBlockDof < localBlocks; ++ localBlockDof )
    {
      int global = globalBlockDofs[localBlockDof];

      if( dirichletBlocks_[ global] != 0 )
      {
	/*
        auto bob = homeGeometry.global( Dune::Fem::coordinate( lagrangePointSet[ localBlockDof ] ) );

        if(std::abs(bob[0])<1.0e-6) bob[0]=0;
        if(std::abs(bob[1])<1.0e-6) bob[1]=0;
        if(std::abs(bob[2])<1.0e-6) bob[2]=0;

        std::cout << "YYY  " << " (  " << bob[0] << "  " << bob[1] << "  " << bob[2] << " ) " << bob.two_norm() << std::endl;
	*/
        for( int l = 0; l < localBlockSize; ++ l, ++ localDof )
        {
          // clear all other columns
          localMatrix.clearRow( localDof );

          // set diagonal to 1
          double value = slaveDofs.isSlave( global )? 0.0 : 1.0;
          localMatrix.set( localDof, localDof, value );
        }
      }
      else
      {
        // increase localDof anyway
        localDof += localBlockSize ;
      }
    }
  }

  /*! treatment of Robin-DoFs for one entity
   *
   *  add a diagonal element of beta to rows for Robin-DoFs
   *
   *   \note A LagrangeDiscreteFunctionSpace is implicitly assumed.
   *
   *   \param[in]  entity  entity to perform Robin treatment on
   */
#if 0
  template< class IntersectionIt, class LinearOperator >
  void couplingDofRobinTreatment( LinearOperator& linearOperator,
                                      const IntersectionIt &rIIt ) const
  {
#if DROP
    abort();
#else
    // get slave dof structure (for parallel runs)   /*@LST0S@*/
    SlaveDofsType &slaveDofs = this->slaveDofs();

    auto entityPointer = rIIt->inside();

    auto &entity = *entityPointer;

    typedef typename DiscreteFunctionSpaceType :: LagrangePointSetType
      LagrangePointSetType;

    // lets start with a one point quadrature
    auto homeCenter = rIIt->geometryInInside().center();

    // multiply Robin parameter through with "volume" of glue object on which the surface integration will be performed
    double value = rIIt->geometry().volume();
    if ( flip_ )
      value *= ( 1.0 - std::max(gamma_,0.0) );
    else
      value *= std::abs(gamma_);

    const LagrangePointSetType &lagrangePointSet = space_.lagrangePointSet( entity );
#if FEM_FEM
    typedef typename LinearOperator :: LocalMatrixType LocalMatrixType;

    // get local matrix from linear operator
    LocalMatrixType localMatrix = linearOperator.localMatrix( entity, entity );

    const unsigned int numDofs = space_.blockMapper().maxNumDofs();

    typedef typename DiscreteFunctionSpaceType :: RangeType RangeType;
    std::vector< RangeType > phi( numDofs );

    auto &baseSet = localMatrix.domainBasisFunctionSet();
    const unsigned int numBaseFunctions = baseSet.size();
    baseSet.evaluateAll( homeCenter, phi );

    // add the value to the system matrix
    for( unsigned int localCol = 0; localCol < numBaseFunctions; ++localCol )
    {
      // add phi_j phi_i   for i=0,...,N-1, j=localCol
      localMatrix.column( localCol ).axpy( phi, phi[localCol], value );
    }
#endif
#endif
  }
#endif


  //! set the dirichlet points to exact values
  template< class EntityType, class GridFunctionType, class DiscreteFunctionType >
  void dirichletDofTreatment( const EntityType &entity,
                              const GridFunctionType& u,
                              DiscreteFunctionType &w ) const
  {
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
      DiscreteSpaceType;
    typedef typename GridFunctionType :: LocalFunctionType GridLocalFunctionType;
    typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;

    typedef typename DiscreteSpaceType :: LagrangePointSetType
      LagrangePointSetType;

    typedef typename DiscreteSpaceType::DomainType DomainType;

    /*
    // home geometry
    typedef typename EntityType::Geometry GeometryType;
    const GeometryType homeGeometry = entity.geometry();
    */

    // get local functions of result
    LocalFunctionType wLocal = w.localFunction( entity );

    // get local functions of argument
    GridLocalFunctionType uLocal = u.localFunction( entity );

    const LagrangePointSetType &lagrangePointSet = w.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    int localDof = 0;
    const unsigned int localBlockSize = w.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    space_.blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( unsigned int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
      int global = globalBlockDofs[ localBlock ];
      if( dirichletBlocks_[ global ] > 0 )
      {
        // only allow filling in of Dirichlet data if not on a coupling node
        if ( couplingBlocks_[ global ] == 0 )
        {
          typedef typename DiscreteFunctionSpaceType :: RangeType RangeType;
          RangeType phi( 0 );

          // evaluate data
          uLocal.evaluate( lagrangePointSet[ localBlock ], phi );
	  /*
          auto bob = homeGeometry.global( Dune::Fem::coordinate( lagrangePointSet[ localBlock ] ) );

          if(std::abs(bob[0])<1.0e-6) bob[0]=0;
          if(std::abs(bob[1])<1.0e-6) bob[1]=0;
          if(std::abs(bob[2])<1.0e-6) bob[2]=0;

	  std::cout <<" TTT " << " (  " << bob[0] << "  " << bob[1] << "  " << bob[2] << " ) " << bob.two_norm() << " = " << "  " << phi << std::endl;
	  */
          // store result to dof vector
          for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
          {
            // store result
            wLocal[ localDof ] = phi[ l ];
            wLocal[ localDof ] /= dirichletBlocks_[ global ];
          }
        }
        else
        {
          // increase localDofs by block size
          localDof += localBlockSize ;
        }
      }
      else if( dirichletBlocks_[ global ] < 0)
      {
        if ( couplingBlocks_[ global ] == 0 )
        {
          for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
          {
            // store result
            wLocal[ localDof ] = 0.;
          }
        }
        else
        {
          // increase localDofs by block size
          localDof += localBlockSize ;
        }
      }
      else
      {
        // increase localDofs by block size
        localDof += localBlockSize ;
      }

    }
  }


template <class DiscreteFunctionSpaceType>
class CouplingBuilder
    : public CommDataHandleIF< CouplingBuilder<DiscreteFunctionSpaceType>, int >
{
public:
  typedef DiscreteFunctionSpaceType SpaceType;
  typedef typename SpaceType::BlockMapperType MapperType;

  enum { nCodim = SpaceType :: GridType :: dimension + 1 };

public:
  typedef int DataType;

  const int myRank_;
  const int mySize_;

  typedef std::vector< short int > CouplingBlocksType;
  CouplingBlocksType &couplingBlocks_;

  const SpaceType &space_;
  const MapperType &mapper_;

public:
  CouplingBuilder( CouplingBlocksType &couplingBlocks,
                    const SpaceType &space,
                    const MapperType &mapper,
                    const bool &binaryBlocks )
  : myRank_( space.gridPart().comm().rank() ),
    mySize_( space.gridPart().comm().size() ),
    couplingBlocks_( couplingBlocks ),
    space_( space ),
    binaryBlocks_( binaryBlocks ),
    mapper_( mapper )
  {
  }
  bool contains ( int dim, int codim ) const
  {
    return mapper_.contains( codim );
  }

  bool fixedsize ( int dim, int codim ) const
  {
    return false;
  }

  //! read buffer and apply operation
  template< class MessageBuffer, class Entity >
  inline void gather ( MessageBuffer &buffer,
                       const Entity &entity ) const
  {
    unsigned int localBlocks = mapper_.numEntityDofs( entity );
    std::vector<std::size_t> globalBlockDofs(localBlocks);
    mapper_.mapEntityDofs( entity, globalBlockDofs );
    for( unsigned int localBlockDof = 0 ; localBlockDof < localBlocks; ++ localBlockDof )
    {
      if ( couplingBlocks_[ globalBlockDofs[localBlockDof] ] != 0  )
        buffer.write( 1 );
      else
        buffer.write( 0 );
    }
  }

  //! read buffer and apply operation
  //! scatter is called for one every entity
  //! several times depending on how much data
  //! was gathered
  template< class MessageBuffer, class EntityType >
  inline void scatter ( MessageBuffer &buffer,
                        const EntityType &entity,
                        size_t n )
  {
    unsigned int localBlocks = mapper_.numEntityDofs( entity );
    std::vector<std::size_t> globalBlockDofs(localBlocks);
    mapper_.mapEntityDofs( entity, globalBlockDofs );
    assert( n == globalBlockDofs.size() );
    assert( n == size(entity) );
    for( unsigned int localBlock = 0 ; localBlock < localBlocks; ++ localBlock )
    {
      int val;
      buffer.read(val);
      if (val == 1)
      {
        if( binaryBlocks_ )
	{
          couplingBlocks_[ globalBlockDofs[localBlock] ] = 1;
        }
        else if ( couplingBlocks_[ globalBlockDofs[localBlock] ] > 0)
	{
          // flag original Coupling status
          couplingBlocks_[ globalBlockDofs[localBlock] ] += 1;
        }
        else
	{
          // flag acquired Coupling status
          couplingBlocks_[ globalBlockDofs[localBlock] ] -= 1;
        }
      }
    }
  }

  //! return local dof size to be communicated
  template< class Entity >
  size_t size ( const Entity &entity ) const
  {
    return mapper_.numEntityDofs( entity );
  }
private:
  bool binaryBlocks_;
};



public:

  // detect all DoFs on the Coupling boundary
  template < class GridFunctionType, class OtherGridFunctionType, class DiscreteFunctionType >
  void updateCouplingDofs( const GridFunctionType& u, const OtherGridFunctionType& v, DiscreteFunctionType& w ) const
  {
    // if( sequence_ != space_.sequence() )
    {
      // resize flag vector with number of blocks and reset flags
      const int blocks = space_.blockMapper().size() ;
      couplingBlocks_.resize( blocks );
      for( int i=0; i<blocks; ++i )
      {
        couplingBlocks_[ i ] = 0 ;
      }

    typedef typename DiscreteFunctionSpaceType :: GridPartType MyGridPartType;
    static const bool flip = MyGridPartType::dimension != MyGridPartType::dimensionworld;

#if DROP
    dirneu_ = model_.hasDirichletBoundary();
#endif

    GlueType* newglue = CStore::glueHome( 0, !flip_ );

    // left geometry glued to right geometry
    if ( ! flip_ )
    {
      typename GlueType::Grid0IntersectionIterator rIIt    = newglue->template ibegin<0>();
      const typename GlueType::Grid0IntersectionIterator rIEndIt = newglue->template iend<0>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
#if FEM_FEM
          searchEntityCouplingDofs<true>::apply( rIIt, u, v, w , couplingBlocks_ );
#else
          searchEntityCouplingDofs<!flip>::apply( rIIt, u, v, w , couplingBlocks_ );
#endif
        }
      }
    }
    // right geometry glued to left geometry
    else
    {
      typename GlueType::Grid1IntersectionIterator rIIt    = newglue->template ibegin<1>();
      const typename GlueType::Grid1IntersectionIterator rIEndIt = newglue->template iend<1>();
      for (; rIIt!=rIEndIt; ++rIIt)
      {
        if (rIIt->self() && rIIt->neighbor())
        {
#if FEM_FEM
          searchEntityCouplingDofs<true>::apply( rIIt, u, v, w , couplingBlocks_ );
#else
          searchEntityCouplingDofs<flip>::apply( rIIt, u, v, w , couplingBlocks_ );
#endif
        }
      }
    }

      // update sequence number
      // sequence_ = space_.sequence();
      if( space_.gridPart().comm().size() > 1 )
      {
        try
        {
#if DROP
          typedef typename DiscreteFunctionSpaceType :: GridPartType TestGridPartType;
          static const bool myflip = TestGridPartType::dimension != TestGridPartType::dimensionworld;
#else
          static const bool myflip = false;
#endif
	  CouplingBuilder<DiscreteFunctionSpaceType> handle( couplingBlocks_, space_ , space_.blockMapper(), (!dirneu_) || myflip );
          space_.gridPart().communicate
            ( handle, GridPartType::indexSetInterfaceType, Dune::ForwardCommunication );
        }
        // catch possible exceptions here to have a clue where it happend
        catch( const Dune::Exception &e )
        {
          std::cerr << e << std::endl;
          std::cerr << "Exception thrown in: " << __FILE__ << " line:" << __LINE__ << std::endl;
          abort();
        }
      }
      // let updateDirichletDofs update the sequence number
      // sequence_ = space_.sequence();
    }
  }
protected:

  //! pointer to slave dofs
  const ModelType& model_;
  const DiscreteFunctionSpaceType& space_;
  const GridPartType gridPart_;
  SlaveDofsType *const slaveDofs_;
  mutable std::vector< short int > dirichletBlocks_;
  mutable std::vector< short int > couplingBlocks_;
  mutable bool hasDirichletDofs_ ;
  mutable bool dirneu_ ;
  mutable bool robin_ ;
  mutable bool flip_;
  mutable double gamma_;
  mutable int sequence_ ;

  // return slave dofs
  static SlaveDofsType *getSlaveDofs ( const DiscreteFunctionSpaceType &space )
  {
    SlaveDofsKeyType key( space, space.blockMapper() );
    return &(SlaveDofsProviderType :: getObject( key ));
  }

  // return reference to slave dofs
  SlaveDofsType &slaveDofs () const
  {
    slaveDofs_->rebuild();
    return *slaveDofs_;
  }
};  // end of coupling constraints class

} // end namespace Dune

#endif // DUNE_fem_fem_coupling.hh
