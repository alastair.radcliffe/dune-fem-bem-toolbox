#ifndef SURFACE_GRID_HH
#define SURFACE_GRID_HH

#include <dune/grid/sgrid.hh>

#include <complex>

#if HAVE_ALBERTA
#include <dune/grid/albertagrid.hh>
#endif

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif


#if HAVE_DUNE_FOAMGRID
#include <dune/foamgrid/foamgrid.hh>
#endif


#include <dune/grid-glue/extractors/extractorpredicate.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>

#if FILTERED
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

#include <dune/grid-glue/extractors/codim0extractor.hh>

#include <dune/fem/gridpart/filteredgridpart.hh>
#include "function.hh"
#include <dune/fem_bem_toolbox/fem_objects/radialfilter.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#endif

#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim0extractor.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/lagrangeinterpolation.hh>
#include <dune/fem/space/lagrange.hh>

#if DEFORM
#include <dune/grid-glue/extractors/codim0extractor.hh>
#if DROP
#include "../../case_studies/charged_droplet/source/deformation.hh"
#include "../../case_studies/charged_droplet/source/volumedeformation.hh"
#include <dune/fem/space/combinedspace/combineddiscretefunctionspace.hh>
#else
#include "../../tests/meanCurvFlow/source/deformation.hh"
#endif
#endif


namespace Dune
{
  namespace FemFemCoupling
  {
    // general definition of template class without implementation
    template <class Grid>
    struct ExtractedSurfaceGrid;

    // specific implementations



#if HAVE_ALBERTA

    template <>
    struct ExtractedSurfaceGrid< Dune::SGrid< 2, 2 > >
    {
      typedef Dune::SGrid< 1, 2 > type;
    };

    template <>
    struct ExtractedSurfaceGrid< Dune::YaspGrid< 2 > >
    {
      typedef Dune::SGrid< 1, 2 > type;
    };

#endif




#if HAVE_DUNE_ALUGRID

    template <>
    struct ExtractedSurfaceGrid< Dune::SGrid< 3, 3 > >
    {
      typedef Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> type;
    };

    template <>
    struct ExtractedSurfaceGrid< Dune::YaspGrid< 3 > >
    {
      typedef Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> type;
    };

#endif




#if HAVE_ALBERTA && HAVE_DUNE_ALUGRID
    template < Dune::ALUGridElementType elType,Dune::ALUGridRefinementType refineType, class Comm >
    struct ExtractedSurfaceGrid< Dune::ALUGrid< 2, 2, elType, refineType, Comm > >
    {
      typedef Dune::AlbertaGrid< 1, 2 > type;
    };
#endif




#if HAVE_DUNE_ALUGRID

    template < Dune::ALUGridElementType elType,Dune::ALUGridRefinementType refineType, class Comm >
    struct ExtractedSurfaceGrid< Dune::ALUGrid< 3, 3, elType, refineType, Comm > >
    {
#if 0 // DROP // HAVE_DUNE_FOAMGRID
#warning using dune-foamgrid
      typedef Dune::FoamGrid<2,3> type;
#else
      // typedef Dune::ALUGrid<2,3,elType,refineType> type;
      typedef Dune::ALUGrid<2,3,Dune::simplex,refineType, Comm> type;
#endif
    };

#endif

#if USE_EIGEN
template <class G, Dune::PartitionIteratorType pid = Dune::All_Partition>
struct GPSelector
{
  typedef Dune::Fem::LeafGridPart< G > type;
};
#else
template <class G, Dune::PartitionIteratorType pid = Dune::All_Partition>
struct GPSelector
{
  typedef Dune::Fem::AdaptiveLeafGridPart< G, pid > type;
};
#endif

    // general definition of template class without implementation
    template <class Grid, bool special>
    struct Glue
    {
      // Grid Types
      #if FEM_FEM
         typedef Grid GrydType;
         #if FILTERED
            typedef typename GPSelector< Grid >::type HostGridPartType;
            typedef School::RadialFilter< HostGridPartType > FilterType;
            typedef Dune::Fem::FilteredGridPart< HostGridPartType, FilterType, true > GridPartType;
            typedef Dune::Fem::FilteredGridPart< HostGridPartType, FilterType, true > GrydPartType;
            typedef typename ExtractedSurfaceGrid< Grid >::type OutsideHostGridType;
         #else
            typedef typename GPSelector< Grid >::type HostGridPartType;
            typedef typename GPSelector< Grid, Dune::InteriorBorder_Partition >::type GridPartType;
            typedef typename GPSelector< Grid, Dune::InteriorBorder_Partition >::type GrydPartType;
            // unused definition just required to compile sideBySide example
            typedef typename ExtractedSurfaceGrid< Grid >::type OutsideHostGridType;
         #endif
      #else
         #if FILTERED
            typedef typename GPSelector< Grid >::type HostGridPartType;
            typedef School::RadialFilter< HostGridPartType > FilterType;
            typedef Dune::Fem::FilteredGridPart< HostGridPartType, FilterType, true > GridPartType;
            typedef typename ExtractedSurfaceGrid< Grid >::type OutsideHostGridType;
            typedef typename ExtractedSurfaceGrid< Grid >::type GrydType;
         #else
            #if DROP
               typedef typename GPSelector< Grid >::type InsideHostGridPartType;

               typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, Grid::dimensionworld > InsideHostFunctionSpaceType;

               typedef Dune::Fem::LagrangeDiscreteFunctionSpace< InsideHostFunctionSpaceType, InsideHostGridPartType, 1 > InsideDiscreteHostFunctionSpaceType;


               typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< InsideDiscreteHostFunctionSpaceType > InsideHostDiscreteFunctionType;

               typedef VolumeDeformationDiscreteFunction< InsideHostDiscreteFunctionType > InsideDeformationType ;

               typedef Dune::GeometryGrid< Grid, InsideDeformationType > GridType;

               typedef typename GPSelector< GridType >::type GridPartType;
            #else
            typedef typename GPSelector< Grid >::type GridPartType;
            #endif
            #if DEFORM
               #if DROP
                  typedef typename ExtractedSurfaceGrid< Grid >::type OutsideHostGridType;
                  typedef Dune::Fem::LeafGridPart< OutsideHostGridType > OutsideHostGridPartType;
                  typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, Grid::dimensionworld > OutsideHostFunctionSpaceType;
               #else
                  // unused definition just required to compile meanCurvFlow example
                  typedef typename Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming> OutsideHostGridType;
               typedef Dune::Fem::LeafGridPart< Grid > OutsideHostGridPartType;

               typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, Grid::dimensionworld > OutsideHostFunctionSpaceType;
               #endif

               typedef Dune::Fem::LagrangeDiscreteFunctionSpace< OutsideHostFunctionSpaceType, OutsideHostGridPartType, 1 > OutsideDiscreteHostFunctionSpaceType;

            #else
               typedef typename ExtractedSurfaceGrid< Grid >::type OutsideHostGridType;
               typedef typename ExtractedSurfaceGrid< Grid >::type GrydType;
            #endif
         #endif
      #endif

      // Function Spaces                         std::complex<double>
      #if DROP
         typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, Grid::dimensionworld > FunctionSpaceType;
      #else
         #if COMPLEX
           // use a complex function space type
           typedef Dune::Fem::FunctionSpace< double, std::complex<double>,Grid::dimensionworld, 1 > FunctionSpaceType;
         #else
      typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, 1 > FunctionSpaceType;
         #endif
      #endif
      #if DROP
         // Position Space for ALE Scheme
         typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, GridType::dimensionworld > FunctionSpaceInsideType;
         typedef Dune::Fem::LagrangeDiscreteFunctionSpace<   FunctionSpaceInsideType, GridPartType, POLORDER   > DiscreteFunctionSpaceInsideType;
         // Velocity + Pressure Space for NS scheme
         typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, GridType::dimensionworld+1 > AugmentedFunctionSpaceInsideType;
      #endif

      #if !FEM_FEM
         #if DEFORM

            typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< OutsideDiscreteHostFunctionSpaceType > OutsideHostDiscreteFunctionType;

            #if DROP
               typedef SurfaceDeformationDiscreteFunction< OutsideHostDiscreteFunctionType > OutsideDeformationType ;
            #else
            typedef DeformationDiscreteFunction< OutsideHostDiscreteFunctionType > OutsideDeformationType ;
            #endif

            #if DROP
               typedef Dune::GeometryGrid< OutsideHostGridType, OutsideDeformationType > GrydType;
            #else
            typedef Dune::GeometryGrid< Grid, OutsideDeformationType > GrydType;
            #endif
         #endif

         typedef typename Dune::Fem::LeafGridPart< GrydType > GrydPartType;

         #if DEFORM
            typedef Dune::Fem::FunctionSpace< double, double, GrydType::dimensionworld, GrydType::dimensionworld > FunctionSpaceOutsideType;
            #if DROP && BEMPP
               typedef Dune::Fem::FunctionSpace< double, double, GrydType::dimensionworld, 1 > ChargeFunctionSpaceOutsideType;
            #endif
         #else
           #if COMPLEX
             // use a complex function space type
             typedef Dune::Fem::FunctionSpace< double, std::complex<double>, GrydType::dimensionworld, 1 > FunctionSpaceOutsideType;
             // typedef Dune::Fem::FunctionSpace< double, std::complex<double>, Grid::dimensionworld, 1 > PlotFunctionSpaceOutsideType;
           #else
            typedef Dune::Fem::FunctionSpace< double, double, GrydType::dimensionworld, 1 > FunctionSpaceOutsideType;
            // typedef Dune::Fem::FunctionSpace< double, double, Grid::dimensionworld, 1 > PlotFunctionSpaceOutsideType;
           #endif
         #endif

         typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceOutsideType, GrydPartType, POLORDER > DiscreteFunctionSpaceOutsideType;
      #endif

      // Grid views
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
      typedef typename Dune::Fem::GridPart2GridView< GridPartType > VolDomGridView;
      typedef typename Dune::Fem::GridPart2GridView< GrydPartType > SurfDomGridView;
#else
      typedef typename Dune::Fem::GridPartView< GridPartType > VolDomGridView;
      typedef typename Dune::Fem::GridPartView< GrydPartType > SurfDomGridView;
#endif
      typedef typename std::conditional<special,SurfDomGridView,VolDomGridView>::type DomGridView;
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 4)
      typedef typename Dune::Fem::GridPart2GridView< GrydPartType > TarGridView;
#else
      typedef typename Dune::Fem::GridPartView< GrydPartType > TarGridView;
#endif

      // Extractors
      typedef Dune::GridGlue::Codim1Extractor<DomGridView> VolDomExtractor;
      typedef Dune::GridGlue::Codim0Extractor<DomGridView> SurfDomExtractor;
      typedef typename std::conditional<special,SurfDomExtractor,VolDomExtractor>::type DomExtractor;

      #if !FEM_FEM
         typedef Dune::GridGlue::Codim0Extractor<TarGridView> TarExtractor;
      #else
         typedef Dune::GridGlue::Codim1Extractor<TarGridView> TarExtractor;
      #endif

      // for volume to surface gluing
      typedef Dune::GridGlue::GridGlue<DomExtractor,TarExtractor> GlueType;
    };


  } // namespace FemFemCoupling

} // namespace Dune

#endif  // #ifndef SURFACE_GRID_HH
