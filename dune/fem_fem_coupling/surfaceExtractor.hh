
#ifndef SURFACEEXTRACTOR_HH
#define SURFACEEXTRACTOR_HH

//- Dune-fem includes
#include <map>
#include <dune/geometry/referenceelements.hh>
#include <dune/fem/quadrature/caching/twistutility.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/quadrature/intersectionquadrature.hh>
#include <dune/fem/operator/common/spaceoperatorif.hh>
#include <dune/fem/operator/matrix/blockmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/alugrid/common/alugrid_assert.hh>
#include <dune/grid/common/gridfactory.hh>

// SurfaceExtractor
// ---------
template< class GridPart, class OutsideHostGridType >
class SurfaceExtractor
{
  typedef SurfaceExtractor< GridPart, OutsideHostGridType > ThisType;

public:
  typedef GridPart GridPartType;
  typedef typename GridPartType::template Codim<0>::IteratorType IteratorType;
  typedef typename GridPartType :: GridType GridType;
  typedef typename GridPartType :: IndexSetType IndexSetType;
  typedef typename GridPartType :: IntersectionIteratorType IntersectionIteratorType;

  typedef typename IntersectionIteratorType :: Intersection IntersectionType;

  typedef typename GridType :: template Codim< 0 > :: Entity ElementType;
  typedef typename GridType :: template Codim< 0 > :: EntityPointer
    ElementPointerType;
  typedef typename ElementType::Geometry GeometryType;
  static const int dimension = 3; // GridType :: dimension;
  typedef typename GridPartType  :: IndexSetType  IndexSet ;
  typedef typename IndexSet  :: IndexType IndexType ;

  int node1;
  int node2;
  int node3;

  std::ofstream vertex;
  std::ofstream simplex;

private:
  const GridPartType &gridPart_;
  const IndexSetType &indexSet_;
  typedef Dune::GridFactory<OutsideHostGridType> GridFactory;
  GridFactory factory_;

  // create new vector holding vertex ids
  // typedef std::map< IndexType, int > VertexMapType ;
  typedef std::map< IndexType, std::pair<int,Dune::FieldVector<double, dimension> > > VertexMapType;

  VertexMapType vertexId ;
public:
  explicit SurfaceExtractor ( const GridPart &gridPart )
  : gridPart_( gridPart ),
    indexSet_( gridPart_.indexSet() ),
    factory_( )
  {}

  //! accumulate volume of all elements
  double volume ()
  {
    double result = 0.0;
    const IteratorType end = gridPart_.template end<0>();
    for( IteratorType it = gridPart_.template begin<0>(); it != end; ++it )
    {
      result += it->geometry().volume();
    }
    return result;
  }

  //! extract surface
  OutsideHostGridType* extractSurface ()
  {
    // initialize the node counters
    node1 = 0; node2 = 1; node3 = 2;

    // open file to hold the vertices
    vertex.open("vertex.dat");

    // specify the Dune grid format
    vertex << "DGF" << std::endl;
    vertex << std::endl;
    vertex << std::endl;

    vertex << "VERTEX" << std::endl;

    // open file to hold the simplices
    simplex.open("simplex.dat");
    simplex << "SIMPLEX" << std::endl;

    int nextLocalIndex = 0;

    // calculate local estimator
    const IteratorType end = gridPart_.template end<0>();
    for( IteratorType it = gridPart_.template begin<0>(); it != end; ++it )
    {
#if !BEMPP
      if (it->hasBoundaryIntersections())
#endif
        extractSurfaceLocal( *it, nextLocalIndex,3 );
    }

    // close the simplex file
    simplex << "#" << std::endl;
    simplex << std::endl;
    simplex << std::endl;

    simplex << "CUBE" << std::endl;

    // calculate local estimator
    for( IteratorType it = gridPart_.template begin<0>(); it != end; ++it )
    {
#if !BEMPP
      if (it->hasBoundaryIntersections())
#endif
        extractSurfaceLocal( *it, nextLocalIndex,4 );
    }

    // close the simplex file
    simplex << "#" << std::endl;
    simplex << std::endl;
    simplex << std::endl;

    simplex << "GRIDPARAMETER" << std::endl;
    simplex << "NAME surface" << std::endl;
    simplex << "REFINEMENTEDGE ARBITRARY" << std::endl;
    simplex << "#" << std::endl;
    simplex << std::endl;
    simplex << std::endl;

    simplex << "#" << std::endl;
    simplex << std::endl;

    simplex.close();

    // close the vertex file
    vertex << "#" << std::endl;
    vertex << std::endl;
    vertex << std::endl;
    vertex.close();

    // create the new dgf file
    std::system("cat vertex.dat simplex.dat > surface.dgf");
    return factory_.createGrid( );
  }

  //! caclulate error on element
  void extractSurfaceLocal ( const ElementType &entity, int &nextLocalIndex, int corners )
  {
    const typename ElementType :: Geometry &geometry = entity.geometry();

    // calculate face contribution
    IntersectionIteratorType end = gridPart_.iend( entity );
    for( IntersectionIteratorType it = gridPart_.ibegin( entity ); it != end; ++it )
    {
      const IntersectionType &intersection = *it;
      // if we not got an element neighbor
      if( intersection.boundary() )
      {
        const int face = intersection.indexInInside();
        extractSurfaceIntersection( intersection, entity, face, nextLocalIndex,corners );
        // extractSurfaceIntersection( intersection, entity );
      }
    }
  }


  template <class X>
  bool equal( const X& x1, const X& x2)
  {
    double norm = (x1-x2).two_norm();
    return norm < 1e-10;
  }

  //! caclulate error on boundary intersections
  void extractSurfaceIntersection ( const IntersectionType &intersection,
                                    const ElementType &inside, const int &faceNr,
                                    int &nextLocalIndex,
                                    int writecorners)
  {
#if HOLE_HACK
    // ignore boundaries on the boundary towards the origin
    if (intersection.centerUnitOuterNormal()*intersection.geometry().center() < 0.)
      return;
#endif

    const typename VertexMapType::iterator endVxMap = vertexId.end();
    // Dimension of the intersection
    const int dim = 3; // IntersectionType::dimension;


    const Dune::ReferenceElement< double, dim > & refElem =
                Dune::ReferenceElements< double, dim >::general( inside.type() );
    int corners = refElem.size( faceNr, 1 ,dim );

    if (corners != writecorners)
      return;

    const typename ElementType :: Geometry &geo = inside.geometry();
    std::vector<unsigned int> bib;
    Dune::FieldVector<double,dim> v0;
    Dune::FieldVector<double,dim> e1;
    Dune::FieldVector<double,dim> e2;
    for( int i = 0; i != corners; ++i )
    {
      // get vertex number (local)
      int vtxNr = refElem.subEntity( faceNr, 1, i, dim );
      // get the global index
      int vtxIndex = indexSet_.subIndex(inside,vtxNr,dim);
      Dune::FieldVector<double, dim> vtx = geo.corner( vtxNr );
      // have a look if that vertex has already been touched
      auto mapIterator = vertexId.find( vtxIndex );
      int node;
      if( mapIterator == endVxMap )
      {
        // if it hasn't, then insert into the map ...
        vertexId[ vtxIndex ] = std::make_pair(nextLocalIndex,vtx);
        // ... and print the vertex's coordinates into the vertex file
        for (int p=0;p<dim;++p)
          vertex << vtx[p] << " ";
        factory_.insertVertex( vtx );
        vertex << std::endl;
        // output the new node number to the simplex list
        node = nextLocalIndex;
        // set-up the node number to be used for the next new node
        ++nextLocalIndex;
      }
      else
      {
        // just add the ver
        node =mapIterator->second.first;
        assert( equal( vtx, mapIterator->second.second ) );
      }

      bib.push_back(node);
      if (bib.size()==1)
        v0 = vtx;
      else if (bib.size()==2)
      {
        e1 = vtx; e1 -= v0;
      }
      else if (bib.size()==3)
      {
        e2 = vtx; e2 -= v0;
      }
    }
    // new line for the simplex file
    // std::cout << "Inserting element " << bib << std::endl;
    const int dimworld = GridType :: dimensionworld ;

    Dune::GeometryType elementType(Dune::GeometryType::simplex, dimworld-1);

    // check if orientation of simplex is correct
    Dune::FieldVector<double,dim> n;
    n[0] = e1[1]*e2[2]-e1[2]*e2[1];
    n[1] = e1[2]*e2[0]-e1[0]*e2[2];
    n[2] = e1[0]*e2[1]-e1[1]*e2[0];
    if (intersection.centerUnitOuterNormal()*n < 0)
      std::swap(bib[1],bib[2]);
    simplex << bib[0] << " " << bib[1] << " " << bib[2];

    factory_.insertElement( elementType, bib );
    if (bib.size()==4)
    {
      std::vector<unsigned int> q(3);
      q[0] = bib[3];
      q[1] = bib[2];
      q[2] = bib[1];
      factory_.insertElement( elementType, q );
    }

    simplex << std::endl;
  }


  // #if 0
  //! caclulate error on boundary intersections
  void extractSurfaceIntersection ( const IntersectionType &intersection,
                              const ElementType &inside )
  {
    // Dimension of the intersection
    const int dim = IntersectionType::dimension;

    const typename IntersectionType :: Geometry &geo = intersection.geometry();

    if (dim == 2)
    {
      for( int i = 0; i != geo.corners(); ++i )
      {
        Dune::FieldVector<double, dim> bob = geo.corner(i);
	vertex << bob[0] << "  " << bob[1] << std::endl;
      }
      simplex << node1 << "  " << node2 << std::endl;
    }
    else if (dim == 3)
    {
      for( int i = 0; i != geo.corners(); ++i )
      {
        Dune::FieldVector<double, dim> bob = geo.corner(i);
	vertex << bob[0] << "  " << bob[1] << "  " << bob[2] << std::endl;
        factory_.insertVertex( bob );
      }
      simplex << node1 << "  " << node2 << "  " << node3 << std::endl;
      Dune::FieldVector<int, dim> bub;
      bub[ 0 ] = node1; bub[ 1 ] = node2; bub[ 2 ] = node3;
      factory_.insertElement( simplex, bub );
    }
    else
    {
      DUNE_THROW(Dune::Exception, "Bad surface extractor dimension.");
    }
    node1+=dim; node2+=dim; node3+=dim;
  }
  // #endif
};

#endif // #ifndef SURFACEEXTRACTOR_HH

