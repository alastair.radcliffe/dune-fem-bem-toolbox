#ifndef ELLIPTIC_HH
#define ELLIPTIC_HH

#include <dune/common/fmatrix.hh>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/stencil.hh>

#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem_fem_coupling/fem_fem_coupling.hh>

#include "../../../case_studies/charged_droplet/source/addpressure.hh"


template<bool>
struct boundaryForcing
{
  //! comment
  template< class ConstraintsType, class DiscreteFunctionType, class BoundaryDataFunctionSpaceType >
  static void apply( const ConstraintsType &constraints, const DiscreteFunctionType &u, const BoundaryDataFunctionSpaceType &s, DiscreteFunctionType &w )
  {
    // apply boundary forcing through the constraints class
    typedef typename BoundaryDataFunctionSpaceType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = s.space().gridPart();

    typedef AddPressure< BoundaryDataFunctionSpaceType, DiscreteFunctionType > AddPressureType;
    typedef Dune::Fem::LocalFunctionAdapter< AddPressureType > AddPressureFunctionType;
    AddPressureType addpressure( s );
    AddPressureFunctionType addPressureFunction( "addpressure", addpressure, otherGridPart_ );

    // apply boundary forcing through the constraints class
    constraints( u, addPressureFunction, w );
    // std::cout << "Applied surface forcing." << std::endl;
  }
};

template<>
struct boundaryForcing<false>
{
  //! null method for applying the boundary forcing
  template< class ConstraintsType, class DiscreteFunctionType, class BoundaryDataFunctionSpaceType >
  static void apply( const ConstraintsType &constraints, const DiscreteFunctionType &u, const BoundaryDataFunctionSpaceType &s, DiscreteFunctionType &w )
  {}
};

template<bool>
struct incidentForcing
{
  //! comment
  template< class ConstraintsType, class DiscreteFunctionType >
  static void apply( const bool &doit, const ConstraintsType &constraints, const DiscreteFunctionType &u, DiscreteFunctionType &w )
  {
    // apply boundary forcing through the constraints class
    if( doit )
    {
    constraints.incident( u, w );
    std::cout << "Applied surface incident forcing." << std::endl;
    }
  }
};

template<>
struct incidentForcing<false>
{
  //! null method for applying the boundary forcing
  template< class ConstraintsType, class DiscreteFunctionType >
  static void apply( const bool &doit, const ConstraintsType &constraints, const DiscreteFunctionType &u, DiscreteFunctionType &w )
  {}
};

// EllipticOperator
// ----------------

template< class DiscreteFunction, class Model >
struct EllipticOperator
: public virtual Dune::Fem::Operator< DiscreteFunction >
{
  typedef DiscreteFunction DiscreteFunctionType;
  typedef Model            ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename ModelType::BoundaryDataFunctionSpaceType BoundaryDataFunctionSpaceType;
  typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
  typedef typename LocalFunctionType::RangeType RangeType;
  typedef typename LocalFunctionType::JacobianRangeType JacobianRangeType;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity       EntityType;
  typedef typename EntityType::Geometry       GeometryType;

  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;

  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;

  typedef Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GridPartType FixedGridPartType;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GrydPartType GrydPartType;

  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;

  //! type of Dirichlet constraints
  typedef Dune::CouplingConstraints< ModelType, DiscreteFunctionSpaceType, false > ConstraintsType;

public:
  //! contructor
  EllipticOperator ( const ModelType &model, const DiscreteFunctionSpaceType &space )
  : model_( model )
  , constraints_( model, space )
  {}

  // prepare the solution vector
  template <class Function>
  void prepare( const Function &func, DiscreteFunctionType &u )
  {
    // set boundary values for solution
    constraints()( func, u );
  }

  // initialize the coupling
  void coupleInit( const GridPartType &gridPart )
  {
#if !DIAG_PLOT
    // create the glue object
    constraints()( );
#endif
  }

  // setup the glue coupling
  template <class Function, class OtherFunction>
  void coupleSetup( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    constraints().updateCouplingDofs( func, funk, u );
  }

  // prepare the glue coupling
  template <class Function, class OtherFunction>
  void coupling( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    // set coupling values for solution
    constraints()( func, funk, u );
  }

  void initializeInitialSolution( const DiscreteFunction &u, DiscreteFunction &w)
  {
    constraints().initializeInitialSolution(u,w);
  }

  // reset
  void reset()
  {
    // reset
    constraints().reset();
  }

  //! application operator
  virtual void
  operator() ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const;
  virtual void
  operator() ( const DiscreteFunctionType &u, const DiscreteFunctionType &v, DiscreteFunctionType &w ) const;
  virtual void
  operator() ( const DiscreteFunctionType &u, const BoundaryDataFunctionSpaceType &s, DiscreteFunctionType &w ) const;
  virtual void
  operator() ( const DiscreteFunctionType &u, const BoundaryDataFunctionSpaceType &s, const DiscreteFunctionType &v, DiscreteFunctionType &w ) const;

protected:
  const ModelType &model () const { return model_; }
  const ConstraintsType &constraints () const { return constraints_; }

private:
  ModelType model_;
  ConstraintsType constraints_;
};

// DifferentiableEllipticOperator
// ------------------------------

template< class JacobianOperator, class Model >
struct DifferentiableEllipticOperator
: public EllipticOperator< typename JacobianOperator::DomainFunctionType, Model >,
  public Dune::Fem::DifferentiableOperator< JacobianOperator >
{
  typedef EllipticOperator< typename JacobianOperator::DomainFunctionType, Model > BaseType;

  typedef JacobianOperator JacobianOperatorType;

  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  typedef typename BaseType::ModelType ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
  typedef typename LocalFunctionType::RangeType RangeType;
  typedef typename LocalFunctionType::JacobianRangeType JacobianRangeType;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity       EntityType;
  typedef typename EntityType::Geometry       GeometryType;

  typedef typename DiscreteFunctionSpaceType :: DomainType DomainType;

  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;

  typedef typename BaseType::QuadratureType QuadratureType;

public:
  //! contructor
  DifferentiableEllipticOperator ( const ModelType &model, const DiscreteFunctionSpaceType &space, bool sw=true )
  : BaseType( model, space )
  {}

  //! method to setup the jacobian of the operator for storage in a matrix
  void jacobian ( const DiscreteFunctionType &u, JacobianOperatorType &jOp ) const;

protected:
  using BaseType::model;
  using BaseType::constraints;
};

// Implementation of EllipticOperator
// ----------------------------------

template< class DiscreteFunction, class Model >
void EllipticOperator< DiscreteFunction, Model >
  ::operator() ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const
{
  // clear destination
  w.clear();

  // get discrete function space
  const DiscreteFunctionSpaceType &dfSpace = w.space();

  // iterate over grid
  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    // get entity (here element)
    const EntityType &entity = *it;
    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    // get local representation of the discrete functions
    const LocalFunctionType uLocal = u.localFunction( entity );
    LocalFunctionType wLocal = w.localFunction( entity );

    // obtain quadrature order
    const int quadOrder = uLocal.order() + wLocal.order();

    { // element integral
      QuadratureType quadrature( entity, quadOrder );
      const size_t numQuadraturePoints = quadrature.nop();
      for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
      {
        const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
        const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

        RangeType vu;
        uLocal.evaluate( quadrature[ pt ], vu );

        JacobianRangeType du;
        uLocal.jacobian( quadrature[ pt ], du );

        // compute mass contribution (studying linear case so linearizing around zero)
        RangeType avu( 0 );
        model().linSource( vu, entity, quadrature[ pt ], vu, du, avu );
        avu *= weight;
        // add to local functional wLocal.axpy( quadrature[ pt ], avu );
        JacobianRangeType adu( 0 );
#if !DROP
        // apply diffusive flux
        model().diffusiveFlux( entity, quadrature[ pt ], vu, du, adu );
        adu *= weight;
#endif

        // add to local function
        wLocal.axpy( quadrature[ pt ], avu, adu );
      }
    }
  }

  // add normal derivative of an incident field if present
  incidentForcing<true>::apply( model().assembleRHSwithOperator(), constraints_, u, w );

  // communicate data (in parallel runs)
  w.communicate();

  // apply constraints, e.g. Dirichlet contraints, to the result
  constraints()( u, w );
}

// Implementation of EllipticOperator with Boundary Data
// -----------------------------------------------------

template< class DiscreteFunction, class Model >
void EllipticOperator< DiscreteFunction, Model >
  ::operator() ( const DiscreteFunctionType &u, const BoundaryDataFunctionSpaceType &s, DiscreteFunctionType &w ) const
{
  // clear destination
  w.clear();

  // get discrete function space
  const DiscreteFunctionSpaceType &dfSpace = w.space();

  // iterate over grid
  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    // get entity (here element)
    const EntityType &entity = *it;
    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    // get local representation of the discrete functions
    const LocalFunctionType uLocal = u.localFunction( entity );
    LocalFunctionType wLocal = w.localFunction( entity );

    // obtain quadrature order
    const int quadOrder = uLocal.order() + wLocal.order();

    { // element integral
      QuadratureType quadrature( entity, quadOrder );
      const size_t numQuadraturePoints = quadrature.nop();
      for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
      {
        const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
        const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

        RangeType vu;
        uLocal.evaluate( quadrature[ pt ], vu );

        JacobianRangeType du;
        uLocal.jacobian( quadrature[ pt ], du );

        // compute mass contribution (studying linear case so linearizing around zero)
        RangeType avu( 0 );
        model().linSource( vu, entity, quadrature[ pt ], vu, du, avu );
        avu *= weight;
        // add to local functional wLocal.axpy( quadrature[ pt ], avu );
        JacobianRangeType adu( 0 );
#if !DROP
        // apply diffusive flux
        model().diffusiveFlux( entity, quadrature[ pt ], vu, du, adu );
        adu *= weight;
#endif

        // add to local function
        wLocal.axpy( quadrature[ pt ], avu, adu );
      }
    }
  }

  // add normal derivative of an incident field if present
  incidentForcing<true>::apply( model().assembleRHSwithOperator(), constraints_, u, w );

  // add boundary forcing if present
  boundaryForcing<Model::applyBoundaryForcing>::apply( constraints_, u, s, w );

  // communicate data (in parallel runs)
  w.communicate();

  // apply constraints, e.g. Dirichlet contraints, to the result
  constraints()( u, w );
}

// Implementation of DifferentiableEllipticOperator
// ------------------------------------------------

template< class JacobianOperator, class Model >
void DifferentiableEllipticOperator< JacobianOperator, Model >
  ::jacobian ( const DiscreteFunctionType &u, JacobianOperator &jOp ) const
{
  typedef typename JacobianOperator::LocalMatrixType LocalMatrixType;
  typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;

  const DiscreteFunctionSpaceType &dfSpace = u.space();

  Dune::Fem::DiagonalStencil<DiscreteFunctionSpaceType,DiscreteFunctionSpaceType> stencil(dfSpace,dfSpace);
  jOp.reserve(stencil);
  jOp.clear();

  const int blockSize = dfSpace.localBlockSize; // is equal to 1 for scalar functions
  std::vector< typename LocalFunctionType::RangeType > phi( dfSpace.blockMapper().maxNumDofs()*blockSize );
  std::vector< typename LocalFunctionType::JacobianRangeType > dphi( dfSpace.blockMapper().maxNumDofs()*blockSize );

  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;
    const GeometryType &geometry = entity.geometry();

    const LocalFunctionType uLocal = u.localFunction( entity );
    LocalMatrixType jLocal = jOp.localMatrix( entity, entity );

    const BasisFunctionSetType &baseSet = jLocal.domainBasisFunctionSet();
    const unsigned int numBasisFunctions = baseSet.size();

    QuadratureType quadrature( entity, 2*dfSpace.order() );
    const size_t numQuadraturePoints = quadrature.nop();
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
      const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

      // evaluate all basis functions at given quadrature point
      baseSet.evaluateAll( quadrature[ pt ], phi );

      // evaluate jacobians of all basis functions at given quadrature point
      baseSet.jacobianAll( quadrature[ pt ], dphi );

      // get value for linearization
      RangeType u0;
      JacobianRangeType jacU0;
      uLocal.evaluate( quadrature[ pt ], u0 );
      uLocal.jacobian( quadrature[ pt ], jacU0 );

      RangeType aphi( 0 );
      JacobianRangeType adphi( 0 );
      for( unsigned int localCol = 0; localCol < numBasisFunctions; ++localCol )
      {
        // if mass terms or right hand side is present
        model().linSource( u0, entity, quadrature[ pt ], phi[ localCol ], dphi[ localCol ], aphi );

        // if gradient term is present
        model().linDiffusiveFlux( u0, jacU0, entity, quadrature[ pt ], phi[ localCol ], dphi[ localCol ], adphi );

        // get column object and call axpy method
        jLocal.column( localCol ).axpy( phi, dphi, aphi, adphi, weight );
      }
    }
  } // end grid traversal

  // apply constraints to matrix operator
#if DROP || !DEFORM
  constraints().applyToOperator( jOp );
#endif
  jOp.communicate();
}

// Implementation of Special Modified EllipticOperator
// ---------------------------------------------------

template< class DiscreteFunction, class Model >
void EllipticOperator< DiscreteFunction, Model >
::operator() ( const DiscreteFunctionType &u, const DiscreteFunctionType &v, DiscreteFunctionType &w ) const
{
  // clear destination
  w.clear();

  // get discrete function space
  const DiscreteFunctionSpaceType &dfSpace = w.space();

  // iterate over grid
  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    // get entity (here element)
    const EntityType &entity = *it;
    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    // get local representation of the discrete functions
    const LocalFunctionType uLocal = u.localFunction( entity );
    const LocalFunctionType vLocal = v.localFunction( entity );
    LocalFunctionType wLocal = w.localFunction( entity );

    // obtain quadrature order
    const int quadOrder = uLocal.order() + wLocal.order();

    { // element integral
      QuadratureType quadrature( entity, quadOrder );
      const size_t numQuadraturePoints = quadrature.nop();
      for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
      {
        const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
        const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

        RangeType vu;
        uLocal.evaluate( quadrature[ pt ], vu );

        RangeType vv;
        vLocal.evaluate( quadrature[ pt ], vv );

        JacobianRangeType du;
        uLocal.jacobian( quadrature[ pt ], du );

        // compute mass contribution (studying linear case so linearizing around zero)
        RangeType avu( 0 );
        model().linSource( vv, entity, quadrature[ pt ], vu, du, avu );
        avu *= weight;
        // add to local functional wLocal.axpy( quadrature[ pt ], avu );
        JacobianRangeType adu( 0 );
#if !DROP
        // apply diffusive flux
        model().diffusiveFlux( entity, quadrature[ pt ], vu, du, adu );
        adu *= weight;
#endif

        // add to local function
        wLocal.axpy( quadrature[ pt ], avu, adu );
      }
    }
  }

  // add normal derivative of an incident field if present
  incidentForcing<true>::apply( model().assembleRHSwithOperator(), constraints_, u, w );

  // communicate data (in parallel runs)
  w.communicate();

  // apply constraints, e.g. Dirichlet contraints, to the result
  constraints()( u, w );
}

// Implementation of Special Modified EllipticOperator with Boundary Data
// ----------------------------------------------------------------------

template< class DiscreteFunction, class Model >
void EllipticOperator< DiscreteFunction, Model >
::operator() ( const DiscreteFunctionType &u, const BoundaryDataFunctionSpaceType &s, const DiscreteFunctionType &v, DiscreteFunctionType &w ) const
{
  // clear destination
  w.clear();

  // get discrete function space
  const DiscreteFunctionSpaceType &dfSpace = w.space();

  // iterate over grid
  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    // get entity (here element)
    const EntityType &entity = *it;
    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    // get local representation of the discrete functions
    const LocalFunctionType uLocal = u.localFunction( entity );
    const LocalFunctionType vLocal = v.localFunction( entity );
    LocalFunctionType wLocal = w.localFunction( entity );

    // obtain quadrature order
    const int quadOrder = uLocal.order() + wLocal.order();

    { // element integral
      QuadratureType quadrature( entity, quadOrder );
      const size_t numQuadraturePoints = quadrature.nop();
      for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
      {
        const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
        const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

        RangeType vu;
        uLocal.evaluate( quadrature[ pt ], vu );

        RangeType vv;
        vLocal.evaluate( quadrature[ pt ], vv );

        JacobianRangeType du;
        uLocal.jacobian( quadrature[ pt ], du );

        // compute mass contribution (studying linear case so linearizing around zero)
        RangeType avu( 0 );
        model().linSource( vv, entity, quadrature[ pt ], vu, du, avu );
        avu *= weight;
        // add to local functional wLocal.axpy( quadrature[ pt ], avu );
        JacobianRangeType adu( 0 );
#if !DROP
        // apply diffusive flux
        model().diffusiveFlux( entity, quadrature[ pt ], vu, du, adu );
        adu *= weight;
#endif

        // add to local function
        wLocal.axpy( quadrature[ pt ], avu, adu );
      }
    }
  }

  // add normal derivative of an incident field if present
  incidentForcing<true>::apply( model().assembleRHSwithOperator(), constraints_, u, w );

  // add boundary forcing if present
  boundaryForcing<Model::applyBoundaryForcing>::apply( constraints_, u, s, w );

  // communicate data (in parallel runs)
  w.communicate();

  // apply constraints, e.g. Dirichlet contraints, to the result
  constraints()( u, w );
}

#endif // #ifndef ELLIPTIC_HH
