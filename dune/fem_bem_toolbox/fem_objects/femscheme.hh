#ifndef ELLIPT_FEMSCHEME_HH
#define ELLIPT_FEMSCHEME_HH

// iostream includes
#include <iostream>
#include <complex>

// include discrete function space
#include <dune/fem/space/lagrange.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/common/adaptmanager.hh>

// include discrete function
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>

#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
/*********************************************************/

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include <dune/fem_fem_coupling/surfaceExtractor.hh>

// local includes
#include "probleminterface.hh"

#include "model.hh"

#include "rhs.hh"
#include "elliptic.hh"
#include <dune/fem_bem_toolbox/special_solvers/solver.hh>

// DataOutputParameters
// --------------------

struct DataOutputParameters
: public Dune::Fem::LocalParameter< Dune::Fem::DataOutputParameters, DataOutputParameters >
{
  DataOutputParameters ( const int step, const std::string &preprefix = "" )
    : step_( step ),
      pre_(preprefix)
  {}

  DataOutputParameters ( const DataOutputParameters &other )
  : step_( other.step_ )
  {}

  std::string prefix () const
  {
    std::stringstream s;
    s << pre_ << "-poisson-" << step_ << "-";
    return s.str();
  }

private:
  int step_;
  std::string pre_;
};

// FemScheme
//----------

/*******************************************************************************
 * template arguments are:
 * - GridPart: the part of the grid used to tessellate the
 *             computational domain
 * - Model: description of the data functions and methods required for the
 *          elliptic operator (massFlux, diffusionFlux)
 *     Model::ProblemType boundary data, exact solution,
 *                        and the type of the function space
 *******************************************************************************/
template < class Model, SolverType solver=istl >
class FemScheme
{
public:
  //! type of the mathematical model
  typedef Model ModelType ;

  //! grid view (e.g. leaf grid view) provided in the template argument list
  typedef typename ModelType::GridPartType GridPartType;

  typedef Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GridPartType FixedGridPartType;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GrydPartType GrydPartType;

  typedef typename Dune::FemFemCoupling::Glue< GT, false >::OutsideHostGridType OutsideHostGridType;

  //! type of underyling hierarchical grid needed for data output
  typedef typename GridPartType::GridType GridType;

  //! type of function space (scalar functions, f: \Omega -> R)
  typedef typename ModelType :: FunctionSpaceType   FunctionSpaceType;

  //! choose type of discrete function space
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPartType, POLORDER > DiscreteFunctionSpaceType;

  // choose type of discrete function, Matrix implementation and solver implementation
#if COMPLEX
  typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;
  typedef Dune::Fem::ISTLLinearOperator< DiscreteFunctionType, DiscreteFunctionType > LinearOperatorType;
  typedef Dune::Fem::ISTLCGOp< DiscreteFunctionType, LinearOperatorType > LinearInverseOperatorType;
  // typedef Dune::Fem::ISTLGMResOp< DiscreteFunctionType, LinearOperatorType > LinearInverseOperatorType;
#else
  typedef Solvers<DiscreteFunctionSpaceType,solver,true> SolverType;
  typedef typename SolverType::DiscreteFunctionType DiscreteFunctionType;
  typedef typename SolverType::LinearOperatorType LinearOperatorType;
  typedef typename SolverType::LinearInverseOperatorType LinearInverseOperatorType;
#endif

  /*********************************************************/

  //! define Laplace operator
  typedef DifferentiableEllipticOperator< LinearOperatorType, ModelType > EllipticOperatorType;

  //! type of surface extractor
  typedef SurfaceExtractor< GridPartType, OutsideHostGridType > SurfaceExtractorType ;

  FemScheme( GridPartType &gridPart,
             const ModelType& implicitModel, const std::string prefix="" )
    : implicitModel_( implicitModel ),
      gridPart_( gridPart ),
      discreteSpace_( gridPart_ ),
      solution_( prefix+"solution", discreteSpace_ ),
      polution_( "polution", discreteSpace_ ),
      incident_( "incident", discreteSpace_ ),
      rhs_( "rhs", discreteSpace_ ),
      // the elliptic operator (implicit)
      implicitOperator_( implicitModel_, discreteSpace_ ),
      // create linear operator (domainSpace,rangeSpace)
      linearOperator_( "assempled elliptic operator", discreteSpace_, discreteSpace_ ),
      // tolerance for iterative solver
      solverEps_( Dune::Fem::Parameter::getValue< double >( "poisson.solvereps", 1e-8 ) ),
      // surface extractor
      surfExt_( gridPart ),
      first_(true)
  {
    // set all DoF to zero
    solution_.clear();
    polution_.clear();
    incident_.clear();
    // create the glue object
    implicitOperator_.coupleInit( gridPart );
  }

  const ModelType& model() const
  {
    return implicitModel_;
  }

  bool useGmres()
  {
    return implicitModel_.useGmres();
  }

  DiscreteFunctionType &solution()
  {
    return solution_;
  }
  const DiscreteFunctionType &solution() const
  {
    return solution_;
  }

  DiscreteFunctionType &incident()
  {
    return incident_;
  }
  const DiscreteFunctionType &incident() const
  {
    return incident_;
  }

  DiscreteFunctionType &rhs()
  {
    return rhs_;
  }
  const DiscreteFunctionType &rhs() const
  {
    return rhs_;
  }

  DiscreteFunctionSpaceType &discreteSpace()
  {
    return discreteSpace_;
  }
  const DiscreteFunctionSpaceType &discreteSpace() const
  {
    return discreteSpace_;
  }

  // flag whether the solution is different from its previous value
  bool stop()
  {
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    NormType norm( gridPart_ );
    double error = std::real(norm.distance( solution_, polution_ ));
    double magnitude = std::real(norm.norm( solution_ ));
    // std::cout << "Iteration error = " << error << " = " << 100. * error / magnitude << " % of " << magnitude << std::endl;
    polution_.assign( solution_ );
    return error < 0.0001 * magnitude;
  }

  template <class OtherDiscreteFunctionType>
  //! sotup the right hand side and physical boundaries
  void prepare(OtherDiscreteFunctionType &otherSolution_)
  {
    if (first_ || !useGmres() )
    {
#if !DROP
      // set boundary values on physical boundary for solution
      implicitOperator_.prepare( implicitModel_.dirichletBoundary(), solution_ );
#endif

      // assemble rhs
      if( implicitModel_.assembleRHSwithOperator() )
      {
        // first bung the incident wave field into a variable
        fillRHS ( implicitModel_.rightHandSide(), incident_ );

        // now apply the operator to this incident wave field to get the rhs
        implicitOperator_( incident_, rhs_ );
      }
      else
      {
        assembleRHS ( implicitModel_.rightHandSide(), rhs_ );
      }

      // apply constraints, e.g. Dirichlet contraints, to the result
      implicitOperator_.prepare( solution_, rhs_ );
      first_  = false;
#if DROP
      // for some reason rhs gets corrupted with nans the any second time around, so just wipe it
      // (the only values it needs get filled in by the coupling in the next step anyway ...)
      rhs_.clear();
#endif
    }
    else
      rhs_.clear();
    // apply coupling constraints
    implicitOperator_.coupling( solution_, otherSolution_, rhs_ );
    rhs_.communicate();
    implicitOperator_.prepare( rhs_, solution_ );
  }

  void preparePartOne()
  {
    // set boundary values on physical boundary for solution
    implicitOperator_.prepare( implicitModel_.dirichletBoundary(), solution_ );
  }

  void preparePartThree()
  {
    // apply constraints, e.g. Dirichlet contraints, to the result
    implicitOperator_.prepare( solution_, rhs_ );
  }

  template <class OtherDiscreteFunctionType>
  //! set-up the rhs for the coupling values
  void setup(OtherDiscreteFunctionType &otherSolution_)
  {
    // apply coupling constraints
    implicitOperator_.coupleSetup( solution_, otherSolution_, rhs_ );
  }

  //! reset
  void reset()
  {
    // reset
    implicitOperator_.reset();
    // set all DoF to a recognizable value for debugging
    // solution_.clear(value);
  }

  //! compute volume
  double volume()
  {
    return surfExt_.volume();
  }

  //! extract surface
  OutsideHostGridType* extractSurface()
  {
    return surfExt_.extractSurface();
  }

  //! solve the system
  void solve ( bool assemble )
  {
    if( assemble )
    {
      // assemble linear operator (i.e. setup matrix)
      implicitOperator_.jacobian( solution_ , linearOperator_ );
    }

    // if( !useGmres() )
    //   implicitOperator_.initializeInitialSolution(rhs_,solution_);

    // inverse operator using linear operator
    LinearInverseOperatorType invOp( linearOperator_, solverEps_, solverEps_ );
    // solve system
    invOp( rhs_, solution_ );

    // fillRHS ( implicitModel_.rightHandSide(), solution_ );

    // form the total field (in the incident variable) to give to bempp
    if( implicitModel_.assembleRHSwithOperator() )
    {
      incident_ += solution_;
    }
  }

protected:
  SurfaceExtractorType surfExt_;  // surface extractor

  const ModelType& implicitModel_;   // the mathematical model

  GridPartType  &gridPart_;         // grid part(view), e.g. here the leaf grid the discrete space is build with

  DiscreteFunctionSpaceType discreteSpace_; // discrete function space
  DiscreteFunctionType solution_;   // the unknown
  DiscreteFunctionType polution_;   // the previous solution
  DiscreteFunctionType incident_;   // the incident field
  DiscreteFunctionType rhs_;        // the right hand side

  EllipticOperatorType implicitOperator_; // the implicit operator

  LinearOperatorType linearOperator_;  // the linear operator (i.e. jacobian of the implicit)

  const double solverEps_ ; // eps for linear solver
  bool first_;
};

#endif // end #if ELLIPT_FEMSCHEME_HH
