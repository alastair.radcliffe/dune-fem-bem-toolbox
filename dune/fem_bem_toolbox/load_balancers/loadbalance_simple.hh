#ifndef LOADBALANCE_SIMPLE_HH
#define LOADBALANCE_SIMPLE_HH

/********************************************************************
 *
 *  Simple repartition handle for ALUGrid
 *
 ********************************************************************/
template< class Grid >
struct SimpleLoadBalanceHandle
{
  SimpleLoadBalanceHandle ( const Grid &grid, const bool fake = false )
  : maxRank_( grid.comm().size() )
  , fake_( fake )
  {}

  /** this method is called before invoking the re-partition
      method on the grid, to check if the user defined
      partitioning needs to be readjusted */
  bool repartition ()
  {
    // only do special repartitioning if the rank is divisible by 2
    return ( maxRank_ != 1 ) && ( ( maxRank_%2 == 0 ) || ( maxRank_ == 3 ) );
  }

  /** This is the method, called from the grid for each macro element.
      It returns the rank to which the element is to be moved. */
  template < class ReceivedElement >
  int operator()( const ReceivedElement &element, const double skipRadius = -1.0 ) const
  {
    auto w = element.geometry().center();

    // don't specify a rank for any element within a skip radius (if set)
    if( w.two_norm() < skipRadius ) return -1;

    int rank = 0;
    if( fake_ )
    {
      // "fake" load balancing just puts everything onto rank 0
      rank = 0;
    }
    else if( maxRank_ > 11 )
    {
      double ang = std::atan2(std::abs(w[0]),w[1]);
      if ( ang > 8.0*M_PI/maxRank_ ) rank += 8;
      else if ( ang > 4.0*M_PI/maxRank_ ) rank += 4;
      if ( w[2] > 0 ) rank += 2;
      if ( w[0] > 0 ) rank += 1;
    }
    else if( maxRank_ == 8 )
    {
      if ( w[1] > 0 ) rank += 4;
      if ( w[2] > 0 ) rank += 2;
      if ( w[0] > 0 ) rank += 1;
    }
    else if( maxRank_ == 6 )
    {
      double ang = std::atan2(std::abs(w[0]),w[1]);
      if ( ang > 4.0*M_PI/maxRank_ ) rank += 4;
      else if ( ang > 2.0*M_PI/maxRank_ ) rank += 2;
      if ( w[0] > 0 ) rank += 1;
    }
    else if( maxRank_ == 4 )
    {
      if ( w[2] > 0 ) rank += 2;
      if ( w[0] > 0 ) rank += 1;
    }
    else if( maxRank_ == 3 )
    {
      double ang = std::atan2(std::abs(w[0]),w[1]);
      if ( ang > 2.0*M_PI/maxRank_ ) rank += 2;
      else if ( ang > 1.0*M_PI/maxRank_ ) rank += 1;
    }
    else if( maxRank_ == 2 )
    {
      if ( w[0] > 0 ) rank += 1;
    }
    return rank;
  }

  /** This method can simply return false, in which case ALUGrid
      will internally compute the required information through
      some global communication. To avoid this overhead the user
      can provide the ranks of particians from which elements will
      be moved to the calling partitian. */
  bool importRanks( std::set<int> &ranks) const { return false; }
private:
  int maxRank_;
  const bool fake_;
};

/********************************************************************
 *
 *  Simple weights used with ALUGrid load balancing
 *
 ********************************************************************/
template< class Grid >
struct SimpleLoadBalanceWeights
{
  typedef typename Grid :: Traits :: template Codim<0> :: Entity Element;
  typedef typename Grid :: Traits :: HierarchicIterator HierarchicIterator;

  SimpleLoadBalanceWeights ( const Grid &grid )
    : grid_( grid )
  {}

  /** This method is called for each macro element to determine the weight
      in the dual graph. Here, we compute the number of tree elements underneeth
      the macro element. */
  long int operator()( const Element &element ) const
  {
    const int mxl = grid_.maxLevel();
    const HierarchicIterator end = element.hend( mxl );
    int leafElements = 1 ;
    for( HierarchicIterator it = element.hbegin( mxl ); it != end; ++it )
      ++ leafElements ;
    return leafElements ;
  }

protected:
  const Grid& grid_;
};

#endif // #ifndef LOADBALNCE_HH
