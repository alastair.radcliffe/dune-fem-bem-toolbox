#ifndef PRIVATEBEMOPERATOR_HH
#define PRIVATEBEMOPERATOR_HH

#include "bemoperator.hh"

#include <iostream>

// PrivateBEMOperator
// ----------------

template< class DomainFunction, class RangeFunction, class Model >
struct PrivateBemOperator
{
  typedef DomainFunction   DiscreteFunctionType;
  typedef RangeFunction    P0DiscreteFunctionType;
  typedef Model            ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename P0DiscreteFunctionType::DiscreteFunctionSpaceType P0DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;
  typedef typename GridPartType::GridType GridType;

  //! type of Dirichlet constraints
  typedef Dune::CouplingConstraints< ModelType, DiscreteFunctionSpaceType > ConstraintsType;

  typedef BemOperator< DomainFunction,RangeFunction,Model,ConstraintsType > BaseType;

  typedef Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue< GT >::GridPartType FixedGridPartType;
  typedef typename Dune::FemFemCoupling::Glue< GT >::GrydPartType GrydPartType;

public:

  //! contructor
  PrivateBemOperator ( const ModelType &model, const DiscreteFunctionSpaceType &space, const DiscreteFunctionSpaceType &fullSpace  )
  : model_( model )
  , space_(space)
  , shared_( fullSpace.gridPart().comm().size() != 1 )
  , fullSpace_(fullSpace)
  , fullGridPart_( fullSpace.gridPart() )
  , fullP0Space_( fullSpace.gridPart() )
  // which rank is allowed to fill in a particular dof (of the whole surface mesh)
  , dofRank_( "dofRank", fullSpace )
  , sharedP0Data_("sharedNeuData", true, fullP0Space_ )
  , sharedP1Data_("sharedDirData", true, fullSpace )
  , constraints_( new ConstraintsType( model, space ) )
  , fullConstraints_( new ConstraintsType( model, fullSpace, Dune::Fem::MPIManager::rank() ) )
  , basetype_( new BaseType(model,fullSpace) )
  {
    // initialize all full surface grid solution dofs to be matched up to surface section solution dofs by rank 0
    dofRank_.clear();

    if(!shared_)
      std::cout << "Using private bempp operator." << std::endl;
    else
      std::cout << "Using shared bempp operator." << std::endl;

    // allow first gridpart address to be passed through to glue home for eventual glue object creation
    basetype_->coupleInit( fullConstraints_ );

    // if not sharing, DON'T pass a second gridpart object address through to glue home, and thus avoid a second glue object
    // being created, the constraints here (for the bem scheme) will then automatically use the first glue object created
    // if(!shared_) return;

    // create a second glue object (by giving glue home a second gridpart address) for the exchange of data
    // from the full to partitioned surface grids
    constraints()( );
  }
  ~PrivateBemOperator()
  {
  }

  DiscreteFunctionType &dofRank()
  {
    return dofRank_;
  }
  const DiscreteFunctionType &dofRank() const
  {
    return dofRank_;
  }

  DiscreteFunctionType &fullDirData()
  {
    return sharedP1Data_;
  }
  const DiscreteFunctionType &fullDirData() const
  {
    return sharedP1Data_;
  }

  P0DiscreteFunctionType &fullSolution()
  {
    return sharedP0Data_;
  }
  const P0DiscreteFunctionType &fullSolution() const
  {
    return sharedP0Data_;
  }

  // set up which ranks are allowed to fill in which full surface solution dof
  template< class LoadBalancer >
  double setDofRank( const LoadBalancer &loadbal )
  {
    // if(!shared) return 0;

    // typedef typename Grid :: Traits :: template Codim<0> :: Entity IteratorType;
    typedef typename GridPartType::template Codim<0>::IteratorType IteratorType;

    // initialize an area checker
    double result = 0.0;

    // run through all the elements of the full (un-partitioned) surface grid
    const IteratorType end = fullGridPart_.template end<0>();
    for( IteratorType it = fullGridPart_.template begin<0>(); it != end; ++it )
    {
      // keep track of the total surface area as a check
      result += it->geometry().volume();

      // determine the rank the load balancer would have given this element
      int destRank = loadbal.operator()( *it );
      setDof( *it, destRank );
    }
    return result;
  }

  //! set the entity dofs to the value of the rank (if the greatest)
  template< class EntityType >
  void setDof( const EntityType &entity, const int dest )
  {
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteSpaceType;
    typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename DiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename DiscreteSpaceType :: RangeType RangeType;

    // get local functions of result
    LocalFunctionType wLocal = dofRank_.localFunction( entity );

    const LagrangePointSetType &lagrangePointSet = dofRank_.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    // typedef typename GridFunctionType :: RangeType RangeAType;

    int localDof = 0;
    const unsigned int localBlockSize = dofRank_.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    dofRank_.space().blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( unsigned int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
      RangeType phi( dest );

      // store result to dof vector
      for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
      {
        // whichever rank is the greatest gets to set the dof
        if( phi[ l ] > wLocal[ localDof ] ) wLocal[ localDof ] = phi[ l ];
      }
    }
  }

  // setup the glue coupling
  template <class Function, class OtherFunction>
  void coupleSetup( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    // set coupling values for solution
    constraints().updateCouplingDofs( func, funk, u );
    // BaseType::coupleSetup( func, *(memoryPtr_->get()), func  );
    basetype_->coupleSetup( dofRank_, func, sharedP1Data_ );
  }

  // prepare the glue coupling
  template <class Function, class OtherFunction>
  void coupling( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    // set coupling values for solution
    // basetype_->coupling( func, funk, u );
    constraints()( func, funk, u );
  }

  // prepare the glue coupling
  template <class Function, class OtherFunction>
  void collate( const Function &func, const OtherFunction &funk )
  {
    // set coupling values for solution
    // BaseType::constraints()( func, funk, u );
    fullConstraints()( dofRank_, funk, sharedP1Data_ );
  }

  // Construct the right-hand-side grid function
  void rhsPrepare( const bool &gmres, DomainFunction &rhs ) const
  {
    // form full rhs vector from rhs vector on different surface sections
    fullConstraints()( dofRank_, rhs, sharedP1Data_ );

    // call underlying rhs prepare for this full rhs vector
    // basetype_->rhsPrepare( gmres, sharedP1Data_ );
  }

  void testing( const bool &gmres )
  {
    basetype_->rhsPrepare( gmres, sharedP1Data_ );
  }

  //! reset
  void reset()
  {
    // reset
#if DROP
    delete basetype_;
    basetype_ = new BaseType( model_, fullSpace_ );
    basetype_->traint( fullConstraints_ );
#else
    basetype_->reset();
#endif
    // set all DoF to a recognizable value for debugging
    // solution_.clear(value);
  }

  // calling the basetype brackets operator
  void operatorBrackets()
  {
    basetype_->operator()(sharedP1Data_,sharedP0Data_);
  }

  //! application operator
  virtual void
  operator() ( const DomainFunction &u, RangeFunction &w ) const;

protected:

  const ModelType &model () const { return model_; }
  const ConstraintsType &constraints () const { return *constraints_; }
  const ConstraintsType &fullConstraints () const { return *fullConstraints_; }
  GridPartType &fullGridPart_;

private:
  // flag for whether running in MPI parallel or not
  bool shared_;
  ModelType model_;
  ConstraintsType* fullConstraints_;
  ConstraintsType* constraints_;

  const DiscreteFunctionSpaceType &space_;
  const DiscreteFunctionSpaceType &fullSpace_;
  const P0DiscreteFunctionSpaceType fullP0Space_;
  DiscreteFunctionType dofRank_;
  BaseType* basetype_;
  // create (or just refer to) a new shared memory of the given name
  DiscreteFunctionType sharedP1Data_;
  P0DiscreteFunctionType sharedP0Data_;
};

template< class DomainFunction, class RangeFunction, class Model >
void PrivateBemOperator< DomainFunction, RangeFunction, Model >
  ::operator() ( const DomainFunction &u, RangeFunction &w ) const
{
  // call bem operator on full surface
  // basetype_->operator()(sharedP1Data_,sharedP0Data_);

  // copy solution from full grid to all the seperate ranked parts
  // the "true" argument requests the second glue object be used
  // (that between the full and partitioned surfaces rather than that
  // between the interior (partitioned) volume and the (partitioned) surface)
  // BaseType::constraints()( u, *(memoryPtr_->get()), w, true );
}

#endif // #ifndef ELLIPTIC_HH
