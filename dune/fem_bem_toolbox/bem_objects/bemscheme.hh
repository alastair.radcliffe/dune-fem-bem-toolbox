#ifndef ELLIPT_BEMSCHEME_HH
#define ELLIPT_BEMSCHEME_HH

// preproc flip switch for recovering the non-shared (private) bem operator stuff
#define PRIVBEMM

// iostream includes
#include <iostream>

// include discrete function space
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/finitevolume.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/common/adaptmanager.hh>

// include discrete function
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>

#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
/*********************************************************/

#if DROP
#include "fixthingy.hh"
#endif

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include "bempp.hh"
#include "sharedbemoperator.hh"

struct ExternalDataOutputParameters
: public Dune::Fem::LocalParameter< Dune::Fem::DataOutputParameters, ExternalDataOutputParameters >
{
  ExternalDataOutputParameters ( const int step, const std::string &preprefix = "" )
    : step_( step ),
      pre_(preprefix)
  {}

  ExternalDataOutputParameters ( const ExternalDataOutputParameters &other )
  : step_( other.step_ )
  {}

  std::string prefix () const
  {
    std::stringstream s;
    s << pre_ << "-external-" << step_ << "-";
    return s.str();
  }

private:
  int step_;
  std::string pre_;
};

// BemScheme
//----------

/*******************************************************************************
 * template arguments are:
 * - GridPart: the part of the grid used to tessellate the
 *             computational domain
 * - Model: description of the data functions and methods required for the
 *          elliptic operator (massFlux, diffusionFlux)
 *     Model::ProblemType boundary data, exact solution,
 *                        and the type of the function space
 *******************************************************************************/
template < class Model >
class BemScheme
{
public:
  //! type of the mathematical model
  typedef Model ModelType ;

  //! grid view (e.g. leaf grid view) provided in the template argument list
  typedef typename ModelType::GridPartType GridPartType;

  //! type of underyling hierarchical grid needed for data output
  typedef typename GridPartType::GridType GridType;

  //! type of function space (scalar functions, f: \Omega -> R)
  typedef typename ModelType :: FunctionSpaceType   FunctionSpaceType;

  //! choose type of discrete function space
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPartType, 1 > P1DiscreteFunctionSpaceType;
  typedef Dune::Fem::FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0 > DiscreteFunctionSpaceType;

  typedef typename Solvers<P1DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType P1DiscreteFunctionType;
  typedef typename Solvers<DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType DiscreteFunctionType;

  // special shared eigenvector for vector surface node movements
  typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, 3 > NudgeVectorFunctionSpaceType;
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< NudgeVectorFunctionSpaceType, GridPartType, 1 > NudgeVectorP1DiscreteFunctionSpaceType;
  typedef typename Solvers<NudgeVectorP1DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType NudgeVectorP1DiscreteFunctionType;

  typedef DiscreteFunctionType BemDiscreteFunctionType;
  typedef DiscreteFunctionSpaceType BemDiscreteFunctionSpaceType;

  /*********************************************************/

  //! type of Dirichlet constraints
  typedef Dune::CouplingConstraints< ModelType, P1DiscreteFunctionSpaceType, false > ConstraintsType;

  //! define Laplace operator
#ifdef PRIVBEM
  typedef BemOperator< P1DiscreteFunctionType, DiscreteFunctionType, ModelType, ConstraintsType > BemOperatorType;
#else
  typedef SharedBemOperator< P1DiscreteFunctionType, DiscreteFunctionType, NudgeVectorP1DiscreteFunctionType, ModelType, ConstraintsType > BemOperatorType;
#endif

  BemScheme( GridPartType &gridPart, GridPartType &fullGridPart,
             const ModelType& implicitModel, const std::string prefix="" )
    : implicitModel_( implicitModel ),
      gridPart_( gridPart ),
      fullGridPart_( fullGridPart ),
      p1Space_( gridPart_ ),
      fullP1Space_( fullGridPart_ ),
      p0Space_( gridPart_ ),
      solution_( prefix+"solution", p0Space_ ),
      polution_( "polution", p0Space_ ),
      dirichlet_( "dirichlet", p1Space_ ),
      rhs_( "rhs", p1Space_ ),
      oldRhs_( "oldRhs", p1Space_ ),
      // the elliptic operator (implicit)
#ifdef PRIVBEM
      implicitOperator_( new BemOperatorType( implicitModel_, p1Space_, constraints_ ) ),
#else
      implicitOperator_( new BemOperatorType( implicitModel_, p1Space_, fullP1Space_, constraints_ ) ),
#endif
      constraints_( new ConstraintsType( implicitModel_, p1Space_ ) )
  {
    // set all DoF to zero
    solution_.clear();
    polution_.clear();
    FTClass::ft(true,true);
  }

  const ModelType& model() const
  {
    return implicitModel_;
  }

  bool useGmres()
  {
    return implicitModel_.useGmres();
  }

  DiscreteFunctionType &solution()
  {
    return solution_;
  }
  const DiscreteFunctionType &solution() const
  {
    return solution_;
  }

  P1DiscreteFunctionType &dofRank()
  {
#ifdef PRIVBEM
    return rhs_;
#else
    return implicitOperator_->dofRank();
#endif
  }
  const P1DiscreteFunctionType &dofRank() const
  {
#ifdef PRIVBEM
    return rhs_;
#else
    return implicitOperator_->dofRank();
#endif
  }
  P1DiscreteFunctionType &fullDirData()
  {
#ifdef PRIVBEM
    return rhs_;
#else
    return implicitOperator_->fullDirData();
#endif
  }
  const P1DiscreteFunctionType &fullDirData() const
  {
#ifdef PRIVBEM
    return rhs_;
#else
    return implicitOperator_->fullDirData();
#endif
  }
#if DROP
  NudgeVectorP1DiscreteFunctionType &fullVectorDirData()
  {
#ifdef PRIVBEM
    return rhs_;
#else
    return implicitOperator_->fullVectorDirData();
#endif
  }
  const NudgeVectorP1DiscreteFunctionType &fullVectorDirData() const
  {
#ifdef PRIVBEM
    return rhs_;
#else
    return implicitOperator_->fullVectorDirData();
#endif
  }
#endif
  DiscreteFunctionType &fullSolution()
  {
#ifdef PRIVBEM
    return solution_;
#else
    return implicitOperator_->fullSolution();
#endif
  }
  const DiscreteFunctionType &fullSolution() const
  {
#ifdef PRIVBEM
    return solution_;
#else
    return implicitOperator_->fullSolution();
#endif
  }

  DiscreteFunctionType &bemSolution()
  {
    return solution_;
  }
  const DiscreteFunctionType &bemSolution() const
  {
    return solution_;
  }

  const P1DiscreteFunctionType &dirichlet() const
  {
    return dirichlet_;
  }

  const P1DiscreteFunctionType &rhs() const
  {
    return rhs_;
  }

  DiscreteFunctionSpaceType &discreteSpace()
  {
    return p0Space_;
  }
  const DiscreteFunctionSpaceType &discreteSpace() const
  {
    return p0Space_;
  }

  const BemDiscreteFunctionSpaceType &bemDiscreteSpace() const
  {
    return p0Space_;
  }

  // flag whether the solution is different from its previous value
  bool stop()
  {
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    NormType norm( gridPart_ );
    double error = norm.distance( solution_, polution_ );
    double magnitude = norm.norm( solution_ );
    std::cout << "Iteration error = " << error << " = " << 100. * error / magnitude << " % of " << magnitude << std::endl;
    polution_.assign( solution_ );
    return error < 0.005 * magnitude;
  }

  // set up which ranks are allowed to fill in which full surface solution dof
  template< class LoadBalancer >
  double setDofRank( const LoadBalancer &loadbal )
  {
#ifdef PRIVBEM
    return 0;
#else
    return implicitOperator_->setDofRank( loadbal );
#endif
  }

  //! set-up the rhs for the coupling values
  template <class FillDiscreteFunctionType>
  void erfSetup(FillDiscreteFunctionType &erfSolution)
  {
    // apply coupling constraints
    implicitOperator_->erfSetup( erfSolution );
  }

  //! set-up the rhs for the coupling values
  template <class OtherDiscreteFunctionType>
  void setup(OtherDiscreteFunctionType &otherSolution_)
  {
    // apply coupling constraints
    implicitOperator_->coupleSetup( solution_, otherSolution_, rhs_ );
  }

  //! set-up the rhs for the coupling values
  template <class OtherDiscreteFunctionType>
  void prepare(OtherDiscreteFunctionType &otherSolution_)
  {
    oldRhs_.assign(rhs_);
    rhs_.clear();
#if DROP
    typedef typename OtherDiscreteFunctionType::GridPartType OtherGridPartType;
    OtherGridPartType otherGridPart_ = otherSolution_.space().gridPart();

    typedef FixThingy< OtherDiscreteFunctionType, DiscreteFunctionType > FixThingyType;
    typedef Dune::Fem::LocalFunctionAdapter< FixThingyType > FixThingyFunctionType;
    FixThingyType fixthingy( otherSolution_ );
    FixThingyFunctionType fixThingyFunction( "fixthingy", fixthingy, otherGridPart_ );

    // apply coupling constraints using new expanded solution
    implicitOperator_->coupling( oldRhs_, fixThingyFunction, rhs_ );
    // implicitOperator_->coupling( oldRhs_, otherSolution_, rhs_ );
#else
    // apply coupling constraints
    implicitOperator_->coupling( oldRhs_, otherSolution_, rhs_ );
    dirichlet_.assign( rhs_ );
#endif
    // apply rhsOp to rhs_
    implicitOperator_->rhsPrepare( useGmres(), rhs_ );
#ifndef PRIVBEM
  }

  template <class OtherDiscreteFunctionType>
  //! sotup the right hand side and physical boundaries
  void collate(OtherDiscreteFunctionType &otherSolution_)
  {
    // apply coupling constraints
    implicitOperator_->collate( otherSolution_ );
#endif
  }

  // makes a copy of full bem solution for gmres (if gmres and if shared bemoperator)
  void makeSolutionCopy()
  {
#ifndef PRIVBEM
    implicitOperator_->makeSolutionCopy();
#endif
  }

  // adds the solution copy (from earlier) to the final solution
  void fullBemSolutionAxpy()
  {
#ifndef PRIVBEM
    implicitOperator_->fullBemSolutionAxpy();
#endif
  }

  //! reset
  void reset()
  {
    // reset
#if DROP

#ifdef PRIVBEM
    delete implicitOperator_;
    implicitOperator_ = new BemOperatorType( implicitModel_, p1Space_, constraints_ );
#else
    implicitOperator_->reset();
#endif

#else
    implicitOperator_->reset();
#endif
    // set all DoF to a recognizable value for debugging
    // solution_.clear(value);
  }

  //! special routine to populate solution via exterior representation formluae
  template <class FillDiscreteFunctionType>
  void fill( FillDiscreteFunctionType &fillSolution_, double uincFactor)
  {
    // apply coupling constraints
    fillExternal(fillSolution_,uincFactor);
    // implicitOperator_->filling( rhs_, solution_, fillSolution_ );
  }

  //! solve the system
  void solve ( bool assemble, double incFactor=0. )
  {
    implicitOperator_->operator()(rhs_,solution_);
  }

  template <class DF>
  void fillExternal(DF &extDF,double uincFactor=1) const
  {
    const int dim = DF::GridPartType::dimension;
    Bempp::Matrix<double> evaluationPoints( 3, extDF.gridPart().indexSet().size(dim) );
    const auto end = extDF.space().gridPart().template end<dim>();
    auto it = extDF.space().gridPart().template begin<dim>();
    for (; it!=end; ++it )
    {
      evaluationPoints(0,extDF.space().gridPart().indexSet().index(*it)) = it->geometry().corner(0)[0];
      evaluationPoints(1,extDF.space().gridPart().indexSet().index(*it)) = it->geometry().corner(0)[1];
      evaluationPoints(2,extDF.space().gridPart().indexSet().index(*it)) = it->geometry().corner(0)[2];
    }
    if ( extDF.dofVector().coefficients().size() != evaluationPoints.cols() )
    {
      std::cout << "Something is going wrong with dimensions in the fill method!" << std::endl;
      assert(0);
    }
    implicitOperator_->fill( dirichlet(), solution(), evaluationPoints, extDF.dofVector().coefficients(), uincFactor );
  }
  template <class ExternalGrid>
  void outputExternal(ExternalGrid &grid)
  {
    typedef Dune::Fem::LeafGridPart< ExternalGrid > LeafGridPart;
    LeafGridPart leafGridPart(grid);

    typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, LeafGridPart, 1 > DiscreteFunctionSpaceType;
    typedef typename Solvers<DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType DiscreteFunctionType;
    DiscreteFunctionSpaceType space( leafGridPart );
    DiscreteFunctionType solution( "u_inf", space );

    fillExternal( solution );

    if( Dune::Fem::MPIManager::rank() == 0 )
    {
    typedef Dune::tuple< DiscreteFunctionType* > IOTupleType;
    typedef Dune::Fem::DataOutput< ExternalGrid, IOTupleType > DataOutputType;
    IOTupleType ioTuple( &solution );
    DataOutputType dataOutput( grid, ioTuple, ExternalDataOutputParameters( 0 ) );
    dataOutput.write();
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

protected:

  const ModelType& implicitModel_;      // the mathematical model

  GridPartType  &gridPart_;             // grid part(view), e.g. here the leaf grid the discrete space is build with
  GridPartType  &fullGridPart_;

  DiscreteFunctionSpaceType p0Space_;   // discrete function space
  DiscreteFunctionType solution_;       // the unknown
  DiscreteFunctionType polution_;       // the previous solution
  P1DiscreteFunctionSpaceType p1Space_; // discrete function space
  P1DiscreteFunctionSpaceType fullP1Space_; // discrete function space
  P1DiscreteFunctionType rhs_,oldRhs_;  // the right hand side
  P1DiscreteFunctionType dirichlet_;

public:
  ConstraintsType* constraints_;

protected:

  BemOperatorType* implicitOperator_;    // the implicit operator
};

#endif // end #if ELLIPT_BEMSCHEME_HH
