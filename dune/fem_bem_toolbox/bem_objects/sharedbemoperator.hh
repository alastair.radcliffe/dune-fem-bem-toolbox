#ifndef SHAREDBEMOPERATOR_HH
#define SHAREDBEMOPERATOR_HH

#include "bemoperator.hh"
#include <dune/fem_bem_toolbox/coupling_schemes/dofrankpad.hh>

#include <iostream>

// SharedBEMOperator
// ----------------

template< class DomainFunction, class RangeFunction, class NudgeVectorP1DiscreteFunctionType, class Model, class Constraints >
struct SharedBemOperator
{
  typedef DomainFunction   DiscreteFunctionType;
  typedef RangeFunction    P0DiscreteFunctionType;
  typedef Model            ModelType;
  typedef Constraints      ConstraintsType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename P0DiscreteFunctionType::DiscreteFunctionSpaceType P0DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;
  typedef typename GridPartType::GridType GridType;

  // special shared eigenvector for vector surface node movements
  typedef typename NudgeVectorP1DiscreteFunctionType::DiscreteFunctionSpaceType NudgeVectorP1DiscreteFunctionSpaceType;

  //! type of Dirichlet constraints
  typedef Dune::CouplingConstraints< ModelType, DiscreteFunctionSpaceType, true > SpecialConstraintsType;

  typedef BemOperator< DomainFunction,RangeFunction,Model,SpecialConstraintsType > BaseType;

  typedef Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GridPartType FixedGridPartType;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GrydPartType GrydPartType;

public:

  //! contructor
  SharedBemOperator ( const ModelType &model, const DiscreteFunctionSpaceType &space, const DiscreteFunctionSpaceType &fullSpace, ConstraintsType* traints  )
  : model_( model )
  , constraints_( traints )
  , space_(space)
  , fullSpace_(fullSpace)
  , fullGridPart_( fullSpace.gridPart() )
  , fullP0Space_( fullSpace.gridPart() )
#if DROP
  , fullP1NudgeSpace_( fullSpace.gridPart() )
#endif
  // which rank is allowed to fill in a particular dof (of the whole surface mesh)
  , dofRank_( "dofRank", fullSpace )
  , sharedP0Data_("sharedNeuData", true, fullP0Space_ )
  , sharedP1Data_("sharedDirData", true, fullSpace )
#if DROP
  , sharedVectorP1Data_("sharedNudgeDirData", true, fullP1NudgeSpace_ )
#else
  , sharedP0DataCopy_("sharedNeuDataCopy", true, fullP0Space_ )
#endif
  , partConstraints_( new SpecialConstraintsType( model, space ) )
  , fullConstraints_( new SpecialConstraintsType( model, fullSpace ) )
  , basetype_( 0 )
  {
    // create a (full) bemoperator only on rank zero
    if( Dune::Fem::MPIManager::rank() == 0 )
      basetype_ = new BaseType(model,fullSpace,fullConstraints_);
    else
      fullConstraints()( );

    // initialize all full surface grid solution dofs to be matched up to surface section solution dofs by rank 0
    dofRank_.clear();

    // create the glue object
    constraints()( );

    // create a second glue object for the exchange of data
    // from the full to partitioned surface grids
    partConstraints()( );
  }
  ~SharedBemOperator()
  {
    if( Dune::Fem::MPIManager::rank() == 0 )
    delete basetype_;
  }

  // setup the glue coupling
  template <class Function, class OtherFunction>
  void coupleSetup( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    // set coupling values for solution
    constraints().updateCouplingDofs( func, funk, u );
    partConstraints().updateCouplingDofs( func, sharedP0Data_, u );
    // BaseType::coupleSetup( func, *(memoryPtr_->get()), func  );
    if( Dune::Fem::MPIManager::rank() == 0 )
    basetype_->coupleSetup( dofRank_, func, sharedP1Data_ );
    else
      fullConstraints().updateCouplingDofs( dofRank_, func, sharedP1Data_ );
  }

  DiscreteFunctionType &dofRank()
  {
    return dofRank_;
  }
  const DiscreteFunctionType &dofRank() const
  {
    return dofRank_;
  }

  DiscreteFunctionType &fullDirData()
  {
    return sharedP1Data_;
  }
  const DiscreteFunctionType &fullDirData() const
  {
    return sharedP1Data_;
  }

#if DROP
  NudgeVectorP1DiscreteFunctionType &fullVectorDirData()
  {
    return sharedVectorP1Data_;
  }
  const NudgeVectorP1DiscreteFunctionType &fullVectorDirData() const
  {
    return sharedVectorP1Data_;
  }
#endif

  P0DiscreteFunctionType &fullSolution()
  {
    return sharedP0Data_;
  }
  const P0DiscreteFunctionType &fullSolution() const
  {
    return sharedP0Data_;
  }

  // set up which ranks are allowed to fill in which full surface solution dof
  template< class LoadBalancer >
  double setDofRank( const LoadBalancer &loadbal )
  {
    // if(!shared) return 0;

    // typedef typename Grid :: Traits :: template Codim<0> :: Entity IteratorType;
    typedef typename GridPartType::template Codim<0>::IteratorType IteratorType;

    // initialize an area checker
    double result = 0.0;

    // run through all the elements of the full (un-partitioned) surface grid
    const IteratorType end = fullGridPart_.template end<0>();
    for( IteratorType it = fullGridPart_.template begin<0>(); it != end; ++it )
    {
      // keep track of the total surface area as a check
      result += it->geometry().volume();

      // determine the rank the load balancer would have given this element
      int destRank = loadbal.operator()( *it );
      setDof( *it, destRank );
    }
    return result;
  }

  //! set the entity dofs to the value of the rank (if the greatest)
  template< class EntityType >
  void setDof( const EntityType &entity, const int dest )
  {
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteSpaceType;
    typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename DiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename DiscreteSpaceType :: RangeType RangeType;

    // get local functions of result
    LocalFunctionType wLocal = dofRank_.localFunction( entity );

    const LagrangePointSetType &lagrangePointSet = dofRank_.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    // typedef typename GridFunctionType :: RangeType RangeAType;

    int localDof = 0;
    const unsigned int localBlockSize = dofRank_.space().localBlockSize;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    dofRank_.space().blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( unsigned int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
      RangeType phi( dest );

      // store result to dof vector
      for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
      {
        // whichever rank is the greatest gets to set the dof
        if( std::abs(phi[ l ]) > std::abs(wLocal[ localDof ]) ) wLocal[ localDof ] = phi[ l ];
      }
    }
  }

  // prepare the glue coupling
  template <class Function, class OtherFunction>
  void coupling( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    // set coupling values for solution
    constraints()( func, funk, u );
  }

  // Construct the right-hand-side grid function
  void rhsPrepare( const bool &gmres, DomainFunction &rhs ) const
  {
    // form full rhs vector from rhs vector on different surface sections
    fullConstraints()( dofRank_, rhs, sharedP1Data_ );

    // ensure all rhs data has been collected together first ...
    MPI_Barrier(MPI_COMM_WORLD);

    // call underlying rhs prepare for this full rhs vector
    if( Dune::Fem::MPIManager::rank() == 0 )
    basetype_->rhsPrepare( gmres, sharedP1Data_ );
  }

  // makes a copy of full bem solution for gmres (if gmres and if shared bemoperator)
  void makeSolutionCopy()
  {
#if !DROP
    if( Dune::Fem::MPIManager::rank() == 0 )
    sharedP0DataCopy_.assign( sharedP0Data_ );
#endif
  }

  // adds the solution copy (from earlier) to the final solution
  void fullBemSolutionAxpy()
  {
#if !DROP
    if( Dune::Fem::MPIManager::rank() == 0 )
    sharedP0Data_.axpy( 1., sharedP0DataCopy_ );
    MPI_Barrier(MPI_COMM_WORLD);
#endif
  }

  // reset
  void reset()
  {
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
    // reset
#if DROP
    delete basetype_;
    basetype_ = new BaseType( model_, fullSpace_, fullConstraints_, false );
#else
    basetype_->reset();
#endif
    }
    // set all DoF to a recognizable value for debugging
    // solution_.clear(value);
  }

  template <class Points, class Values>
  void fill( const DomainFunction &u, const RangeFunction &w,
             const Points &points, Values &values, double uincFactor)
  {
    fullConstraints()( dofRank_, u, sharedP1Data_ );
    MPI_Barrier(MPI_COMM_WORLD);
    if( Dune::Fem::MPIManager::rank() == 0 )
    basetype_->fill( sharedP1Data_, sharedP0Data_, points, values, uincFactor );
  }

  // prepare the glue coupling
  template <class OtherFunction>
  void collate( const OtherFunction &funk )
  {
#if DROP
    // need to pad out the dofRank to look like a vector
    typedef DofRankPad< DiscreteFunctionType, NudgeVectorP1DiscreteFunctionType > DofRankPadType;
    typedef Dune::Fem::LocalFunctionAdapter< DofRankPadType > DofRankPadFunctionType;
    DofRankPadType dofrankpad( dofRank_ );
    DofRankPadFunctionType dofrankpadFunction( "dofrankpad", dofrankpad, fullGridPart_ );

    // set coupling values for solution
    // BaseType::constraints()( func, funk, u );
    fullConstraints()( dofrankpadFunction, funk, sharedVectorP1Data_ );

    // ensure all rhs data has been collected together first ...
    MPI_Barrier(MPI_COMM_WORLD);
#endif
  }

  //! application operator
  virtual void
  operator() ( const DomainFunction &u, RangeFunction &w ) const;

protected:

  const ModelType &model () const { return model_; }
  const ConstraintsType &constraints () const { return *constraints_; }
  const SpecialConstraintsType &partConstraints () const { return *partConstraints_; }
  const SpecialConstraintsType &fullConstraints () const { return *fullConstraints_; }
  GridPartType &fullGridPart_;

private:
  ModelType model_;
  ConstraintsType* constraints_;
  SpecialConstraintsType* fullConstraints_;
  SpecialConstraintsType* partConstraints_;

  const DiscreteFunctionSpaceType &space_;
  const DiscreteFunctionSpaceType &fullSpace_;
  const P0DiscreteFunctionSpaceType fullP0Space_;
#if DROP
  const NudgeVectorP1DiscreteFunctionSpaceType fullP1NudgeSpace_;
#endif
  DiscreteFunctionType dofRank_;
  BaseType* basetype_;
  // create (or just refer to) a new shared memory of the given name
  mutable DiscreteFunctionType sharedP1Data_;
  mutable P0DiscreteFunctionType sharedP0Data_;
#if DROP
  mutable NudgeVectorP1DiscreteFunctionType sharedVectorP1Data_;
#else
  mutable P0DiscreteFunctionType sharedP0DataCopy_;
#endif
};

template< class DomainFunction, class RangeFunction, class NudgeVectorP1DiscreteFunctionType, class Model, class Constraints >
void SharedBemOperator< DomainFunction, RangeFunction, NudgeVectorP1DiscreteFunctionType, Model, Constraints >
  ::operator() ( const DomainFunction &u, RangeFunction &w ) const
{
  // call bem operator on full surface
  if( Dune::Fem::MPIManager::rank() == 0 )
  basetype_->operator()(sharedP1Data_,sharedP0Data_);
  // no copying over of solution unitl it is ready
  MPI_Barrier(MPI_COMM_WORLD);
  // bring back the solution on to the local grid part
  partConstraints()( u, sharedP0Data_, w );
}

#endif // #ifndef ELLIPTIC_HH
