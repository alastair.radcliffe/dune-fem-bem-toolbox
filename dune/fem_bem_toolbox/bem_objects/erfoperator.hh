#ifndef ERFOPERATOR_HH
#define ERFOPERATOR_HH

#include <dune/common/fmatrix.hh>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/stencil.hh>

#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem_fem_coupling/fem_fem_coupling.hh>

#include "bempp.hh"

// ERFOperator
// ----------------
  using namespace Bempp;

template< class DomainFunction, class RangeFunction, class Model >
struct ErfOperator
: public virtual Dune::Fem::Operator< DomainFunction,RangeFunction >
{
  typedef DomainFunction   DiscreteFunctionType;
  typedef Model            ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;
  typedef typename GridPartType::GridType GridType;

  typedef Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue< GT >::GridPartType FixedGridPartType;
  typedef typename Dune::FemFemCoupling::Glue< GT >::GrydPartType GrydPartType;

  //! type of Dirichlet constraints
  typedef Dune::CouplingConstraints< ModelType, DiscreteFunctionSpaceType > ConstraintsType;

public:
  GridType &dunegrid;
  shared_ptr<Grid> grid;
  PiecewiseLinearContinuousScalarSpace<BFT> pwiseLinears;
  PiecewiseConstantScalarSpace<BFT> pwiseConstants;
  AccuracyOptions accuracyOptions;
  static AccuracyOptions &setAccuracyOptions( AccuracyOptions &accuracyOptions )
  {
    accuracyOptions.doubleRegular.setRelativeQuadratureOrder(2);
    accuracyOptions.singleRegular.setRelativeQuadratureOrder(2);
    return accuracyOptions;
  }

  NumericalQuadratureStrategy<BFT, RT> quadStrategy;
  AssemblyOptions assemblyOptions;
  EvaluationOptions evaluationOptions;
  Context<BFT, RT> context;
#if COMPLEX
  const double omega_;
#endif
  BoundaryOperator<BFT, RT> slpOp;
  BoundaryOperator<BFT, RT> dlpOp;
  BoundaryOperator<BFT, RT> idOp;
  BoundaryOperator<BFT, RT> rhsOp;
#if COMPLEX
  Helmholtz3dSingleLayerPotentialOperator<BFT> slPotOp;
  Helmholtz3dDoubleLayerPotentialOperator<BFT> dlPotOp;
#else
  Laplace3dSingleLayerPotentialOperator<BFT, RT> slPotOp;
  Laplace3dDoubleLayerPotentialOperator<BFT, RT> dlPotOp;
#endif
  Bempp::Matrix<RT> mat;
  //! contructor
  ErfOperator ( const ModelType &model, const DiscreteFunctionSpaceType &space )
  : model_( model )
  , constraints_( model, space )
  , first_(true)
  , dunegrid( const_cast<GridType&>(space.gridPart().grid()) )
  , grid( new ConcreteGrid<GridType>(&dunegrid,GridParameters::TRIANGULAR,false) )
  , pwiseLinears( grid )
  , pwiseConstants( grid )
  , quadStrategy(setAccuracyOptions( accuracyOptions) )
  , context(make_shared_from_ref(quadStrategy), assemblyOptions)
#if COMPLEX
  , omega_( Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ) )
  , slpOp( helmholtz3dSingleLayerBoundaryOperator<BFT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                omega_ ) )
  , dlpOp( helmholtz3dDoubleLayerBoundaryOperator<BFT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                omega_ ) )
  , slPotOp( omega_ )
  , dlPotOp( omega_ )
#else
  , slpOp( laplace3dSingleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants)) )
  , dlpOp( laplace3dDoubleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants)) )
#endif
  , idOp( identityOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants)) )
  , rhsOp( -0.5 * idOp + dlpOp )
  , mat(slpOp.weakForm()->asMatrix())
  {
  }

  // initialize the coupling
  void coupleInit( const GridPartType &gridPart )
  {
#if !DIAG_PLOT
    // create the glue object
    constraints()( );
#endif
  }

  //! application operator
  virtual void
  operator() ( const DomainFunction &u, RangeFunction &w ) const;

protected:
  const ModelType &model () const { return model_; }
  const ConstraintsType &constraints () const { return constraints_; }

private:
  ModelType model_;
  ConstraintsType constraints_;
  mutable bool first_;
};

template< class DomainFunction, class RangeFunction, class Model >
void ErfOperator< DomainFunction, RangeFunction, Model >
  ::operator() ( const DomainFunction &u, RangeFunction &w ) const
{
  // clear destination
  w.clear();
  // bempp(w.gridPart().grid(),u,w);
  auto& dirichletDataDofs = u.dofVector().coefficients();
  auto& neumannDataDofs = w.dofVector().coefficients();

  if (first_)
  {
    GridFunction<BFT, RT> dirichletDataAnalytic(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                surfaceNormalAndDomainIndexDependentFunction(DirichletData()));
#if COMPLEX
    const_cast<Bempp::Vector<std::complex<double>>&>(dirichletDataDofs) = dirichletDataAnalytic.coefficients();
#else
    const_cast<Bempp::Vector<double>&>(dirichletDataDofs) = dirichletDataAnalytic.coefficients();
#endif
    first_ = false;
    // Warp::set();
    std::cout << "\n Using Analytic Dirichlet data \n" << std::endl;
  }
  else
    std::cout << "\n Using Nummeric Dirichlet data \n" << std::endl;

  GridFunction<BFT, RT> dirichletDataNummeric(
              make_shared_from_ref(context),
              make_shared_from_ref(pwiseLinears),
              dirichletDataDofs);

  // Construct the right-hand-side grid function
  GridFunction<BFT, RT> rhs = rhsOp * dirichletDataNummeric;

  // First get the projections onto the space of piecewise constant functions.
  Vector<RT> rhsVector = rhs.projections(make_shared_from_ref(pwiseConstants));

  // Solve the dense system via Eigen
  Vector<RT> solVector = mat.llt().solve(rhsVector);

  // Create a grid function from the result
  const GridFunction<BFT,RT> solFun(make_shared_from_ref(context),
                              make_shared_from_ref(pwiseLinears),
                              solVector);

  neumannDataDofs = solFun.coefficients();
}

#endif // #ifndef ELLIPTIC_HH
