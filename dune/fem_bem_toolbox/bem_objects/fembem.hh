#ifndef FEMBEM_HH
#define FEMBEM_HH


#include <cassert>
#include <cmath>

#if COMPLEX
#include "../../../tests/helmholtz/source/helmholtz.hh"
#include "../../../case_studies/scattering_wave/source/wavescat.hh"
#endif

// a reentrant corner problem with a 270 degree corner
template <class FunctionSpace>
class BulkBemProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;

public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  BulkBemProblem() {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& p,
                 RangeType& phi) const
  {
    phi = 0;
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(0);
  }

  //! the exact bulk solution
  virtual void u(const DomainType& p,
                 RangeType& ret) const
  {
    double x = p[0], y = p[1], z = p[2];
    double r = p.two_norm();
    ret[0] = 2 * x * z / (r * r * r * r * r) - y / (r * r * r);
    // ret[0] = -1 / r;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret[0][0] = 2.*x[0];
    ret[0][1] = 2.*x[1];
    ret[0][2] = 2.*x[2];
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    // assert( x.two_norm() < 0.5 );
    return true ;
  }
};

// not really used....
template <class FunctionSpace>
class SurfaceBemProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  typedef typename FunctionSpace::HessianRangeType HessianRangeType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& ret) const
  {
    ret = 0;
    abort();
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = 0;
    abort();
  }

  //! the exact normal derivative on the surface derives from the chosen bulk interior solution
  virtual void u(const DomainType& p, RangeType& phi) const
  {
    double x = p[0], y = p[1], z = p[2];
    double r = p.two_norm();
    phi[0] = -6 * x * z / (r * r * r * r * r * r) + 2 * y / (r * r * r * r);
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x, JacobianRangeType& ret) const
  {
    ret[0][0] = 2.;
    ret[0][1] = 2.;
    ret[0][2] = 2.;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true ;
  }
};

// not really used....
template <class FunctionSpace>
class CoulombProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  typedef typename FunctionSpace::HessianRangeType HessianRangeType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  CoulombProblem()
    : def_( Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ) ),
      // assume ellipsoid alligned along x-axis
      fita_( def_ / std::pow( def_ , 1.0/3.0 ) ),
      fitb_( 1.0  / std::pow( def_ , 1.0/3.0 ) )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& ret) const
  {
    ret = 0;
    abort();
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = 0;
    abort();
  }

  //! the exact solution
  virtual void u(const DomainType& xx,
                 RangeType& phi) const
  {
    // radial distance
    double x = std::sqrt( xx[ 1 ]*xx[ 1 ] + xx[ 0 ]*xx[ 0 ] );

    // The y to go with the given x
    double y = x / fitb_;
    y = y * y;
    y = fita_ * sqrt( 1.0 - y );

    // std::cout << "Mean " << x << "  " << y << "  " << xx[ 0 ] << std::endl;

    // Constructions
    double r1 = fitb_ * fitb_; double r2 = fita_ * fita_;

    double H = r1 * r2; double K = r1;

    r1 = x * x / ( r1 * r1 ); r2 = y * y / ( r2 * r2 );

    r2 = sqrt( r1 + r2 );

    // Principle radii of Curvature
    r1 = H * r2 * r2 * r2;
    r2 = K * r2;

    // Mean Curvature - the Algebraic Mean
    // phi = ( r1 + r2 ) / ( 2.0 * r1 * r2 );

    // Gaussian Curvature - the Geometric Mean
    // Square of surface charge on an ellipse follows the Gaussian curvature (need extra sqrt here ...)
    phi = 1.0 / std::pow( r1 * r2 , 0.25 );

    /*
            ! Use spheroid analytic form when close to a sphere
            else if(sphereish) then
              ! Get spheroid Mean and Gaussian curvatures
              call fitHK(X_g(3),ek,Ex(j))
              ek = 2.0d0 * ek; Ex(j) = 2.0d0 * Ex(j) * ( omeg / fourpi ) ** 2


    phi = 5.6;
    phi = std::cos( M_PI*time() );
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    */
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x, JacobianRangeType& ret) const
  {
    ret[0][0] = 2.;
    ret[0][1] = 2.;
    ret[0][2] = 2.;
  }

  //! return true if Dirichlet boundary is present (default is true)
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true ;
  }
private:
  double def_;
  double fita_;
  double fitb_;
};

#endif
