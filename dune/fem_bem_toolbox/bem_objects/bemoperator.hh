#ifndef BEMOPERATOR_HH
#define BEMOPERATOR_HH

#include <dune/common/fmatrix.hh>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/stencil.hh>

#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem_fem_coupling/fem_fem_coupling.hh>

#include "bempp.hh"

// BEMOperator
// ----------------
  using namespace Bempp;

template< class DomainFunction, class RangeFunction, class Model, class Constraints >
struct BemOperator
: public virtual Dune::Fem::Operator< DomainFunction,RangeFunction >
{
  typedef DomainFunction   DiscreteFunctionType;
  typedef Model            ModelType;
  typedef Constraints      ConstraintsType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;
  typedef typename GridPartType::GridType GridType;

  typedef Dune::GridSelector::GridType GT;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GridPartType FixedGridPartType;
  typedef typename Dune::FemFemCoupling::Glue< GT, false >::GrydPartType GrydPartType;

public:
  GridType &dunegrid;
  boost::shared_ptr<Grid> grid;
  PiecewiseLinearContinuousScalarSpace<BFT> pwiseLinears;
  PiecewiseConstantScalarSpace<BFT> pwiseConstants;
  AccuracyOptions accuracyOptions;
  static AccuracyOptions &setAccuracyOptions( AccuracyOptions &accuracyOptions )
  {
    accuracyOptions.doubleRegular.setRelativeQuadratureOrder(2);
    accuracyOptions.singleRegular.setRelativeQuadratureOrder(2);
    return accuracyOptions;
  }

  NumericalQuadratureStrategy<BFT, RT> quadStrategy;
  AssemblyOptions assemblyOptions;
  EvaluationOptions evaluationOptions;
  Context<BFT, RT> context;
#if COMPLEX
  const double omega_;
#endif
  BoundaryOperator<BFT, RT> slpOp;
  BoundaryOperator<BFT, RT> dlpOp;
  BoundaryOperator<BFT, RT> idOp;
  BoundaryOperator<BFT, RT> rhsOp;
#if FILTERED
  // the points at which the erf will be needed
  Bempp::Matrix<CT> points;
  // the incident field (if any) at these points
  Bempp::Matrix<RT> iStore ;
#endif
#if COMPLEX
  Helmholtz3dSingleLayerPotentialOperator<BFT> slPotOp;
  Helmholtz3dDoubleLayerPotentialOperator<BFT> dlPotOp;
#else
  Laplace3dSingleLayerPotentialOperator<BFT, RT> slPotOp;
  Laplace3dDoubleLayerPotentialOperator<BFT, RT> dlPotOp;
#endif
  AssembledPotentialOperator<BFT, RT> *slPotOpAss;
  AssembledPotentialOperator<BFT, RT> *dlPotOpAss;
  Bempp::Matrix<RT> mat;
  //! contructor
  BemOperator ( const ModelType &model, const DiscreteFunctionSpaceType &space, ConstraintsType* traints, bool doGlue = true )
  : model_( model )
  , constraints_( traints )
  , space_(space)
  , first_(true)
  , dunegrid( const_cast<GridType&>(space.gridPart().grid()) )
  , grid( new ConcreteGrid<GridType>(&dunegrid,GridParameters::TRIANGULAR,false) )
  , pwiseLinears( grid )
  , pwiseConstants( grid )
  , quadStrategy(setAccuracyOptions( accuracyOptions) )
  , context(make_shared_from_ref(quadStrategy), assemblyOptions)
#if COMPLEX
  , omega_( Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ) )
  , slpOp( helmholtz3dSingleLayerBoundaryOperator<BFT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                omega_ ) )
  , dlpOp( helmholtz3dDoubleLayerBoundaryOperator<BFT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants),
                omega_ ) )
  , slPotOp( omega_ )
  , dlPotOp( omega_ )
#else
  , slpOp( laplace3dSingleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants)) )
  , dlpOp( laplace3dDoubleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants)) )
#endif
  , idOp( identityOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants)) )
  , slPotOpAss(0)
  , dlPotOpAss(0)
#if DROP
  , rhsOp( idOp )
#else
  , rhsOp( -0.5 * idOp + dlpOp )
#endif
  , mat(slpOp.weakForm()->asMatrix())
#if COMPLEX
  , decomp_( mat.lu() )
#else
  , decomp_( mat.llt() )
#endif
  {
    // create the glue object
    if( doGlue )
    constraints()( );
  }
  ~BemOperator()
  {
    delete slPotOpAss;
    delete dlPotOpAss;
  }

  // prepare the solution vector
  template <class Function>
  void prepare( const Function &func, DiscreteFunctionType &u )
  {
    // set boundary values for solution
    constraints()( func, u );
  }

  // setup the glue coupling
  template <class Function, class OtherFunction>
  void coupleSetup( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    // set coupling values for solution
    constraints().updateCouplingDofs( func, funk, u );
  }

#if BEMPP && FILTERED

  // set-up the points array for the exterior representation formulae
  template <class FillDiscreteFunctionType>
  void erfSetup( const FillDiscreteFunctionType &u )
  {
    std::cout << "Shouldn't call erfSetup" << std::endl;
    abort();
    typedef typename FillDiscreteFunctionType::DiscreteFunctionSpaceType FillDiscreteFunctionSpaceType;

    typedef typename FillDiscreteFunctionSpaceType::IteratorType IteratorType;
    typedef typename IteratorType::Entity       EntityType;

    // get discrete function space
    const FillDiscreteFunctionSpaceType &erfSpace = u.space();

    // array to store previously computed erf values for efficiency
    const int blocks = erfSpace.blockMapper().size() ;

    points.resize(3, blocks);
    iStore.resize(blocks);

    // iterate over grid
    const IteratorType end = erfSpace.end();
    for( IteratorType it = erfSpace.begin(); it != end; ++it )
    {
      // get entity (here element)
      const EntityType &entity = *it;

      fillPoints( erfSpace, entity );
    }

    // create and assemble the single and double layer operators to be used for the ERF calculations at the points just found
    assert( !slPotOpAss );
    slPotOpAss = new AssembledPotentialOperator<BFT, RT> (
      slPotOp.assemble( make_shared_from_ref(pwiseConstants),
                        make_shared_from_ref(points),
                        quadStrategy, evaluationOptions ) );
    assert( !dlPotOpAss );
    dlPotOpAss = new AssembledPotentialOperator<BFT, RT> (
      dlPotOp.assemble( make_shared_from_ref(pwiseLinears),
                        make_shared_from_ref(points),
                        quadStrategy, evaluationOptions ) );
  }

  //! fill up the points array with the coordinates of the points where the ERF will need to be evaluated
  template< class FillDiscreteSpaceType, class EntityType >
  void fillPoints( const FillDiscreteSpaceType &erfSpace, const EntityType &entity )
  {
    typedef typename FillDiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename EntityType::Geometry GeometryType;

    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    const LagrangePointSetType &lagrangePointSet = erfSpace.lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    int localDof = 0;
    const int localBlockSize = FillDiscreteSpaceType :: localBlockSize ;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    erfSpace.blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
        auto where = geometry.global( Dune::Fem::coordinate( lagrangePointSet[ localBlock ] ) );

        CT x = where[0];
        CT y = where[1];
        CT z = where[2];

        points[ 0, globalBlockDofs[ localBlock ] ] = x;
        points[ 1, globalBlockDofs[ localBlock ] ] = y;
        points[ 2, globalBlockDofs[ localBlock ] ] = z;

        // determine the incident field at these points
        IncidentData incident;
        IncidentData::ValueType values;

        incident.evaluate( x, y, z, values );

        // std::cout << "EP " << x << "  " << y << "  " << z << "  " << std::sqrt(x*x+y*y+z*z) << "  " << values << std::endl;

        iStore[ globalBlockDofs[ localBlock ] ] = values;

        // std::cout << "PE " << x << "  " << y << "  " << z << "  " << std::sqrt(x*x+y*y+z*z) << std::endl;
    }
  }

  // apply the exterior representation formulae
  template <class DirichletFunction, class NeumannFunction, class FillDiscreteFunctionType>
  void filling( const DirichletFunction &func, const NeumannFunction &funk, FillDiscreteFunctionType &u )
  {
    typedef typename FillDiscreteFunctionType::DiscreteFunctionSpaceType FillDiscreteFunctionSpaceType;

    typedef typename FillDiscreteFunctionSpaceType::IteratorType IteratorType;
    typedef typename IteratorType::Entity       EntityType;

    // get discrete function space
    const FillDiscreteFunctionSpaceType &dfSpace = u.space();

    // array to store previously computed erf values for efficienty
    const int blocks = dfSpace.blockMapper().size() ;

    typedef typename FillDiscreteFunctionType :: DiscreteFunctionSpaceType FillDiscreteSpaceType;
    typedef typename FillDiscreteSpaceType :: RangeType RangeType;

    std::vector< RangeType > store;

    store.resize( blocks );

    // set-up Dirichlet and Neumann data to be used in the filling
    auto& dirichletDataDofs = func.dofVector().coefficients();
    auto& neumannDataDofs = funk.dofVector().coefficients();

    GridFunction<BFT, RT> dirichletData(
              make_shared_from_ref(context),
              make_shared_from_ref(pwiseLinears),
              dirichletDataDofs);

    GridFunction<BFT, RT> neumannData(
              make_shared_from_ref(context),
              make_shared_from_ref(pwiseConstants),
              neumannDataDofs);

    // calculate the new erf values without redoing all the potential operators
    Bempp::Matrix<RT> sStore = slPotOpAss->apply( neumannData );
    Bempp::Matrix<RT> dStore = dlPotOpAss->apply( dirichletData );

    for( int i=0; i<blocks; ++i )
    {
      store[ i ] = 1000.0;
      // arma::Mat<RT> feeld = sStore(i) + dStore(i);
      // store[ i ] = sStore(i) + dStore(i) + iStore(i); // feeld(0, 0);
    }

    // iterate over grid
    const IteratorType end = dfSpace.end();
    for( IteratorType it = dfSpace.begin(); it != end; ++it )
    {
      // get entity (here element)
      const EntityType &entity = *it;

      erfApplication( entity, func, funk, u, dirichletData, neumannData, store );
    }
  }



  //! set the rhs dirichlet point values to those required for the coupling
  template< class EntityType, class DirichletGridFunctionType, class NeumannGridFunctionType, class FillDiscreteFunctionType, class RangeType >
  void erfApplication( const EntityType &entity,
                              const DirichletGridFunctionType& u,
                              const NeumannGridFunctionType& v,
                       FillDiscreteFunctionType &w, GridFunction<BFT, RT> &dirichletData, GridFunction<BFT, RT> &neumannData, std::vector< RangeType > &store )
  {
    typedef typename FillDiscreteFunctionType :: DiscreteFunctionSpaceType FillDiscreteSpaceType;
    typedef typename FillDiscreteFunctionType :: LocalFunctionType LocalFunctionType;
    typedef typename FillDiscreteSpaceType :: LagrangePointSetType LagrangePointSetType;

    typedef typename EntityType::Geometry GeometryType;

    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    // get local functions of result
    LocalFunctionType wLocal = w.localFunction( entity );

    const LagrangePointSetType &lagrangePointSet = w.space().lagrangePointSet( entity );

    // get number of Lagrange Points
    const int numBlocks = lagrangePointSet.size();

    int localDof = 0;
    const int localBlockSize = FillDiscreteSpaceType :: localBlockSize ;

    // map local to global BlockDofs
    std::vector<std::size_t> globalBlockDofs(numBlocks);
    w.space().blockMapper().map(entity,globalBlockDofs);

    // iterate over face dofs and set unit row
    for( int localBlock = 0 ; localBlock < numBlocks; ++ localBlock )
    {
        // Construct the array 'evaluationPoints' containing the coordinate
        // of the point where the solution should be evaluated

        assert( globalBlockDofs[ localBlock ] < store.size() );

#if 1
        if( std::abs(store[globalBlockDofs[ localBlock ]].two_norm()) > 500.0 )
        {

        const int dimWorld = 3;
        Bempp::Matrix<CT> evaluationPoints(dimWorld, 1);

        auto where = geometry.global( Dune::Fem::coordinate( lagrangePointSet[ localBlock ] ) );

        CT x = where[0];
        CT y = where[1];
        CT z = where[2];

        evaluationPoints(0, 0) = x;
        evaluationPoints(1, 0) = y;
        evaluationPoints(2, 0) = z;

        // determine the incident field at these points
        IncidentData incident;
        IncidentData::ValueType values;

        incident.evaluate( x, y, z, values );

        // Use the Green's representation formula to evaluate the solution
        Bempp::Matrix<RT> field =
            - slPotOp.evaluateAtPoints(neumannData, evaluationPoints,
                                       quadStrategy, evaluationOptions) +
             dlPotOp.evaluateAtPoints(dirichletData, evaluationPoints,
                                      quadStrategy, evaluationOptions) + values;

        // std::cout << "EP " << x << "  " << y << "  " << z << "  " << std::sqrt(x*x+y*y+z*z) << "  " << store[globalBlockDofs[ localBlock ]] << "  " << field(0, 0) << "  " << std::endl;

        store[globalBlockDofs[ localBlock ]] = field(0, 0);

        // put the solution into the scheme's solution
        }
#endif


        RangeType phi = store[ globalBlockDofs[ localBlock ] ];


        // store result to dof vector
        for( int l = 0; l < localBlockSize ; ++ l, ++localDof )
        {
          wLocal[ localDof ] = phi[ l ];
        }
    }
  }
#endif

  // prepare the glue coupling
  template <class Function, class OtherFunction>
  void coupling( const Function &func, const OtherFunction &funk, DiscreteFunctionType &u )
  {
    // set coupling values for solution
    constraints()( func, funk, u );
  }

  // Construct the right-hand-side grid function
  void rhsPrepare( const bool &gmres, DomainFunction &rhs ) const
  {
    // bempp(w.gridPart().grid(),u,w);
    auto& rhsDataDofs = rhs.dofVector().coefficients();

    // set-up (analytic) incident data
    GridFunction<BFT, RT> incidentDataAnalytic(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                surfaceNormalAndDomainIndexDependentFunction(IncidentData()));

    // gmres does not allow starts from anything other than incident data
    if( gmres && first_ )
      rhsDataDofs = incidentDataAnalytic.coefficients();
#if DROP
    else
#else
    // Gauss-Seidel, however, allows an initial guess of the solution to be made
    else if (first_)
#endif
    {
      // set-up analytic Dirichlet data
      GridFunction<BFT, RT> dirichletDataAnalytic(
                  make_shared_from_ref(context),
                  make_shared_from_ref(pwiseLinears),
                  make_shared_from_ref(pwiseLinears),
                  surfaceNormalAndDomainIndexDependentFunction(DirichletData()));

      // do not add incident field if computing scattered onl fields
      if( model().assembleRHSwithOperator() )
      {
        GridFunction<BFT, RT> newRhs = rhsOp * dirichletDataAnalytic;
        rhsDataDofs = newRhs.coefficients();
      }
      // total field computations are driven by the incident field added to the bem rhs
      else
      {
        GridFunction<BFT, RT> newRhs = rhsOp * dirichletDataAnalytic + incidentDataAnalytic;
        rhsDataDofs = newRhs.coefficients();
      }
      std::cout << "\n Using Analytic Dirichlet data \n" << std::endl;
    }
#if !DROP
    else
    {
      // set-up numeric Dirichlet data
      GridFunction<BFT, RT> dirichletDataNummeric(
                  make_shared_from_ref(context),
                  make_shared_from_ref(pwiseLinears),
                  rhsDataDofs);

      // do not add incident field if computing scattered onl fields
      if( gmres || model().assembleRHSwithOperator() )
      {
        GridFunction<BFT, RT> newRhs = rhsOp * dirichletDataNummeric;
        rhsDataDofs = newRhs.coefficients();
      }
      // total field computations are driven by the incident field added to the bem rhs
      else
      {
        GridFunction<BFT, RT> newRhs = rhsOp * dirichletDataNummeric + incidentDataAnalytic;
        rhsDataDofs = newRhs.coefficients();
      }
      std::cout << "\n Using Nummeric Dirichlet data \n" << std::endl;
    }
#endif
    first_ = false;
  }

  // reset
  void reset()
  {
    // reset
    constraints().reset();
  }

  template <class Points, class Values>
  void fill( const DomainFunction &u, const RangeFunction &w,
             const Points &points, Values &values, double uincFactor)
  {
    auto& dirichletDataDofs = u.dofVector().coefficients();
    auto& neumannDataDofs = w.dofVector().coefficients();
    GridFunction<BFT, RT> dirichletFunction(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                dirichletDataDofs);
    GridFunction<BFT, RT> neumannFunction(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                neumannDataDofs);
    Bempp::Matrix<RT> store =
        -slPotOp.evaluateAtPoints(neumannFunction,   points, quadStrategy, evaluationOptions) +
         dlPotOp.evaluateAtPoints(dirichletFunction, points, quadStrategy, evaluationOptions);

    IncidentData incident;
    for (unsigned int i=0;i<values.size();++i)
    {
      incident.evaluate( points(0,i), points(1,i), points(2,i), values(i) );
      values[i] *= uincFactor;
      values(i) += store(0,i);
    }

  }

  //! application operator
  virtual void
  operator() ( const DomainFunction &u, RangeFunction &w ) const;

protected:

  const ModelType &model () const { return model_; }
  const ConstraintsType &constraints () const { return *constraints_; }

private:
  ModelType model_;
  ConstraintsType* constraints_;
  mutable bool first_;
  const DiscreteFunctionSpaceType &space_;
#if COMPLEX
  decltype(mat.lu()) decomp_;
#else
  decltype(mat.llt()) decomp_;
#endif
};

template< class DomainFunction, class RangeFunction, class Model, class Constraints >
void BemOperator< DomainFunction, RangeFunction, Model, Constraints >
  ::operator() ( const DomainFunction &u, RangeFunction &w ) const
{
  // clear destination
  w.clear();

  // bempp(w.gridPart().grid(),u,w);
  auto& dirichletDataDofs = u.dofVector().coefficients();
  auto& neumannDataDofs = w.dofVector().coefficients();

  GridFunction<BFT, RT> dirichletDataNummeric(
              make_shared_from_ref(context),
              make_shared_from_ref(pwiseLinears),
              dirichletDataDofs);

  // First get the projections onto the space of piecewise constant functions.
  Vector<RT> rhsVector = dirichletDataNummeric.projections(make_shared_from_ref(pwiseConstants));

  // Solve the dense system via Eigen
  Vector<RT> solVector = decomp_.solve(rhsVector);

  GridFunction<BFT,RT> solution(make_shared_from_ref(context),
                                make_shared_from_ref(pwiseConstants),
                                solVector);

  // const GridFunction<BFT, RT>& solFun = solution.gridFunction();
  neumannDataDofs = solution.coefficients();
}

#endif // #ifndef ELLIPTIC_HH
