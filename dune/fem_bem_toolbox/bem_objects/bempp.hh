#ifndef BEMPP_HH
#define BEMPP_HH
// Copyright (C) 2011 by the BEM++ Authors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if BEMPP
/*
#include "bempp/assembly/assembly_options.hpp"
#include "bempp/assembly/boundary_operator.hpp"
#include "bempp/assembly/discrete_boundary_operator.hpp"
#include "bempp/assembly/assembled_potential_operator.hpp"
#include "bempp/assembly/context.hpp"
#include "bempp/assembly/evaluation_options.hpp"
#include "bempp/assembly/grid_function.hpp"
#include "bempp/assembly/l2_norm.hpp"
#include "bempp/assembly/numerical_quadrature_strategy.hpp"
#include "bempp/assembly/surface_normal_independent_function.hpp"
*/


#include "bempp/assembly/boundary_operator.hpp"
#include "bempp/assembly/evaluation_options.hpp"
#include "bempp/assembly/grid_function.hpp"
#include "bempp/assembly/l2_norm.hpp"
#include "bempp/assembly/numerical_quadrature_strategy.hpp"
#include "bempp/assembly/surface_normal_and_domain_index_dependent_function.hpp"
#include "bempp/assembly/discrete_boundary_operator.hpp"
#include "bempp/common/global_parameters.hpp"


#include "bempp/assembly/identity_operator.hpp"
#if COMPLEX
#include "bempp/assembly/helmholtz_3d_single_layer_boundary_operator.hpp"
#include "bempp/assembly/helmholtz_3d_double_layer_boundary_operator.hpp"
#include "bempp/assembly/helmholtz_3d_single_layer_potential_operator.hpp"
#include "bempp/assembly/helmholtz_3d_double_layer_potential_operator.hpp"
#else
#include "bempp/assembly/laplace_3d_single_layer_boundary_operator.hpp"
#include "bempp/assembly/laplace_3d_double_layer_boundary_operator.hpp"
#include "bempp/assembly/laplace_3d_single_layer_potential_operator.hpp"
#include "bempp/assembly/laplace_3d_double_layer_potential_operator.hpp"
#endif

#include "bempp/common/boost_make_shared_fwd.hpp"

#include "bempp/grid/grid.hpp"
#include "bempp/grid/concrete_grid.hpp"
#include "bempp/grid/grid_factory.hpp"
// #include "bempp/grid/concrete_grid.hpp"

// #include "bempp/linalg/default_iterative_solver.hpp"

#include "bempp/space/piecewise_linear_continuous_scalar_space.hpp"
#include "bempp/space/piecewise_constant_scalar_space.hpp"

#include "bempp/common/eigen_support.hpp"

#include <iostream>
#include <fstream>

typedef double BFT; // basis function type
#if COMPLEX
typedef std::complex<double> RT; // result type (type used to represent discrete operators)
#else
typedef double RT; // result type (type used to represent discrete operators)
#endif
typedef double CT; // coordinate type

// class that gives either a decaying exponential or an incident wave, to then be used accordingly by derived classes
class ShapeData
{
public:
    // Type of the function's values (e.g. float or std::complex<double>)
    typedef RT ValueType;
    // Type of coordinates (must be the "real part" of ValueType)
    typedef CT CoordinateType;

    // Number of components of the function's argument
    int argumentDimension() const { return 3; }
    // Number of components of the function's value
    int resultDimension() const { return 1; }

    ShapeData()
      : omega_( Dune::Fem::Parameter::getValue< double >( "helmholtz.omega", 0 ) )
      , amplitude_( Dune::Fem::Parameter::getValue< double >( "helmholtz.amplitude", 0 ) )
      , directionA_( CoordinateType(0) )
      , directionB_( CoordinateType(0) )
      , directionC_( CoordinateType(0) )
      , incident_( false )
    {
      directionA_ = 1.0 / std::sqrt(5.0);
      directionB_ = 2.0 / std::sqrt(5.0);
      directionC_ = 0.0;
      const std::string problemNames [] = { "bulkprob", "surfprob", "bulkwave", "surfwave" };
      const int problemNumber = Dune::Fem::Parameter::getEnum("volume.problem", problemNames, 0 );
      incident_ =  problemNumber > 1;
    }

    // Evaluate the function at the point "point" and store result in
    // the array "result"
    inline void evaluate(const Eigen::Ref<Bempp::Vector<CoordinateType>>& point,
                         const Eigen::Ref<Bempp::Vector<CoordinateType>>& normal,
                         int domainIndex,
                         Eigen::Ref<Bempp::Vector<ValueType>> result) const
    {
      evaluate( point(0),point(1),point(2),result(0) );
    }

    inline void evaluate(const CoordinateType &x, const CoordinateType &y, const CoordinateType &z,
                         ValueType &result) const {
        CoordinateType r = sqrt(x*x+y*y+z*z);
#if DROP
        // set constant electric surface potential
        result = 1.0;
#else
        // the original real scalar test example
        if( amplitude_ < 1.0e-8 )
        {
          result = 2 * x * z / (r * r * r * r * r) - y / (r * r * r);
          return;
        }

        // for Helmholtz problem with an incident field, "r" refers to the dot product with the wave direction
        if( incident_ )
          r = x * directionA_ + y * directionB_ + z * directionC_;

        // real part of solution
        double real = std::cos( omega_ * r );
        // imaginary part of solutionn
        double imag = std::sin( omega_ * r );

#if COMPLEX
        result = std::complex<double>(real,imag);
        if( incident_ )
        result *= amplitude_;
#else
        // real part of solution
        result = real * amplitude_;
        // imaginary part of solutionn
        // result(1) = imag * amplitude_;
#endif
        // extra division to create decaying exponential function
        if( !incident_ )
        {
            result /= 4.*M_PI*r;
        }
#endif
        // result = -1 / r;
    }
private:
  const double omega_;
  const double amplitude_;
  CoordinateType directionA_;
  CoordinateType directionB_;
  CoordinateType directionC_;
  bool incident_;
};

class FTClass
{
public:
    static bool ft(bool set, bool value)
    {
      static bool firsttime=true;
      if(set) firsttime = value;
      return firsttime;
    };
};

class DirichletData : public ShapeData
{
  typedef ShapeData BaseType;
public:
    DirichletData()
      : squash_( Dune::Fem::Parameter::getValue< double >( "helmholtz.squash", 1.0 ) )
    {}

    inline void evaluate(const Eigen::Ref<Bempp::Vector<CoordinateType>>& point,
                         const Eigen::Ref<Bempp::Vector<CoordinateType>>& normal,
                         int domainIndex,
                         Eigen::Ref<Bempp::Vector<ValueType>> result) const {
      BaseType::evaluate(point,normal,domainIndex,result);
        result *= squash_;
    }
private:
  const double squash_;
};

class IncidentData : public ShapeData
{
  typedef ShapeData BaseType;
public:
    IncidentData()
      : zap_( true )
    {
      const std::string problemNames [] = { "bulkprob", "surfprob", "bulkwave", "surfwave" };
      const int problemNumber = Dune::Fem::Parameter::getEnum("volume.problem", problemNames, 0 );
      zap_ =  problemNumber < 2;
    }

    inline void evaluate(const CoordinateType &x, const CoordinateType &y, const CoordinateType &z,
                         ValueType &result) const {
      if( zap_ )
        result = 0;
      else
        BaseType::evaluate( x,y,z,result );
    }

    inline void evaluate(const Eigen::Ref<Bempp::Vector<CoordinateType>>& point,
                         const Eigen::Ref<Bempp::Vector<CoordinateType>>& normal,
                         int domainIndex,
                         Eigen::Ref<Bempp::Vector<ValueType>> result) const {
      if( zap_ )
        result(0) = 0;
      else
        BaseType::evaluate(point,normal,domainIndex,result);
    }
private:
  double zap_;
};

class ExactNeumannData
{
public:
    // Type of the function's values (e.g. float or std::complex<double>)
    typedef RT ValueType;
    // Type of coordinates (must be the "real part" of ValueType)
    typedef CT CoordinateType;

    // Number of components of the function's argument
    int argumentDimension() const { return 3; }
    // Number of components of the function's value
    int resultDimension() const { return 1; }

    // Evaluate the function at the point "point" and store result in
    // the array "result"
    inline void evaluate(const Eigen::Ref<Bempp::Vector<CoordinateType>>& point,
                         const Eigen::Ref<Bempp::Vector<CoordinateType>>& normal,
                         int domainIndex,
                         Eigen::Ref<Bempp::Vector<ValueType>> result) const {
        CoordinateType x = point(0), y = point(1), z = point(2);
        CoordinateType r = sqrt(point(0) * point(0) +
                point(1) * point(1) +
                point(2) * point(2));
        result(0) = -6 * x * z / (r * r * r * r * r * r) + 2 * y / (r * r * r * r);
    }
};

#if 0
template <class GridType, class DirichletDataType, class NeumannDataType>
int bempp(const GridType &dgrid, DirichletDataType &dirichletDeta, NeumannDataType &neumannData)
{
    auto& dirichletDataDofs = dirichletDeta.dofVector().coefficients();
    auto& neumannDataDofs = neumannData.dofVector().coefficients();
    GridType &dunegrid = const_cast<GridType&>(dgrid);
    // Import symbols from namespace Bempp to the global namespace

    using namespace Bempp;

    // Load mesh

    shared_ptr<Grid> grid ( new ConcreteGrid<GridType>(&dunegrid,GridParameters::TRIANGULAR,false) );

    // Initialize the spaces

    PiecewiseLinearContinuousScalarSpace<BFT> pwiseLinears(grid);
    PiecewiseConstantScalarSpace<BFT> pwiseConstants(grid);

    // Define the quadrature strategy

    AccuracyOptions accuracyOptions;
    // Increase by 2 the order of quadrature rule used to approximate
    // integrals of regular functions on pairs on elements
    accuracyOptions.doubleRegular.setRelativeQuadratureOrder(2);
    // Increase by 2 the order of quadrature rule used to approximate
    // integrals of regular functions on single elements
    accuracyOptions.singleRegular.setRelativeQuadratureOrder(2);
    NumericalQuadratureStrategy<BFT, RT> quadStrategy(accuracyOptions);

    // Specify the assembly method. We want to use ACA

    AssemblyOptions assemblyOptions;
    // AcaOptions acaOptions; // Default parameters for ACA
    // assemblyOptions.switchToAcaMode(acaOptions);

    // Create the assembly context

    Context<BFT, RT> context(make_shared_from_ref(quadStrategy), assemblyOptions);

    // Construct elementary operators

    BoundaryOperator<BFT, RT> slpOp =
            laplace3dSingleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseConstants),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants));
    BoundaryOperator<BFT, RT> dlpOp =
            laplace3dDoubleLayerBoundaryOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants));
    BoundaryOperator<BFT, RT> idOp =
            identityOperator<BFT, RT>(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseLinears),
                make_shared_from_ref(pwiseConstants));

    // Form the right-hand side sum

    BoundaryOperator<BFT, RT> rhsOp = -0.5 * idOp + dlpOp;

    // Construct the grid function representing the (input) Dirichlet data

    GridFunction<BFT, RT> dirichletDataNummeric(
                make_shared_from_ref(context),
                make_shared_from_ref(pwiseLinears),
                dirichletDataDofs);

    if ( FTClass::ft(false,false) )
    {
      GridFunction<BFT, RT> dirichletDataAnalytic(
                  make_shared_from_ref(context),
                  make_shared_from_ref(pwiseLinears),
                  make_shared_from_ref(pwiseLinears),
                  surfaceNormalAndDomainIndexDependentFunction(DirichletData()));
      const_cast<Bempp::Vector<double>&>(dirichletDataDofs) = dirichletDataAnalytic.coefficients();
      FTClass::ft(true,false);
      // Warp::set();
      std::cout << "\n Using Analytic Dirichlet data \n" << std::endl;
      dirichletDataNummeric = dirichletDataAnalytic;
    }
    else
      std::cout << "\n Using Nummeric Dirichlet data \n" << std::endl;

    // GridFunction<BFT, RT>& dirichletData = *dirichletDataPointer;

    // Construct the right-hand-side grid function

    GridFunction<BFT, RT> rhs = rhsOp * dirichletDataNummeric;

    // Initialize the solver

    DefaultIterativeSolver<BFT, RT> solver(slpOp);
    solver.initializeSolver(defaultGmresParameterList(1e-5));

    // Solve the equation

    Solution<BFT, RT> solution = solver.solve(rhs);
    std::cout << solution.solverMessage() << std::endl;

    // Extract the solution in the form of a grid function
    // and export it in VTK format

    const GridFunction<BFT, RT>& solFun = solution.gridFunction();
    /*
    exportToVtk(solFun, VtkWriter::CELL_DATA, "Neumann_data", "solution");

    // Compare the numerical and analytical solution on the grid

    // GridFunction<BFT, RT> exactSolFun(
    //             make_shared_from_ref(context),
    //             make_shared_from_ref(pwiseConstants),
    //             make_shared_from_ref(pwiseConstants),
    //             surfaceNormalIndependentFunction(ExactNeumannData()));
    CT absoluteError, relativeError;
    estimateL2Error(
                solFun, surfaceNormalIndependentFunction(ExactNeumannData()),
                quadStrategy, absoluteError, relativeError);
    std::cout << "Relative L^2 error: " << relativeError << std::endl;

    // GridFunction<BFT, RT> diff = solFun - exactSolFun;
    // double relativeError = diff.L2Norm() / exactSolFun.L2Norm();
    // std::cout << "Relative L^2 error: " << relativeError << std::endl;

    // Prepare to evaluate the solution on an annulus outside the sphere

    // Create potential operators

    Laplace3dSingleLayerPotentialOperator<BFT, RT> slPotOp;
    Laplace3dDoubleLayerPotentialOperator<BFT, RT> dlPotOp;

    // Construct the array 'evaluationPoints' containing the coordinates
    // of points where the solution should be evaluated

    const int rCount = 51;
    const int thetaCount = 361;
    const CT minTheta = 0., maxTheta = 2. * M_PI;
    const CT minR = 1., maxR = 2.;
    const int dimWorld = 3;
    arma::Mat<CT> evaluationPoints(dimWorld, rCount * thetaCount);
    for (int iTheta = 0; iTheta < thetaCount; ++iTheta) {
        CT theta = minTheta + (maxTheta - minTheta) *
            iTheta / (thetaCount - 1);
        for (int iR = 0; iR < rCount; ++iR) {
            CT r = minR + (maxR - minR) * iR / (rCount - 1);
            evaluationPoints(0, iR + iTheta * rCount) = r * cos(theta); // x
            evaluationPoints(1, iR + iTheta * rCount) = r * sin(theta); // y
            evaluationPoints(2, iR + iTheta * rCount) = 0.;             // z
        }
    }

    // Use the Green's representation formula to evaluate the solution

    EvaluationOptions evaluationOptions;

    arma::Mat<RT> field =
        -slPotOp.evaluateAtPoints(solFun, evaluationPoints,
                                  quadStrategy, evaluationOptions) +
         dlPotOp.evaluateAtPoints(dirichletData, evaluationPoints,
                                  quadStrategy, evaluationOptions);

    // Export the solution into text file

    std::ofstream out("solution.txt");
    out << "# x y z u\n";
    for (int i = 0; i < rCount * thetaCount; ++i)
        out << evaluationPoints(0, i) << ' '
            << evaluationPoints(1, i) << ' '
            << evaluationPoints(2, i) << ' '
            << field(0, i) << '\n';
    */
    neumannDataDofs = solFun.coefficients();
}
#endif

#endif

#endif
