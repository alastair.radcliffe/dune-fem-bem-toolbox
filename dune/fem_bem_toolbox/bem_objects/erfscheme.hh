#ifndef ELLIPT_ERFSCHEME_HH
#define ELLIPT_ERFSCHEME_HH

// iostream includes
#include <iostream>

// include discrete function space
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/finitevolume.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/common/adaptmanager.hh>

// include discrete function
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>

#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
/*********************************************************/

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include "bempp.hh"
#include "erfoperator.hh"

// ErfScheme
//----------

/*******************************************************************************
 * template arguments are:
 * - GridPart: the part of the grid used to tessellate the
 *             computational domain
 * - Model: description of the data functions and methods required for the
 *          elliptic operator (massFlux, diffusionFlux)
 *     Model::ProblemType boundary data, exact solution,
 *                        and the type of the function space
 *******************************************************************************/
template < class Model, class BemScheme >
class ErfScheme
{
public:
  //! type of the mathematical model
  typedef Model ModelType ;
  typedef BemScheme BemSchemeType ;

  //! grid view (e.g. leaf grid view) provided in the template argument list
  typedef typename ModelType::GridPartType GridPartType;

  //! type of underyling hierarchical grid needed for data output
  typedef typename GridPartType::GridType GridType;

  //! type of function space (scalar functions, f: \Omega -> R)
  typedef typename ModelType :: FunctionSpaceType   FunctionSpaceType;

  //! choose type of discrete function space
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPartType, 1 > DiscreteFunctionSpaceType;
  typedef typename Solvers<DiscreteFunctionSpaceType,eigen,true>::DiscreteFunctionType DiscreteFunctionType;

  typedef typename BemSchemeType::DiscreteFunctionType BemDiscreteFunctionType;
  typedef typename BemSchemeType::DiscreteFunctionSpaceType BemDiscreteFunctionSpaceType;

  /*********************************************************/

  //! define Laplace operator
  typedef ErfOperator< DiscreteFunctionType, DiscreteFunctionType, ModelType > ErfOperatorType;

  ErfScheme( GridPartType &gridPart,
             const ModelType& implicitModel, BemSchemeType& bemScheme, const std::string prefix="" )
    : implicitModel_( implicitModel ),
      bemScheme_( bemScheme ),
      gridPart_( gridPart ),
      p1Space_( gridPart_ ),
      solution_( prefix+"solution", p1Space_ ),
      // the elliptic operator (implicit)
      implicitOperator_( implicitModel_, p1Space_ )
  {
    // set all DoF to zero
    solution_.clear();
    // create the glue object
    implicitOperator_.coupleInit( gridPart );
    FTClass::ft(true,true);
    // bemScheme_.erfSetup(solution_);
  }

  const DiscreteFunctionSpaceType &discreteSpace() const
  {
    return p1Space_;
  }

  DiscreteFunctionType &solution()
  {
    return solution_;
  }
  const DiscreteFunctionType &solution() const
  {
    return solution_;
  }

  BemDiscreteFunctionType &bemSolution()
  {
    return bemScheme_.solution();
  }

  const BemDiscreteFunctionType &bemSolution() const
  {
    return bemScheme_.solution();
  }

  const BemDiscreteFunctionSpaceType &bemDiscreteSpace() const
  {
    return bemScheme_.discreteSpace();
  }

  //! set-up the rhs for the coupling values
  template <class OtherDiscreteFunctionType>
  void setup(OtherDiscreteFunctionType &otherSolution_)
  {
    bemScheme_.setup( otherSolution_ );
  }

  //! set-up the rhs for the coupling values
  template <class OtherDiscreteFunctionType>
  void prepare(OtherDiscreteFunctionType &otherSolution_)
  {
    bemScheme_.prepare( otherSolution_ );
  }

  //! solve the system
  void solve ( bool assemble, double uincFactor )
  {
    bemScheme_.solve(assemble);
    // bemScheme_.solution() *= factor;
    std::cout << "     BEM FILL...";
    bemScheme_.fill( solution_, uincFactor );
  }

protected:

  const ModelType& implicitModel_;      // the mathematical model

  BemSchemeType& bemScheme_;      // the BEM scheme

  GridPartType  &gridPart_;             // grid part(view), e.g. here the leaf grid the discrete space is build with

  DiscreteFunctionSpaceType p1Space_; // discrete function space
  DiscreteFunctionType solution_;       // the unknown

  ErfOperatorType implicitOperator_;    // the implicit operator
};

#endif // end #if ELLIPT_ERFSCHEME_HH
