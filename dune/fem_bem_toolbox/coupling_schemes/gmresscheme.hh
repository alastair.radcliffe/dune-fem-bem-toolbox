#ifndef GMRESSCHEME_HH
#define GMRESSCHEME_HH

// iostream includes
#include <iostream>

#include "couplingscheme.hh"

// include discrete function space
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/finitevolume.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/common/adaptmanager.hh>

// include discrete function
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>

#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
/*********************************************************/

#include <dune/fem_bem_toolbox/special_solvers/istlinverseoperators.hh>

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>
#include <dune/fem_bem_toolbox/bem_objects/bemscheme.hh>
#include "gmresoperator.hh"

template < class SchemeOne, class SchemeTwo >
class GMResCouplingScheme : public CouplingScheme< SchemeOne, SchemeTwo >
{
public:
  typedef CouplingScheme< SchemeOne, SchemeTwo > BaseType;
  typedef typename SchemeOne::DiscreteFunctionType DiscreteFunctionTypeOne;
  typedef typename SchemeTwo::BemDiscreteFunctionType DiscreteFunctionTypeTwo;
  //! define Laplace operator
  //! define Laplace operator
  typedef GMResOperator< SchemeOne, SchemeTwo, DiscreteFunctionTypeOne> GMResOperatorType;
  typedef Dune::Fem::ISTLInverseOperator< DiscreteFunctionTypeOne, Dune::Fem::ISTLRestartedGMRes > LinearInverseOperatorType;

  GMResCouplingScheme( SchemeOne &schemeOne, SchemeTwo &schemeTwo )
    : BaseType(schemeOne,schemeTwo)
    , solution_( "solution_rhs", schemeOne.discreteSpace() )
    , bRhs_( "gmres_bRhs", schemeTwo.bemDiscreteSpace() )
    , rhs_( "gmres_rhs", schemeOne.discreteSpace() )
    , fbop_( schemeOne,schemeTwo )
    , solverEps_( Dune::Fem::Parameter::getValue< double >( "poisson.solvereps", 1e-8 ) )
  {
    solution_.clear();
  }

  //! set-up the rhs for the coupling values
  void prepare( bool assemble )
  {
    // Dirichlet data (will be zero for sor) given to BEM
    schemeTwo_.prepare( schemeOne_.solution() );
    std::cout << "     SCHEME TWO SOLVE ..." << std::endl;

    // BEM solve to give Neumann data ( compute V^{-1} u_inc )
    schemeTwo_.solve( assemble, 1. );

    bRhs_.assign( schemeTwo_.bemSolution() );  // this should be - A_FB A_BB^{-1} u_inc as is
    schemeTwo_.makeSolutionCopy();        // needed to reconstruct u_B from u_F at the end

    std::cout << "     SCHEME ONE PREPARE ..." << std::endl;

    // Dirichlet data set on edge of FEM mesh or Neumann data given to FEM ( set V^{-1} u_inc as Neumann data in fem scheme )
    schemeOne_.prepare( schemeTwo_.solution() );
    solution_.assign( schemeOne_.solution() );  // store dirichlet data for initial guess

    std::cout << "     SCHEME ONE SOLVE ..." << std::endl;

    // FEM solve to give Dirichlet data ( = rhs for gmres )
    schemeOne_.solve( assemble );

    rhs_.assign( schemeOne_.solution() );
  }

  void setup()
  {
    // set-up the coupling
    schemeTwo_.setup( schemeOne_.solution() );
    schemeOne_.setup( schemeTwo_.solution() );
  }

  //! solve the system
  void solve ( bool assemble = false )
  {
    // solution_.clear();

    std::cout << "SOLVE" << std::endl;

    // on-the-fly version (does not work with ISTL solvers)
    LinearInverseOperatorType solver( fbop_, solverEps_, solverEps_, 200, true );

    // solve system
    solver( rhs_, solution_ );

    std::cout << "SOLVE COMPLETED" << std::endl;

    // copy into femscheme to make original output still work
    schemeOne_.solution().assign( solution_ );

    // reconstruct u_B
    schemeTwo_.prepare( schemeOne_.solution() );
    schemeTwo_.solve(true,1.);
    schemeTwo_.bemSolution().axpy( 1., bRhs_ );
    schemeTwo_.fullBemSolutionAxpy();

    schemeOne_.rhs().assign( rhs_ );

    BaseType::norm();
  }

protected:
  using BaseType::schemeOne_;
  using BaseType::schemeTwo_;
  using BaseType::n_;
  using BaseType::error_;
  using BaseType::errer_;
  DiscreteFunctionTypeOne solution_;
  DiscreteFunctionTypeTwo bRhs_;
  DiscreteFunctionTypeOne rhs_;
  GMResOperatorType fbop_;
  const double solverEps_;
};

#endif
