#ifndef JACOBISCHEME_HH
#define JACOBISCHEME_HH

// iostream includes
#include <iostream>

#include "couplingscheme.hh"

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include <dune/fem_bem_toolbox/fem_objects/femscheme.hh>

template < class SchemeOne, class SchemeTwo >
class JacobiCouplingScheme : public CouplingScheme< SchemeOne, SchemeTwo >
{
public:
  typedef CouplingScheme< SchemeOne, SchemeTwo > BaseType;
  typedef typename SchemeOne::DiscreteFunctionType DiscreteFunctionTypeOne;
  typedef typename SchemeTwo::DiscreteFunctionType DiscreteFunctionTypeTwo;

  JacobiCouplingScheme( SchemeOne &schemeOne, SchemeTwo &schemeTwo )
    : BaseType(schemeOne,schemeTwo)
  {}

  //! solve the system
  void solve ( bool assemble = false )
  {
    // iterate a certain number of times
    for( n_ = 0; n_ <= maxIt_; ++n_ )
    {
      // set-up the coupling
      schemeOne_.setup(schemeTwo_.solution());
      schemeTwo_.setup(schemeOne_.solution());

      // setup the right hand side
      schemeOne_.prepare(schemeTwo_.solution());
      schemeTwo_.prepare(schemeOne_.solution());

      // solve once
      schemeOne_.solve( n_ == 0 || assemble );
      schemeTwo_.solve( n_ == 0 || assemble );

      BaseType::norm();

      // check for convergence
      bool done = schemeOne_.stop(); bool dune = schemeTwo_.stop();
      if ( ( n_ > 3 ) && done && dune )
        break;

      // only continue iterating if solution still converging
      else
      {
        // reset the problem
        schemeOne_.reset();
        schemeTwo_.reset();
      }
    }
  }

protected:
  using BaseType::schemeOne_;
  using BaseType::schemeTwo_;
  using BaseType::n_;
  using BaseType::maxIt_;
  using BaseType::error_;
  using BaseType::errer_;
};

#endif
