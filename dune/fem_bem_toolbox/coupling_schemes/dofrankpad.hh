#ifndef DOFRANKPAD_HH
#define DOFRANKPAD_HH


template < class DFT , class outDFT >
struct DofRankPad
{
  // extract type of discrete function space
  typedef typename DFT::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // extract type of function space
  static const int dimensionworld = GridPartType::dimensionworld;

  typedef Dune::Fem::FunctionSpace<double,double,DFT::DiscreteFunctionSpaceType::dimDomain,
                              outDFT::DiscreteFunctionSpaceType::dimRange> FunctionSpaceType;
  // range type that is required
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  // constructor
  DofRankPad( const DFT &f1 )
  : lf1_( f1 )
  {}

  template< class Point >
  void evaluate ( const Point &x, RangeType &ret ) const
  {
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    typename DFT::RangeType phi;
    lf1_.evaluate( x, phi );
    int i = 0;
    for( ; i < dimRange; ++i )
      ret[ i ] = phi[ 0 ];
  }
  template< class Point >
  void jacobian ( const Point &x, JacobianRangeType &ret ) const
  {
    const int dimRange = FunctionSpaceType::RangeType::dimension;
    typename DFT::JacobianRangeType phi;
    lf1_.jacobian( x, phi );
    int i = 0;
    for( ; i < dimensionworld; ++i )
      ret[ i ] = phi[ i ];
    for( ; i < dimRange; ++i )
      ret[ i ] = 0;
  }

  // initialize to new entity
  void init( const EntityType &entity )
  {
    lf1_.init( entity );
  }

  private:
    typename DFT::LocalFunctionType lf1_;
};


#endif // end #if DOFRANKPAD_HH
