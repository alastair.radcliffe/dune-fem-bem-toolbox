#ifndef COUPLINGSCHEME_HH
#define COUPLINGSCHEME_HH

// iostream includes
#include <iostream>

// include discrete function space
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/finitevolume.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/space/common/adaptmanager.hh>

// include discrete function
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>

#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>

// lagrange interpolation
#include <dune/fem/operator/lagrangeinterpolation.hh>
/*********************************************************/

#include <dune/fem_bem_toolbox/special_solvers/istlinverseoperators.hh>

// include norms
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

template < class SchemeOne, class SchemeTwo >
class CouplingScheme
{
public:
  CouplingScheme( SchemeOne &schemeOne, SchemeTwo &schemeTwo )
    : schemeOne_(schemeOne)
    , schemeTwo_(schemeTwo)
    , n_(0)
    , maxIt_( Dune::Fem::Parameter::getValue< int >( "coupling.maxit", 31 ) )
    , error_(0)
    , errer_(0)
  {}

  //! set-up the rhs for the coupling values
  void prepare( bool assemble )
  {}

  void setup()
  {
    // set-up the coupling
    schemeTwo_.setup( schemeOne_.solution() );
    schemeOne_.setup( schemeTwo_.solution() );
  }

  //! solve the system
  void norm ()
  {
    // calculate standard error
    // select norm for error computation
    typedef typename SchemeOne::ModelType::GridPartType GridPartType;
    typedef typename SchemeTwo::ModelType::GridPartType GrydPartType;
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    typedef Dune::Fem::L2Norm< GrydPartType > NurmType;
    NormType norm( schemeOne_.discreteSpace().gridPart() );
    NurmType nurm( schemeTwo_.discreteSpace().gridPart() );
    auto gridExactSolution = schemeOne_.model().exactSolution();
    auto grydExactSolution = schemeTwo_.model().exactSolution();

    error_ = norm.distance( gridExactSolution, schemeOne_.solution() );
    errer_ = nurm.distance( grydExactSolution, schemeTwo_.solution() );

    std::cout << "Error at iteration " << n_ << " = " << error_ << " + " << errer_ << " = " << error_ + errer_ << std::endl;
  }

  int n(){return n_;}
  double error(){return error_ + errer_;}
  double errorOne(){return error_;}
  double errorTwo(){return errer_;}

protected:
  SchemeOne &schemeOne_;
  SchemeTwo &schemeTwo_;
  int n_;
  int maxIt_;
  double error_;
  double errer_;
};

#endif
