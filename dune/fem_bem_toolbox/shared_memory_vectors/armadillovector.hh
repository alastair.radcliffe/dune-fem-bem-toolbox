#ifndef ARMADILLOVECTOR_HH
#define ARMADILLOVECTOR_HH
#if USE_ARMADILLO
#include <armadillo>
#include <dune/fem/storage/vector.hh>
namespace Dune
{
  namespace Fem
  {
    template< class Field >
    class ArmadilloVector
    : public VectorDefault< Field, ArmadilloVector< Field > >
    {
      typedef ArmadilloVector< Field > ThisType;
      typedef VectorDefault< Field, ThisType > BaseType;

    public:
      //! field type of the vector
      typedef Field FieldType;

      using BaseType :: assign;

    protected:
      typedef arma::Col< FieldType > DofStorageType;
      DofStorageType fields_;

    public:
      //! Constructor setting up a vector of a specified size
      inline explicit ArmadilloVector ( unsigned int size = 0 )
      : fields_( size )
      {}

      //! Constructor setting up a vector iniitialized with a constant value
      inline ArmadilloVector ( unsigned int size,
                               const FieldType s )
      : fields_( size )
      {
        assign( s );
      }

      //! Copy constructor setting up a vector with the data of another one
      template< class T >
      inline ArmadilloVector ( const VectorInterface< T > &v )
      : fields_()
      {
        assign( v );
      }

      //! Copy constructor setting up a vector with the data of another one (of the same type)
      inline ArmadilloVector ( const ThisType &v )
      : fields_()
      {
        assign( v );
      }

      //! Assign another vector to this one
      template< class T >
      inline ThisType &operator= ( const VectorInterface< T > &v )
      {
        assign( v );
        return *this;
      }

      //! Assign another vector (of the same type) to this one
      inline ThisType &operator= ( const ThisType &v )
      {
        assign( v );
        return *this;
      }

      //! Initialize all fields of this vector with a scalar
      inline ThisType &operator= ( const FieldType s )
      {
        assign( s );
        return *this;
      }

      inline const FieldType &operator[] ( unsigned int index ) const
      {
        return fields_( index );
      }

      inline FieldType &operator[] ( unsigned int index )
      {
        return fields_( index );
      }

      /*
      template< class T >
      inline void assign ( const VectorInterface< T > &v )
      {
        fields_.assign( v );
      }
      */

      inline const DofStorageType &coefficients () const
      {
        return fields_;
      }
      inline DofStorageType &coefficients ()
      {
        return fields_;
      }

      inline const FieldType *leakPointer () const
      {
        return fields_.memptr();
      }

      inline FieldType *leakPointer ()
      {
        return fields_.memptr();
      }

      inline void reserve ( unsigned int newSize )
      {
        fields_.set_size( newSize );
      }

      inline void resize ( unsigned int newSize )
      {
        fields_.set_size( newSize );
      }

      inline void resize ( unsigned int newSize,
                           const FieldType defaultValue )
      {
        fields_.set_size( newSize ); fields_.fill( defaultValue );
      }

      inline unsigned int size () const
      {
        return fields_.size();
      }
    };
  }
}
#endif
#endif // ARMADILLOVECTOR_HH
