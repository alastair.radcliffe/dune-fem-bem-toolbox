#ifndef EIGENVECTOR_HH
#define EIGENVECTOR_HH
#if USE_EIGEN
// #error INCLUDING EIGENVECTOR_HH
// #include <eigen>
#include <dune/fem/storage/vector.hh>

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <mpi.h>

// using namespace boost::interprocess;

template< class SharedObjectType, class Field >
struct SharedMemory
{
  static void clearSharedMemory( const char *name)
  {
    boost::interprocess::shared_memory_object::remove(name);
  }

  SharedMemory( const char *name, unsigned int size)
  {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    // (rank == 0)
    {
      Field field;
      unsigned int bytesPerSizeUnit = sizeof(field);

      std::cout << "Setting-up shared memory of size " << size << " (" << size*bytesPerSizeUnit << " bytes)" << " with name " << name << " ..." << std::endl;

      // creating our first shared memory object.
      boost::interprocess::shared_memory_object sharedmem1(boost::interprocess::open_or_create, name, boost::interprocess::read_write);

      // setting the size of the shared memory
      sharedmem1.truncate (size*bytesPerSizeUnit);

      // map the shared memory to current process
      region_ = boost::interprocess::mapped_region(sharedmem1, boost::interprocess::read_write);

      // access the mapped region using get_address
      std::cout << "AAA " << rank << "  " << sharedmem1.get_name() << '\n';

      boost::interprocess::offset_t sized;
      if (sharedmem1.get_size(sized))
        std::cout << "BBB " << rank << "  " << sized << '\n';
    }
    sharedObject_ = new SharedObjectType ( (Field*)(region_.get_address()),size );
  }
  SharedObjectType *get()
  {
    return static_cast<SharedObjectType* >(region_.get_address());
  }
  SharedObjectType *getso()
  {
    return sharedObject_;
  }
  private:
  SharedObjectType* sharedObject_;
  boost::interprocess::mapped_region region_;
};

namespace Dune
{
  namespace Fem
  {
    template< class Field >
    class EigenVector
    : public VectorDefault< Field, EigenVector< Field > >
    {
      typedef EigenVector< Field > ThisType;
      typedef VectorDefault< Field, ThisType > BaseType;

    public:
      //! field type of the vector
      typedef Field FieldType;

      using BaseType :: assign;

    protected:
      // typedef Bempp::Vector< FieldType > DofStorageType;
      typedef Bempp::Vector< FieldType > BaseDofStorageType;
      typedef Eigen::Map< BaseDofStorageType > DofStorageType;
      DofStorageType* fields_;

    public:

      static int getMemId()
      {
        static int memoryId = 0;
        std::cout << "Memory id = " << memoryId << std::endl;
        return memoryId++;
      }

      DofStorageType* createFields(const bool shared, unsigned int siz)
      {
        if(!shared)
        {
          notshared_ = new FieldType[siz];
          return new DofStorageType(notshared_,siz);
        }

        // create a unique shared memory identification name
        const std::string sharedMemoryNames_ [] = { "one", "two", "thr", "fou", "fiv" , "six" , "sev" , "eig" , "nin" , "ten"};
        const char * memName = sharedMemoryNames_[ getMemId() ].c_str();
        std::cout << "memName is " << memName << std::endl;
        try {

          int rank,size;
          MPI_Comm_rank(MPI_COMM_WORLD,&rank);
          MPI_Comm_size(MPI_COMM_WORLD,&size);

          std::cout << "starting: " << rank << "/" << size << "  name is " << memName << std::endl;

          // clear any previous instances of shared objects with the same name
          SharedMemory<DofStorageType,FieldType>::clearSharedMemory(memName);

          // sync processes
          MPI_Barrier(MPI_COMM_WORLD);

          // create (or just refer to) a new shared memory of the given name
          memoryPtr_ = new SharedMemory<DofStorageType,FieldType>(memName,siz);

          std::cout << "returned: " << rank << "/" << size << std::endl;

          /*
          // at the moment only rank zero can fill the buffer
          if (rank == 0)
          std::strcpy(memory.get(), "Hello World!");
          std::cout << "Process zero fills the memory..." << std::endl;
          // sync processes
          MPI_Barrier(MPI_COMM_WORLD);
          std::cout << "...waiting" << std::endl;
          char *str1 = memory.get();
          std::cout << "rank=" << rank << " " << str1 << std::endl;
          MPI_Finalize();
          */

        } catch (boost::interprocess::interprocess_exception& e) {
        // .. .  clean up
        }
        return memoryPtr_->getso();
      }

      //! Constructor setting up a shared vector of a specified size
      inline explicit EigenVector ( unsigned int size = 0, const bool shared = false )
        : fields_( createFields(shared,size) )
      {
        if(shared)
          std::cout << "Requested a shared dof vector of size " << size << " units\n" << std:: endl;
        else
          std::cout << "Requested a private dof vector of size " << size << " units\n" << std:: endl;
      }

      //! Constructor setting up a vector iniitialized with a constant value
      inline EigenVector ( unsigned int size,
                               const FieldType s )
      : fields_( new DofStorageType(size) )
      {
        assign( s );
      }

      //! Copy constructor setting up a vector with the data of another one
      template< class T >
      inline EigenVector ( const VectorInterface< T > &v )
      : fields_( new DofStorageType() )
      {
        assign( v );
      }

      //! Copy constructor setting up a vector with the data of another one (of the same type)
      inline EigenVector ( const ThisType &v )
      : fields_( new DofStorageType() )
      {
        assign( v );
      }

      //! Assign another vector to this one
      template< class T >
      inline ThisType &operator= ( const VectorInterface< T > &v )
      {
        assign( v );
        return *this;
      }

      //! Assign another vector (of the same type) to this one
      inline ThisType &operator= ( const ThisType &v )
      {
        assign( v );
        return *this;
      }

      //! Initialize all fields of this vector with a scalar
      inline ThisType &operator= ( const FieldType s )
      {
        assign( s );
        return *this;
      }

      inline const FieldType &operator[] ( unsigned int index ) const
      {
        return (*fields_)( index );
      }

      inline FieldType &operator[] ( unsigned int index )
      {
        return (*fields_)( index );
      }

      /*
      template< class T >
      inline void assign ( const VectorInterface< T > &v )
      {
        fields_.assign( v );
      }
      */

      inline const DofStorageType &coefficients () const
      {
        return *fields_;
      }
      inline DofStorageType &coefficients ()
      {
        return *fields_;
      }

      inline const FieldType *leakPointer () const
      {
        return fields_->memptr();
      }

      inline FieldType *leakPointer ()
      {
        return fields_->memptr();
      }

      inline void reserve ( unsigned int newSize )
      {
        fields_->resize( newSize );
      }

      inline void resize ( unsigned int newSize )
      {
        fields_->resize( newSize );
      }

      inline void resize ( unsigned int newSize,
                           const FieldType defaultValue )
      {
        fields_->resize( newSize ); assign( defaultValue );
      }

      inline unsigned int size () const
      {
        return fields_->size();
      }
    private:
      SharedMemory<DofStorageType,FieldType>* memoryPtr_;
      FieldType* notshared_;
    };
  }
}
#endif
#endif // EIGENVECTOR_HH
