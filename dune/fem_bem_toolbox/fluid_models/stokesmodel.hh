#ifndef STOKES_MODEL_HH
#define STOKES_MODEL_HH

#include <cassert>
#include <cmath>

#include <dune/fem/io/parameter.hh>
#include <dune/fem/function/common/gridfunctionadapter.hh>

#include <dune/fem_bem_toolbox/fem_objects/probleminterface.hh>
#include <dune/fem_bem_toolbox/fem_objects/model.hh>

#if NOCOUPLING
template< class FunctionSpace, class GridPart >
struct StokesModel : public DiffusionModel<FunctionSpace,GridPart>
{
  typedef DiffusionModel<FunctionSpace,GridPart> BaseType;
#else
template< class FunctionSpace, class GridPart, class BoundaryDataFunctionSpace >
struct StokesModel : public DiffusionModel<FunctionSpace,GridPart,BoundaryDataFunctionSpace>
{
  typedef DiffusionModel<FunctionSpace,GridPart,BoundaryDataFunctionSpace> BaseType;
  typedef BoundaryDataFunctionSpace BoundaryDataFunctionSpaceType;
#endif
  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef ProblemInterface< FunctionSpaceType > ProblemType ;

  //! constructor
  StokesModel( const ProblemType& problem,
             const GridPart &gridPart )
    : BaseType(problem,gridPart),
      stab_( Dune::Fem::Parameter::getValue< double >( "stokes.stability", 1e-4 ) )
  {
  }

  template< class Entity, class Point >
  void source ( const Entity &entity,
                const Point &x,
                const RangeType &value,
                const JacobianRangeType &gradient,
                RangeType &flux ) const
  {
    linSource( value, entity, x, value, gradient, flux );
  }

  // the linearization of the source function
  template< class Entity, class Point >
  void linSource ( const RangeType& uBar,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    flux = 0.0;

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      // try conventional incompressibility condition = - nabla dot u times q in dimdomain(=pressure)  row of aphi
      flux[ dimDomain ] -= gradient[ localRow ][ localRow ];

      // integration by parts of conventional incompressibility constraint (as above) to allow inclusion within adphi
      // adphi[ dimDomain ][ localRow ] -= phi[ localCol ][ localRow ];
    }
  }

  // the linearization of the advective term
  template< class Entity, class Point >
  void advectionSource ( const RangeType& uBar,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    const DomainType xGlobal = entity.geometry().global( coordinate( x ) );
    RangeType n;
    problem_.n(xGlobal,n);

    static const int dimDomain = RangeType::dimension-1;

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      for( unsigned int localCol = 0; localCol < dimDomain; ++localCol )
        flux[ localRow ] -= n[ localRow ] * uBar[ localCol ] * gradient[ localRow ][ localCol ];
    }
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void diffusiveFlux ( const Entity &entity,
                       const Point &x,
                       const RangeType &value,
                       const JacobianRangeType &gradient,
                       JacobianRangeType &flux ) const
  {
    linDiffusiveFlux( value, gradient, entity, x, value, gradient, flux );
  }

  // linearization of diffusiveFlux
  template< class Entity, class Point >
  void linDiffusiveFlux ( const RangeType& uBar,
                          const JacobianRangeType& gradientBar,
                          const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          const JacobianRangeType &gradient,
                          JacobianRangeType &flux ) const
  {
    flux = 0.0;

    static const int dimDomain = RangeType::dimension-1;

    const auto &geometry = entity.geometry();
    const double scaling = std::pow( geometry.volume() , 1.0 / 3.0 );

    for( unsigned int localRow = 0; localRow < dimDomain; ++localRow )
    {
      // velocity equations -- localRow = 0, 1, 2 = dimDomain - 1
      flux[ localRow ] += gradient[ localRow ];
      flux[ localRow ][ localRow ] -= value[ dimDomain ];

      // pressure equation -- localRow = dimdomain

      // stabilization term = - k * nabla p dot nabla q  where p and q are pressure shape functions
      flux[ dimDomain ][ localRow ] -= stab_ * scaling * gradient[ dimDomain ][ localRow ];
#if DROP
      for( unsigned int localCol = 0; localCol < dimDomain; ++localCol )
      {
        // extra transpose of gradient term
        flux[ localRow ][ localCol ] += gradient[ localCol ][ localRow ];
      }
#endif
    }
  }

  //! exact some methods from the problem class
  bool hasDirichletBoundary () const
  {
    return BaseType::hasDirichletBoundary() ;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  bool isDirichletPoint( const DomainType& x ) const
  {
    return BaseType::isDirichletPoint(x) ;
  }

  template< class Entity, class Point >
  void g( const RangeType& uBar,
          const Entity &entity,
          const Point &x,
          RangeType &u ) const
  {
    BaseType::g(uBar,entity,x,u);
  }

  // return Fem::Function for Dirichlet boundary values
  typename BaseType::DirichletBoundaryType dirichletBoundary( ) const
  {
    return BaseType::dirichletBoundary();
  }

protected:
  using BaseType::problem_;
  bool implicit_;
  double stab_;

};
#endif // #ifndef STOKES_MODEL_HH
