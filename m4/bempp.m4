# searches for bempp-headers and libs

# DUNE_PATH_BEMPP()
AC_DEFUN([DUNE_PATH_BEMPP],[
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_WITH(bempp,
    AC_HELP_STRING([--with-bempp=PATH],[directory where BEMPP is installed]))

# store old values
ac_save_LDFLAGS="$LDFLAGS"
ac_save_CPPFLAGS="$CPPFLAGS"
ac_save_LIBS="$LIBS"

# initilize to sane value
HAVE_BEMPP=0

## do nothing if no --with-bempp was supplied
if test x$with_bempp != xno ; then

  # is --with-bempp=PATH used?
  AS_IF([test "x$with_bempp" != "x"],[
    AS_IF([test -d $with_bempp],[
      AC_MSG_NOTICE([searching for BEMPP in $with_bempp...])
      BEMPPROOT=`cd $with_bempp && pwd`
    ],[
      AC_MSG_WARN([BEMPP directory '$with_bempp' does not exist or is inaccessible])
    ])
  ],[
    # educated guess for bempp root
    for d in /usr /usr/local /usr/local/bempp /opt/bempp; do
      AC_MSG_NOTICE([searching for BEMPP in $d...])
      AS_IF([test -f $d/lib/pkgconfig/bempp.pc -o -x $d/bin/bemppversion],[
        BEMPPROOT="$d"
        break
      ])
    done
  ])

  REM_PKG_CONFIG_PATH=$PKG_CONFIG_PATH
  PKG_CONFIG_PATH="$BEMPPROOT:$BEMPPROOT/lib:$BEMPPROOT/external/lib/pkconfig:$PKG_CONFIG_PATH"

  # lib dir and include path 

  BEMPP_INCLUDE_PATHS="-I$BEMPPROOT/include -I$BEMPPROOT/include/bempp -I$BEMPPROOT/external/include -I$BEMPPROOT/external/include/eigen3 -I$BEMPPROOT/external/include/tbb"

  BEMPP_LIB_PATHS="-L$BEMPPROOT/lib -L$BEMPPROOT/external/lib -rdynamic -rpath $BEMPPROOT/lib -rpath $BEMPPROOT/external/lib"

  BEMPP_LIBS="-lbempp  -ltbb_debug -ltbb -ltbbmalloc -ltbbmalloc_debug"
  # -Wl,-rpath,/-l:/external/-l:/external/-l64:" 

  # restore PKG_CONFIG_PATH 
  PKG_CONFIG_PATH=$REM_PKG_CONFIG_PATH

  AC_LANG_PUSH([C++])

  # set variables so that tests can use them
  BEMPP_INC_FLAG="$BEMPP_INCLUDE_PATHS -DENABLE_BEMPP=1"
  CPPFLAGS="$ac_save_CPPFLAGS $BEMPP_INC_FLAG"
  # check for header
  AC_CHECK_HEADERS([bempp/config_bempp.hpp], 
     [BEMPP_CPPFLAGS="$BEMPP_INC_FLAG"
      BEMPP_LDFLAGS="$BEMPP_LIB_PATHS"
      BEMPP_LIBS="$BEMPP_LIBS"
      HAVE_BEMPP="1"],
      AC_MSG_WARN([bempp/config_bempp.hpp not found in $BEMPP_INCLUDE_PATH]))
   
  # We check only whether linking with the library works, not for an actual
  # function from that library.  So we won't need any special stuff in the
  # CPPFLAGS
  CPPFLAGS="$ac_save_CPPFLAGS"
  ac_save_LDFLAGS="$LDFLAGS"
  LDFLAGS="$LDFLAGS $BEMPP_LIB_PATHS"
  ac_save_LIBS="$LIBS"
  LIBS="$LIBS $BEMPP_LIBS"

  LDFLAGS="$ac_save_LDFLAGS"
  LIBS="$ac_save_LIBS"

  AC_LANG_POP([C++])

## end of bempp check (--without wasn't set)
fi

# survived all tests?
if test x$HAVE_BEMPP = x1 ; then
  AC_SUBST(BEMPP_LIBS, $BEMPP_LIBS)
  AC_SUBST(BEMPP_LDFLAGS, $BEMPP_LDFLAGS)
  AC_SUBST(BEMPP_CPPFLAGS, $BEMPP_CPPFLAGS)
  AC_DEFINE(HAVE_BEMPP, ENABLE_BEMPP,
    [This is only true if BEMPP-library was found by configure 
     _and_ if the application uses the BEMPP_CPPFLAGS])

  # add to global list
  DUNE_ADD_ALL_PKG([BEMPP], [\${BEMPP_CPPFLAGS}],
                   [\${BEMPP_LDFLAGS}], [\${BEMPP_LIBS}])

  with_bempp="version $BEMPP_VERSION"
  with_bempp_long="$BEMPPROOT"
else
  AC_SUBST(BEMPP_LIBS, "")
  AC_SUBST(BEMPP_LDFLAGS, "")
  AC_SUBST(BEMPP_CPPFLAGS, "")

  # set variable for summary
  with_bempp="no"
  with_bempp_long=""
fi
  
# also tell automake
AM_CONDITIONAL(BEMPP, test x$HAVE_BEMPP = x1)

# reset old values
LIBS="$ac_save_LIBS"
CPPFLAGS="$ac_save_CPPFLAGS"
LDFLAGS="$ac_save_LDFLAGS"

DUNE_ADD_SUMMARY_ENTRY([BEMPP],[$with_bempp],[$with_bempp_long])

])
