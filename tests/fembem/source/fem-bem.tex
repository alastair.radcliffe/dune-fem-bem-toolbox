\documentclass{article}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{tikz}
    \addtolength{\oddsidemargin}{-.875in}
    \addtolength{\evensidemargin}{-.875in}
    \addtolength{\textwidth}{1.75in}

    \addtolength{\topmargin}{-.875in}
    \addtolength{\textheight}{1.75in}
\begin{document}

\title{A FEM-BEM Coupling Example}
\author{Matthew Scroggs, UCL}

\maketitle

\section*{The Problem}

Let \(\Omega=[0,1]^3\) be the unit cube. Let \(\Omega'=\mathbb{R}^3\setminus\Omega\).
Let \(\Gamma = \partial\Omega\) be the boundary between \(\Omega\) and \(\Omega'\).

%\begin{center}\input{Diagram1.tex}\end{center}

We will look for solutions to the following PDE:
$$
\begin{array}{ll}
\Delta u + (kn(x))^2u = 0 & \mathrm{in\ }\Omega\\
\Delta u + k^2u = 0 & \mathrm{in\ }\Omega'
\end{array}
$$

This models an incident wave travelling through a cube of varying density. \(u\)
is the amplitude of the wave. \(k\) is the wave number of the wave. \(n(x)\) varies
within \(\Omega\) to model the change in wave number due to varying density.

The incident wave, \(u^\mathrm{inc}\) is given by $$u^\mathrm{inc}=e^{ik\mathbf{x}\cdot\mathbf{d}},$$ where \(\mathbf{d}\)
is the direction of the wave.

In the example presented, $$n(x)=
\frac
{1-\frac{1}{2}e^{
    -\max\left(
            \left(x_0-\frac{1}{2}\right)^2,
            \left(x_1-\frac{1}{2}\right)^2,
            \left(x_2-\frac{1}{2}\right)^2
\right)}}
{1-\frac{1}{2}e^{-\frac{1}{4}}}
$$ will be used. A 1D slice through \(n\) looks like:

%\begin{center}\includegraphics[width=200px]{wavenumber.png}\end{center}

This \(n\) has been chosen so that is is \(1\) on \(\Gamma\) and decreases
towards the centre of the cube, representing an object which is denser towards
its centre.

\section*{The Weak Form}
The problem can be rewritten as:
$$
\begin{array}{ll}
\Delta u_1 + (kn(x))^2u_1 = 0 & \mathrm{in\ }\Omega\\
\Delta u_2 + k^2u_2 = 0 & \mathrm{in\ }\Omega'\\
u_1 = u_2 & \mathrm{on\ }\Gamma\\
\frac{\partial u_1}{\partial n} = \frac{\partial u_2}{\partial n} & \mathrm{on\ }\Gamma
\end{array}
$$
A finite element method will be used in the interior \(\Omega\) to approximate \(u_1\)
 and a boundary element method will be used in the exterior \(\Omega'\) to approximate \(u_2\).

\subsection*{The FEM Part}
In $\Omega$, the weak form for \(u_1\) is
$$\int_\Omega \nabla u_1\cdot\nabla v -k^2\int_\Omega n^2u_1v - \int_{d\Omega} v\frac{\partial u_1}{\partial n} = 0,$$
or
$$\langle\nabla u_1,\nabla v\rangle_\Omega - k^2\langle n^2u_1,v\rangle_\Omega - \langle \lambda,v\rangle_\Gamma=0,$$
where $\lambda=\frac{\partial u_1}{\partial n}$.

Later, we will write this as
$$Au_1-k^2 Mu_1-M_\Gamma u_1 = 0$$

\subsection*{The BEM Part}

In \(\Omega'\), \(u_2\) can be written as $$u_2 = u^\mathrm{inc}+u^\mathrm{s},$$ where
\(u^\mathrm{inc}\) is the incident wave and \(u^\mathrm{s}\) is the scattered wave.

As given in \cite{coltonandkress},
$$0 = \mathcal{K}u^\text{inc}-\mathcal{V}\frac{\partial u^{inc}}{\partial n}$$
and
$$u^\text{s} = \mathcal{K}u^\text{s}-\mathcal{V}\frac{\partial u^{s}}{\partial n},$$
where $\mathcal{V}$ and $\mathcal{K}$ are the single and double layer potentials respectively. Adding these, we get
$$u^\text{s} = \mathcal{K}u_2-\mathcal{V}\frac{\partial u_2}{\partial n}.$$
This will be used to find $u^\text{s}$ for plotting later.

Taking the trace on the boundary gives
$$
u^\text{inc} =u_2-u^s = \left(\frac{1}{2}I-K\right)u_2+V\lambda.
$$
where $\lambda=\frac{\partial u_2}{\partial n}=\frac{\partial u_1}{\partial n}$.

\subsection*{The Full Formulation (Total Fields only)}

Fenics (Combined solution)

$$
\begin{bmatrix}
    A-k^2 M & -M_\Gamma\\
    \frac{1}{2}I-K & V
\end{bmatrix}
\begin{bmatrix}
    u\\
    \lambda
\end{bmatrix}
=
\begin{bmatrix}
    0\\
    u^\text{inc}
\end{bmatrix}.
$$

Dune (Iterative solution)

$$
V\lambda_2^{(n)}
=
\left(-\frac{1}{2}I+K\right)u_1^{(n-1)}
+
    u^\text{inc}
$$

$$
    \left(A-k^2 M\right)u_1^{(n+1)}
=
    M_\Gamma\lambda_2^{(n)}
$$

\subsection*{The Full Formulation (Mixed Fields)}

$$
\begin{bmatrix}
    A-k^2 M & -M_\Gamma\\
    \frac{1}{2}I-K & V
\end{bmatrix}
\begin{bmatrix}
    u\\
    \lambda
\end{bmatrix}
=
\begin{bmatrix}
    0\\
    u^\text{inc}
\end{bmatrix}.
$$
\section*{Implementation}

When implemented in BEM++ and FEniCS, with \(\mathbf{d}=\left(\frac{1}{\sqrt{5}},\frac{2}{\sqrt{5}},0\right)\),
a 2D slice through the solution at \(x_2=\frac{1}{2}\) looks as follows. The second diagram shows where \(\Omega\), \(\Omega'\)
and \(\mathbf{d}\) are for reference.

%\begin{center}\includegraphics[width=300px]{solution_correct.png}\end{center}


%\begin{center}\includegraphics[width=300px]{solution_correct_annotated.png}\end{center}

\begin{thebibliography}{1}

\bibitem{coltonandkress}
  David Colton and Rainer Kress,
  \emph{Integral Equation Methods in Scattering Theory}.

\end{thebibliography}

\end{document}
