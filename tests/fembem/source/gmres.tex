\documentclass{article}
\newcommand{\ABB}{V}
\newcommand{\AFF}{\left(A-k^2 M\right)}
\newcommand{\AFi}{\left(A-k^2 M\right)}
\newcommand{\AFd}{\left(A_{\Gamma}-k^2 M_{\Gamma}\right)}
\newcommand{\ABF}{\left(\frac{1}{2}I-K\right)}
\newcommand{\AFB}{\left(-M_{\Gamma}\right)}
\newcommand{\Afem}{A_{\rm fem}}
\newcommand{\Abem}{A_{\rm bem}}
\newcommand{\ED}{K'}
\newcommand{\EN}{(-V')}
\newcommand{\uB}{\lambda}
\newcommand{\uF}{u}
\newcommand{\uFi}{u}
\newcommand{\uFd}{u_{\Gamma}}
\newcommand{\lB}{u^{inc}}
\newcommand{\lF}{0}
\newcommand{\gd}{g}
\newcommand{\bprep}{{\rm bem.prepare}\ }
\newcommand{\bsolve}{{\rm bem.solve}\ }
\newcommand{\fprep}{{\rm fem.prepare}\ }
\newcommand{\fsolve}{{\rm fem.solve}\ }
\begin{document}

\section{Iterative Coupling Methods}

\subsection{Problem setting}

Consider the Helmholtz equation in three dimensions with a variable wave number inside an interior domain, $\Omega$, to be discretized by finite elements, which has a boundary, $\Gamma$, along which boundary element integrals will be located to support an exterior domain $\Omega^\infty$, in which the wave number is considered to be constant.

The governing set of equations and coupling conditions may be defined as

\begin{eqnarray}
\Delta u_1 + (kn(x))^2u_1 &= 0 & \mathrm{in\ }\Omega
\label{interioreqn}
\\
\Delta u_2 + k^2u_2 &= 0 & \mathrm{in\ }\Omega^+
\label{exterioreqn}
\\
u_1-u_2 &= 0 & \mathrm{on\ }\Gamma
\label{outerdirichletcontinuity}
\end{eqnarray}

\noindent with the additional boundary condition of {\emph{either}}

\begin{eqnarray}
\frac{\partial u_1}{\partial n} -\frac{\partial u_2}{\partial n} &= 0 & \mathrm{on\ }\Gamma
\label{neumanncontinuity}
\end{eqnarray}

\noindent for the non-overlapping coupling, {\emph{or}}

\begin{eqnarray}
u_1-u_2 & = 0 & \mathrm{on\ }\Gamma^+
\label{innerdirichletcontinuity}
\end{eqnarray}

\noindent for the overlapping case, where the BEM integrals are now supported on a boundary $\Gamma^+ \neq \Gamma$ that lies inside the finite element region, $dist(\Gamma^+,\Gamma)>0$.

The FEM discretisation of equation~(\ref{interioreqn}), multipling through by a function $v$ and integrate by parts, is as before


\begin{eqnarray}
\nabla\,u_1\nabla\,v - (kn(x))^2u_1 - u_{1,n} v &= 0 & \mathrm{in\ }\Omega
\label{interioreqndisc}
\end{eqnarray}

Similarly, the BEM discretization of equation~(\ref{exterioreqn}), will give

\begin{equation}
 \left(\frac{1}{2}I-K\right)u_2+V\lambda.= u^{inc}
\label{exterioreqndisc}
\end{equation}

\noindent where the single layer $V$ and double-layer $K$ operators are as defined before.

For the non-overlapping coupling, we require continuity of Dirichlet and Neumann data over the same boundary $\Gamma$, and these two equations will be sufficient to derive the iterative scheme.

For the overlapping scheme, however, we require the exterior representation formula, which is only valid for scattered fields

\begin{equation}
u^s = \mathcal{K}u_2-\mathcal{V}\frac{\partial u_2}{\partial n}.
\end{equation}

\noindent to provide the BEM Dirichlet data for $u_2$ on the boundary $\Gamma$ which is some distance further out than $\Gamma^+$.

\subsection{Methods}

\subsubsection{Non-Overlapping Dirichlet-Neumann Coupling}

For the non-overlapping iterative coupling, the combined FEM-BEM problem may be written as an augmented matrix system

\begin{equation}
   \left( \begin{array}{cc}
     \AFF & \AFB \\ \ABF & \ABB
   \end{array} \right)
   \left( \begin{array}{c}
     \uF \\ \uB
   \end{array} \right) =
   \left( \begin{array}{c}
     \lF \\ \lB
   \end{array} \right)
\label{nonoverlappingcombined}
\end{equation}

\noindent where we have used equations~(\ref{interioreqndisc}) and~(\ref{exterioreqndisc}) and both equations are assembled in the combined system~(\ref{nonoverlappingcombined}) so as to refer to the same Dirichlet, $u$, and Neumann, $\lambda$, values held in the solution vector, and thus satisfy the boundary conditions~(\ref{outerdirichletcontinuity}) and~(\ref{neumanncontinuity}).

% Method we have now is a simple Gauss-Seidel method:
% $$ \uB^{k+1} = \ABB^{-1}(\lB - \ABF\uF^k)~, \qquad
%    \uF^{k+1} = \AFF^{-1}(\lF - \AFB\uB^{k+1})
% $$


The second row of~(\ref{nonoverlappingcombined}) allows us to form the Neumann data as

\begin{equation}
 \uB = \ABB^{-1}\lB - \ABB^{-1}\ABF\uF
\end{equation}

\noindent which may be substituted into the first row to give

\begin{equation}
\AFF \uF + \AFB\ABB^{-1}\lB - \AFB\ABB^{-1}\ABF\uF = 0
\end{equation}

Multiplying through by $\AFF^{-1}$ we have

\begin{equation}
\uF - \AFF^{-1}\AFB\ABB^{-1}\ABF\uF = - \AFF^{-1}\AFB\ABB^{-1}\lB
\end{equation}

\noindent which is of the form $A\,u=b$ where

\begin{eqnarray}
A &=& I -  \AFF^{-1}\AFB\ABB^{-1}\ABF
\\
   b &=& - \AFF^{-1}\AFB\ABB^{-1}\lB
\end{eqnarray}


\subsubsection{Overlapping Dirichlet-Dirichlet coupling}

The FEM problem with Dirichlet conditions is constructed in the form (splitting degrees
of freedom in the inside of $\Omega$ from those on the boundary):
$$
   \left( \begin{array}{cc}
     \AFi & \AFd \\ 0 & I
   \end{array} \right)
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right) =
   \left( \begin{array}{c}
     \lF \\ \gd
   \end{array} \right)
$$
where $\gd$ is the Dirichlet data on $\partial\Omega$.
In the following we refer to the matrix for this problem with $\Afem$.

We assume ${\rm dist}(\Gamma,\partial\Omega)>h$ so that the
bem scheme is based on the interior degrees of freedom only. So the bem
problem on $\Gamma$ reads

$$ \ABB\uB + \ABF\uFi = \lB~. $$

\noindent where the projection of the finite element solution $\uFi$ onto the boundary $\Gamma$ to form its Dirichlet trace -- which is acted on by the integral operator $\ABF$ -- is assumed.

Using the exterior representation formula for the {\emph{scattered}} field, we must add the incident field, to form the ``total'' Dirichlet data $\gd$ required on the edge of the finite element mesh

\begin{eqnarray}
\gd &=& \lB + \ED\uFi + \EN\uB
\\
       &=& \lB + \ED\uFi + \EN\ABB^{-1}\left(\lB - \ABF\uFi\right)
\\
       &=& \lB + \EN\ABB^{-1}\lB + \left(\ED - \EN\ABB^{-1}\ABF\right) \uFi
\\
       &=&  \lB + \EN\ABB^{-1}\lB + \Abem\uFi.
\end{eqnarray}


Inserting this into the fem problem leads to

\begin{equation}
   \left( \begin{array}{cc}
     \AFi & \AFd \\ -\Abem & I
   \end{array} \right)
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right) =
   \left( \begin{array}{c}
     \lF \\ \lB + \EN\ABB^{-1}\lB \\
   \end{array} \right)
   \label{overlappingfull}
\end{equation}

Note that the matrix is of the form
\begin{eqnarray}
   \left( \begin{array}{cc}
     \AFi & \AFd \\ 0 & I
   \end{array} \right)
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right)
   +
   \left( \begin{array}{cc}
     0 & 0 \\ -\Abem & 0
   \end{array} \right)
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right)
   \nonumber\\
   =
   \Afem
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right)
   +
   \left( \begin{array}{cc}
     0 & 0 \\ -\Abem & 0
   \end{array} \right)
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right)
\end{eqnarray}

So multiplying equation~(\ref{overlappingfull}) through with $\Afem^{-1}$ leads to

$$
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right)
   + \Afem^{-1}
   \left( \begin{array}{cc}
     0 & 0 \\ -\Abem & 0
   \end{array} \right)
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right) =
   \Afem^{-1}
   \left( \begin{array}{c}
     \lF \\  \lB + \EN\ABB^{-1}\lB \\
   \end{array} \right)
$$
or
$$
  \left[
   I
   - \Afem^{-1}
   \left( \begin{array}{cc}
     0 & 0 \\ \Abem & 0
   \end{array} \right)
   \right]
   \left( \begin{array}{c}
     \uFi \\ \uFd
   \end{array} \right) =
   \Afem^{-1}
   \left( \begin{array}{c}
     \lF \\  \lB + \EN\ABB^{-1}\lB \\
   \end{array} \right)
$$


\noindent which is of the form $A\,u=b$ where


\begin{eqnarray}
A &=& I  - \Afem^{-1}
   \left( \begin{array}{cc}
     0 & 0 \\ \Abem & 0
   \end{array} \right)
\\
   b &=&  \Afem^{-1}
   \left( \begin{array}{c}
     \lF \\  \lB + \EN\ABB^{-1}\lB \\
   \end{array} \right)
\end{eqnarray}

\subsection{Implementation using Gmres}

The complete implementation for the gmres solution of both the non-overlapping and the overlapping formulations may be organised into the application of four simple programming blocks: ``prepare'' and ``solve'' procedures involving the BEM operands, and similarly ``prepare'' and ``solve'' procedures on the FEM discretisations.


\begin{enumerate}

\item[{\bf{BEM Prepare:}}] Prepares the Dirichlet data rhs of a BEM DtN solve:
      \\
      $b_{bem}(u)=\left[\lB\right]+\ABF u$

\item[{\bf{BEM Solve}}] Applies the Dirichlet-to-Neumann mapping to the Dirichlet data, and then, for overlapping only, uses the exterior representation formulae:
      \\
      $\lambda  =\ABB^{-1}b_{bem}(u)$
      \\
      For overlapping only:
\\
      $\uFd(u)=\left[\lB\right]+\EN\lambda-\ED u$

\item[{\bf{FEM Prepare:}}] Simply forms the rhs for the fem problem:
\\
Non-overlapping: $b_{fem}(u)=-\AFB\lambda$
\\
Overlapping: $b_{fem}(u)=\left( \begin{array}{c}
     \lF \\  \uFd(u) \\
   \end{array} \right)$

\item[{\bf{FEM Solve}}] Solves the FEM problem with its Dirichlet (overlapping) or Neumann (non-overlapping) boundaries:
\\
$b(u)=\Afem^{-1}b_{fem}(u)$

Note: $\Afem=\AFF$ for non-overlapping

\item[{\bf{Gmres Thing}}] Updates the Gmres residual:
\\
$w = \uF - b(u)$
\end{enumerate}

Clearly, $b(u=0)$ will just give us the rhs $b$ appropriate to either overlapping or non-overlapping formulation. So before performing the gmres iterations, we may form the rhs to use for either the non-overlapping or the overlapping case, by executing each of the first four coding blocks above once, in the order in which they are presented, but starting
 with a zero ``guess'' for $u$.


 Similarly, for the Matrix-Vector multiply required for gmres, we wish to compute $w=A\uF$, given $\uF$, where

 $$
 A=
   I
   - \Afem^{-1}
   \left( \begin{array}{cc}
     0 & 0 \\ \Abem & 0
   \end{array} \right)
   $$
 and

$$
\Abem
=
\EN\ABB^{-1}\ABF
-\ED
$$

\noindent for the overlapping case and

\begin{eqnarray}
A &=& I -  \AFF^{-1}\AFB\ABB^{-1}\ABF
\end{eqnarray}

\noindent for the non-overlapping scheme.

This may be done by running through all five code blocks above, with now a non-zero $u$, in the order they are presented, but without the incident field terms inside the square brackets $\left[\lB\right]$.




Now taking $\Afem=\AFF$ (same as before but with Neuman b.c.) and $\Abem=-\AFB\ABB^{-1}\ABF$ this is of the
same form as for the overlapping case. Overlapping:
$$ {\rm Overlapping} \quad A = I + \Afem^{-1}\Abem~, \qquad
   {\rm Non-Overlapping} \quad I + \Afem^{-1} \left( \begin{array}{cc}
     0 & 0 \\ \Abem & 0
   \end{array} \right)
$$
In both cases the operator $\Abem$ takes the fem solution through the bem
scheme and provides the boundary data (Neuman or Dirichlet) for the fem
scheme.
\end{document}
