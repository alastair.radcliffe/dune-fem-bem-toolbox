#include <config.h>

// iostream includes
#include <iostream>

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

// include header for heat model
#include "heat.hh"

#include "heatmodel.hh"
#include "heatscheme.hh"

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( HGridType &grid, HGridType &gryd, int step )
{
  typedef Dune::Fem::FunctionSpace< double, double, HGridType::dimensionworld, 1 > FunctionSpaceType;

  // create time provider
  Dune::Fem::GridTimeProvider< HGridType > timeProvider( grid );

  // we want to solve the problem on the leaf elements of the grid
  typedef Dune::Fem::AdaptiveLeafGridPart< HGridType, Dune::InteriorBorder_Partition > GridPartType;
  GridPartType gridPart(grid);
  GridPartType grydPart(gryd);

  // type of the mathematical model used
  typedef TimeDependentCosinusProduct< FunctionSpaceType > ProblemType;
  typedef HeatModel< FunctionSpaceType, GridPartType > ModelType;

  ProblemType problem( timeProvider ) ;

  // implicit model for left hand side
  ModelType implicitModel( problem, gridPart, true );
  ModelType implycitModel( problem, grydPart, true );

  // explicit model for right hand side
  ModelType explicitModel( problem, gridPart, false );
  ModelType explycitModel( problem, grydPart, false );

  // create heat scheme
  typedef HeatScheme< ModelType, ModelType > SchemeType;
  SchemeType scheme( gridPart, implicitModel, explicitModel );
  SchemeType schyme( grydPart, implycitModel, explycitModel );

  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GridPartType > GridExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, gridPart, 5 );
  GridExactSolutionType grydExactSolution("exact solution", problem, grydPart, 5 );
  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *, GridExactSolutionType * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution) ; // tuple with pointers
  IOTupleType ioTople( &(schyme.solution()), &grydExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step , "lower" ) );
  DataOutputType detaOutput( gryd, ioTople, DataOutputParameters( step , "upper" ) );

  const double endTime  = Dune::Fem::Parameter::getValue< double >( "heat.endtime", 2.0 );
  const double dtreducefactor = Dune::Fem::Parameter::getValue< double >("heat.reducetimestepfactor", 1 );
  double timeStep = Dune::Fem::Parameter::getValue< double >( "heat.timestep", 0.125 );

  timeStep *= pow(dtreducefactor,step);

  // initialize with fixed time step
  timeProvider.init( timeStep ) ;

  // initialize scheme and output initial data
  scheme.initialize();
  schyme.initialize();
  // write initial solve
  dataOutput.write( timeProvider );
  detaOutput.write( timeProvider );

  // calculate error
  double error = 0 ;
  double errer = 0 ;

  // time loop, increment with fixed time step
  for( ; timeProvider.time() < endTime; timeProvider.next( timeStep ) )
  {
    // iterate a certain number of times
    for( int n = 0; n <= 11; ++n )
    {
    // set-up the coupling
    scheme.setup(gridPart,grydPart,schyme.solution(),false);
    schyme.setup(gridPart,grydPart,scheme.solution(),true);
    // assemble explicit pare
    scheme.prepare(gridPart);
    schyme.prepare(grydPart);
    // apply the coupling
    scheme.couple(gridPart,grydPart,schyme.solution(),false);
    schyme.couple(gridPart,grydPart,scheme.solution(),true);
    // solve once (we need to assemble the system the first time the method
    // is called)
    scheme.solve( (timeProvider.timeStep()==0) );
    schyme.solve( (timeProvider.timeStep()==0) );
    dataOutput.write( timeProvider );
    detaOutput.write( timeProvider );
    // reset the problem
    scheme.reset();
    schyme.reset();

  // output final solution
  dataOutput.write( timeProvider );
  detaOutput.write( timeProvider );

  // select norm for error computation
  typedef Dune::Fem::L2Norm< GridPartType > NormType;
  NormType norm( gridPart );
  NormType nurm( grydPart );
  error = norm.distance( gridExactSolution, scheme.solution() );
  errer = nurm.distance( grydExactSolution, schyme.solution() );
  std::cout << "Error at iteration " << n << " = " << error << " + " << errer << " = " << error + errer << std::endl;
    }
  }
  return error + errer;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // set slice value to be used for the gluing
  SliceStore::sliceHome(0.5);
  SliceStore::x(1); SliceStore::y(0); SliceStore::z(0);

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );
  const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << " and " << grydfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  Dune::GridPtr< HGridType > grydPtr( grydfile );
  HGridType& grid = *gridPtr ;
  HGridType& gryd = *grydPtr ;

  // do initial load balance
  grid.loadBalance();
  gryd.loadBalance();

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "heat.repeats", 0 );

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "heat.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );
  Dune::Fem::GlobalRefine::apply( gryd, level * refineStepsForHalf );

  // calculate first step
  double oldError = algorithm( grid, gryd, (repeats > 0) ? 0 : -1 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );
    Dune::Fem::GlobalRefine::apply( gryd, refineStepsForHalf );

    const double newError = algorithm( grid, gryd, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

