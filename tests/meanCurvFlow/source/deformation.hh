#ifndef DEFORMATION_HH
#define DEFORMATION_HH

#include <dune/grid/geometrygrid/coordfunction.hh>
#include <dune/fem/space/common/functionspace.hh>

// DeformationCoordFunction
// ------------------------

template < class SurfaceEvolution >
class DeformationCoordFunction
  : public Dune::AnalyticalCoordFunction< double, 3, 3, DeformationCoordFunction< SurfaceEvolution > >
{
  typedef Dune::AnalyticalCoordFunction< double, 3, 3, DeformationCoordFunction< SurfaceEvolution > > BaseType;

public:
  typedef SurfaceEvolution SurfaceEvolutionType;
  typedef Dune::Fem::FunctionSpace< double, double, 3, 3 > FunctionSpaceType;

  typedef typename SurfaceEvolutionType::DomainType DomainVector;
  typedef typename SurfaceEvolutionType::RangeType RangeVector;
  typedef double RangeFieldType;

  DeformationCoordFunction ( const SurfaceEvolution &surfaceEvolution )
    : surfaceEvolution_( surfaceEvolution )
  {}

  void evaluate ( const DomainVector &x, RangeVector &y ) const
  {
    surfaceEvolution_.evaluate( x, y );
  }

  void setTime( const double time )
  {
    surfaceEvolution_.setTime( time );
  }

private:
  SurfaceEvolution surfaceEvolution_;
};

template <class DiscreteFunctionType>
class DeformationDiscreteFunction
: public Dune::DiscreteCoordFunction< double, 3, DeformationDiscreteFunction< DiscreteFunctionType > >
{
  typedef Dune::DiscreteCoordFunction< double, 3, DeformationDiscreteFunction< DiscreteFunctionType > > BaseType;

  typedef typename DiscreteFunctionType :: GridType GridType ;
  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType ;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;

  typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType ;
  typedef typename DiscreteFunctionType :: RangeType  RangeType ;
public:
  DeformationDiscreteFunction ( GridType &grid)
  : gridPart_( grid ),
    space_( gridPart_ ),
    vertices_( "deformation", space_ )
  {}

  template< class HostEntity , class RangeVector >
  void evaluate ( const HostEntity &hostEntity, unsigned int corner,
                  RangeVector &y ) const
  {
    abort();
  }

  template <class RangeVector>
  void evaluate ( const typename GridType :: template Codim<0>::Entity &entity,
		              unsigned int corner,
                  RangeVector &y ) const
  {
    typedef typename GridType::ctype  ctype;
    enum { dim = GridType::dimension };

    const Dune::ReferenceElement< ctype, dim > &refElement
      = Dune::ReferenceElements< ctype, dim >::general( entity.type() );

    LocalFunctionType localVertices = vertices_.localFunction( entity );

    localVertices.evaluate( refElement.position( corner, dim ), y );
  }

  template <class DF>
  void setTime( const double time, DF &solution )
  {
    const typename DiscreteFunctionType::DofIteratorType end = vertices_.dend();
    typename DF::DofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::DofIteratorType it = vertices_.dbegin();
	       it != end; ++it, ++git )
      *it = *git;
  }

  template <class GF, class DF>
  void initialize(const GF& gf, DF &solution)
  {
    typedef Dune::Fem::LagrangeInterpolation< GF, DiscreteFunctionType > VertexInterpolationType;
    VertexInterpolationType interpolation;
    interpolation( gf, vertices_ );

    const typename DiscreteFunctionType::DofIteratorType end = vertices_.dend();
    typename DF::DofIteratorType git = solution.dbegin();
    for( typename DiscreteFunctionType::DofIteratorType it = vertices_.dbegin();
	       it != end; ++it, ++git )
      *git = *it;
  }

  double radius(double &max, double &min)
  {
    max = 0.0;
    min = 1.0e6;
    double average = 0.;
    double count = 0;
    const DiscreteFunctionSpaceType &dfSpace = vertices_.space();

    const IteratorType end = dfSpace.end();
    for( IteratorType it = dfSpace.begin(); it != end; ++it )
      {
	const EntityType &entity = *it;

	for( unsigned int corner = 0; corner < 3; ++corner )
	  {
	    RangeType y;
	    evaluate( entity, corner, y );

	    double r = y.two_norm();
	    average += r;
	    ++count;

	    if( r > max )
	      max = r;
	    if( r < min )
	      min = r;
	  }
      }
    average /= count;

    if( max - min > 1.0e-8 )
      {
	std::cout << "not circular! (max: " << max << " min: " << min << " )" << std::endl;
      }

    return average;
  }

protected:
  GridPartType gridPart_;
  DiscreteFunctionSpaceType space_;
  DiscreteFunctionType vertices_;
};

#endif // #ifndef DEFORMATION_HH
