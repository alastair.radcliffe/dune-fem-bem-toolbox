#ifndef MCF_HH
#define MCF_HH

#include <cassert>
#include <cmath>

#include <dune/fem/function/common/function.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/quadrature/quadrature.hh>

#include "../../common/source/temporalprobleminterface.hh"

// EvolutionFunction
// -----------------

template< class FunctionSpace, class Impl >
struct EvolutionFunction
: public Dune::Fem::Function< FunctionSpace, Impl >
{
  explicit EvolutionFunction ( const double time = 0.0 )
  : time_( time )
  {}

  const double &time () const
  {
    return time_;
  }

  void setTime ( const double time )
  {
    time_ = time;
  }

private:
  double time_;
};

namespace Problem
{
  // SurfaceEvolution
  // ----------------
  template< class FunctionSpace >
  struct SurfaceEvolution
    : public EvolutionFunction< FunctionSpace, SurfaceEvolution< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    SurfaceEvolution()
    : def_( Dune::Fem::Parameter::getValue< double >( "de.form", 0.5 ) )
    {}

    void evaluate( const DomainType &x, RangeType &y ) const
    {
      assert( dimRange == 3 );
      assert( time() < 1.0e-10 );
      y = x;
      y /= x.two_norm();

      y /= std::pow( def_ , 1.0/3.0 );

      // double r = 1.;
      // double r = 1.5+cos(M_PI*y[0])*sin(M_PI*y[1]);
      // y *= r;
      // y *= 2;
      y[2] *= def_;
      return;
    }

    using EvolutionFunction< FunctionSpace, SurfaceEvolution< FunctionSpace > >::time;

private:
  double def_;
  };
#if 0
  // RHSFunction
  // -----------

  template< class FunctionSpace >
  struct RHSFunction
  : public EvolutionFunction< FunctionSpace, RHSFunction< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    void evaluate( const DomainType &x, RangeType &phi ) const
    {
      phi = RangeType( 0 );
    }

    using EvolutionFunction< FunctionSpace, RHSFunction< FunctionSpace > >::time;
  };
#endif

}


template <class FunctionSpace>
class TimeDependentCosinusProduct : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  TimeDependentCosinusProduct( const Dune::Fem::TimeProviderBase &timeProvider )
    : BaseType( timeProvider )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi  = M_PI*(4*dimDomain*M_PI*std::cos( M_PI*time() ) - std::sin( M_PI*time() ));
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
  }

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    phi = std::cos( M_PI*time() );
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::cos( M_PI*time() )*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }
};

#endif // #ifndef MCF_HH
