#ifndef POISSON_PROBLEMS_HH
#define POISSON_PROBLEMS_HH

#include <cassert>
#include <cmath>
#include <complex>

#include "probleminterface.hh"


// -laplace u + omega * omega u = 0 with Dirichlet boundary conditions on spherical annulus
// Exact solution is u = exp( i omega r ) / ( 4 pi r )
template <class FunctionSpace>
class HelmholtzFundamental : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  HelmholtzFundamental(const double omega)
  : omega_( omega )
  , shift_( Dune::Fem::Parameter::getValue< double >( "helmholtz.shift", 0 ) )
  , c_(1.)
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi = RangeType(0);
  }

  //! the exact solution
  virtual void u(const DomainType& y,
                 RangeType& phi) const
  {
    DomainType x = y;
    x[0] +=  shift_;
    double r = x.two_norm();
    // real part of solution
    double real = std::cos( omega_ * r );
    // imaginary part of solutionn
    double imag = std::sin( omega_ * r );
#if COMPLEX
    phi = std::complex<double>(real,imag);
#else
    // real part of solution
    phi[0] = real;
    // imaginary part of solutionn
    phi[1] = imag;
#endif
    //
    phi /= 4*M_PI*r;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret = 0;
  }
  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = -omega_*omega_/c_*c_;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true;
  }
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

private:
  const double omega_;
  const double shift_;
  const double c_;
};

// -laplace u + omega * omega u = 0 with Dirichlet boundary conditions on spherical annulus
// Exact solution is u = exp( i omega r ) / ( 4 pi r )
template <class FunctionSpace>
class SurfaceHelmholtzFundamental : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  SurfaceHelmholtzFundamental(const double omega)
  : omega_( omega )
  , c_(1.)
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi = RangeType(0);
  }

  //! the exact normal derivative of the solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    double r = x.two_norm();
    // real part of solution
    double real = - omega_ * std::sin( omega_ * r ) - std::cos( omega_ * r ) / r;
    // imaginary part of solutionn
    double imag =   omega_ * std::cos( omega_ * r ) - std::sin( omega_ * r ) / r;
#if COMPLEX
    phi = std::complex<double>(real,imag);
#else
    // real part of solution
    phi[0] = real;
    // imaginary part of solutionn
    phi[1] = imag;
#endif
    //
    phi /= 4*M_PI*r;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret = 0;
  }
  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = -omega_*omega_/c_*c_;
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true;
  }
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }

private:
  const double omega_;
  const double c_;
};

#endif // #ifndef POISSON_HH
