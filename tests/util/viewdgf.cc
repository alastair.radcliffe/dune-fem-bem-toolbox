// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#define FILTERED 1
#define FEM_FEM 0
#define POLORDER 1

#include <dune/grid/test/gridcheck.cc>
#include <dune/grid/test/checkgeometryinfather.cc>
#include <dune/grid/test/checkintersectionit.cc>

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/fem/io/parameter.hh>

#include <dune/fem_fem_coupling/surfacegridclass.hh>
#include <dune/fem_fem_coupling/surfaceExtractor.hh>

using namespace Dune;
template< class Grid >
void test ( Grid &grid )
{
  gridcheck( grid );

  // check the method geometryInFather()
  std::cout << "  CHECKING: geometry in father" << std::endl;
  checkGeometryInFather( grid );
  // check the intersection iterator and the geometries it returns
  std::cout << "  CHECKING: intersections" << std::endl;
  bool skip = ! EnableLevelIntersectionIteratorCheck< Grid >::v;
  checkIntersectionIterator( grid, skip );
}

int main(int argc, char ** argv, char ** envp)
try {
  typedef GridSelector::GridType GridType;
  Dune::Fem::MPIManager::initialize( argc, argv );
  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  std::string filename;

  if (argc>=2)
  {
    filename = argv[1];
  }
  else
  {
    std::cout << "need a dgf file as first run time parameter" << std::endl;
    return 1;
  }

  std::cout << "tester: start grid reading; file " << filename << std::endl;

  typedef GridType::LeafGridView GridView;
  typedef GridView::IndexSet IndexSetType;

  // create Grid from DGF parser
  GridType *grid;
  size_t nofElParams( 0 ), nofVtxParams( 0 );
  std::vector< double > eldat( 0 ), vtxdat( 0 );
  {
    GridPtr< GridType > gridPtr( filename.c_str() );
    gridPtr.loadBalance();
    grid = gridPtr.release();
  }

  GridView gridView = grid->leafGridView();
  std::cout << "Grid size: " << grid->size(0) << std::endl;
  // display
  VTKWriter<GridView> vtkWriter(gridView);
  vtkWriter.write( filename );

#if CHECK
  std::cout << "tester: refine grid" << std::endl;
  grid->globalRefine(Dune::DGFGridInfo<GridType>::refineStepsForHalf());
  std::cout << "Grid size: " << grid->size(0) << std::endl;
  test(*grid);
#endif

  // surface grid
  typedef typename Dune::FemFemCoupling::Glue< GridType >::GrydType GrydType;
  // create grid from DGF file
  const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", GrydType::dimension );
  const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );

  typedef Dune::Fem::LeafGridPart<GridType> GridPart;
  GridPart gridPart( *grid );
  // construct macro using the DGF Parser
  typedef SurfaceExtractor< GridPart > SurfaceExtractorType ;
  SurfaceExtractorType surfaceExtractor(gridPart);
  surfaceExtractor.extractSurface();
  Dune::GridPtr< GrydType > grydPtr( grydfile );
  GrydType& gryd = *grydPtr ;
  typedef GrydType::LeafGridView GrydView;
  GrydView grydView = gryd.leafGridView();
  std::cout << "Gryd size: " << gryd.size(0) << std::endl;
  // display
  VTKWriter<GrydView> vtkWryter(grydView);
  vtkWryter.write( "surface" );

  delete grid;
  return 0;
}
catch( const Dune::Exception &e )
{
  std::cerr << e << std::endl;
  return 1;
}
catch (...)
{
  std::cerr << "Generic exception!" << std::endl;
  return 1;
}
