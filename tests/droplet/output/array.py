try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

s0012chargepoisson0000000_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000000.pvtu'] )

a1_surfacechargesolution0_PVLookupTable = GetLookupTableForArray( "surface chargesolution0", 1, RGBPoints=[0.3, 0.23, 0.299, 0.754, 1.7, 0.706, 0.016, 0.15], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ColorSpace='Diverging', ScalarRangeInitialized=1.0, LockScalarRange=1 )

a1_surfacechargesolution0_PiecewiseFunction = CreatePiecewiseFunction()

s0012chargepoisson0000000_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000000_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

RenderView1 = GetRenderView()

DataRepresentation9 = Show()
DataRepresentation9.Position = [6.0,0.0, 1.55]
DataRepresentation9.ScalarOpacityFunction = []
DataRepresentation9.ColorArrayName = 'surface chargesolution0'
DataRepresentation9.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation9.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000030_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000030.pvtu'] )


s0012chargepoisson0000030_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000030_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation030 = Show()
DataRepresentation030.Position = [4.0, 0.0, 1.55]
DataRepresentation030.ScalarOpacityFunction = []
DataRepresentation030.ColorArrayName = 'surface chargesolution0'
DataRepresentation030.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation030.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000070_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000070.pvtu'] )


s0012chargepoisson0000070_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000070_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation070 = Show()
DataRepresentation070.Position = [2.0, 0.0, 1.55]
DataRepresentation070.ScalarOpacityFunction = []
DataRepresentation070.ColorArrayName = 'surface chargesolution0'
DataRepresentation070.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation070.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000105_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000105.pvtu'] )


s0012chargepoisson0000105_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000105_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation105 = Show()
DataRepresentation105.Position = [0, 0.0, 1.55]
DataRepresentation105.ScalarOpacityFunction = []
DataRepresentation105.ColorArrayName = 'surface chargesolution0'
DataRepresentation105.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation105.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000120_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000120.pvtu'] )


s0012chargepoisson0000120_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000120_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation120 = Show()
DataRepresentation120.Position = [-2.0, 0.0, 1.55]
DataRepresentation120.ScalarOpacityFunction = []
DataRepresentation120.ColorArrayName = 'surface chargesolution0'
DataRepresentation120.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation120.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000135_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000135.pvtu'] )


s0012chargepoisson0000135_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000135_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation135 = Show()
DataRepresentation135.Position = [-4.0, 0.0, 1.55]
DataRepresentation135.ScalarOpacityFunction = []
DataRepresentation135.ColorArrayName = 'surface chargesolution0'
DataRepresentation135.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation135.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000145_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000145.pvtu'] )


s0012chargepoisson0000145_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000145_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation145 = Show()
DataRepresentation145.Position = [-6.0, 0.0, 1.55]
DataRepresentation145.ScalarOpacityFunction = []
DataRepresentation145.ColorArrayName = 'surface chargesolution0'
DataRepresentation145.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation145.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000160_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000160.pvtu'] )


s0012chargepoisson0000160_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000160_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation160 = Show()
DataRepresentation160.Position = [6.0, 0.0, -1.55]
DataRepresentation160.ScalarOpacityFunction = []
DataRepresentation160.ColorArrayName = 'surface chargesolution0'
DataRepresentation160.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation160.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000180_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000180.pvtu'] )


s0012chargepoisson0000180_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000180_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation180 = Show()
DataRepresentation180.Position = [4.0, 0.0, -1.55]
DataRepresentation180.ScalarOpacityFunction = []
DataRepresentation180.ColorArrayName = 'surface chargesolution0'
DataRepresentation180.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation180.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000200_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000200.pvtu'] )


s0012chargepoisson0000200_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000200_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation200 = Show()
DataRepresentation200.Position = [2.0, 0.0, -1.55]
DataRepresentation200.ScalarOpacityFunction = []
DataRepresentation200.ColorArrayName = 'surface chargesolution0'
DataRepresentation200.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation200.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000230_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000230.pvtu'] )


s0012chargepoisson0000230_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000230_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation230 = Show()
DataRepresentation230.Position = [0, 0.0, -1.55]
DataRepresentation230.ScalarOpacityFunction = []
DataRepresentation230.ColorArrayName = 'surface chargesolution0'
DataRepresentation230.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation230.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000267_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000267.pvtu'] )


s0012chargepoisson0000267_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000267_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation267 = Show()
DataRepresentation267.Position = [-2.0, 0.0, -1.55]
DataRepresentation267.ScalarOpacityFunction = []
DataRepresentation267.ColorArrayName = 'surface chargesolution0'
DataRepresentation267.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation267.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000294_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000294.pvtu'] )


s0012chargepoisson0000294_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000294_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation294 = Show()
DataRepresentation294.Position = [-4.0, 0.0, -1.55]
DataRepresentation294.ScalarOpacityFunction = []
DataRepresentation294.ColorArrayName = 'surface chargesolution0'
DataRepresentation294.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation294.ColorAttributeType = 'CELL_DATA'
s0012chargepoisson0000327_pvtu = XMLPartitionedUnstructuredGridReader( FileName=['/storage/maths/masnaj/19.11.15/zoltan/zoltan/dune-fem-fem-coupling/tests/droplet/output/minerva/s0012-charge-poisson-0-000327.pvtu'] )


s0012chargepoisson0000327_pvtu.PointArrayStatus = ['outside exact solution0']
s0012chargepoisson0000327_pvtu.CellArrayStatus = ['rank', 'surface chargesolution0', 'volume']

DataRepresentation327 = Show()
DataRepresentation327.Position = [-6.0, 0.0, -1.55]
DataRepresentation327.ScalarOpacityFunction = []
DataRepresentation327.ColorArrayName = 'surface chargesolution0'
DataRepresentation327.LookupTable = a1_surfacechargesolution0_PVLookupTable
DataRepresentation327.ColorAttributeType = 'CELL_DATA'
RenderView1.CenterAxesVisibility = 0
RenderView1.CameraClippingRange = [2.829189166134136, 11.592396355311493]


Render()
