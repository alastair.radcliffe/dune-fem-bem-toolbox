



module SIZES

    ! Total number of nodes = degrees of freedom
    integer :: maxnod = 23000
    ! Total number of elements
    integer :: maxele = 21000
    ! Total number of "special" nodes
    integer :: maxsid = 30000
    ! Original (static declaration) estimated total number of nodes - for mesh reading purposes
    integer :: omaxnod = 23000
    ! Estimated maximum number of node connections for any row of the complete system matrix
    integer :: width = 60
    ! Compact (no dummey rows) 3 times maxnod
    integer :: c3maxnod = 0

    contains

      !function oldrecord
      !omaxnod=maxnod
      !end function oldrecord

      subroutine set(mxnd,mxel,mxsd,omxnd)
      implicit none
      integer mxnd, mxel, mxsd, omxnd
      maxnod = mxnd; maxele = mxel; maxsid = mxsd; omaxnod = omxnd
      end subroutine set

      subroutine miniset()
      implicit none
      c3maxnod = 3 * maxnod
      end subroutine miniset

end module SIZES



module PLACE

    ! Skyline storage position array
    integer, allocatable, target :: ia(:)

    ! Skyline storage position array
    integer, allocatable, target :: ja(:)

    ! Arrays to keep track of dummey rows
    integer, allocatable :: nroot(:)
    integer, allocatable :: proot(:)
    integer, allocatable :: troot(:)
    integer, allocatable :: vroot(:)
    integer, allocatable :: pproot(:,:)
    integer, allocatable :: vvroot(:,:)

    ! Avoid actually calculating dummey rows
    integer, allocatable :: shunt(:)

    ! Arrays for doing the edges
    integer, allocatable :: edl(:)
    real,    allocatable :: edx(:)
    real,    allocatable :: edy(:)
    integer, allocatable :: eds(:)

    integer :: prevele  = 0
    integer :: pprevele = 0
    integer :: pstore   = 0
    integer :: mitch    = 0
    integer :: match    = 0
    integer :: n(3)     = -1
    integer :: m(3)     = 0
    real    :: x(3)     = 0.0
    real    :: y(3)     = 0.0
    integer :: count    = 0
    integer :: numedges = 0

    logical :: listAligned     = .false.
    logical :: listAlignResult = .false.

    ! Temporary Debugging Array
    integer, allocatable :: store(:,:)

    contains



      subroutine skyinit(firsttime)

      use CONTROL, only : skyline
      use SIZES, only : c3maxnod, width

      implicit none

      integer err
      logical firsttime

      if(skyline) then
        if(firsttime) then
          allocate(ia(c3maxnod+1),ja(c3maxnod*width),stat=err)
        else
          err = 0
        end if
        if(err.ne.0) then
          print*,'ERROR: Insufficient Memory for Skyline arrays !!!'
          stop
        else
          ia = 0; ja = 0
        end if
      end if

      end subroutine skyinit



      integer function stE(edg)

      implicit none
      integer edg

      stE = store(edg,1)

      end function stE

      integer function stA(edg)

      implicit none
      integer edg

      stA = store(edg,2)

      end function stA


      integer function stB(edg)

      implicit none
      integer edg

      stB = store(edg,3)

      end function stB

      
      
      subroutine associateIAJA(pIA,pJA)

      implicit none

      integer, pointer :: pIA(:)
      integer, pointer :: pJA(:)

      pIA => ia
      pJA => ja

      end subroutine associateIAJA



      logical function listAlign(n,x1,y1,x2,y2,x3,y3 )

      implicit none
      integer n
      real x1, y1, x2, y2, x3, y3, d1, d2

      if(n.eq.0) then
        ! Determine midpoints of "first" and "second" 'J' boundary segments
        x(1) = (x1+x2)/2.0; y(1)=(y1+y2)/2.0
        x(2) = (x3+x2)/2.0; y(2)=(y3+y2)/2.0
        ! Flag that list aligning data is now set
        listalign = .true.
        listAligned = .false.
      else if(listAligned) then
        listAlign = listAlignResult
      else
        ! Check the 'H' to 'J' list alignments
        d1 = (x(1)-HX(n))**2 + (y(1)-HY(n))**2
        d2 = (x(2)-HX(n))**2 + (y(2)-HY(n))**2
        if( d1 < d2 / 2000.0 ) then
          listAlign = .false.
        else if( d2 < d1 / 2000.0 ) then
          listAlign = .true.
        else
          print*,'Error in List Aligning !!!',x(1),y(1),x(2),y(2),n,HX(n),HY(n)
          stop
        endif
        ! All future checks will now just refer to the result of the first - avoids problems if nodes get nudged later ...
        listAlignResult = listAlign
        listAligned = .true.
      endif

      end function listAlign


      logical function isproot(test)

      use CONTROL, only : disgal

      implicit none
      integer test

      if(.not.disgal) then
        isproot = .true.
      else
        isproot = proot(test).eq.test
      endif

      end function isproot


      logical function istroot(test)

      use CONTROL, only : disgal, heat

      implicit none
      integer test

      if(.not.heat) then
        istroot = .false.
      else if(.not.disgal) then
        istroot = .true.
      else
        istroot = (troot(test).eq.test) .or. (troot(test).lt.0)
      endif

      end function istroot


      logical function isroot(test)

      use CONTROL, only : disgal

      implicit none
      integer test

      if(.not.disgal) then
        isroot = .true.
      else
        isroot = nroot(test).eq.test
      endif

      end function isroot


      logical function isvroot(test)

      use CONTROL, only : disgal

      implicit none
      integer test

      if(.not.disgal) then
        isvroot = .true.
      else
        isvroot = vroot(test).eq.test
      endif

      end function isvroot


      integer function noderoot(test)

      use CONTROL, only : disgal

      implicit none
      integer test

      if(.not.disgal) then
        noderoot = test
      else
        noderoot = nroot(test)
      endif

      end function noderoot




      subroutine ppalcheck()!,Type,X,Y)

      use CONTROL, only : disgal
      use SIZES, only : maxnod

      implicit none
      integer i, j, pal, palcount
      !character*1 Type(maxnod)
      !real X(maxnod), Y(maxnod)

      if(.not.disgal) return

      ! Check the pressure pal info
      do i = 1, maxnod
        if(i.ne.proot(i)) cycle
        palcount = 0
        do j = 1, 3
          pal = pproot(i,j)
          if(pal.eq.0) cycle
          if((pproot(pal,1).eq.i).or.(pproot(pal,2).eq.i).or.(pproot(pal,3).eq.i)) palcount = palcount + 1
        end do

        !if(((palcount.ne.3).and.(index('JPQVUHI',Type(i)).eq.0)).or.((palcount.lt.2).and.(index('PQVU',Type(i)).ne.0))) then
        !  print*,'Pal problem ....',pal,palcount,Type(i),X(i),Y(i)
        !endif

      end do

      end subroutine ppalcheck





      subroutine jumpcheck(X,Y)!,P)

      use CONTROL, only : disgal
      use SIZES, only : maxnod

      implicit none
      integer i
      real X(maxnod), Y(maxnod), tol, a(8)
      logical d1, d2, d3, d4
      !double precision P(3*maxnod)

      if(.not.disgal) return

      do i = 1, numedges

        if((store(i,4).eq.0).and.(store(i,5).eq.0).and.(store(i,6).eq.0)) cycle

        a(1) = sqrt( (X(store(i,2))-X(store(i,5)))** 2 + (Y(store(i,2))-Y(store(i,5)))** 2 )
        a(2) = sqrt( (X(store(i,2))-X(store(i,6)))** 2 + (Y(store(i,2))-Y(store(i,6)))** 2 )
        a(3) = sqrt( (X(store(i,3))-X(store(i,5)))** 2 + (Y(store(i,3))-Y(store(i,5)))** 2 )
        a(4) = sqrt( (X(store(i,3))-X(store(i,6)))** 2 + (Y(store(i,3))-Y(store(i,6)))** 2 )

        tol = max(a(1),a(2),a(3),a(4)) / 1000.0

        d1 = a(1).lt.tol
        d2 = a(2).lt.tol
        d3 = a(3).lt.tol
        d4 = a(4).lt.tol

        if(.not.((d1.and.d4).or.(d2.and.d3))) then
          write(*,'(a37,7i6)') 'Jumpcheck: Mis-matched Edges !!!!!!! ', i, store(i,:)
          write(*,'(6(a1,f7.4,a1,f7.4,a3))')  &
                              '[',X(store(i,1)),',',Y(store(i,1)),']  ', &
                              '(',X(store(i,2)),',',Y(store(i,2)),')  ', &
                              '(',X(store(i,3)),',',Y(store(i,3)),')  ', &
                              '[',X(store(i,4)),',',Y(store(i,4)),']  ', &
                              '(',X(store(i,5)),',',Y(store(i,5)),')  ', &
                              '(',X(store(i,6)),',',Y(store(i,6)),')  '
          stop
        endif

   !     l = sqrt(  (X(store(i,2))-X(store(i,3)))** 2 + (Y(store(i,2))-Y(store(i,3)))** 2 )

!        a(1) = P(rrr(1,store(i,1))) + P(rrr(1,store(i,3))) - P(rrr(1,store(i,2)))
!        a(2) = P(rrr(1,store(i,1))) + P(rrr(1,store(i,2))) - P(rrr(1,store(i,3)))

!        a(3) = P(rrr(1,store(i,4))) + P(rrr(1,store(i,6))) - P(rrr(1,store(i,5)))
!        a(4) = P(rrr(1,store(i,4))) + P(rrr(1,store(i,5))) - P(rrr(1,store(i,6)))


!        a(5) = P(rrr(2,store(i,1))) + P(rrr(2,store(i,3))) - P(rrr(2,store(i,2)))
!        a(6) = P(rrr(2,store(i,1))) + P(rrr(2,store(i,2))) - P(rrr(2,store(i,3)))

!        a(7) = P(rrr(2,store(i,4))) + P(rrr(2,store(i,6))) - P(rrr(2,store(i,5)))
!        a(8) = P(rrr(2,store(i,4))) + P(rrr(2,store(i,5))) - P(rrr(2,store(i,6)))

        !print*,'jump ',a(1),a(2),a(3),a(4),' || ',a(5),a(6),a(7),a(8)

      enddo

      end subroutine jumpcheck


      subroutine fixseat(i)

      ! Fixes damage caused by falsely labelled boundary nodes used by seat routine

      use SIZES, only : maxnod
      use CONTROL, only : heat

      implicit none
      integer z, i, nod

      if(.not.heat) return
      ! Determine the falsely declared node number
      nod = m(i)
      ! Zap the 'm' value to stop it being used by record routine
      m(i) = 0
      ! Flag in shunt that a dummey row
      shunt(2*maxnod+nod) = 0
      ! Indicate that there is one more dummey row above for all the rows after this dummey row
      do z = 2 * maxnod + nod + 1, 3 * maxnod
        if(shunt(z).ne.0) shunt(z) = shunt(z) - 1
      end do
            
      end subroutine fixseat


      ! Store root nodes for dummey pressure rows
      subroutine nset(nod,match)

      use CONTROL, only : disgal

      implicit none
      integer nod, match

      nroot(nod) = nroot(match)

      end subroutine nset



      ! Store root nodes for dummey pressure rows
      subroutine seat(posn,nod,ele,lab)

      use SIZES, only : maxnod
      use CONTROL, only : disgal, heat

      implicit none
      integer posn, nod, ele, z
      character*1 lab

      if(.not.disgal) return

      !if(.false.) then

      !  ! Linear Conforming Pressure
      !  proot(nod) = proot(match)

      !else

        ! Check if current node label refers to a boundary edge and, if so, record it
        if(heat.and.(index('HRZ',lab).ne.0)) then
          if(match.eq.0) then
            match = posn
          else
            mitch = posn
          end if
        end if
          
        ! Peicewise Constant Non-Conforming Pressure
        if(ele.ne.pprevele) then
          pprevele = ele
          pstore = nod
        else
          ! Dummey pressure row
          proot(nod) = pstore

          ! If existing boundary node still with no spare row assigned, then assign one here
          if(mitch.ne.0) then
            m(mitch) = nod
            mitch = 0
          else if(match.ne.0) then
            m(match) = nod
            match = 0
          else
            
          ! Flag in shunt that a dummey row
          shunt(2*maxnod+nod) = 0
          ! Indicate that there is one more dummey row above for all the rows after this dummey row
          do z = 2 * maxnod + nod + 1, 3 * maxnod
            if(shunt(z).ne.0) shunt(z) = shunt(z) - 1
          end do
            
          end if
        endif

      !endif

      end subroutine seat




      ! Set-up root nodes for dummey velocity rows
      subroutine record(ele,xx,yy,nod,siz)

      use CONTROL, only : disgal, heat
      use SIZES, only : maxnod

      implicit none
      integer ele, nod, i, k
      real siz, xx, yy, xnew, ynew
      logical newedg

      if((ele.ne.prevele).and.(prevele.ne.0)) then

        ! Loop over (three) edges of element
        do i = 1, 3

          newedg = .true.

          ! Determine edge mid-point
          xnew = ( x(modulo(i,3)+1) + x(modulo(i+1,3)+1) ) / 2.0
          ynew = ( y(modulo(i,3)+1) + y(modulo(i+1,3)+1) ) / 2.0

          ! Check to see if edge already exists ...
          do k = 1, numedges
            if((edx(k)-xnew)**2+(edy(k)-ynew)**2.lt.siz**2) then
              if((n(i).lt.1).or.(n(i).gt.maxnod)) then
                print*,'bad n',n(i),maxnod
                stop
              elseif((edl(k).lt.1).or.(edl(k).gt.maxnod)) then
                print*,'bad edl'
                stop
              endif
              vroot(n(i)) = vroot(edl(k))
              ! If edge is shared, then with velocity on one side, temperature can be stored on the other
              if(heat) troot(edl(k)) = troot(n(i))
              newedg = .false.
              store(k,4) = n(i)
              store(k,5) = n(modulo(i,3)+1)
              store(k,6) = n(modulo(i+1,3)+1)

              ! Sort-out pressure pals
              pproot( proot(   n(i) ) ,   i     ) = proot( edl(k) )
              pproot( proot( edl(k) ) , eds(k)  ) = proot(   n(i) )

              ! Sort-out velocity pals that depend on matched edge orientation
              if((nroot(store(k,2)).eq.nroot(store(k,5))).and.(nroot(store(k,3)).eq.nroot(store(k,6)))) then

                vvroot( store(k,2) , eds(k)  ) = store(k,5)
                vvroot( store(k,5) ,   i     ) = store(k,2)
                vvroot( store(k,3) , eds(k)  ) = store(k,6)
                vvroot( store(k,6) ,   i     ) = store(k,3)

              else if((nroot(store(k,2)).eq.nroot(store(k,6))).and.(nroot(store(k,3)).eq.nroot(store(k,5)))) then

                vvroot( store(k,2) , eds(k)  ) = store(k,6)
                vvroot( store(k,6) ,   i     ) = store(k,2)
                vvroot( store(k,3) , eds(k)  ) = store(k,5)
                vvroot( store(k,5) ,   i     ) = store(k,3)

              else

                print*,'Velocity pal Failure.'
                stop

              endif

              ! Velocity pals that don't depend on shared edge orientation ...
              vvroot( store(k,1) , eds(k)  ) = store(k,4)
              vvroot( store(k,4) ,   i     ) = store(k,1)

              exit
            endif
          end do

          if(newedg) then
            numedges = numedges + 1
            edl(numedges) = n(i); edx(numedges) = xnew; edy(numedges) = ynew; eds(numedges) = i
            store(numedges,1) = n(i)
            store(numedges,2) = n(modulo(i,3)+1)
            store(numedges,3) = n(modulo(i+1,3)+1)
            ! If seat has given an 'm' number for the corresponding node, then this new edge will never be matched
            ! (ie: shared by another element) and so the corresponding temperature (if required) row will need to 
            ! be taken from one of the dummey pressure rows instead ...
            if(m(i).ne.0) troot(n(i)) = - m(i)
          ! Not a new edge (make explicit to avoid confusion with nested 'if' block)
          else if(.not.newedg) then
            if(.not.heat) then

            ! Flag in shunt that a dummey (velocity) row
            shunt(n(i)) = 0
            do k = n(i) + 1, 3 * maxnod
              if(shunt(k).ne.0) shunt(k) = shunt(k) - 1
            end do
            
            end if
            
            shunt(maxnod+n(i)) = 0
            ! Indicate that there is one more dummey row above for all the rows after this dummey row
            do k = maxnod + n(i) + 1, 3 * maxnod
              if(shunt(k).ne.0) shunt(k) = shunt(k) - 1
            end do
          endif

        end do

        count = 0; m = 0

      endif

      prevele = ele; count = count + 1

      n(count) = nod; x(count) = xx; y(count) = yy

      end subroutine record


      real function HX(test)

      use CONTROL, only : disgal
      
      implicit none
      integer test, i
      logical done

      done = .false.

      do i = 1, numedges
        if(edl(i).eq.vroot(test)) then
          HX = edx(i)
          done = .true.
          exit
        endif
      end do

      if((.not.disgal).or.(.not.done)) then
        write(*,*) 'ERROR in HX: no edge mid-point x-coordinate found !!!'
        stop
      end if

      end function HX


      real function HY(test)

      use CONTROL, only : disgal

      implicit none
      integer test, i
      logical done

      done = .false.

      do i = 1, numedges
        if(edl(i).eq.vroot(test)) then
          HY = edy(i)
          done = .true.
          exit
        endif
      end do

      if((.not.disgal).or.(.not.done)) then
        write(*,*) 'ERROR in HX: no edge mid-point y-coordinate found !!!'
        stop
      end if

      end function HY


      subroutine printout()

      use SIZES, only : maxnod
      use CONTROL, only : disgal

      implicit none
      integer i, count1, count2

      if(.not.disgal) return

      count1 = 0; count2 = 0
      do i = 1, maxnod
        if(i.ne.vroot(i)) count1 = count1 + 1
        if(i.ne.proot(i)) count2 = count2 + 1
        print*,'vroot ',i,'  ',vroot(i),'   proot ',i,'  ',proot(i)
      end do
      print*,'Count 1 = ',count1, ' Count 2 = ',count2
      end subroutine printout






      ! Creates and Initializes Placement Arrays
      subroutine init()

      use SIZES, only : maxnod
      use CONTROL, only : disgal, heat

      implicit none
      integer i

      if(.not.disgal) return

      allocate(vroot(maxnod),proot(maxnod),pproot(maxnod,3),vvroot(maxnod,3),nroot(maxnod),shunt(3*maxnod), &
                                               edl(maxnod),edx(maxnod),edy(maxnod),eds(maxnod),store(maxnod,6),stat=i)

      ! Check for allocation error
      if(i.ne.0) then
        print*,'Insufficient memory for placement arrays !!!'
        stop
      endif

      ! Set-up default values for arrays
      do i = 1, maxnod
        vroot(i) = i; proot(i) = i; pproot(i,:) = 0; vvroot(i,:) = 0; nroot(i) = i
        edl(i) = 0; edx(i) = 0.0; edy(i) = 0.0; eds(i) = 0; store(i,:) = 0
        shunt(i) = i; shunt(maxnod + i) = maxnod + i; shunt(2 * maxnod + i) = 2 * maxnod + i
      end do

      if(heat) then
        allocate(troot(maxnod),stat=i)

        ! Check for allocation error
        if(i.ne.0) then
          print*,'Insufficient memory for (temperature) placement arrays !!!'
          stop
        endif

        ! Set-up default values for arrays
        do i = 1, maxnod
          troot(i) = i
        end do
      end if

      end subroutine init


      subroutine wipe()

      use SIZES, only : maxnod, omaxnod, c3maxnod

      implicit none

      integer i, dif

      dif = omaxnod - maxnod

      ! Shunt the shunt array so that it can be used with the new maxnod
      do i = 1, maxnod
        if(shunt(omaxnod+i).eq.0) then
          shunt(  maxnod+i) = 0
        else
          shunt(  maxnod+i) = shunt(  omaxnod+i) - dif
        end if
      end do
      do i = 1, maxnod
        if(shunt(2*omaxnod+i).eq.0) then
          shunt(2*maxnod+i) = 0
        else
          shunt(2*maxnod+i) = shunt(2*omaxnod+i) - 2 * dif
        end if
      end do

      ! Set compact 3 times maxnod value
      do i = 3*maxnod, 1, -1
        if(shunt(i).ne.0) then
          c3maxnod = shunt(i)
          exit
        end if
      end do

      !deallocate(edl,edx,edy,eds)

      end subroutine wipe


      ! Row Placement
      integer function rr(i,k)

      use SIZES, only : maxnod, maxsid
      use CONTROL, only : disgal

      implicit none
      integer  i, k

      ! Specials always indexed the same way
      if(k.le.0) then
        rr = (1-i)*maxsid + k
        return

      ! Conforming Elements
      else if(.not.disgal) then
        rr = (i-1)*maxnod + k
        return

      ! Dis-Continuous Temperature Rows
      else if(i.eq.4) then
        ! Temperature uses unused u-velocity rows when available ...
        rr = troot(k)
        ! ... or spare pressure rows when not (indicated by a negative temperature root node)
        if(rr.lt.0) rr = 2*maxnod - rr

      ! Dis-Continuous Pressure Rows
      else if(i.eq.3) then
        rr = (i-1)*maxnod + proot(k)

      ! Dis-Continuous Velocity Rows
      else
        rr = (i-1)*maxnod + vroot(k)
      endif

      ! Shunt rows up to avoid calculating with dummey rows
      rr = shunt(rr)

      end function rr







      ! Pressure Pal Column Placement
      integer function pp(j,k)

      use SIZES, only : maxnod
      use CONTROL, only : disgal

      implicit none
      integer  j, k

      ! Conforming Elements
      if(.not.disgal) then
        pp = (3-1)*maxnod + k
        return

      ! If no pressure partner for k on side j, then just do as cc
      else if( pproot(proot(k),j).eq.0 ) then

        pp = (3-1)*maxnod + proot(k)

      else

        pp = (3-1)*maxnod + pproot(proot(k),j)

      endif

      ! Shunt rows up to avoid calculating with dummey rows
      pp = shunt(pp)

      end function pp


      ! Skyline matrix positioning
      integer function sky(r,c)

      use SIZES, only : width

      implicit none

      integer r, c, k, posn

      ! Loop the pre-allocated space available to row "r"
      do k = 1, width
        ! Construct a (temporary) position
        posn = k+(r-1)*width
        if(ja(posn).eq.c) then
          ! Position already assigned - so just return it
          sky = posn
          exit
        else if(ja(posn).eq.0) then
          ! Row-column combination as yet unassigned ...
          ja(posn) = c
          sky = posn
          ! Keep a count of the number of entries in the row
          ia(r) = ia(r) + 1
          exit
        else if(k.eq.width) then
          print*,'ERROR: RC loop overrun !!!', r, c
          stop
        end if
      end do

      end function sky
      


      ! Combined row & column placement
      integer function cr(i,j)

      use SIZES, only : c3maxnod

      implicit none
      integer  i, j

      cr = i + c3maxnod * ( j - 1 )

      end function cr



      ! Combined row & column placement
      integer function rc(i,m,j,n)

      use SIZES, only : c3maxnod
      use CONTROL, only : skyline

      implicit none
      integer  i, m, j, n, r, c

      if(skyline) then
        ! First determine requested row and column positions
        r = rr(i,m); c = cc(j,n)
        ! Get the skyline position
        rc = sky(r,c)
      else
        ! Regular un-compact matrix storage
        rc = rr(i,m) + c3maxnod * ( cc(j,n) - 1 )
      end if

      end function rc





      ! Column Placement
      integer function cc(j,k)

      use SIZES, only : maxnod
      use CONTROL, only : disgal

      implicit none
      integer  j, k

      ! Conforming Elements
      if(.not.disgal) then
        cc = (j-1)*maxnod + k
        return

      ! Dis-Continuous Temperature Rows
      else if(j.eq.4) then
        ! Temperature uses unused u-velocity rows when available ...
        cc = troot(k)
        ! ... or spare pressure rows when not (indicated by a negative temperature root node)
        if(cc.lt.0) cc = 2*maxnod - cc

      ! Dis-Continuous Pressure Rows
      else if(j.eq.3) then
        cc = (j-1)*maxnod + proot(k)

      ! Dis-Continuous Velocity Rows
      else
        cc = (j-1)*maxnod + vroot(k)
      endif

      ! Shunt rows up to avoid calculating with dummey rows
      cc = shunt(cc)

      end function cc



      integer function rrr(i,k)
      use SIZES, only : maxnod
      implicit none
      integer  i, k
      rrr = (i-1)*maxnod + k
      end function rrr

      
      integer function oor(i,k)

      use SIZES, only : maxnod, maxele, omaxnod
      use CONTROL, only : disgal

      implicit none
      integer  i, k

      ! Conforming Elements
      if(.not.disgal) then
        oor = (i-1)*omaxnod + k

      ! Dis-Continuous Pressure Rows
      else if(i.eq.3) then
        oor = (i-1)*omaxnod + proot(k)

      ! Dis-Continuous Velocity Rows
      else
        oor = (i-1)*omaxnod + vroot(k)
      endif

      end function oor

      
      integer function oc(j,k)

      implicit none
      integer  maxnod, maxele, j, k, wdlength, omaxnod
      character working_directory*100
      common working_directory, wdlength, maxnod, maxele,omaxnod

      oc = (j-1)*omaxnod + k

      end function oc

end module PLACE

