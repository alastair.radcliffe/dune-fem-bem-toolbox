


module radrop_util


    contains

      function nch(x)

      real x
      character*12 nch

      write(nch,'(f12.5)') x

      end function nch


      function ch(xx)

      implicit none
      integer i, p, index
      real x, xx
      character ch*5, string*10, digit(5)*1

      string='0123456789'
      x=xx

      if(x.lt.0.0) then
        p=int(-x)
        do 10 i = 4, 0, -1
          index=int(p/10**i)
          digit(5-i)=string(index+1:index+1)
          p=mod(p,10**i)
   10   continue
      else
        do 20 i = 1, -2, -1
          index=mod(int((x+0.00001)/10.0**i),10)
          digit(3-i)=string(index+1:index+1)
          x=mod(x,10.0**i)
   20   continue
        digit(1)=digit(2)
        digit(2)=digit(3)
        digit(3)='.'
      endif
      ch=digit(1)//digit(2)//digit(3)//digit(4)//digit(5)
      end function ch

end module radrop_util




module CONTROL

    ! Flag for BEM only calculations
    logical :: bemo = .false.
    integer :: bemf = 1

    integer :: o = 1

    ! Flag for first run through code
    logical :: firsttime = .true.

    logical :: drag = .false.

    ! Flag for linux system or not
    logical :: linux = .false.

    ! Surface resistivity to electrical charge (inverse conductivity)
    logical :: resist = .false.
    logical :: perfect = .true.
    real :: cond = 20000.0

    ! Flag for an additional temperature field
    logical :: heat = .false.

    ! Maximum velocity
    double precision :: maxvel = 0.0d0

    ! Controlling parameters for penalty function
    real :: pnfa = 2.2
    real :: pnfb = 0.0
    real :: pnfc = 0.0
    real :: pnfd = 0.0
    real :: pnfe = 1000.0
    
    ! Flag to allowone dekink (used for oblate run)
    logical :: onedekink = .false.

    ! Multiplier to reduce charge on prolate explosion
    real :: ejectQ = 0.71

    ! Percentage of initial fluid volume to be lost at explosion
    real :: ejectP = 0.005

    ! Gradient of droplet surface at tip just before explosion
    double precision :: ejectG = -0.5d0

    ! Scale factor to transform to canonical tip geometry
    double precision :: ejectK = 1.0d0

    ! Vertical displacement to transform to canonical tip geometry
    double precision :: ejectC = 0.0d0

    ! Initial volume holder for (micro) mass conservations
    double precision :: voluhold = -1.0d0

    ! Flag for exploding the prolates when an inflexion occurs at the droplet tip
    logical :: inflex = .false.

    ! Flag that such an inflexion has actually occured
    logical :: inflexion = .false.

    ! Equilibrium geometric parameters with contact angle
    real :: eqContactAngle = 15.0
    double precision :: eqConstant = 0.5d0
    real :: qa = 1.0
    real :: qb = 1.0
    real :: qc = 1.0
    double precision :: qt = 1.0d0
    double precision :: qs = 1.0d0
    double precision :: aq = 1.0d0
    double precision :: bq = 1.0d0
    double precision :: cq = 1.0d0
    double precision :: qac = 1.0d0

    ! Radiation parameter
    double precision :: radparam = 0.0d0

    ! Clip-off some terms for very low velocities
    double precision :: clipvel = 0.0d0

    ! Switch for calculating (and displaying) divergence for mass conservation
    logical :: masscheck = .false.

    ! Switch for calculating (and displaying) vorticity
    logical :: vortcheck = .false.

    ! Initial deformation of the ellipsoid for use in gmsh mesh generation
    real :: initdef = 1.0

    ! Flag for keeping charge monotonically increasing from the base to the tip of the droplet
    ! Needed for excessively small meshes
    logical :: monocharge = .false.

    ! For oblate experiments only, sample the a / b ratio ...
    real :: sampleValue(10) = 10.0  ! Sample value set to ensure no immediate stepping in charge

    ! ... at (non-dimensional) time ...
    real :: sampleTime(10) = 99999.0
    
    ! ... every sample time period ...
    real :: samplePeriod = 1000.0

    ! ... from the store of samples at position ...
    integer :: samplePosition = 0

    ! ... and if unchanged, increase the charge loading by ...
    real :: fissjump = 0.0

    ! ... on the previous fissility
    real :: prevfiss

    ! Flag for doing Axi-Symmetric Experiments
    logical :: axisym = .false.

    ! Flag for doing Reflection-Symmetric Experiments (in X-axis)
    logical :: refsym = .true.

    ! Flag for doing Reflection-Symmetric Experiments (in Y-axis)
    logical :: rifsym = .false.

    ! Flag for using Tabata elements
    logical :: tabata = .false.

    ! Flag for using tractions from air exterior medium
    logical :: air = .false.

    ! Initial fluid temperature
    double precision :: initemp = 0.0d0

    ! Reference temperature of substrate wall
    double precision :: refwalltemp = 1.0d0

    ! Peclet number (Non-dimensional (inverse) thermal diffusion coefficient)
    double precision :: peclet = 0.0d0

    ! Froude number (Non-dimensional (inverse) gravitational acceleration)
    double precision :: froude = 0.0d0

    ! Expansion coefficient
    real :: expansion = 1.0

    ! Order of Legendre polynomial fitting
    integer :: lp = 4

    ! Store of Legendre coefficient values
    double precision :: param(4)
    double precision :: legvals(4) = -10.0d0

    real :: tol  = 0.0001
    double precision :: zero = 0.1d-7

    ! Pre- and Post-fixes for output file names
    character prefix*5

    ! give the variables default values
    real :: pard = 0.0
    real :: squa = -0.1999
    real :: ccaa = 0.0
    real :: omeg = 0.0
    real :: anal = 5.00000
    real :: troc = 0.0
    real :: pcor = -1.0000
    logical :: pcored = .false.
    real :: ordv = -1.0      
    real :: ordp = 0.0
    real :: gint = 0.0
    real :: scal = 1.0
    real :: face = 1.0
    real :: sdiv = 1.0
    real :: bulk = 1.0
    real :: spec = 0.0
    real :: cflr = 0.005
    real :: cflrorig = 0.005
    double precision :: step = 1.0d0
    real :: coun = 4001.0
    real :: dura = 1.0
    real :: xxxx = 0.0
    real :: yyyy = 0.0
    real :: bres = 0.07
    real :: frie = 1.0
    logical :: free = .true.
    real :: alph = 0.3333
    double precision :: stan = 1.0d0
    real :: nu   = 1.0
    real :: symt = 0.0
    double precision :: rho  = 1.0d0
    
    ! Ohnesorge number
    real :: ohne = 1.0

    ! Initial flow strength
    real :: flow = 0.0

    ! Derived variables
    integer :: lim  = 3
    integer :: ord  = 1
    integer :: ov   = 1
    integer :: op   = 1
    integer :: odv  = 1
    integer :: odp  = 1
    integer :: mlim = 3
    integer :: nlim = 3

    integer :: dg  = 0
    real    :: dgr = 0.0

    ! Logical derived parameters
    logical :: disgal   = .false.
    logical :: inviscid = .false.
    logical :: blob     = .true.

    ! Flag for using a skyline storage of the system matrix
    logical :: skyline = .false.

    ! Flag for being near a sphere
    logical :: sphereish = .true.

    ! Flag for pushing things onto the Legendre interpolation
    logical :: legendre = .false.

    ! Flag for using a spline representation of the boundary for the surface force calculations
    logical :: splines = .false.

    ! Flag for freezing top of mesh
    logical :: meshfreeze = .false.

    real :: temp = 0.0

    ! Set-up some useful constants
    double precision :: oneOverTwoPi  = 1.0d0 / ( 2.0d0 * 3.1417214d0 )
    double precision :: oneOverFourPi = 1.0d0 / ( 4.0d0 * 3.1417214d0 )
    double precision :: oneOverEightPi = 1.0d0 / ( 8.0d0 * 3.1417214d0 )
    double precision :: pi = 3.1417214d0  ! atan2(0.0,-1.0)
    double precision :: piontwo = 3.1417214d0 / 2.0
    double precision :: pionfour = 3.1417214d0 / 4.0
    double precision :: twoonpi = 2.0d0 / 3.1417214d0
    double precision :: twopi = 2.0d0 * 3.1417214d0
    double precision :: fourpi = 4.0d0 * 3.1417214d0

    ! Flag for full output
    logical :: fish = .false.
    logical :: relaxing = .true.

    ! Results from the ellipsoid fitting
    real :: fita = 1.0
    real :: fitb = 1.0
    real :: fitn = 2.0

    ! Actual a and b
    real :: fitaa = 1.0
    real :: fitbb = 1.0

    ! Useful combinations from ellipsoid fitting
    double precision :: fitan = 1.0d0
    double precision :: fitbn = 1.0d0
    double precision :: fitanbn = 1.0d0

    ! Flag for oblate experiment
    logical :: oblatexpt = .false.

    contains


      ! Calculates penalty parameter to use
      double precision function penfun(locvel)

      implicit none
      double precision locvel

      penfun = dble(pnfa)
      if(pnfb.gt.0.0) penfun = penfun * min(max(locvel,pnfd),pnfe) ** dble(pnfb)
      if(pnfc.gt.0.0) penfun = penfun * min(max(maxvel,pnfd),pnfe) ** dble(pnfc)

      end function penfun


      ! Performs tip geometry transformation from pre- to post-explosion
      subroutine transform(r,z)

      implicit none

      real r, z
      double precision d

      if(z.gt.real(ejectC/ejectK)) then
        ! Compute the scaled displacement to use  
        d = 4.0d0 * ejectG * ejectK * dble(r) + 2.0d0 * ejectG * ejectG
        d = ( 4.0d0 * ejectK * ejectK * dble(r) * dble(r) - ejectG * ejectG ) / d
        d = 1.0d0 + d
        ! Apply it to the z coordinate
        z = z - real( d * ( dble(z) - ejectC / ejectK ) )
      end if

      end subroutine transform
      


      ! Allows default settings to be over-ridden
      subroutine control_load

      ! read in the control.dat file into the relevant variables
      ! this way of doing things is not pretty, but it will work,
      ! and isn't dependent upon the order of values in the file
      ! nor does it have problems with comments in the control.dat file

      !use CONTROL

      use radrop_util, only : ch

      implicit none
      character*4 strg
      integer ios1
      character whole_line*200

      ios1 = 0
               
      open(88, file='control.dat',status='old',err=100)

      do
        read(88, '(a)', iostat=ios1) whole_line
        if (ios1 .lt. 0) then
            close(88)
            exit
        else if(index(whole_line, '.') .eq. 0) then
            cycle
        endif

        if (index(whole_line, 'cond') .gt. 0) then
            read(whole_line,  * ) strg, cond
            write(*,*) 'cond = ', cond
        else if (index(whole_line, 'squa') .gt. 0) then
            read(whole_line,  * ) strg, squa  
            write(*,*) 'squa = ', squa
        else if (index(whole_line, 'ccaa') .gt. 0) then
            read(whole_line,  * ) strg, ccaa  
            write(*,*) 'ccaa = ', ccaa
        else if (index(whole_line, 'omeg') .gt. 0) then
            read(whole_line,  * ) strg, omeg  
            write(*,*) 'omeg = ', omeg
        else if (index(whole_line, 'anal') .gt. 0) then
            read(whole_line,  * ) strg, anal  
            write(*,*) 'anal = ', anal
        else if (index(whole_line, 'trac') .gt. 0) then
            read(whole_line,  * ) strg, troc
            write(*,*) 'trac = ', troc
        else if (index(whole_line, 'pcor') .gt. 0) then
            read(whole_line,  * ) strg, pcor
            pcored = abs(pcor).gt.0.0
            write(*,*) 'pcor = ', pcor, pcored
        else if (index(whole_line, 'ordv') .gt. 0) then
            read(whole_line,  * ) strg, ordv  
            write(*,*) 'ordv = ', ordv
        else if (index(whole_line, 'ordp') .gt. 0) then
            read(whole_line,  * ) strg, ordp  
            write(*,*) 'ordp = ', ordp
        else if (index(whole_line, 'bulk') .gt. 0) then
            read(whole_line,  * ) strg, bulk  
            write(*,*) 'bulk = ', bulk
        else if (index(whole_line, 'face') .gt. 0) then
            read(whole_line,  * ) strg, face  
            write(*,*) 'face = ', face
        else if (index(whole_line, 'sdiv') .gt. 0) then
            read(whole_line,  * ) strg, sdiv  
            write(*,*) 'sdiv = ', sdiv
        else if (index(whole_line, 'cflr') .gt. 0) then
            read(whole_line,  * ) strg, cflr
            write(*,*) 'cflr = ', cflr
            cflrorig = cflr
        else if (index(whole_line, 'coun') .gt. 0) then
            read(whole_line, *) strg, coun
            ios1 = index(whole_line,' ')
            read(whole_line(ios1:200), *) coun
            write(*,*) 'coun = ', coun
        else if (index(whole_line, 'dura') .gt. 0) then
            read(whole_line, *) strg, dura
            ios1 = index(whole_line,' ')
            read(whole_line(ios1:200), *) dura
            write(*,*) 'dura = ', dura
        else if (index(whole_line, 'pnfa') .gt. 0) then
            read(whole_line,  * ) strg, pnfa  
            write(*,*) 'pnfa = ', pnfa
        else if (index(whole_line, 'pnfb') .gt. 0) then
            read(whole_line,  * ) strg, pnfb  
            write(*,*) 'pnfb = ', pnfb
        else if (index(whole_line, 'pnfc') .gt. 0) then
            read(whole_line,  * ) strg, pnfc  
            write(*,*) 'pnfc = ', pnfc
        else if (index(whole_line, 'free') .gt. 0) then
            read(whole_line,  * ) strg, frie
            free = frie.ne.0.0
            write(*,*) 'free = ', frie, free
        else if (index(whole_line, 'pnfd') .gt. 0) then
            read(whole_line,  * ) strg, pnfd  
            write(*,*) 'pnfd = ', pnfd
        else if (index(whole_line, 'pnfe') .gt. 0) then
            read(whole_line,  * ) strg, pnfe  
            write(*,*) 'pnfe = ', pnfe
        else
            write(*,*) 'undefined parameter'
            write(*,*) 'unknown line:', whole_line
            stop
        endif

      enddo

      close(88)

      ! Iteration number filename prefix to be used on all iterated output
  100 prefix = ch(-real(floor(coun)))

      ov = nint(ordv)
      op = nint(ordp)

      odv = nint(abs(ordv))
      odp = nint(abs(ordp))

      mlim = ( odv + 1 ) * ( odv + 2 ) / 2
      nlim = ( odp + 1 ) * ( odp + 2 ) / 2

      lim = mlim
      ord = odv

      call presscorr()
    
      ! Check for Dis-Continuous Galerkin
      disgal = odv.ne.ov

      if(disgal) then
        dg  = 2
        dgr = 0.0
      else
        dg  = 1
        dgr = 1.0
      endif

      inviscid = nu.eq.0.0

      rho =  1.0d0 / dble(ohne)

      stan = 1.0d0 / dble(ohne)

      ! For the oblate case only
      if(oblatexpt) then
        ! Given omeg interpreted as the charge (or fissility) jump ...
        fissjump = omeg
        ! ... and then set to the starting value
        omeg = 1.01
        ! Record the value of this fissility
        prevfiss = omeg
        ! Given duration interpreted as the time to be taken between samples ...
        samplePeriod = dura
        ! ... and then set to something big
        dura = samplePeriod * 200.0
        ! Time of last sample set to a value that ensures a sample is taken at first iteration
        sampleTime = 2.0 * dura
      end if

      ! Check for charge experiment (flow rotation specified but with no flow)
      if((ccaa.lt.zero).and.(omeg.gt.zero)) then
        ! Set flag for relaxing
        relaxing = omeg .lt. 1.0
        ! Total non-dimensional charge on the unit hemisphere
        omeg = 2.0 * twopi * sqrt( omeg / ohne )
      end if

      blob = anal.ge.5.0

      skyline = pard.ne.0.0

      end subroutine control_load


      subroutine paramzap()

      nu = 1.0; rho = 0.0d0; step = 1.0d0

      end subroutine paramzap



      ! Calculates the Mean, H, and Gaussian, K, Curvatures for the ellipse
      subroutine fitHK(x,H,K)

      implicit none

      double precision x, y, H, K, r1, r2

      ! The y to go with the given x
      y = x / dble(fitb)
      y = y * y
      y = dble(fita) * sqrt( 1.0d0 - y )

      ! Constructions
      r1 = dble(fitb) * dble(fitb); r2 = dble(fita) * dble(fita)

      H = r1 * r2; K = r1

      r1 = x * x / ( r1 * r1 ); r2 = y * y / ( r2 * r2 )

      r2 = sqrt( r1 + r2 )

      ! Principle radii of Curvature
      r1 = H * r2 * r2 * r2
      r2 = K * r2

      ! Mean Curvature - the Algebraic Mean
      H = ( r1 + r2 ) / ( 2.0d0 * r1 * r2 )

      ! Gaussian Curvature - the Geometric Mean
      K = 1.0d0 / sqrt( r1 * r2 )

      end subroutine fitHK




      subroutine presscorr()

      implicit none
      integer expo
      real mant

      if(pcor.gt.0.0) then
        ! expo = nint( 100.0 * ( pcor - real(mant) ) )
        write(*,'(a23,f10.5,a9,i4,a3,e16.4)') &
                       'Pressure correction is ',pcor,' * 10 ** ',0,' = ',pcor
      else
        mant = floor(abs(pcor))
        expo = nint( 100.0 * ( pcor + real(mant) ) )
        pcor = mant * 10.0 ** expo
        write(*,'(a23,f10.5,a9,i4,a3,e16.4)') &
                       'Pressure correction is ',mant,' * 10 ** ',expo,' = ',pcor
      endif

      end subroutine presscorr


      subroutine oblatejump(fish)

      integer i, count
      real junk
      logical fish

      ! Set default value for fish
      fish = .false.

      ! Return immediately if not doing oblates
      if(.not.oblatexpt) return

      ! Return if an insufficient time has elapsed since the last a / b sample
      if(sampleTime(max(1,samplePosition))-dura.lt.samplePeriod/10.0) return

      ! Use each of the 10 available sample slots in turn
      samplePosition = samplePosition + 1

      if(samplePosition.gt.10) samplePosition = 1

      ! Check to see if the a / b ratio has changed significantly
      if( abs( fitaa/fitbb - sampleValue(samplePosition) ) .lt. 0.001 ) then
        ! If not, then output the fissility a / b pair to the equilibrium file
        count = 0
        open(69,file='equil.cnt',status='old',err=5)
        read(69,'(i10)',err=10) count
        close(69)
        open(71,file='equil.dat',status='old',err=5)
        do i = 1, count
          read(71,'(f10.5)',err=10) junk
        end do
        goto 10
    5   open(71,file='equil.dat',status='new')
   10   write(71,'(6f10.5)') prevfiss, sampleValue(samplePosition), sampleTime(samplePosition), dura, samplePeriod, fitaa/fitbb
        close(71)
        open(69,file='equil.cnt')
        write(69,'(i10)') count + 1
        close(69)
        
        ! Put something down into the main output file so can see start and end points of period used
        write(58,'("I = ",i5," T = ",f9.4, &
      &" A = ",f5.3," B = ",f5.3," A/B = ",f5.3," N = ",f5.3," AR = ",f6.2," % VL = ",f6.2," % Q = ",f6.1, &
      &" E = ",f9.5," + ",f9.5," + ",f9.5," = ",f9.5 &
       &          )') &
                       nint(coun),        sampleTime(samplePosition), &
        1.01*fitaa,fitbb,1.01*fita/fitb,fitn,0.0d0,0.0d0,omeg,0.0d0,0.0d0,0.0d0,0.0d0
        write(58,'("I = ",i5," T = ",f9.4, &
      &" A = ",f5.3," B = ",f5.3," A/B = ",f5.3," N = ",f5.3," AR = ",f6.2," % VL = ",f6.2," % Q = ",f6.1, &
      &" E = ",f9.5," + ",f9.5," + ",f9.5," = ",f9.5 &
       &          )') &
                       nint(coun),        dura, &
        1.01*fitaa,fitbb,1.01*fita/fitb,fitn,0.0d0,0.0d0,omeg,0.0d0,0.0d0,0.0d0,0.0d0
        ! Now set-up the next charge loading in the sequence
        omeg = prevfiss + fissjump
        ! Make a note of this fissility before the omeg variable is overlaid with the non-dimensional charge
        prevfiss = omeg
        ! Total non-dimensional charge on the unit hemisphere
        omeg = 2.0 * twopi * sqrt( omeg / ohne )
        ! Take a snapshot of the mesh etc at this transition
        fish = .true.
        ! Allow a single dekink call to tidy-up boundary
        onedekink = .true.
        ! Reset ALL samples
        sampleValue = 10.0
      end if  

      ! Record a new sample
      sampleValue(samplePosition) = fitaa / fitbb
      sampleTime(samplePosition)  = dura

      end subroutine oblatejump


      subroutine setup()

      implicit none

      integer sp, dp
      character wd*200, mr*1
      real rt(16)


      ! First try creating a windows current working directory ...
      call system('echo %CD% > curdir.txt')

      ! Now check that it worked, if not then try Linux ...
      open(18,file='curdir.txt',status='old',err=5)
      
      ! Now read the contents to get the working directory
      read(18,'(a)') wd

      close(18,status='delete')
      
      ! Determine the start point for the folder name
      sp = 1 + index(wd,'\',back=.true.)

      if(sp.lt.2) goto 5

      goto 15
 
      ! ... otherwise, try a Linux one ...
    5 call system('pwd > curdir.txt')

      ! Now check that it worked, if not then fail ...
      open(18,file='curdir.txt',status='old',err=10)
      
      ! Now read the contents to get the working directory
      read(18,'(a)') wd
   
      close(18,status='delete')
      
      ! Determine the start point for the folder name
      sp = 1 + index(wd,'/',back=.true.)

      linux = .true.

      goto 15
 
      ! Failed to create working directory file
   10 print*,'Cannot determine working directory!'
      stop

      ! Mesh resolution
   15 mr = wd(sp:sp)

      ! Setup a mesh resolution table
      rt( 1) = 0.29    !aaa
      rt( 2) = 0.23    !bbb
      rt( 3) = 0.19    !ccc
      rt( 4) = 0.15    !ddd
      rt( 5) = 0.12    !eee
      rt( 6) = 0.10    !fff
      rt( 7) = 0.07    !ggg
      rt( 8) = 0.05    !hhh
      rt( 9) = 0.03    !iii
      rt(10) = 0.02    !jjj
      rt(11) = 0.015   !kkk
      rt(12) = 0.013   !mmm
      rt(13) = 0.011   !nnn
      !rt(14) = 0.010   !ppp
      !rt(15) = 0.009   !qqq
      !rt(16) = 0.008   !rrr

      ! Get the letter governing the mesh resolution
      dp = max(index('abcdefghijkmnpqr',mr),index('ABCDEFGHIJKMNPQR',mr))

      ! Anything above an "f" requires pardiso
      if(linux) pard = 1.0

      ! Actual element diameter corresponding to this mesh resolution
      bres = rt(dp)

      ! Choose penalty parameter according to requested mesh resolution
      pcor = 0.5 / bres

      ! Last decimal point
      dp = index(wd,'.',back=.true.)

      ! Ohnesorge number
      read( wd(dp-1:dp+3), * ) ohne

      ! Eject slightly more charge + liquid for more viscous droplets
      if(ohne.gt.0.110) then
        ! ejectQ = 0.80
        ejectP = 0.02
      end if

      !        ===============  THE EXPERIMENTS  ================

      
      
      ! 3-D Cylinder drag with axisymmetric BEM only
      if(index(wd,'BAM').ne.0) then
        initdef = ohne
        dura = 400.0
        coun = 1001.0
        froude = 10000.04082d0
        drag = .true.
        flow = 0.5
        ! BEM only works with continuous functions
        ordv = 1.0
        bemo = .true.
        refsym = .false.
        rifsym = .false.
        bemf = 0
        air = .true.

      ! 2-D Cylinder drag with BEM only
      else if(index(wd,'BEM').ne.0) then
        initdef = ohne
        dura = 400.0
        coun = 1001.0
        froude = 10000.04082d0
        drag = .true.
        flow = 0.5
        ! BEM only works with continuous functions
        ordv = 1.0
        bemo = .true.
        refsym = .false.
        rifsym = .true.
        bemf = 0
        air = .true.

      ! Heated 2 mm glycol droplet
      else if(index(wd,'DRAG').ne.0) then
        initdef = ohne
        dura = 400.0
        coun = 1001.0
        froude = 10000.04082d0
        drag = .true.
        flow = 0.5

      ! Heated 2 mm glycol droplet
      else if(index(wd,'SPREAD').ne.0) then
        initdef = 0.9
        dura = 400.0
        coun = 2001.0
        froude = 10000.04082d0
        peclet = 100.0d0
        heat = .true.

        ! Contact angle in degrees
        eqContactAngle = 30.0 * pi / 180.0 !125
        eqConstant = 1.25d0 * ohne
        ! Determine initial flow to go with this value
        call SetFlow()

      ! Heated 2 mm glycol droplet
      else if(index(wd,'THERMAL').ne.0) then
        initdef = 0.9
        dura = 400.0
        coun = 14001.0
        heat = .true.
        ! Given ohnesorge used as bouyancy (expansion) coefficient for boy routine ...
        expansion = ohne
        ! ... and then set to the appropriate value for glycol
        ohne = 0.10
        froude = 0.04082d0
        peclet = 50.0d0 !15.20d0
        initemp = 0.0d0
        refwalltemp = 10.0d0
        ! Contact angle in degrees
        eqContactAngle = 50.0 * pi / 180.0
        !flow = 0.03
        ! masscheck = .true.
        clipvel = 0.2d0
        radparam = 0.1d-3

      ! Heated 2 mm glycol droplet
      else if(index(wd,'GLYCOL').ne.0) then
        !omeg = 1.01
        initdef = 0.9
        dura = 40.0
        coun = 5001.0
        heat = .true.
        ! Given ohnesorge used as bouyancy (expansion) coefficient for boy routine ...
        expansion = ohne
        ! ... and then set to the appropriate value for glycol
        ohne = 0.261
        froude = 0.0282d0
        peclet = 50.20d0
        initemp = 0.0d0
        refwalltemp = 10.0d0
        ! Contact angle in degrees
        eqContactAngle = 65.0 * pi / 180.0
        !flow = 0.03
        !masscheck = .true.

      ! Heat just diffusing through a relatively motionless droplet
      else if(index(wd,'STATICHEAT').ne.0) then
        ! omeg = 1.01
        initdef = 0.95
        dura = 40.0
        coun = 4001.0
        froude = 1000.0d0
        !monocharge = .true.
        !flow = -0.07
        heat = .true.

        peclet = 50.20d0
        initemp = 0.0d0
        refwalltemp = 10.0d0
        ! Contact angle in degrees
        eqContactAngle = 55.0 * pi / 180.0

      ! Oblate experiments
      else if(index(wd,'OBLATE').ne.0) then

        ! Default fissility jump - NOT fissility !!!
        omeg = 0.02
        ! Default sample time - NOT duration !!!
        dura = 2.0
        initdef = 0.95
        cflr = 0.01
        !flow = 0.07
        coun = 50001.0
        oblatexpt = .true.

      ! Uncharged micro oscillation 
      else if(index(wd,'MICRO').ne.0) then
        flow = -0.07
        dura = 10.00
        coun = 60000
        pcor = 2.0 * bres

      ! Semi-charged micro oscillation
      else if(index(wd,'MICRQ').ne.0) then
        flow = -0.07
        dura = 10.00
        coun = 60000
        omeg =  0.5
        pcor = 2.0 * bres
        monocharge = .true.

      ! Relaxation from above
      else if(index(wd,'TOP').ne.0) then
        initdef = 2.75
        dura = 5.0
        
      ! Relaxation from below
      else if(index(wd,'BOT').ne.0) then
        initdef = 0.80
        dura = 5.0
        
      ! Sit and do nothing experiment
      else if(index(wd,'SIT').ne.0) then
        dura = 5.0
        !omeg = 1.0
        masscheck = .true.
        coun = 101.0
        
      ! Prolate experiment
      else if(index(wd,'PROLATE').ne.0) then
        omeg = 1.0
        !initdef = 1.1
        dura = 40.0
        coun = 15001.0
        monocharge = .true.
        flow = -0.07

      ! Prolate experiment but using a surface inflexion to trigger the explosion instead
      else if(index(wd,'INFLEX').ne.0) then
        omeg = 1.0
        !initdef = 1.1
        dura = 40.0
        coun = 15001.0
        monocharge = .true.
        flow = -0.07
        inflex = .true.

      ! Prolate experiment but using a surface inflexion to trigger the explosion instead
      else if(index(wd,'RESIST').ne.0) then
        omeg = 1.0
        !initdef = 1.1
        dura = 40.0
        coun = 15001.0
        monocharge = .true.
        flow = -0.07
        inflex = .true.
        resist = .true.

      ! Unrecognized experiment
      else
        print*, 'Unrecognized experiment !!!'
        stop
      end if

      ! Create the starting mesh file if required
      call makequarter()

      return

      end subroutine setup


      subroutine SetFlow()

      ! Determines appropriate initial flow strength to go with a specified equilibrium contact
      ! angle and given initial geometry (assume 90 degrees for the moment)

      flow = real( ( dble(pi) / 2.0d0 - eqContactAngle ) ** 3.0d0 ) / ( eqConstant * dble(fitbb) )

      end subroutine SetFlow


      subroutine VolumeUpdate(V,Xvel)

      ! Sets up the volume dependent contact angle parameters

      implicit none
      double precision angle, Xvel, V

      if(.not.heat) return

      ! Determines factors derived from the given equilibrium contact angle
      ! Note that the volume used in the code is divided by 2 pi.

      ! Calculate dynamic contact angle from X-node velocity and equilibrium contact angle using the formula:
      !
      !          eqConstant * Xvel = ( angle - eqContactAngle ) ** 3
      if(Xvel.gt.0.0d0) then
        angle = dble(eqContactAngle) + ( eqConstant * Xvel ) ** (1.0d0/3.0d0)
      else
        angle = dble(eqContactAngle)
      end if
      ! Form the tangent
      qt = tan(angle)
      ! The s construction
      qs = 1.0d0 / qt + sqrt( 1.0d0 + 1.0d0 / ( qt * qt ) )
      ! Volume multiplier within cube root for a
      aq = 1.0d0 + 3.0d0 * qs * qs
      aq = 12.0d0 / aq
      ! Volume multiplier within cube root for b
      bq = 3.0d0 + 1.0d0 / ( qs * qs )
      bq = 12.0d0 * qs / bq
      ! Volume multiplier within cube root for c
      !cq = 3.0d0 + 1.0d0 / ( qs * qs )
      !cq = cq * qt * qt * qt
      !cq = 12.0d0 * qs / cq

      qa = ( aq * V ) ** ( 1.0d0 / 3.0d0 )
      qb = ( bq * V ) ** ( 1.0d0 / 3.0d0 )
      ! qc = ( cq * V ) ** ( 1.0d0 / 3.0d0 )
      qc = ( qb * qb - qa * qa ) / ( 2.0d0 * qa )

      if(fish) then

        qac = qa + 2.0d0 * qc

        open(12,file=prefix//'eqcircle.geo')

        write(12,*)'h = 0.1;'
        write(12,*)'Point(20000) = {0, ',-qc,' ,0,h};'
        write(12,*)'Point(20006) = { ',qb,' ,0,0,h};'
        write(12,*)'Point(20007) = {0, ',qa,' ,0,h};'
        write(12,*)'Point(20008) = {-',qb,' ,0,0,h};'
        write(12,*)'Point(20009) = {0,-',qac,' ,0,h};'

        write(12,*)'Circle(20106) = {20006,20000,20007};'
        write(12,*)'Circle(20107) = {20007,20000,20008};'
        write(12,*)'Circle(20108) = {20008,20000,20009};'
        write(12,*)'Circle(20109) = {20009,20000,20006};'

        write(12,*)' Line Loop(22) = {20106,20107,20108,20109};'
 
        write(12,*)' Plane Surface(60001) = {22};'

        close(12)
      end if

      qac = ( qa + qc ) ** 2

      end subroutine VolumeUpdate

      
      subroutine EquilibriumCircle(x,y)

      ! Relocates points lying within the substrate to lie on displaced equilibrium circle

      implicit none
      double precision x, y

      !x = atan2(y-0.5d0,x)
      
      !y = 0.5d0 + 0.5d0 * sin(2.0d0*x)

      !x = 0.5d0 * cos(2.0d0*x)

      !return

      if((.not.heat).or.(y.ge.0.0d0)) return

      ! call gmshSP(13,real(x),real(y),0.0d0)

      ! If asking for points below the bottom of the equilibrium circle
      if( qac - ( qc + y ) ** 2 .le. 0.0d0 ) then
        x = dble(fitbb) - qb + y + qa + 2.0d0 * qc
        y = -(qa + 2.0d0 * qc)
      else
        x = dble(fitbb) - qb + sqrt( qac - ( qc + y ) ** 2 )
        ! Use the other root if x turned-out negative with the first root tried
        if(x.lt.0.0d0) then
          x = dble(fitbb) - qb - sqrt( qac - ( qc + y ) ** 2 )
        end if
      end if

      ! call gmshSP(13,real(x),real(y),1.0d0)

      end subroutine EquilibriumCircle


      subroutine RealEquilibriumCircle(x,y)

      ! Relocates points lying within the substrate to lie on displaced equilibrium circle

      implicit none
      real x, y

      if((.not.heat).or.(y.ge.0.0)) return

      ! If asking for points below the bottom of the equilibrium circle
      if( qac - ( qc + dble(y) ) ** 2 .le. 0.0d0 ) then
        x = fitbb - real(qb) + y + real(qa + 2.0d0 * qc)
        y = -real(qa + 2.0d0 * qc)
      else
        x = fitbb + real( -qb + sqrt( qac - ( qc + dble(y) ) ** 2 ) )
        ! Use the other root if x turned-out negative with the first
        if(x.lt.0.0) then
          x = fitbb + real( -qb - sqrt( qac - ( qc + dble(y) ) ** 2 ) )
        end if
      end if

      end subroutine RealEquilibriumCircle


      double precision function boy(t)

      ! Alters the importance of the gravity term according to temperature

      implicit none
      double precision t

      boy = 1.0d0 - ( 1.0d0 - dble(expansion) ) * ( t - initemp ) / ( refwalltemp - initemp )

      end function boy
      

      double precision function walltemp(r)

      ! Temperature of substrate wall
    
      implicit none
      double precision r

      walltemp = refwalltemp !* ( 1.0d0 - r / dble(fitbb) )

      end function walltemp


      subroutine makequarter()

      use radrop_util, only : nch

      implicit none

      integer loop, i, junk, count, x, z, nods, ios1
      real trial, maxy(2), miny(2)
      character whole_line*100
      logical meshed

      inquire(file='slice.msh',exist=meshed)

      if(meshed) return

      maxy = -9999.0; miny = 9999.0

      ! Will create the geometry file twice, the second time with the extra y-axis nodes
      do loop = 1, 3

        ! Loop 2 just gets the max and min y values of the Tabata nodes needed for the drag setup
        if(loop.eq.2) then
          if(drag) then
            goto 5
          else
            cycle
          end if
        end if

        ! Open the geometry file
        open(56,file='slice.geo')

        write(56,*) ''
        write(56,*) '// Mesh Resolution'
        write(56,*) 'h = ',bres,';'
        write(56,*) ''
        write(56,*) '// Increase resolution factor for boundaries that will get stretched ....'
        write(56,*) 'ref = 0.75;'
        write(56,*) ''
        write(56,*) '// Eccentricity'
        write(56,*) 'e = ',abs(initdef),';'
        write(56,*) ''
        write(56,*) '// Calculate Cube root using Halleys method (http://en.wikipedia.org/wiki/Cube_root)'
        write(56,*) 'r = Sqrt(e);'
        write(56,*) 'For num In {1:255}'
        write(56,*) '   rrr = r*r*r;'
        write(56,*) '   top = rrr + 2 * e;'
        write(56,*) '   bot = 2 * rrr + e;'
        write(56,*) '   r = r * top / bot;'
        write(56,*) 'EndFor'
        write(56,*) ''
        write(56,*) '// Radii of Circular Boundaries'
        write(56,*) 'r = 1.0 / r;'
        write(56,*) ''
        if(heat.or.(drag.and.(initdef.lt.0.0))) then
        write(56,*) '// Parameters for control points'
        write(56,*) 'a = e*r;'
        write(56,*) 'b = r;'
        write(56,*) ''
        write(56,*) 'g = 2*h / (2*a*b/(b*b-a*a));'
        end if
        write(56,*) ''       
        write(56,*) '// Origin - the Centre of all Circles'
        write(56,*) 'Point(20000) = { 0.0, 0.0, 0.0, h };'
        write(56,*) ''
        if(heat.or.(drag.and.(initdef.lt.0.0))) then
        write(56,*) '// Control Points for B-Spline'
        write(56,*) 'Point(20001) = { r - g, 2*h, 0.0, h };'
        write(56,*) 'Point(20002) = { r/2 , e*r, 0.0, h };'
        end if
        if(drag.and.(initdef.lt.0.0)) then
        write(56,*) 'Point(20011) = { r - g, -2*h, 0.0, h };'
        write(56,*) 'Point(20012) = { r/2 , -e*r, 0.0, h };'
        end if
        write(56,*) ''
        write(56,*) '// Gamma Boundary ...'
        write(56,*) ''
        write(56,*) '// ... Points'
        if(drag) then
        write(56,*) 'Point(10016) = {  0.0,   1.52*e*r, 0.0, h*ref};'
        end if
        write(56,*) 'Point(20006) = {  0.0,   e*r, 0.0, h*ref};'
        if(drag) then
        write(56,*) 'Point(20026) = {  0.0,  -e*r, 0.0, h*ref};'
        write(56,*) 'Point(10036) = {  0.0,  -1.52*e*r, 0.0, h*ref};'
        write(56,*) 'Point(10017) = {  2*r,   0.0, 0.0, h};'
        end if
        write(56,*) 'Point(20007) = {  r,   0.0, 0.0, h};'
        if(drag) then
        write(56,*) 'Point(10027) = {  1.52*r,   1.52*e*r, 0.0, h};'
        write(56,*) 'Point(10037) = {  1.52*r,  -1.52*e*r, 0.0, h};'
        end if
        write(56,*) ''
        write(56,*) '// ... Lines'
        if(heat.or.(drag.and.(initdef.lt.0.0))) then
        write(56,*) 'BSpline(20106) = {20007,20001,20002,20006};'
        else
        write(56,*) 'Ellipse(20106) = {20007,20000,20006,20006};'
        end if
        if(drag.and.(initdef.lt.0.0)) then
        write(56,*) 'BSpline(20116) = {20026,20012,20011,20007};'
        else if(drag) then
        write(56,*) 'Ellipse(20116) = {20026,20000,20007,20007};'
        end if
        if(drag) then
        write(56,*) 'Line(10108)    = {10027,10016};'
        write(56,*) 'Line(10109)    = {10036,10037};'
        write(56,*) 'Line(20110)    = {10016,20006};'
        write(56,*) 'Line(20111)    = {20026,10036};'
        write(56,*) 'Line(10112)    = {10027,10037};'
        else
        write(56,*) 'Line(20109)    = {20007,20000};'
        write(56,*) 'Line(20110)    = {20006,20000};'
        end if
        write(56,*) ''
        write(56,*) '// ... Loops'
        if(drag) then
        write(56,*) 'Line Loop(22) = {-20106,-20116,20111,10109,-10112,10108,20110};'
        else
        write(56,*) 'Line Loop(22) = {-20106,-20110,20109};'
        end if
        write(56,*) ''
        write(56,*) ''
        if(bemo) then
          write(56,*) '// Outer BEM Region ( Material Number 99 )'
          write(56,*) 'Plane Surface(60099) = {22};'
        else
          write(56,*) '// Inner FEM Region ( Material Number 1 )'
          write(56,*) 'Plane Surface(60001) = {22};'
        end if
        write(56,*) ''
  
        ! Initial step
        if(loop.eq.1) goto 99

        ! Now get the mesh nodes coincident with the y-axis
    5   open(57,file='slice.msh')

        ! Initialize a node number for the y-axis nodes
        count = 1000; nods = 0

        ! Loop all the entries in the msh file
        do 10 i = 1, 10000

          ios1 = 0

          read(57, '(a)', iostat=ios1) whole_line
          if (ios1 .lt. 0) then
            close(57)
            exit
          else if(index(whole_line, '$') .ne. 0) then
            cycle
          else
            whole_line(90:100) = ' 1 1 1 1 1'
          endif

          ! Read the record from the msh
          read(whole_line,*,err=10) junk, x, trial, z

          ! Capture the number of nodes
          if((.not.meshed).and.(junk.gt.10)) then
            meshed = .true.
            nods = junk
            junk = 0
          end if

          ! Check it conforms to the required format
          if(x.ne.0) cycle
            
          if(z.ne.0) cycle

          ! Get the max and min y-values
          if(loop.eq.2) then
            if(trial.gt.tol) then
              maxy(1) = max(maxy(1),trial); miny(1) = min(miny(1),trial)
            else if(trial.lt.-tol) then
              maxy(2) = max(maxy(2),trial); miny(2) = min(miny(2),trial)
            end if
          else

          ! Avoid declaring points which lye on an edge (confuses gmsh ...)
          if(drag) then
            if(abs(trial-maxy(1)).lt.tol) cycle
            if(abs(trial-miny(1)).lt.tol) cycle
            if(abs(trial-maxy(2)).lt.tol) cycle
            if(abs(trial-miny(2)).lt.tol) cycle
          else
          if(trial.lt.tol) cycle
          end if

          ! Adapt the node number to be used
          count = count - 1

          ! Write the record to the geo
          write(56,*) 'Point(',count,') = {',bres,' * 0.6 ,',trial,',0,',bres,' * 1.7};'
          write(56,*) 'Point{',count,'} In Surface{60001};'

          end if

          ! Exit the loop when all the nodes have been examined
          if(junk.eq.nods) exit

   10   end do

        ! Close the msh file
        close(57)

        ! No meshing in loop 2
        if(loop.eq.2) cycle

        ! Close the geo file
   99   close(56)

        ! Build the gmsh mesh
        if(linux) then
          call system('/home/radcliffe/util/gmsh-2.5.0-Linux/bin/gmsh -smooth 5 -2 -v 2 slice.geo')
        else
          call system('C:\Gmsh\gmsh-2.5.0-Windows\gmsh -smooth 5 -2 -v 2 slice.geo')
          !call system('"C:\Alastair\util\gmsh-2.5.0-Windows\gmsh-2.5.0-Windows\gmsh.exe" -smooth 5 -2 -v 2 slice.geo')
          !call system('"C:\Program Files (x86)\Gmsh\gmsh-2.5.0-Windows\gmsh.exe" -smooth 5 -2 -v 2 slice.geo')
        end if

      ! End the loop on creating the geometry file twice
      end do

      end subroutine makequarter


end module CONTROL



module TEMPERATURE

    ! Temperatures on the free edge
    double precision, allocatable::ET(:)

    contains

      subroutine setuptemp(m)
      use CONTROL, only : initemp
      implicit none
      integer err, m
      allocate(ET(m),stat=err)
      if(err.ne.0) then
        print*,'Insufficient Memory for ET Matrix!'     
        stop
      else
        ET = initemp
      end if
      end subroutine setuptemp


      subroutine loadtemp(i,val)
      implicit none
      integer i
      double precision val
      ET(i) = val
      end subroutine loadtemp
      
      
      double precision function radiation(i)
      use CONTROL, only : initemp, radparam
      implicit none
      integer i
      
      radiation = radparam * ( ET(i) - initemp ) ** 4

      end function radiation

end module TEMPERATURE



module BOUNDARY

    integer :: nn
    double precision, allocatable :: r(:)  ! radius
    double precision, allocatable :: t(:)  ! angle theta
    double precision, allocatable :: ddr(:)! second derivative of r with respect to t on N points; calculated in "spline"

    contains


      subroutine SplineSetup(n,List,X,Y)

      use SIZES, only : maxnod

      implicit none

      integer, intent(in) :: n, List(n)
      real, intent(in) :: X(maxnod), Y(maxnod)

      ! Local variables
      integer i
      double precision xx, yy

      nn = n

      allocate(r(nn),t(nn),ddr(nn),stat=i)

      if(i.ne.0) then
        print*,'ERROR in SplineSetup: Not enough memory for arrays !!!'
        stop
      end if

      ! Prepare input for call to spline
      do i = 1, n
        xx = X(List(i)); yy = Y(List(i))
        r(i) = sqrt(xx*xx+yy*yy)
!        t(i) = xx / r(i)
!        t(i) = asin(t(i))
        t(i) = atan2(yy,xx)
      end do

      ! first derivative of r with respect to t is zero at t(1) (Oy axes) for regular tips  (non-singular tips)
      xx = 0.0d0
      ! first derivative of r with respect to t is zero at t(N) (Ox axes) if plane symmetry
      yy = 0.0d0

      ! see numerical recipes p 114
      call spline(t,r,nn,xx,yy,ddr)

      end subroutine SplineSetup




      ! Calculates a mean curvature at (x,y) using the splines
      subroutine splinesmooth(x,y)

      implicit none

      double precision, intent(inout) :: x, y
      logical :: sx, sy

      double precision rr, tt

!      if(x.ne.y) return

      ! Record the original quadrant
      sx = x .lt. 0.0d0; sy = y .lt. 0.0d0

      ! Always work in first quadrant
      if(sx) x = -x
      if(sy) y = -y

      rr = sqrt(x*x+y*y)
      tt = atan2(y,x)

      call splint(t,r,ddr,nn,tt,rr)

      x = rr * cos(tt)
      y = rr * sin(tt)

      ! Restore original quadrant
      if(sx) x = -x
      if(sy) y = -y

      end subroutine splinesmooth



      ! Calculates a mean curvature at (x,y) using the splines
      double precision function spline_H(x,y)

      use CONTROL, only : pi, tol

      double precision cotan,num,denum
      double precision x,y,t,a,b,du,dv,dr,ddrm

      ! t=midpoint
      if(y.gt.tol) then
        t = atan(x / y)
      else
        t = 0.5 * pi-ym/xm
      end if

      ! rm = r at midpoint tm
      call splint(t,r,ddr,nn,t,r)

        ! A at midpoints
!        a = (t(i+1)-tm)/(t(i+1)-t(i))
        ! B at midpoints
        b=1.0d0 - a
        du=r(i+1)-r(i);
!        dv=t(i+1)-t(i);
        ! first derivative of r with respect to t for mid points
        drm=du/dv -dv*ddr(i)*(3.0*am*am-1.0)/6.0 +dv*ddr(i+1)*(3.0*bm*bm-1.0)/6.0

        ! second derivative of r with respect to t for mid points
        ddrm=am*ddr(i)+bm*ddr(i+1)

        cotan = 1.0/tan(tm)

        num = rm*rm*rm+1.5*rm*drm*drm-0.5*cotan*drm*drm*drm -0.5*rm*rm*(cotan*drm + ddrm)

!        denum = rm**(rm*rm+drm*drm,1.5)

        ! mean curvature at midpoints
        spline_H = num / denum

      end function spline_H

end module BOUNDARY

