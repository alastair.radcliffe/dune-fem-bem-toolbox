


module RESISTANCE

    ! Declared storage array size
    integer :: mx = 0

    ! Number of collected values
    integer :: ct = 0

    ! Store the coefficients needed to calculate the surface traction (normal derivative)
    integer, allocatable :: iStore(:)
    integer, allocatable :: jStore(:)
    real, allocatable :: rStore(:,:)
    double precision, allocatable :: xStore(:), yStore(:)

    contains


      subroutine dragSetup(m)

      use CONTROL, only : firsttime, resist

      implicit none

      integer m, err

      if(firsttime) then

      mx = m

      if(resist) then
        allocate(iStore(mx),jStore(mx),rStore(mx,6),xStore(mx),yStore(mx),stat=err)
      else
      allocate(iStore(mx),rStore(mx,6),xStore(mx),yStore(mx),stat=err)
      end if

      ! Check for allocation error
      if(err.ne.0) then
        print*,'ERROR in dragSetup: Insufficient memory for traction store arrays !!!'
        stop
      endif

      else
        ct = 0
      end if

      end subroutine dragSetup


      subroutine dragStore(x1,y1,x2,y2,x3,y3,jj,ii,Rx,Ry)

      use CONTROL, only : resist

      implicit none

      integer ii, jj
      real x1, y1, x2, y2, x3, y3
      double precision Rx, Ry

      ct = ct + 1

      ! Check for list overshoot
      if(ct.gt.mx) then
        print*,'ERROR in dragSetup: Overshoot of traction store arrays !!!'
        stop
      endif

      ! Store the data required for calculating the velocity derivatives later
      rStore(ct,1) = x1; rStore(ct,2) = y1; rStore(ct,3) = x2; rStore(ct,4) = y2; rStore(ct,5) = x3; rStore(ct,6) = y3
      iStore(ct) = ii; xStore(ct) = Rx; yStore(ct) = Ry
      if(resist) jStore(ct) = jj

      end subroutine dragStore


      subroutine dragCalculate(P,sumarea,dragY,dragYp,m,List,E)

      use SIZES, only : maxnod
      use PLACE, only : rr
      use CONTROL, only : resist, sdiv

      implicit none

      integer i, ii, jj, m, aim, List(m)
      real Nx, Ny, x1, y1, x2, y2, x3, y3, crossLight
      double precision P(3*maxnod), E(m), dragX, dragY, dragXp, dragYp, area, Rx, Ry, Ux, Uy, Up, sumarea

      ! Initialize the drag accumulators
      dragX = 0.0d0; dragY = 0.0d0; dragXp = 0.0d0; dragYp = 0.0d0; sumarea = 0.0d0

      ! Loop through all the values collected
      do i = 1, ct

        ! Get the coordinates of the triangle involved (x1,y1) always off the boundary
        x1 = rStore(i,1); y1 = rStore(i,2); x2 = rStore(i,3); y2 = rStore(i,4); x3 = rStore(i,5); y3 = rStore(i,6)

        ! Ensure a standard orientation of the triangle     !   \
        if(crossLight(x2-x1,y2-y1,x3-x1,y3-y1).lt.0.0) then !    \
                                                            !     2----------1
          ! Flip nodes 2 and 3 around                       !      \        /
          Nx = x2; Ny = y2                                  !       \      /
          x2 = x3; y2 = y3                                  !        \    /
          x3 = Nx; y3 = Ny                                  !  y      \  /
        end if                                              !  |       3
                                                            !  |        \
        ! Unit Normal vector (Nx,Ny) to boundary            !  O----x    \
        Nx = y2 - y3; Ny = x3 - x2
        area = sqrt( dble(Nx*Nx + Ny*Ny) )
        Nx = Nx / real(area); Ny = Ny / real(area)

        ! Equation of line from (x2,y2) to (x3,y3) in terms of x = x(y)
        !                                                                   !        ____________
        !      [ x2 - x3 ]                                     !   dx       !       /     | dx |2         _________
        !  x = ----------- [ y - y2 ] + x2  =  Ux * y + Uy     !   -- = Ux  !  dy  /  1 + | -- |  = dy  / 1 + Ux^2
        !      [ y2 - y3 ]                                     !   dy       !    \/       | dy |      \/
        !
        Ux = dble( x2 - x3 ) / dble( y2 - y3 )                                     !   /3\
        Uy = dble(x2) - Ux * dble(y2)                                              !   |            _________
                                                                                   !   |  x(y) dy  / 1 + Ux^2
        ! Area of ring is just the integral of x = x(y) from (x2,y2) to (x3,y3)    !   |         \/
        area = Ux * dble( y3*y3 - y2*y2 ) / 2.0d0 + Uy * dble( y3 - y2 )           ! \2/
        area = area * sqrt( 1.0d0 + Ux * Ux )
        area = abs(area)

        ! Global node making the contribution ...
        ii = iStore(i)
        ! ... has x and y directed velocity values of
        Ux = P(rr(1,ii)); Uy = P(rr(2,ii)); Up = P(rr(3,ii))

        ! Stored differentiation coefficients
        Rx = xStore(i); Ry = yStore(i)

        ! Accumulate the drags
        !
        !      t_j = [ u_{i,j} + u_{j,i} ] n_i       { i, j = x, y }
        !
        !      t_x = [ u_{x,x} + u_{x,x} ] n_x + [ u_{y,x} + u_{x,y} ] n_y
        !
        !      t_y = [ u_{x,y} + u_{y,x} ] n_x + [ u_{y,y} + u_{y,y} ] n_y
        !
        dragX = dragX + area * ( 2.0d0 * Rx * Ux * dble(Nx) + ( Rx * Uy + Ry * Ux ) * dble(Ny) )
        dragY = dragY + area * ( 2.0d0 * Ry * Uy * dble(Ny) + ( Ry * Ux + Rx * Uy ) * dble(Nx) )

        ! Fill-out surface vector E with surface divergence of velocity
        if(resist) then
          ! Identify position in the list of boundary nodes corresponding to global node number making contribution
          jj = jstore(i)
          do aim = 1, m
            if(List(aim).eq.jj) exit
          end do
          E(aim+1) = E(aim+1) + dble(sdiv) * ( dble(-Ny) * Rx * Ux + dble(Nx) * Ry * Uy )
        end if

        if(modulo(i,3).eq.0) then
          dragXp = dragXp - area * Up * Nx
          dragYp = dragYp - area * Up * Ny
          sumarea = sumarea + area
        end if
      end do

      end subroutine dragCalculate


end module RESISTANCE


      subroutine symstretch(x1,x2,x3,y1,y2,y3,image,base)

      use CONTROL, only : disgal, tol

      implicit none

      integer image(3), base
      real x1, x2, x3, y1, y2, y3

      if(x1+x2.eq.0.0) then
        if(     abs(y3-y1).lt.tol) then    ! CASE #4
          base = -40
          if(disgal) then
            image(3) = 1
          else
            image(1) = 3
          end if
          x1 = -x3
        else if(abs(y3-y2).lt.tol) then    ! CASE #1
          base = -10
          if(disgal) then
            image(3) = 2
          else
            image(2) = 3
          end if
          x2 = -x3
        else
          print*,"ERROR in symstretch: Bad core element.",x1, x2, x3, y1, y2, y3
          stop
        end if

      else if(x1+x3.eq.0.0) then
        if(     abs(y2-y1).lt.tol) then    ! CASE #5
          base = -50
          if(disgal) then
            image(2) = 1
          else
            image(1) = 2
          end if
          x1 = -x2
        else if(abs(y2-y3).lt.tol) then    ! CASE #2
          base = -20
          if(disgal) then
            image(2) = 3
          else
            image(3) = 2
          end if
          x3 = -x2
        else
          print*,"ERROR in symstretch: Bad core element.",x1, x2, x3, y1, y2, y3
          stop
        end if

      else if(x2+x3.eq.0.0) then
        if(     abs(y1-y2).lt.tol) then    ! CASE #6
          base = -60
          if(disgal) then
            image(1) = 2
          else
            image(2) = 1
          end if
          x2 = -x1
        else if(abs(y1-y3).lt.tol) then    ! CASE #3
          base = -30
          if(disgal) then
            image(1) = 3
          else
            image(3) = 1
          end if
          x3 = -x1
        else
          print*,"ERROR in symstretch: Bad core element.",x1, x2, x3, y1, y2, y3
          stop
        end if

      end if

      end subroutine symstretch




      subroutine maxVelocity(L,P,Q,Connect,firsttime)

      use PLACE, only : rr, HX, HY, isvroot, istroot, isproot, oor
      use CONTROL, only : flow, heat, initemp, drag, inflex, inflexion, relaxing, fitaa, ejectC, ejectK, ejectP
      use CONTROL, only : maxvel
      use SIZES, only : omaxnod, maxnod, maxele

      implicit none

      integer e, n, i, Connect(maxele,10), r1, r2
      double precision L(3*omaxnod), P(3*maxnod), Q(2*maxnod), testvel
      logical firsttime

      maxvel = 0.0d0

      if(firsttime) Q = 0.0d0

      ! For each new element ...
      do e = 1, maxele
        ! ... loop the three nodes and ...
        do n = 1, 3

          ! Global node number
          i = Connect(e,n)

          ! Initialization for temperature root nodes when required
          if(firsttime.and.istroot(i)) then
            r1 = rr(4,i)
            P(r1) = initemp
          end if

          ! Initialization for pressure root nodes when required
          if(drag.and.isproot(i)) then
            r1 = rr(3,i)
            P(r1) = L(oor(3,i))
          end if

          ! Only interested in velocity root nodes
          if(.not.isvroot(i)) cycle

          r1 = rr(1,i); r2 = rr(2,i)

          ! Reset solution velocities immediately after explosion
          if(inflex.and.inflexion.and.(.not.relaxing)) then
            testvel = dble(HY(i)) / (ejectC/ejectK)
            if(testvel.gt.1.0d0) then
              ! Reverse (and scale down greatly) velocities within tip region
              testvel = - ( testvel - 1.0d0 ) * ejectP * 10000.0d0
            else
              ! Phase-out any existing velocities towards tip base line
              testvel = 1.0d0 - testvel
              testvel = testvel * testvel
            end if
            P(r1) = P(r1) * testvel
            P(r2) = P(r2) * testvel
          end if

          if(drag) then
            P(r1) = L(oor(1,i))
            P(r2) = L(oor(2,i))
            testvel = dble(flow)
          else if(firsttime) then
            P(r1) =           flow * dble(HX(i))
            P(r2) = - 2.0d0 * flow * dble(HY(i))
            testvel = P(r1)*P(r1)+P(r2)*P(r2)
          else
            testvel = P(r1)*P(r1)+P(r2)*P(r2)
          end if

          ! Keep track of maximum velocity magnitude in the analytic flow history
          if(testvel.gt.maxvel) maxvel = testvel

        end do! n = 1, 3

      end do! e = 1, maxele

      end subroutine maxVelocity






      subroutine Simplex(ord,i,z1,z2,z3,finding_node)
 
      implicit none
      integer ord, i, z1, z2, z3, j, k
      integer L(8,3,45), M(8,0:8,0:8,0:8)
      logical finding_node, done                                          
      data done /.False./, L /1080*0/, M /5832*0/
      save done,L,M

      if(done) goto 100

      include "fe2dlist.inc"

      do 20 k = 1, 8
        do 10 j = 1, (k+1)*(k+2)/2
          M(k,L(k,1,j),L(k,2,j),L(k,3,j)) = j
   10   continue
   20 continue

      done = .true.

  100 if(finding_node) then
        i = M(ord,z1,z2,z3)
      else
        z1 = L(ord,1,i); z2 = L(ord,2,i); z3 = L(ord,3,i)
      endif

      return
      end



      subroutine nonlinchk(fil,x1,y1,x2,y2,x3,y3,val)

      integer fil
      real x1,y1,x2,y2,x3,y3
      double precision val

      write(fil,'(a2)')'ST'
      write(fil,'(a1)')'('
      write(fil,'(e18.10,2(a1,e19.10))') x1,',', y1,',',val / 100.0
      write(fil,'(a1)')','
      write(fil,'(e18.10,2(a1,e19.10))') x2,',', y2,',',val / 100.0
      write(fil,'(a1)')','
      write(fil,'(e18.10,2(a1,e19.10))') x3,',', y3,',',val / 100.0
      write(fil,'(a1)')')'
      write(fil,'(a1)')'{'
      write(fil,'(e18.10)') val / 100.0
      write(fil,'(a1)')','
      write(fil,'(e18.10)') val / 100.0
      write(fil,'(a1)')','
      write(fil,'(e18.10)') val / 100.0
      write(fil,'(a2)')'};'

      end subroutine nonlinchk
