


      subroutine Check_Element(X,Y,Connect,P,ele)

      use PLACE, only : rr
      use SIZES, only : maxnod, maxele

      implicit none
      integer ele, Connect(maxele,10)
      integer image(3)
      real X(maxnod), Y(maxnod)
      double precision S(10,10), T(10,10), W(10,10), RPx(10,10), RPy(10,10), RQx(10,10), RQy(10,10)
      double precision Sxx(10,10), Sxy(10,10), Syx(10,10), Syy(10,10), Rx(10,10), Ry(10,10), RRx(10,3,10), RRy(10,3,10)
      double precision P(3*maxnod), H(10,3,10), Ex(10,3,10), Ey(10,3,10), TV(10,10), TP(10,10)

      call EleComp(X,Y,Connect,Rx,Ry,RPx,RPy,RQx,RQy,S,Sxx,Sxy,Syx,Syy,T,TV,TP,RRx,RRy,W,Ex,Ey,H,ele)

      call Element_Check(X(Connect(ele,1)),Y(Connect(ele,1)), &
                         X(Connect(ele,2)),Y(Connect(ele,2)), &
                         X(Connect(ele,3)),Y(Connect(ele,3)), &
                         Rx,Ry,T,TP,image,                    &
                         P(rr(1,Connect(ele,1))),P(rr(2,Connect(ele,1))),P(rr(3,Connect(ele,1))),  &
                         P(rr(1,Connect(ele,2))),P(rr(2,Connect(ele,2))),P(rr(3,Connect(ele,2))),  &
                         P(rr(1,Connect(ele,3))),P(rr(2,Connect(ele,3))),P(rr(3,Connect(ele,3)))   )

      end subroutine Check_Element


      

      subroutine Element_Check(xx1,yy1,xx2,yy2,xx3,yy3,Rx,Ry,T,TP,image,p11,p12,p13,p21,p22,p23,p31,p32,p33)

      use CONTROL, only : tol, zero

      implicit none

      integer image(3), li, lj, ii, jj, i, j, err, fil
      real xx1, yy1, xx2, yy2, xx3, yy3, x(3), y(3), ims, jms, cmn
      double precision Rx(10,10), Ry(10,10), T(10,10), TP(10,10), z, s, p11,p12,p13,p21,p22,p23,p31,p32,p33, u(3), v(3)
      logical check

      x(1) = xx1; y(1) = yy1; x(2) = xx2; y(2) = yy2; x(3) = xx3; y(3) = yy3

      u(1) = p11; u(2) = p21; u(3) = p31; v(1) = p12; v(2) = p22; v(3) = p32

      check = u(1)+u(2)+u(3)+v(1)+v(2)+v(3) .ne. 0.0d0

      ! Rx row sum equal to zero
      do i = 1, 3
        z = 0.0d0
        do j = 1, 3
          z = z + Rx(i,j)
        end do
        if(abs(z).gt.zero) print*, 'Rx row sum fail ',i,z
      end do

      ! Ry row sum equal to zero
      do i = 1, 3
        z = 0.0d0
        do j = 1, 3
          z = z + Ry(i,j)
        end do
        if(abs(z).gt.zero) print*, 'Ry row sum fail ',i,z
      end do






      ! Loop the divergence testing output files
      do fil = 98, 99

        ! No computed solution divergence for the initial example solution checking
        if((.not.check).and.(fil.eq.99)) cycle

        ! Loop the "testing" shape functions (one per row) for the element
        do li = 1, 3

          ! Retrieve image coding; initialize dummey node flag
          i = image(li)

          ! Check for "image" nodes
          if(i.ne.0) then
            ii  = i
            i   = li
            ims = -1.0
          else
            ! Regular (real) locations
            i   = li
            ii  = i
            ims = +1.0
          end if

          ! Initialize the row summers
          z = 0.0d0; s = 0.0d0

          ! Loop the "interpolation" shape functions (one per column) for the element
          do lj = 1, 3

            ! Retrieve image coding
            j = image(lj)

            ! Check for "image" contributions
            if(j.ne.0) then
              jj  = j
              j   = lj
              jms = -1.0
            else
              ! Regular (real) locations
              j   = lj
              jj  = j
              jms = +1.0
            end if

            ! Common factor for images of everything
            cmn = ims * jms

            ! Checking actual computed solution
            if(check.and.(fil.eq.99)) then
              z = z + ( Rx(i,j) + TP(i,j) ) * u(jj) * jms
              s = s +   Ry(i,j)             * v(jj)
            ! Benoit example
            else if(abs(x(jj)).lt.-tol) then
              z = z + ( Rx(i,j) + TP(i,j) ) * tol / 2.0d0
              s = s +   Ry(i,j)             * ( - y(jj) )
            else
              z = z + ( Rx(i,j) + TP(i,j) ) * ( x(jj) / 2.0d0 ) * jms
              s = s +   Ry(i,j)             * ( - y(jj) )
            end if
          end do

          if((((x(i)-0.0)**2+(y(i)-1.7)**2).lt.0.001).and.(fil.eq.99)) then
            z = z
          end if

          ! Figure-out a percentage error
          if((fil.eq.-99).and.((abs(z).lt.tol).or.(abs(s).lt.tol))) then
            z = 9.9d0
          else
            z = z+s!200.0*abs(z+s)/(abs(z)+abs(s))
          end if

          ! Write Scalar Output Down in GMSH Format
          write(fil,'(a2)')'ST'
          write(fil,'(a1)')'('
          write(fil,'(e18.10,2(a1,e19.10))') xx1,',', yy1,',',z
          write(fil,'(a1)')','
          write(fil,'(e18.10,2(a1,e19.10))') xx2,',', yy2,',',z
          write(fil,'(a1)')','
          write(fil,'(e18.10,2(a1,e19.10))') xx3,',', yy3,',',z
          write(fil,'(a1)')')'
          write(fil,'(a1)')'{'
          write(fil,'(e18.10)') z
          write(fil,'(a1)')','
          write(fil,'(e18.10)') z
          write(fil,'(a1)')','
          write(fil,'(e18.10)') z
          write(fil,'(a2)')'};'

        end do
      end do

      end subroutine Element_Check



      
      subroutine Element_Norm(X,Y,Connect,P,ele)

      use CONTROL, only : lim, disgal, bres
      use PLACE, only : rr, cc, pp
      use SIZES, only : maxnod, maxele

      implicit none
      integer i, j, k, ii, jj, ele, Connect(maxele,10), i1, i2, j1, j2
      real X(maxnod), Y(maxnod), P_ana, U_ana, V_ana, panai, uanai, vanai, panaj, uanaj, vanaj
      double precision S(10,10), T(10,10), W(10,10), H(10,3,10), Ex(10,3,10), Ey(10,3,10), P(3*maxnod), TV(10,10), TP(10,10)
      double precision Sxx(10,10), Sxy(10,10), Syx(10,10), Syy(10,10), Rx(10,10), Ry(10,10), RPx(10,10), RPy(10,10)
      double precision vnorm(6), pnorm(6), RRx(10,3,10), RRy(10,3,10), RQx(10,10), RQy(10,10)
      data vnorm /0.0D0,0.0D0,0.0D0,0.0D0,0.0D0,0.0D0/
      data pnorm /0.0D0,0.0D0,0.0D0,0.0D0,0.0D0,0.0D0/

      save vnorm, pnorm

      if(ele.eq.0) goto 10

      call EleComp(X,Y,Connect,Rx,Ry,RPx,RPy,RQx,RQy,S,Sxx,Sxy,Syx,Syy,T,TV,TP,RRx,RRy,W,Ex,Ey,H,ele)

      do i = 1, lim
        ii = Connect(ele,i)

        i1 = mod(i+1-1,lim)+1
        i2 = mod(i+2-1,lim)+1

        i1 = Connect(ele,i1)
        i2 = Connect(ele,i2)

        do j = 1, lim
          jj = Connect(ele,j)

          j1 = mod(j+1-1,lim)+1
          j2 = mod(j+2-1,lim)+1

          j1 = Connect(ele,j1)
          j2 = Connect(ele,j2)

          ! Numeric Norms

          vnorm(1) = vnorm(1) + P( rr(1,ii) ) *   T(i,j)  * P( rr(1,jj) )   ! u L2
          vnorm(1) = vnorm(1) + P( rr(2,ii) ) *   T(i,j)  * P( rr(2,jj) )   ! v L2
!          pnorm(1) = pnorm(1) + P( rr(3,ii) ) *  TP(i,j)  * P( rr(3,jj) )   ! p L2

          vnorm(2) = vnorm(2) + P( rr(1,ii) ) * Sxx(i,j) * P( rr(1,jj) )   ! udx H1
          vnorm(2) = vnorm(2) + P( rr(2,ii) ) * Sxx(i,j) * P( rr(2,jj) )   ! vdx H1
          !pnorm(2) = pnorm(2) + P( rr(3,ii) ) * Sxx(i,j) * P( rr(3,jj) )   ! pdx H1

          vnorm(2) = vnorm(2) + P( rr(1,ii) ) * Syy(i,j) * P( rr(1,jj) )   ! udy H1
          vnorm(2) = vnorm(2) + P( rr(2,ii) ) * Syy(i,j) * P( rr(2,jj) )   ! vdy H1
          !pnorm(2) = pnorm(2) + P( rr(3,ii) ) * Syy(i,j) * P( rr(3,jj) )   ! pdy H1

          ! Error Norms

          if(disgal) then

            uanai = U_ana( 0.5 * ( X(i1) + X(i2) ) , 0.5 * ( Y(i1) + Y(i2) ) )
            vanai = V_ana( 0.5 * ( X(i1) + X(i2) ) , 0.5 * ( Y(i1) + Y(i2) ) )

            uanaj = U_ana( 0.5 * ( X(j1) + X(j2) ) , 0.5 * ( Y(j1) + Y(j2) ) )
            vanaj = V_ana( 0.5 * ( X(j1) + X(j2) ) , 0.5 * ( Y(j1) + Y(j2) ) )

            panai = P_ana( ( X(ii) + X(i1) + X(i2) ) / 3.0 , ( Y(ii) + Y(i1) + Y(i2) ) / 3.0 )
            panaj = P_ana( ( X(jj) + X(j1) + X(j2) ) / 3.0 , ( Y(jj) + Y(j1) + Y(j2) ) / 3.0 )

          else

            uanai = U_ana( X(ii) , Y(ii) )
            vanai = V_ana( X(ii) , Y(ii) )

            uanaj = U_ana( X(jj) , Y(jj) )
            vanaj = V_ana( X(jj) , Y(jj) )

            panai = P_ana( X(ii) , Y(ii) )
            panaj = P_ana( X(jj) , Y(jj) )

          endif

          vnorm(4) = vnorm(4) + ( P( rr(1,ii) ) - uanai ) *   T(i,j)  * ( P( rr(1,jj) ) - uanaj ) ! u-U L2
          vnorm(4) = vnorm(4) + ( P( rr(2,ii) ) - vanai ) *   T(i,j)  * ( P( rr(2,jj) ) - vanaj ) ! v-V L2
!          pnorm(4) = pnorm(4) + ( P( rr(3,ii) ) - panai ) *  TP(i,j)  * ( P( rr(3,jj) ) - panaj ) ! p-P L2

          vnorm(5) = vnorm(5) + ( P( rr(1,ii) ) - uanai ) * Sxx(i,j) * ( P( rr(1,jj) ) - uanaj ) ! u-U H1
          vnorm(5) = vnorm(5) + ( P( rr(2,ii) ) - vanai ) * Sxx(i,j) * ( P( rr(2,jj) ) - vanaj ) ! v-V H1
          !pnorm(5) = pnorm(5) + ( P( rr(3,ii) ) - panai ) * Sxx(i,j) * ( P( rr(3,jj) ) - panaj ) ! p-P H1

          vnorm(5) = vnorm(5) + ( P( rr(1,ii) ) - uanai ) * Syy(i,j) * ( P( rr(1,jj) ) - uanaj ) ! u-U H1
          vnorm(5) = vnorm(5) + ( P( rr(2,ii) ) - vanai ) * Syy(i,j) * ( P( rr(2,jj) ) - vanaj ) ! v-V H1
          !pnorm(5) = pnorm(5) + ( P( rr(3,ii) ) - panai ) * Syy(i,j) * ( P( rr(3,jj) ) - panaj ) ! p-P H1

          ! Jump Errors
          if(disgal) then
            ! Loop over Edges of Element being Measured
            do k = 1, 3
              ! Skip boundary edges
              if(pp(k,jj).eq.cc(3,jj)) cycle
              ! Interior Only Edge Contributions
              pnorm(2) = pnorm(2) + P( rr(1,ii) ) * H(i,k,j) * P( rr(1,jj) )
              pnorm(2) = pnorm(2) + P( rr(2,ii) ) * H(i,k,j) * P( rr(2,jj) )
              pnorm(5) = pnorm(5) + P( rr(3,ii) ) * Ex(i,k,j) * P( rr(1,jj) )
              pnorm(5) = pnorm(5) + P( rr(3,ii) ) * Ey(i,k,j) * P( rr(2,jj) )
            end do
          endif

        end do
      end do

      return

      ! Form the Numeric Norms
   10 vnorm(3) = sqrt( vnorm(1) + vnorm(2) )!; pnorm(3) = sqrt( pnorm(1) + pnorm(2) )
      vnorm(1) = sqrt( vnorm(1) );            vnorm(2) = sqrt( vnorm(2) )
      pnorm(1) = sqrt( pnorm(1) );            !pnorm(2) = sqrt( pnorm(2) )

      ! Form the Error Norms
      vnorm(6) = sqrt( vnorm(4) + vnorm(5) ); !pnorm(6) = sqrt( pnorm(4) + pnorm(5) )
      vnorm(4) = sqrt( vnorm(4) );            vnorm(5) = sqrt( vnorm(5) )
      pnorm(4) = sqrt( pnorm(4) );            !pnorm(5) = sqrt( pnorm(5) )

      write(*,'(a5,f8.4,2(a3,6e10.3),2i9,a30)') &
        'Norm ',bres, ' V ', vnorm, ' P ', pnorm,maxnod,maxele,' glen [a] [b] [c] [d] [p] time'

      return
      end                                                                                    


      subroutine inSquare(j,mat,x1,y1,x2,y2,x3,y3)

      use CONTROL, only : squa

      implicit none
      integer j, mat, i
      real cross, x(4), y(4), xa, ya, xb, yb, xc, yc, xd, yd, r, x1, y1, x2, y2, x3, y3
      logical c1, c2, c3, c4
      external cross

      ! Prepare corner data
      x(1)=x1; y(1)=y1; x(2)=x2; y(2)=y2; x(3)=x3; y(3)=y3

      ! Prepare median data
      x(4) = ( x1 + x2 + x3 ) / 3.0
      y(4) = ( y1 + y2 + y3 ) / 3.0

      ! Retrieve size of Dirichlet square
      r = squa

      ! Only zap things if requested
      if(r.le.0.0) return

      ! Corners of Dirichlet square
      xa = -r ; ya = -r
      xb =  r ; yb = -r
      xc =  r ; yc =  r
      xd = -r ; yd =  r

      ! Loop the corners and median of the input element
      do i = 1, 4

        ! Compute the cross-products
        c1 = cross( xa - x(i) , ya - y(i)   ,   xb - x(i) , yb - y(i) ) .gt. 0.0
        c2 = cross( xb - x(i) , yb - y(i)   ,   xc - x(i) , yc - y(i) ) .gt. 0.0
        c3 = cross( xc - x(i) , yc - y(i)   ,   xd - x(i) , yd - y(i) ) .gt. 0.0
        c4 = cross( xd - x(i) , yd - y(i)   ,   xa - x(i) , ya - y(i) ) .gt. 0.0

        ! If a node (or the median) has all cross-products positive, then element is to be skipped
        if(c1.and.c2.and.c3.and.c4) then
      
          ! All skipped elements turn into boundary info. indicators for a Dirichlet boundary
          j = 45
          mat = 10001
          return

        endif
      end do

      return
      end




      subroutine Element_Template(R,RP,S,T,TV,TP,H,E,G,F)

      use CONTROL, only : ov, op, lim, disgal, axisym

      ! Computes Reference element templates for Mass (T), Stress (S), Divergence (R), Double Stress (H) and Double Divergence (E)

      implicit none
      integer i, j, k, l, m, n, p, q, err, ac
      double precision R(-60:9,10,3,10), S(-60:9,10,3,3,10), T(-60:9,10,10), H(-60:9,10,3,3,3,3,10), E(-60:9,10,3,3,3,10)
      double precision TP(-6:0,10,10), F(0:9,3,3,3), G(0:9,3,3,3,3), RP(-60:9,10,3,10)
      integer TV(3,1295,7,3)
      
      ! Local construction arrays
      integer,          allocatable :: DD(:,:,:,:)
      double precision, allocatable :: TT(:,:,:,:,:)
      integer,          allocatable :: VT(:,:,:,:,:,:)
      integer,          allocatable :: EE(:,:,:,:,:)
      double precision, allocatable :: ED(:,:,:,:,:,:)
      
      ! Denominators for "DD" and "EE" construction arrays
      integer Ddiv(-1:8), Ediv(-1:8)
      data Ddiv /1,1,1,1,2,3,12,10,60,105/, Ediv /1,1,1,1,2,3,12,10,60,105/

      ! Allocate space for the construction arrays
      allocate(DD(-1:8,10,3,10), TT(-60:9,-1:8,-1:8,10,10), VT(-1:-1,-1:-1,3,1295,7,3), &
                                                                          EE(-1:8,10,3,3,10), ED(0:9,-1:-1,-1:1,3,3,3), stat=err)

      ! Check for allocation errors
      if(err.ne.0) then
        print*,'Failed to allocate DD and/or TT and/or EE construction arrays.'
        stop
      endif

      ! Blank construction arrays and element template arrays
      DD = 0; TT = 0.0D0; VT = 0; EE = 0; ED = 0.0D0; RP = 0.0D0
      R = 0.0D0; S = 0.0D0; T = 0.0D0; TV = 0; TP = 0.0D0; H = 0.0D0; E = 0.0D0; G = 0.0D0; F = 0.0D0

      ! Set the construction array entries
      include "fe2ddmat.inc"
      include "fe2dtmat.inc"
      include "fe2dtaxi.inc"
      include "fe2dtcor.inc"
      include "fe2demat.inc"
      include "fe2dsmat.inc"
      include "fe2dtinv.inc"

      ! Loop extra bits for axi-symmetric
      do ac = -60, 9

      ! First indices of ac = 1 -> 9 of mass matrix construction array "TT" used for axisym, but only an index of ac=0 otherwise
      if((.not.axisym).and.(ac.ne.0)) cycle

      do m = 1, lim
        do n = 1, lim
          T( ac,m,n) = TT(ac,ov,ov,m,n)
!          if(.not.axisym) &
!          TV(ac,m,1,n) = TT(ac,ov,ov,m,n)
          if(mod(ac,10).eq.0) &
          TP(ac/10,m,n) = TT(ac,ov,op,n,m)
          if(ac.eq.0) then
            do i = 1, 1295
              do j = 1, 7
                TV(m,i,j,n) = VT(ov,ov,m,i,j,n)
              end do
            end do
          end if
          do i = 1, 3
            if(disgal.and.(m.le.3).and.(n.le.3).and.(ac.ge.0)) &
              F(ac,m,i,n) = F(ac,m,i,n) + ED(ac,ov,op,i,n,m)
            do p = 1, lim
              R( ac,m,i,n) = R( ac,m,i,n) + dble(DD(ov,n,i,p)) * TT(ac,ov,op,p,m) / dble( Ddiv(ov) )
              RP(ac,m,i,n) = RP(ac,m,i,n) + dble(DD(ov,n,i,p)) * TT(ac,ov,ov,p,m) / dble( Ddiv(ov) )
              do q = 1, lim
                if(disgal.and.(m.le.3).and.(q.le.3).and.(n.le.3).and.(ac.ge.0)) &
                  G(ac,m,i,q,n) = G(ac,m,i,q,n) + dble(DD(ov,n,i,p)) * ED(ac,ov,ov,q,m,p) / dble(Ddiv(ov))
                do j = 1, 3
                  S(ac,m,i,j,n) = S(ac,m,i,j,n) + dble(DD(ov,m,i,p)) * TT(ac,ov,ov,p,q) * dble(DD(ov,n,j,q))/dble(Ddiv(ov)*Ddiv(ov))
                  do k = 1, 3
                    E(ac,m,i,j,k,  n) = E(ac,m,i,j,k,  n) + dble(EE(ov,n,i,j,p)) * TT(ac,ov,ov,p,q) * dble(DD(ov,m,k,  q)) &
                                     / dble( Ediv(ov)*Ddiv(ov) )
                    do l = 1, 3
                      H(ac,m,i,j,k,l,n) = H(ac,m,i,j,k,l,n) + dble(EE(ov,m,i,j,p)) * TT(ac,ov,ov,p,q) * dble(EE(ov,n,k,l,q)) &
                                     / dble( Ediv(ov)*Ediv(ov) )
                    end do
                  end do
                end do
              end do
            end do
          end do
        end do
      end do

      end do

      ! Deallocate the Construction Arrays
      deallocate(DD,TT,EE,ED)

      return
      end

