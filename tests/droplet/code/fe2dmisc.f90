

      subroutine timey(id,be)

      character id*3, be*3, date*8, zeit*10, zone*5, what*30
      parameter (what='ALLFEMBEMFACSOLSPE            ')
      integer values(10,8,2), vulues(8), i !, count, count_max, count_rate

      save values

      call DATE_AND_TIME(date, zeit, zone, vulues)

      values(1+(index(what,id)-1)/3,:,1+(index('BEGEND',be)-1)/3) = vulues

      !call SYSTEM_CLOCK(count, count_rate, count_max)

      if((id.eq.'ALL').and.(be.eq.'END')) &
        write(*,'(10(a4,e10.3))') (' '//what(3*(i-1)+1:3*i),  &
        !                  days            hours                 minutes                    seconds
        modulo (  real( 24*values(i,3,2) + values(i,5,2)) + real(values(i,6,2))/60.0 + real(values(i,7,2))/3600.0 + 0.1e-3 &
                - real( 24*values(i,3,1) + values(i,5,1)) - real(values(i,6,1))/60.0 - real(values(i,7,1))/3600.0 , 744.0 ),i=1,6)
      return
      end



      subroutine Simplex_Worker(ord,nod,z,dir,gam,one,oneh,two,twoh,vertex)
      
      implicit none
      integer ord, nod, dir(2), gam(2), z(-3:3)
      logical one, oneh, two, twoh, vertex
                                    
      z(0)=-1
      call Simplex(ord,nod,z(1),z(2),z(3),.False.)             
      z(-1)=ord-z(1)
      z(-2)=ord-z(2)
      z(-3)=ord-z(3)                          
      
      if((z(dir(1)).eq.0) .or.(z(dir(2)).eq.0)) then
        one=.True.
      else
        one=.False.
      endif                          
      
      if((z(dir(1)).eq.1).or.(z(dir(2)).eq.1)) then
        oneh=.True.
      else
        oneh=.False.
      endif               
      
      if((z(gam(1)).eq.0).or.(z(gam(2)).eq.0)) then
        two=.True.
      else
        two=.False.
      endif              
      
      if((z(gam(1)).eq.1).or.(z(gam(2)).eq.1)) then
        twoh=.True.
      else
        twoh=.False.
      endif               
      
      if((z(1).eq.ord).or.(z(2).eq.ord).or.(z(3).eq.ord)) then
        vertex=.True.
      else
        vertex=.False.
      endif

      return
      end

      subroutine Node_Details(mxel,Mesh,ele,nod,xnew,ynew,ch,lab,val,new)

      use CONTROL, only : ord, disgal, dg, tol, axisym, drag, bemo, rifsym

      implicit none
      integer i, mxel, ele, nod, m, dir(2), gam(2), z(-3:3)
      real Mesh(11,mxel), x1, y1, x2, y2, x3, y3, xnew, ynew, vxnew, vynew, pxnew, pynew, xmax, ymax, xmx, ymx
      double precision val(3)
      real U_ana, V_ana, P_ana
      character*1 ch, lab, tc, td
      logical one, oneh, two, twoh, vertex, mat(99), new, axisim, keepchecking

      ! For axisym keep track of to be Dirichlet Velocity Nodes with biggest X and Y coordinates
      data xmax /0.0/, ymax /0.0/, xmx /-100.0/, ymx /100.0/
      save xmax, ymax, xmx, ymx

      val = 0.0D0

      x1=     Mesh( 1,ele)
      y1=     Mesh( 2,ele)
      x2=     Mesh( 3,ele)
      y2=     Mesh( 4,ele)
      x3=     Mesh( 5,ele)
      y3=     Mesh( 6,ele)
      dir(1)= int(Mesh( 7,ele))
      call decoder(dir,.true.)
      gam(1)= int(Mesh( 8,ele))
      call decoder(gam,.true.)
      m=  int(Mesh(10,ele))        
      do 30 i=1, 99     
        if(i.eq.m) then
          mat(i)=.True.
        else
          mat(i)=.False.
        endif
   30 continue
      call Simplex_Worker(ord,nod,z,dir,gam,one,oneh,two,twoh,vertex)

      xnew = ( x1*real(z(1)) +x2*real(z(2)) +x3*real(z(3)) ) / real(ord)
      ynew = ( y1*real(z(1)) +y2*real(z(2)) +y3*real(z(3)) ) / real(ord)

      if(disgal) then
        vxnew = ( x1*real(1-z(1)) +x2*real(1-z(2)) +x3*real(1-z(3)) ) / real(ord+1)
        vynew = ( y1*real(1-z(1)) +y2*real(1-z(2)) +y3*real(1-z(3)) ) / real(ord+1)
        pxnew = ( x1 + x2 + x3 ) / 3.0
        pynew = ( y1 + y2 + y3 ) / 3.0
      else
        vxnew = xnew
        vynew = ynew
        pxnew = xnew
        pynew = ynew
      endif

      if((ch.eq.'D').and.one.and.mat(99)) then
        lab='S'
      else if(two.and.mat(99)) then
        if(vertex) then       
          lab='H'
        else
          lab='I'
        endif      
      elseif(twoh.and.(mat(99))) then
        if(vertex) then
          lab='T'
        else
          lab='W'     
        endif
      elseif(mat(99)) then
        lab='S'     
      else if(one.and.mat(2)) then
        if(vertex) then
          lab='A'
        else
          lab='B'
        endif
      elseif(one) then
        if(vertex) then
          lab='DP'(dg:dg)
        else
          lab='EQ'(dg:dg)
        endif
      elseif(oneh) then
        if(vertex) then
          lab='FV'(dg:dg)
        else
          lab='FU'(dg:dg)
        endif
      elseif(twoh.and.(ch.eq.'D')) then
        if(vertex) then
          lab='FV'(dg:dg)
        else
          lab='FU'(dg:dg)
        endif
      elseif(two.and.(ch.eq.'D')) then
        if(vertex) then
          lab='DJ'(dg:dg)
        else
          lab='EQ'(dg:dg)
        endif
      elseif(two.and.(ch.eq.'N')) then
        lab='V'
      else if(two.and.twoh.and.((ch.eq.'I').or.(ch.eq.'G'))) then
        if(vertex) then
          lab='H'
        else
          lab='I'
        endif
      elseif(two.and.((ch.eq.'I').or.(ch.eq.'G'))) then 
        if(vertex) then
          lab='HJ'(dg:dg)
        else
          lab='IK'(dg:dg)
        endif
      elseif(twoh.and.(index('IPGN',ch).ne.0)) then
        if(vertex) then
          lab='JH'(dg:dg)
        else
          lab='JI'(dg:dg)
        endif
      else
        lab='F'
      endif

      new = .false.; axisim = (axisym.or.rifsym).and.(ch.ne.'D')

      keepchecking = .true.

      ! Origin labelling the same for both conforming and non-conforming cases (but zap velocities only for non disgal ...)
      if(axisim) then
        tc = 'HJ'(dg:dg); td = 'DP'(dg:dg)
        if( ((lab.eq.tc) .or. (lab.eq.td)) .and. (.not.drag) ) then
          if( (abs(xnew).lt.tol) .and. (abs(ynew).lt.tol) ) then
            lab = 'O'
            if(.not.disgal) then
              val(1) = 0.0d0
              val(2) = 0.0d0
            end if
            keepchecking = .false.

          ! Y node (top of drop) labelling
          else if( (abs(xnew).lt.tol) .and. (abs(ynew).gt.ymax) ) then
            lab = 'Y'
            ymax = abs(ynew)
            new = .true.
            keepchecking = .false.

          ! X node (bottom right of drop) labelling
          else if( (abs(ynew).lt.tol) .and. (abs(xnew).gt.xmax) ) then
            lab = 'X'
            xmax = abs(xnew)
            new = .true.
            keepchecking = .false.

          end if
        end if

        ! For AxiSym Integral Boundary Node Label Replaced with Zero Dirichlet Radial Velocity Label on Y-Axis
        if( keepchecking .and. ((index('HDVP',lab).ne.0).or.(drag.and.(index('FJ',lab).ne.0))) ) then
          if(abs(vxnew).lt.tol) then
            lab = 'R'
            val(1) = 0.0d0
            keepchecking = .false.
            
            ! Y node (top of drop) labelling
            if( bemo .and. (ynew.gt.tol) .and. (ynew.lt.ymx) ) then
              lab = 'Y'
              ymx = ynew
              new = .true.

            ! X node (bottom of drop) labelling (for bemo only)
            else if( bemo .and. (ynew.lt.-tol) .and. (ynew.gt.xmx) ) then
              lab = 'X'
              xmx = ynew
              new = .true.
            end if

          ! ... Replaced with Zero Dirichlet Vertical Velocity Label on X-Axis
          else if((abs(vynew).lt.tol).and.(.not.drag)) then
            lab = 'Z'
            val(2) = 0.0d0
            keepchecking = .false.

          end if
        end if

        ! For AxiSym Integral Boundary Node Label Replaced with Zero Dirichlet Radial Velocity Label on Y-Axis
        if( keepchecking .and. (index('HDP',lab).ne.0) .and.(.not.drag)) then
          if(abs(xnew).lt.tol) then
            lab = 'J'
            val(1) = 0.0d0
            keepchecking = .false.

          ! ... Replaced with Zero Dirichlet Vertical Velocity Label on X-Axis
          else if(abs(ynew).lt.tol) then
            lab = 'J'
            val(2) = 0.0d0
            keepchecking = .false.

          end if
        end if
      end if
      
      ! Standard Dirichlet Node Label (Vertex / Non-vertex)
      if(keepchecking.and.(index('DE',lab).ne.0)) then
        val(1) = U_ana(vxnew,vynew)
        val(2) = V_ana(vxnew,vynew)
        val(3) = P_ana(pxnew,pynew)
      ! Dis-Continuous Galerkin Dirichlet Velocity Node
      else if(keepchecking.and.(index('VU',lab).ne.0)) then
        val(1) = U_ana(vxnew,vynew)
        val(2) = V_ana(vxnew,vynew)
      ! Dis-Continuous Galerkin Dirichlet Pressure Node
      else if(keepchecking.and.(index('PQ',lab).ne.0)) then
        val(3) = P_ana(pxnew,pynew)
      endif

      return
      end                                       
                            

      subroutine Embed_Element(X,Y,Type,Connect,R,P,Q,L,SS,ele,area,volu,kine)

      use CONTROL, only : pcor, lim, disgal, dgr, rho, step, inviscid, axisym, nu, legendre, zero, heat, peclet, froude
      use CONTROL, only : walltemp, boy, masscheck, drag, penfun, vortcheck, resist
      use PLACE, only : rr, rc, cc, pp
      use SIZES, only : maxnod, maxele
      use RESISTANCE, only : dragStore

      implicit none
      integer i, j, k, ii, jj, kk, ele, Connect(maxele,10)
      real X(maxnod), Y(maxnod)
      double precision S(10,10), T(10,10), W(10,10), rhot, pcur
      double precision Sxx(10,10), Sxy(10,10), Syx(10,10), Syy(10,10), Rx(10,10), Ry(10,10), RPx(10,10), RPy(10,10)
      double precision RQx(10,10), RQy(10,10)
      double precision SS(3*maxnod*3*maxnod), L(3*maxnod), R(3,maxele), P(3*maxnod), Q(2*maxnod), H(10,3,10)
      double precision area, volu, kine, Ex(10,3,10), Ey(10,3,10), TV(10,10), TP(10,10), RRx(10,3,10), RRy(10,3,10)
      character Type(maxnod)*1

      call EleComp(X,Y,Connect,Rx,Ry,RPx,RPy,RQx,RQy,S,Sxx,Sxy,Syx,Syy,T,TV,TP,RRx,RRy,W,Ex,Ey,H,ele)

      !call Element_Check(X(Connect(ele,1)),Y(Connect(ele,1)), &
      !                   X(Connect(ele,2)),Y(Connect(ele,2)), &
      !                   X(Connect(ele,3)),Y(Connect(ele,3)), &
      !                   Rx,Ry,T,TP,image,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0)

      ! Compute the per-element Velocity and Pressure (for pressure dependent Viscosity calculation)
      area = 0.0d0; volu = 0.0d0; kine = 0.0d0

      do i = 1, lim
        do j = 1, lim
          area = area + TP(i,j)
          volu = volu +  T(i,j)
          kine = kine + P(rr(1,Connect(ele,i))) * T(i,j) * P(rr(1,Connect(ele,j)))
          kine = kine + P(rr(2,Connect(ele,i))) * T(i,j) * P(rr(2,Connect(ele,j)))
        end do
      end do

      if(disgal) then
        pcur = - dble(pcor)
      else if(axisym) then
        pcur = dble(pcor) * area
      else
        pcur = dble(pcor) * area
      endif

      if(rho.gt.0.0d0) then
        ! Time Derivative Term Factor
        if(drag) then
          rhot = 0.0d0
        else
          rhot = rho
        end if
      else
        rho = 0.0d0; rhot = 0.0d0; step = 1.0d0
      end if

      ! Loop over Observation Nodes
      do 20 i = 1, lim

        ii = Connect(ele,i)

        if(masscheck.or.vortcheck) then
          if(i.eq.1) P(rr(3,ii)) = 0.0d0
        end if

        ! Loop over Integration Nodes
        do 10 j = 1, lim

          jj = Connect(ele,j)

          ! Zero Axi-Symmetric Radial Velocity Dirichlet Boundary Condition
          if(drag.and.(index('HI',Type(ii)).ne.0)) then

            SS( rc(1,ii,1,ii) ) = 1.0d0
            L( rr(1,ii) ) = 0.0d0

          else if((index('DEVUR',Type(ii)).ne.0).or.((.not.disgal).and.(index('OY',Type(ii)).ne.0))) then

            SS( rc(1,ii,1,ii) ) = 1.0d0
            L( rr(1,ii) ) = P( rr(1,ii) )

          ! Zero Axi-Symmetric Radial Velocity Normal Derivative Neumann Boundary Condition
          else if(Type(ii).eq.'#') then

            SS( rc(1,ii,1,jj) ) = RRy(i,1,j)
            L( rr(1,ii) ) = 0.0d0

          else

            ! Element Edge Independent Contributions to System Matrix
            SS(rc(1,ii,3,jj)) = SS(rc(1,ii,3,jj)) - step * Rx(j,i)

            if(.not.inviscid) then
            SS(rc(1,ii,1,jj)) = SS(rc(1,ii,1,jj)) + step * nu * ( S(i,j) + Sxx(i,j) )
            SS(rc(1,ii,2,jj)) = SS(rc(1,ii,2,jj)) + step * nu *            Syx(i,j)
            end if

            ! Loop over Edges of Element being Embedded
            do k = 1, 3

              kk = Connect(ele,k)

              ! Solution sensitive penalty
              pcur = -penfun(abs(P(rr(1,kk))))

              if(((.not.disgal).or.(pp(k,jj).ne.cc(3,jj))).and.(.not.drag)) then
                ! Linearized Advective Terms (soln vel - mesh vel) only for interior edges
                SS(rc(1,ii,1,jj)) = SS(rc(1,ii,1,jj)) + step * rho * ( P(rr(1,kk)) - Q(rr(1,kk)) ) * RPx(i,j)
                SS(rc(1,ii,1,jj)) = SS(rc(1,ii,1,jj)) + step * rho * ( P(rr(2,kk)) - Q(rr(2,kk)) ) * RPy(i,j)
              end if

              if((.not.disgal).or.((pp(k,jj).ne.cc(3,jj)).and.(Type(ii).ne.'Z'))) then
                ! Interior Edge Contributions
                SS(rc(1,ii, 1,jj)) = SS(rc(1,ii, 1,jj)) - step * pcur * H(i,k,j)
                SS(rc(1,ii, 3,jj)) = SS(rc(1,ii, 3,jj)) + step * pcur * Ex(j,k,i) / ( nu ** ( 1.0 - dgr ) )
              else
                ! Exterior Edge Contributions
                !SS(rc(1,ii, 1,jj)) = SS(rc(1,ii, 1,jj)) - step * pcur * H(i,kk,j)
                !SS(rc(1,ii, 3,jj)) = SS(rc(1,ii, 3,jj)) + step * pcur * Ex(j,kk,i)
              endif

            end do

            ! Time Derivative Term
            SS(rc(1,ii,1,jj)) = SS(rc(1,ii,1,jj)) + rhot * T(i,j)

            ! Velocity Load Vector Blocks
            L( rr(1,ii) ) = L( rr(1,ii) ) + T(i,j) * ( step * R(1,ele) + rhot * P(rr(1,jj)) )

            ! Extra terms for the velocity equations in the axi-symmetric case
            if(axisym) then
              SS(rc(1,ii,1,jj)) = SS(rc(1,ii,1,jj)) + step * 2.0d0 * nu * TV(i,j)
              SS(rc(1,ii,3,jj)) = SS(rc(1,ii,3,jj)) - step * TP(j,i)
            end if

          endif

          ! Zero Axi-Symmetric Vertical Velocity Dirichlet Boundary Condition
          if(drag.and.(index('HI',Type(ii)).ne.0)) then

            SS( rc(2,ii,2,ii) ) = 1.0d0
            L(  rr(2,ii) ) = 0.0d0

          else if((index('DEVUZ',Type(ii)).ne.0).or.((.not.disgal).and.(index('OX',Type(ii)).ne.0))) then

            SS( rc(2,ii,2,ii) ) = 1.0d0
            L(  rr(2,ii) ) = P( rr(2,ii) )

          ! Zero Axi-Symmetric Vertical Velocity Normal derivative Neumann Boundary Condition
          else if(Type(ii).eq.'#') then

            SS( rc(2,ii,2,jj) ) = RRx(i,1,j)
            L( rr(2,ii) ) = 0.0d0

          else

            ! Element Edge Independent Contributions to System Matrix
            SS(rc(2,ii,3,jj)) = SS(rc(2,ii,3,jj)) - step * Ry(j,i)

            if(.not.inviscid) then
            SS(rc(2,ii,1,jj)) = SS(rc(2,ii,1,jj)) + step * nu *            Sxy(i,j)
            SS(rc(2,ii,2,jj)) = SS(rc(2,ii,2,jj)) + step * nu * ( S(i,j) + Syy(i,j) )
            end if

            ! Loop over Edges of Element being Embedded
            do k = 1, 3

              kk = Connect(ele,k)

              ! Solution sensitive penalty
              pcur = -penfun(abs(P(rr(2,kk))))

              if(((.not.disgal).or.(pp(k,jj).ne.cc(3,jj))).and.(.not.drag)) then
                ! Linearized Advective Terms (soln vel - mesh vel) only for interior edges
                SS(rc(2,ii,2,jj)) = SS(rc(2,ii,2,jj)) + step * rho * ( P(rr(1,kk)) - Q(rr(1,kk)) ) * RPx(i,j)
                SS(rc(2,ii,2,jj)) = SS(rc(2,ii,2,jj)) + step * rho * ( P(rr(2,kk)) - Q(rr(2,kk)) ) * RPy(i,j)
              end if

              if((.not.disgal).or.((pp(k,jj).ne.cc(3,jj)).and.(Type(ii).ne.'R'))) then
                ! Interior Edge Contributions
                SS(rc(2,ii, 2,jj)) = SS(rc(2,ii, 2,jj)) - step * pcur * H(i,k,j)
                SS(rc(2,ii, 3,jj)) = SS(rc(2,ii, 3,jj)) + step * pcur * Ey(j,k,i) / ( nu ** ( 1.0 - dgr ) )
              else
                ! Exterior Edge Contributions
                !SS(rc(2,ii, 2,jj)) = SS(rc(2,ii, 2,jj)) - step * pcur * H(i,kk,j)
                !SS(rc(2,ii, 3,jj)) = SS(rc(2,ii, 3,jj)) + step * pcur * Ey(j,kk,i)
              endif

            end do

            ! Time Derivative Term
            SS(rc(2,ii,2,jj)) = SS(rc(2,ii,2,jj)) + rhot * T(i,j)

            ! Velocity Load Vector Blocks
            L( rr(2,ii) ) = L( rr(2,ii) ) + T(i,j) * ( step * R(2,ele) + rhot * P(rr(2,jj)) )
            !if(heat) L( rr(2,ii) ) = L( rr(2,ii) ) - T(i,j) * step * boy(P(rr(4,jj))) / froude

          endif

          ! Record data for calculating the drag later
          if((drag.or.resist).and.(index('HI',Type(ii)).ne.0)) then
            k  = modulo(i+1-1,lim) + 1
            k  = Connect(ele,k)
            kk = modulo(i+2-1,lim) + 1
            kk = Connect(ele,kk)
            call dragStore(X(ii),Y(ii),X(k),Y(k),X(kk),Y(kk),ii,jj,Rx(1,j)/volu,Ry(1,j)/volu)
          end if

          ! Pressure Dirichlet Boundary Conditions
          if(index('DEQ',Type(ii)).ne.0) then
            SS( rc(3,ii,3,ii) ) = 1.0d0
            L(  rr(3,ii) ) = P( rr(3,ii) )
          else

            SS(rc(3,ii,1,jj)) = SS(rc(3,ii,1,jj)) - ( Rx(i,j) - pcur * Ex(i,1,j) * dgr )
            SS(rc(3,ii,2,jj)) = SS(rc(3,ii,2,jj)) - ( Ry(i,j) - pcur * Ey(i,1,j) * dgr )
            if(masscheck) then
              P(rr(3,ii)) = P(rr(3,ii)) + Rx(i,j) * P(rr(1,jj)) + Ry(i,j) * P(rr(2,jj))
            else if(vortcheck) then
              P(rr(3,ii)) = P(rr(3,ii)) + Ry(i,j) * P(rr(1,jj)) - Rx(i,j) * P(rr(2,jj))
            end if
            if(axisym) then
              SS(rc(3,ii,3,jj)) = SS(rc(3,ii,3,jj))           - pcur *  S(i,  j) * dgr
            else
              SS(rc(3,ii,3,jj)) = SS(rc(3,ii,3,jj)) +  W(i,j) - pcur *  S(i,  j) * dgr
            end if
            !L( rr(3,ii) ) = L( rr(3,ii) ) + T(i,j) * R(3,ele) &
            !                                 - pcur * ( Rx(j,i) * R(1,ele) + Ry(j,i) * R(2,ele) ) * dgr

!$$$$$$             ! Penalize Tangential Traction Boundary Condition
!$$$$$$             if(index('RZH',Type(ii)).ne.0) then
!$$$$$$               ! Loop over Edges of Element being Embedded
!$$$$$$               do k = 1, 3
!$$$$$$ 
!$$$$$$                 kk = Connect(ele,k)
!$$$$$$ 
!$$$$$$                 ! Only add the contribution for the edge with nothing on the other side
!$$$$$$                 if(pp(k,jj).eq.cc(3,jj)) then
!$$$$$$                   ! Solution sensitive penalty
!$$$$$$                   pcur = -penfun(abs(P(rr(1,kk))))
!$$$$$$                   SS( rc(3,ii,1,jj) ) = SS( rc(3,ii,1,jj) ) + step * pcur * RRx(i,k,j)
!$$$$$$                   ! Solution sensitive penalty
!$$$$$$                   pcur = -penfun(abs(P(rr(2,kk))))
!$$$$$$                   SS( rc(3,ii,2,jj) ) = SS( rc(3,ii,2,jj) ) + step * pcur * RRy(i,k,j)
!$$$$$$                 end if
!$$$$$$               end do
!$$$$$$             end if

            ! Extra "u/r" source term for pressure RHS in the axi-symmetric case
            if(axisym) then
              !L( rr(3,ii) ) = L( rr(3,ii) ) + TP(i,j) * P(rr(1,jj)) * jns
              SS(rc(3,ii,1,jj)) = SS(rc(3,ii,1,jj)) - TP(i,j)
              if(masscheck) then
                P(rr(3,ii)) = P(rr(3,ii)) + TP(i,j) * P(rr(1,jj))
              end if
            end if
          endif

          ! Don't set-up temperature rows if not required
          if(.not.heat) cycle

          ! Temperature Dirichlet Boundary Condition on substrate (reflection symmetry axis)
          if((index('DEVUZ',Type(ii)).ne.0).or.((.not.disgal).and.(index('OX',Type(ii)).ne.0))) then

            SS( rc(4,ii,4,ii) ) = 1.0d0
            L(  rr(4,ii) ) = walltemp(volu/area) !P( rr(4,ii) )

          else

            ! Diffusion Laplacian
            SS(rc(4,ii,4,jj)) = SS(rc(4,ii,4,jj)) + step * ( S(i,j) + Syy(i,j) )

            ! Loop over Edges of Element being Embedded
            do k = 1, 3

              if((.not.disgal).or.((pp(k,jj).ne.cc(3,jj)))) then
                ! Linearized Advective Terms (soln vel - mesh vel) only for interior edges
                kk = Connect(ele,k)
                SS(rc(4,ii,4,jj)) = SS(rc(4,ii,4,jj)) + step * ( P(rr(1,kk)) - Q(rr(1,kk)) ) * RPx(i,j)
                SS(rc(4,ii,4,jj)) = SS(rc(4,ii,4,jj)) + step * ( P(rr(2,kk)) - Q(rr(2,kk)) ) * RPy(i,j)
              end if

              ! Penalize jumps in temperature between neighbouring finite elements
              if((.not.disgal).or.((pp(k,jj).ne.cc(3,jj)).and.(Type(ii).ne.'Z'))) then
                ! Interior Edge Contributions
                SS(rc(4,ii, 4,jj)) = SS(rc(4,ii, 4,jj)) - step * pcur * H(i,k,j)
                if(Type(ii).ne.'R') then
                  SS(rc(4,ii, 3,jj)) = SS(rc(4,ii, 3,jj)) + step * pcur * Ex(j,k,i)
                  SS(rc(4,ii, 3,jj)) = SS(rc(4,ii, 3,jj)) + step * pcur * Ey(j,k,i)
                endif
              endif

            end do

            ! Time Derivative Term
            SS(rc(4,ii,4,jj)) = SS(rc(4,ii,4,jj)) + peclet * T(i,j)

            ! Velocity Load Vector Blocks
            L( rr(4,ii) ) = L( rr(4,ii) ) + peclet * T(i,j) * P(rr(4,jj))

          endif

   10   continue
   20 continue

      return
      end



      subroutine Mesh_Generator(Connect,X,Y,Mesh,L,R,Type,choice)

      use PLACE, only : init, seat, oor, wipe, record, nset, fixseat
      use CONTROL, only : lim, disgal, dg, inviscid, tol, drag, bemo, bemf
      use SIZES, only : miniset, maxnod, maxsid, maxele

      implicit none
      
      integer   i, j, k, Connect(maxele,10), maxX, maxY, a(3), maxned, spfix(100,3), spfxcnt
      real      X(-maxsid+1:maxnod), Y(-maxsid+1:maxnod), Mesh(11,maxele), siz, xnew, ynew
      double precision   L(3*maxnod),R(3,maxele),val(3)
      character Type(-maxsid+1:maxnod)*1, choice*1, lab*1         
      logical new, OneAH, TwoAH, ThrAH, OneNoJ, TwoNoJ, ThrNoJ, OneDiagOverAxes, TwoDiagOverAxes, ThrDiagOverAxes
      
      maxX = 0; maxY = 0; spfxcnt = 0; spfix = 0

      if(disgal) call init()

      maxned=0; maxsid=0
      do 30 i=1, maxele
        call Element_Details(maxele,i,Mesh,siz,val)
        R(1,i)=val(1)
        R(2,i)=val(2)
        R(3,i)=val(3)
        do 20 j=1, lim
          call Node_Details(maxele,Mesh,i,(j),xnew,ynew,choice,lab,val,new)
          do 10 k=-maxsid+1, maxned
            if((X(k)-xnew)**2+(Y(k)-ynew)**2.lt.tol**2) then

              ! Pre-existing node case - if dis-continuous Galerkin, just record the match
              if(disgal) then
                call nset(maxned+1,k)
                exit

              ! Specials don't get relabelled, as will need a positive node number for any new (non-special) label ...
              else if((Type(k).eq.'S').and.(lab.ne.'S')) then
                ! ... and existing negative node number will have to be replaced at the end
                spfxcnt = spfxcnt + 1
                spfix(spfxcnt,1) = k; spfix(spfxcnt,2) = maxned+1
                exit
              else

                ! Relabel only weakly labeled nodes
                if((index('UFSG',Type(k)).ne.0).and.(lab.ne.'S')) then
                  Type(k)=lab
                  ! Keeping track of the 'X' and 'Y' nodes for the axi-symmetric experiments
                  if(new) then
                    if(lab.eq.'X') then
                      if(maxX.ne.0) Type(maxX) = 'RZJ'(bemf+dg:bemf+dg)
                      maxX = maxned
                    else if(lab.eq.'Y') then
                      if(maxY.ne.0) Type(maxY) = 'RJ'(dg:dg)
                      maxY = maxned
                    end if
                  end if
                  if(.not.bemo) then
                  L( oor(1,k) )=val(1)
                  L( oor(2,k) )=val(2)
                  L( oor(3,k) )=val(3)
                  end if
                endif
                Connect(i,j)=k
                goto 20
              endif
            endif
   10     continue
          if((lab.eq.'S').or.(inviscid.and.(index('TWSG',lab).ne.0))) then
            ! Check for released negative node numbers first
            do k = 1, spfxcnt
              if(spfix(k,3).eq.0) then
                X(spfix(k,1))=xnew
                Y(spfix(k,1))=ynew
                Type(spfix(k,1))='S'
                Connect(i,j)=spfix(k,1)
                ! Now flag this (negative) node number as having been reused
                spfix(k,3) = 1
                cycle
              end if
            end do
            maxsid=maxsid+1
            X(-maxsid+1)=xnew
            Y(-maxsid+1)=ynew
            Type(-maxsid+1)='S'
            Connect(i,j)=-maxsid+1
          else
            maxned=maxned+1
            X(maxned)=xnew
            Y(maxned)=ynew
            Type(maxned)=lab
            ! Keeping track of the 'X' and 'Y' nodes for the axi-symmetric experiments
            if(new) then
              if(lab.eq.'X') then
                if(maxX.ne.0) Type(maxX) = 'RZJ'(bemf+dg:bemf+dg)
                maxX = maxned
              else if(lab.eq.'Y') then
                if(maxY.ne.0) Type(maxY) = 'RJ'(dg:dg)
                maxY = maxned
              end if
            end if
            Connect(i,j)=maxned
            if(disgal) then
              call record(i,xnew,ynew,maxned,siz)
              call seat(j,maxned,i,lab)
            end if
            ! Don't let (non-conforming) pressure Dirichlet conditions interfere with any Dirichlet velocities
            if((index('P',lab).eq.0).and.(.not.bemo)) then
              L( oor(1,maxned) ) = val(1)
              L( oor(2,maxned) ) = val(2)
            end if
            ! Don't let (non-conforming) velocity Dirichlet conditions interfere with any Dirichlet pressures
            if((index('VRZ',lab).eq.0).and.(.not.bemo)) then
              L( oor(3,maxned) ) = val(3)
            end if
          endif
   20   continue

        ! Wipe any falsely designated Velocity Dirichlet Nodes
        if(disgal) then
          if((index('VU',Type(Connect(i,1))).ne.0)  &
                .and.((index('PXYZJ',Type(Connect(i,2))).eq.0).or.(index('PXYZJ',Type(Connect(i,3))).eq.0))) then
            Type(    Connect(i,1)  ) = 'F'
            L( oor(1,Connect(i,1)) ) = 0.0D0
            L( oor(2,Connect(i,1)) ) = 0.0D0
            call fixseat(1)
          endif
          if((index('VU',Type(Connect(i,2))).ne.0)  &
                .and.((index('PXYZJ',Type(Connect(i,1))).eq.0).or.(index('PXYZJ',Type(Connect(i,3))).eq.0))) then
            Type(    Connect(i,2)  ) = 'F'
            L( oor(1,Connect(i,2)) ) = 0.0D0
            L( oor(2,Connect(i,2)) ) = 0.0D0
            call fixseat(2)
          endif
          if((index('VU',Type(Connect(i,3))).ne.0)  &
                .and.((index('PXYZJ',Type(Connect(i,1))).eq.0).or.(index('PXYZJ',Type(Connect(i,2))).eq.0))) then
            Type(    Connect(i,3)  ) = 'F'
            L( oor(1,Connect(i,3)) ) = 0.0D0
            L( oor(2,Connect(i,3)) ) = 0.0D0
            call fixseat(3)
          endif
        endif

        a(1) = Connect(i,1); a(2) = Connect(i,2); a(3) = Connect(i,3)
      
        if(drag) then
          ! Wipe any falsely designated 'J' nodes (from conversions of 'H' to 'R' or 'Z' typically)
          OneAH = index('RZ',Type(a(1))).ne.0 ;   OneNoJ = index('JT',Type(a(1))).ne.0
          TwoAH = index('RZ',Type(a(2))).ne.0 ;   TwoNoJ = index('JT',Type(a(2))).ne.0
          ThrAH = index('RZ',Type(a(3))).ne.0 ;   ThrNoJ = index('JT',Type(a(3))).ne.0

          if(OneAH) then
            if(TwoNoJ) Type( Connect(i,2) ) = 'F'
            if(ThrNoJ) Type( Connect(i,3) ) = 'F'
          endif

          if(TwoAH) then
            if(OneNoJ) Type( Connect(i,1) ) = 'F'
            if(ThrNoJ) Type( Connect(i,3) ) = 'F'
          endif

          if(ThrAH) then
            if(OneNoJ) Type( Connect(i,1) ) = 'F'
            if(TwoNoJ) Type( Connect(i,2) ) = 'F'
          endif

          ! Back to the (drag version) definitions for wiping falsely designated Integral Nodes
          OneDiagOverAxes = .false.; TwoDiagOverAxes = .false.; ThrDiagOverAxes = .false.

          OneAH = index('HI',Type(a(1))).ne.0 ;   OneNoJ = index('TJXYR',Type(a(1))).eq.0
          TwoAH = index('HI',Type(a(2))).ne.0 ;   TwoNoJ = index('TJXYR',Type(a(2))).eq.0
          ThrAH = index('HI',Type(a(3))).ne.0 ;   ThrNoJ = index('TJXYR',Type(a(3))).eq.0
        else
          OneDiagOverAxes = ( X(a(2)) + Y(a(3)) .lt. tol ) .or. ( X(a(3)) + Y(a(2)) .lt. tol )
          TwoDiagOverAxes = ( X(a(1)) + Y(a(3)) .lt. tol ) .or. ( X(a(3)) + Y(a(1)) .lt. tol )
          ThrDiagOverAxes = ( X(a(1)) + Y(a(2)) .lt. tol ) .or. ( X(a(2)) + Y(a(1)) .lt. tol )

          OneAH = index('HI',Type(a(1))).ne.0 ;   OneNoJ = index('JXY',Type(a(1))).eq.0
          TwoAH = index('HI',Type(a(2))).ne.0 ;   TwoNoJ = index('JXY',Type(a(2))).eq.0
          ThrAH = index('HI',Type(a(3))).ne.0 ;   ThrNoJ = index('JXY',Type(a(3))).eq.0
        end if

        ! Wipe any falsely designated Integral Nodes
        if(disgal) then
          if(OneAH.and.(TwoNoJ.or.ThrNoJ.or.OneDiagOverAxes)) then
            Type(    Connect(i,1)  ) = 'F'
            call fixseat(1)
          endif
          if(TwoAH.and.(OneNoJ.or.ThrNoJ.or.TwoDiagOverAxes)) then
            Type(    Connect(i,2)  ) = 'F'
            call fixseat(2)
          endif
          if(ThrAH.and.(OneNoJ.or.TwoNoJ.or.ThrDiagOverAxes)) then
            Type(    Connect(i,3)  ) = 'F'
            call fixseat(3)
          endif
        endif
   30 continue

      if(disgal) call record(0,0.0,0.0,0,siz)

      maxnod = maxned
      call miniset()

      if(disgal) call wipe()

      ! Put through any special renumbering that has been done
      do i = 1, spfxcnt
        do j = 1, maxele
          do k = 1, lim
            if(Connect(j,k).eq.spfix(i,1)) then
              Connect(j,k) = spfix(i,2)
            end if
          end do
        end do
      end do

      return
      end

                                                                                                                               
      subroutine Element_Details(maxele,element,Mesh,siz,rho)

      use CONTROL, only : ord, tol

      implicit none
      integer maxele, element, mat
      real Mesh(11,maxele), siz, x1, y1, x2, y2, x3, y3, x, y
      double precision rho(3)
      real Fx_ana, Fy_ana, Fp_ana
 
      x1=     Mesh( 1,element)
      y1=     Mesh( 2,element)
      x2=     Mesh( 3,element)
      y2=     Mesh( 4,element)
      x3=     Mesh( 5,element)
      y3=     Mesh( 6,element)
      mat=int(Mesh(10,element))
      x=(x1+x2+x3)/3.0
      y=(y1+y2+y3)/3.0

      if(mat.eq.1) then
        rho(1) = Fx_ana(x,y)
        rho(2) = Fy_ana(x,y)
        rho(3) = Fp_ana(x,y)
      else if(mat.eq.2) then
        rho(1) = Fx_ana(x,y)
        rho(2) = Fy_ana(x,y)
        rho(3) = Fp_ana(x,y)
      else
        rho(1)=1000.0
        rho(2)=1000.0
        rho(3)=1000.0
      endif   

      siz=tol/10.0 !sqrt(min((x3-x2)**2+(y3-y2)**2,(x2-x1)**2+(y2-y1)**2,(x3-x1)**2+(y3-y1)**2))/(200.1*real(ord))

      return
      end

      subroutine EleComp(X,Y,Connect,Rx,Ry,RPx,RPy,RQx,RQy,S,Sxx,Sxy,Syx,Syy,T,TV,TP,RRx,RRy,W,Ex,Ey,H,element)

      use CONTROL, only : odv, odp, mlim, nlim, disgal, axisym, pcored, tol, tabata
      use SIZES, only : maxnod, maxele

      implicit none
      integer r, i, j, k, l, m, n, p(-1:1), ac, dc, ca, base, typ, rot, ro(3), mm, nn
      integer Connect(maxele,10), element, iA(0:5), iB(0:5), iC(0:5), id
      real X(maxnod), Y(maxnod), leng(3), nx(3), ny(3), Xi, Xj, Xk, Yi, Yj, Yk, xxi, xxj, xxk, yyi, yyj, yyk, xxt, yyt
      real tx(3), ty(3)
      double precision Re(-60:9,10,3,10), Se(-60:9,10,3,3,10), Te(-60:9,10,10), He(-60:9,10,3,3,3,3,10), Ee(-60:9,10,3,3,3,10)
      double precision RPe(-60:9,10,3,10), RPx(10,10), RPy(10,10), AA(0:5), BB(0:5), CC(0:5), LL(0:5)
      double precision S(10,10), Sxx(10,10), Sxy(10,10), Syx(10,10), Syy(10,10), H(10,3,10), Ge(0:9,3,3,3,3), Fe(0:9,3,3,3)
      double precision Rx(10,10), Ry(10,10), Ex(10,3,10), Ey(10,3,10), area, ta, a(6), b(6), c(6), u(-1:1), f, d, ten
      double precision RQx(10,10), RQy(10,10)
      parameter(ten=10.0d0)
      double precision T(10,10), W(10,10), TV(10,10), TP(10,10), RRx(10,3,10), RRy(10,3,10), TPe(-6:0,10,10), cheese
      integer TVe(3,1295,7,3)
      logical interpol, done, ap,bp,bz,bn,cp,cz,cn
      data done /.false./

      save done, Re, RPe, Se, Te, TVe, TPe, He, Ee, Ge, Fe

      call Simplex(odv,i,odv,0,0,.True.)      
      call Simplex(odv,j,0,odv,0,.True.)     
      call Simplex(odv,k,0,0,odv,.True.)
 
      i = Connect(element,i)
      j = Connect(element,j)
      k = Connect(element,k)

      ! Make local copies of the element coordinates
      Xi = X(i); Xj = X(j); Xk = X(k); Yi = Y(i); Yj = Y(j); Yk = Y(k)

      ! Stretch elements across symmetry axis for "Tabata symmetric decomposition" for axisym case
      base = 0; xxi = Xi; xxj = Xj; xxk = Xk; yyi = Yi; yyj = Yj; yyk = Yk
!      if(axisym.and.tabata) call symstretch(Xi,Xj,Xk,Yi,Yj,Yk,image,base)
      
      ! Element Area
      area = abs( ( Xj-Xi ) * ( Yk-Yi ) - ( Xk-Xi ) * ( Yj-Yi ) ) / 2.0D0

      ! Element Side Pre - Normals
      nx(1) = Yk - Yj; ny(1) = Xj - Xk
      nx(2) = Yi - Yk; ny(2) = Xk - Xi
      nx(3) = Yj - Yi; ny(3) = Xi - Xj

      ! Element Side Lengths
      leng(1) = sqrt( nx(1) ** 2 + ny(1) ** 2 )
      leng(2) = sqrt( nx(2) ** 2 + ny(2) ** 2 )
      leng(3) = sqrt( nx(3) ** 2 + ny(3) ** 2 )

      ! Element Side Normals
      nx(1) = nx(1) / leng(1); ny(1) = ny(1) / leng(1)
      nx(2) = nx(2) / leng(2); ny(2) = ny(2) / leng(2)
      nx(3) = nx(3) / leng(3); ny(3) = ny(3) / leng(3)

      ! Element side tangents
      tx(1) = -ny(1); ty(1) = nx(1)
      tx(2) = -ny(2); ty(2) = nx(2)
      tx(3) = -ny(3); ty(3) = nx(3)

      if(   ((nx(1)+nx(2))*nx(3)+(ny(1)+ny(2))*ny(3).ge.0.0).or. &
            ((nx(2)+nx(3))*nx(1)+(ny(2)+ny(3))*ny(1).ge.0.0).or. &
            ((nx(3)+nx(1))*nx(2)+(ny(3)+ny(1))*ny(2).ge.0.0) ) then
        print*,'Element_Compute: Bad Normals.'
        call gmshOpen(51,'BadElement ')
        call gmshST(51,Xi,Yi,0.0d0,Xj,Yj,0.0d0,Xk,Yk,0.0d0,.true.)
        call gmshSLd(51,nx(1)+(Xj+Xk)/2.0,ny(1)+(Yj+Yk)/2.0,(Xj+Xk)/2.0,(Yj+Yk)/2.0,0.0d0)
        call gmshSLd(51,nx(2)+(Xk+Xi)/2.0,ny(2)+(Yk+Yi)/2.0,(Xk+Xi)/2.0,(Yk+Yi)/2.0,0.0d0)
        call gmshSLd(51,nx(3)+(Xi+Xj)/2.0,ny(3)+(Yi+Yj)/2.0,(Xi+Xj)/2.0,(Yi+Yj)/2.0,0.0d0)
        call gmshClose(51)
        stop
      endif

      ! Simplex Constants
      a(1) = dble(Xj * Yk - Xk * Yj);   b(1) = dble(Yj - Yk);   c(1) = dble(Xk - Xj)
      a(2) = dble(Xk * Yi - Xi * Yk);   b(2) = dble(Yk - Yi);   c(2) = dble(Xi - Xk)
      a(3) = dble(Xi * Yj - Xj * Yi);   b(3) = dble(Yi - Yj);   c(3) = dble(Xj - Xi)

      ! Initialize the assumptions indicator & triangle rotation tracker
      typ = 0; ro(1) = 1; ro(2) = 2; ro(3) = 3

      ! Simplex constant combinations and logs for reciprocal radius term
      AA(0) = 1.0d0; BB(0) = 1.0d0; CC(0) = 1.0d0
      iA(0) = 0;     iB(0) = 0;     iC(0) = 0

      ! Temporarily nudge x=0 coordinates off the axis
      if(xxi.lt.tol) xxi = tol; if(xxj.lt.tol) xxj = tol; if(xxk.lt.tol) xxk = tol

      ! If required (rot>0) rotate element numbers around to get an integral type established
      do rot = 0, 2

        ! Two times the element area
        ta = abs( ( xxj-xxi ) * ( yyk-yyi ) - ( xxk-xxi ) * ( yyj-yyi ) )

        ! Special Simplex Constants
        a(4) = dble(xxj * yyk - xxk * yyj);   b(4) = dble(yyj - yyk);   c(4) = dble(xxk - xxj)
        a(5) = dble(xxk * yyi - xxi * yyk);   b(5) = dble(yyk - yyi);   c(5) = dble(xxi - xxk)
        a(6) = dble(xxi * yyj - xxj * yyi);   b(6) = dble(yyi - yyj);   c(6) = dble(xxj - xxi)

        ! Calculate A, B and C simplex combinations
        AA(1)=a(5)*c(4)-a(4)*c(5); BB(1)=-a(5)*c(4)+a(4)*c(5)+a(6)*c(5)-a(5)*c(6); CC(1)=-a(5)*c(4)-a(6)*c(4)+a(4)*c(5)+a(4)*c(6)

        ! Check they are usable ...
        if( (AA(1).gt.0.0d0) .and. (AA(1)+CC(1).ge.0.0d0) ) then

          ! Set-up some logicals
          ap = AA(1) + BB(1) .gt. 0.0d0
          bp =         BB(1) .gt. 0.0d0; bz =         BB(1) .eq. 0.0d0; bn =         BB(1) .lt. 0.0d0
          cp =         CC(1) .gt. 0.0d0; cz =         CC(1) .eq. 0.0d0; cn =         CC(1) .lt. 0.0d0

          ! Determine which assumptions they require
          if(     bp.and.cp.and.ap) then
            typ = 1
          else if(bp.and.cz.and.ap) then
            typ = 2
          else if(bp.and.cn.and.ap) then
            typ = 3
          else if(bz.and.cp.and.ap) then
            typ = 4
          else if(bn.and.cp.and.ap) then
            typ = 5
          else if(bn.and.cz.and.ap) then
            typ = 6
          else if(bz.and.cn.and.ap) then
            typ = 7
          end if

          ! Exit the element rotating loop once an integral type has been found
          if(typ.ne.0) exit
        end if

        ! ... if not, rotate the element numbers around and try again ...
        xxt = xxi; yyt = yyi; xxi = xxj; yyi = yyj; xxj = xxk; yyj = yyk; xxk = xxt; yyk = yyt
        dc  = ro(1);        ro(1) = ro(2);        ro(2) = ro(3);        ro(3) = dc

      end do
        
      ! Report error if no assumptions type found
      if(typ.eq.0) then
        print*,"ERROR IN Element Compute: bad A, B, C !!!",xxi,yyi,xxj,yyj,xxk,yyk
        call gmshOpen(51,'BadElement ')
        call gmshST(51,xxi,yyi,0.0d0,xxj,yyj,0.0d0,xxk,yyk,0.0d0,.true.)
        call gmshClose(51)
        stop
      end if

!      open(34,file='/home/radcliffe/blob/coords.dat')
!      write(34,'(9f12.6)') 1.0,1.0,1.0,xxi,xxj,xxk,yyi,yyj,yyk
!      close(34)

      ! Calculate the various logs of A, B and C appearing in the integration formulae
      LL = 0.0d0
      LL(0) = 1.0d0
      if(AA(1).gt.0.0d0) then
        LL(1) = dlog(AA(1))       - 2.0d0 * dlog(ta)
      else
        LL(1) = -1000.0d0
      end if
      if(BB(1).gt.0.0d0) then
        LL(2) = dlog(BB(1))       - 2.0d0 * dlog(ta)
      else
        LL(2) = -1000.0d0
      end if
      if(CC(1).gt.0.0d0) then
        LL(3) = dlog(CC(1))       - 2.0d0 * dlog(ta)
      else
        LL(3) = -1000.0d0
      end if
      if(AA(1)+BB(1).gt.0.0d0) then
        LL(4) = dlog(AA(1)+BB(1)) - 2.0d0 * dlog(ta)
      else
        LL(4) = -1000.0d0
      end if
      if(AA(1)+CC(1).gt.0.0d0) then
        LL(5) = dlog(AA(1)+CC(1)) - 2.0d0 * dlog(ta)
      else
        LL(5) = -1000.0d0
      end if

      ! Determine the sizes of the A, B & C
      if(AA(1).ne.0.0d0) then
        iA(1) = nint(dlog10(abs(AA(1))))
      else
        iA(1) = 0
      end if
      if(BB(1).ne.0.0d0) then
        iB(1) = nint(dlog10(abs(BB(1))))
      else
        iB(1) = 0
      end if
      if(CC(1).ne.0.0d0) then
        iC(1) = nint(dlog10(abs(CC(1))))
      else
        iC(1) = 0
      end if

      ! Take these sizes out
      AA(1) = AA(1) * ten ** (-iA(1)); BB(1) = BB(1) * ten ** (-iB(1)); CC(1) = CC(1) * ten ** (-iC(1))

      ! Whip-up the powers of A, B and C
      do dc = 2, 5
        AA(dc) = AA(dc-1)*AA(1); BB(dc) = BB(dc-1)*BB(1); CC(dc) = CC(dc-1)*CC(1)
        iA(dc) = iA(dc-1)+iA(1); iB(dc) = iB(dc-1)+iB(1); iC(dc) = iC(dc-1)+iC(1)
      end do

      ! Common denominators for the various assumption types
      if((typ.eq.1).or.(typ.eq.3).or.(typ.eq.5)) then
        id = max( iB(1) , iC(1) )
        d  = 3.0d0 * BB(3) * CC(3) * ( BB(1)*ten**(iB(1)-id) - CC(1)*ten**(iC(1)-id) ) ** 3
        id =         iB(3) + iC(3) +                        id                          * 3
        r  =          2*3  +  2*3  +                         2                          * 3
      else if((typ.eq.2).or.(typ.eq.6)) then
        d  = 9.0d0 * BB(4)
        id =         iB(4)
        r  =          2*4
      else if((typ.eq.4).or.(typ.eq.7)) then
        d  = 9.0d0 * CC(4)
        id =         iC(4)
        r  =          2*4
      else
        d = 0.0d0; id = 0
      end if

      if(d.ne.0.0d0) d = 1.0d0 / ( 2.0d0 * d )

      if(.not.done) then
        done = .true.
        call Element_Template(Re,RPe,Se,Te,TVe,TPe,He,Ee,Ge,Fe)
      endif

      S=0.0d0; Sxx=0.0d0; Sxy=0.0d0; Syx=0.0d0; Syy=0.0d0; Rx=0.0d0; Ry=0.0d0; W=0.0d0; Ex=0.0d0; Ey=0.0d0; H=0.0d0
      T=0.0d0; TV=0.0d0; TP=0.0d0; RPx=0.0d0; RPy=0.0d0; RRx=0.0d0; RRy=0.0d0; RQx=0.0d0; RQy=0.0d0

!      open(34,file='/home/radcliffe/blob/build.dat')
      
      ! Loop extra bits for axi-symmetric
      do dc = 1, 9

      ! Allow modifications of loop counter ........ !!!!!!!!
      ac = dc

      ! Determine axi-symmetric factor
      if((.not.axisym).and.(ac.ne.0)) then
        ! First indices of 2 -> 9 of template arrays (?e) only of relevance to axi-symmetric case
        cycle
      else if(axisym) then ! 3 * ( m - 1 ) + n          a_m c_n
        ca = modulo(ac-1,3)+1
        f = c(ca)
        ca = 1 + ( ac - ca ) / 3
        f = a(ca) * f / ( 2.0D0 * area )
      else
        f = 1.0d0
      endif

      ! (Re-) Set first index position to that appropriate for (core-) element type (base = 0 for non-core integrations)
      ac = base + ac

      do m = 1, mlim
        call linint(odv,odp,m,p,u,interpol)
        do n = 1, mlim

          ! Mass Matrix
          T(m,n) =  T(m,n) + f * area *  Te( ac ,m,n)
!          if(.not.axisym) &
!          TV(m,n) =               area * TVe(base/10,m,1,n)
          if(dc.eq.1) then
            ! Radius free mass matrix
            TP(m,n) =               area * TPe(base/10,m,n)
            W( m,n) =               area * Te( base,   m,n)
!            open(35,file='/home/radcliffe/blob/start.dat')
!            do i = 0, 5
!              write(35,'(i6,4e14.6)')   i,   &
!                AA(i)*ten**iA(i)*ta**(-2*i), &
!                BB(i)*ten**iB(i)*ta**(-2*i), &
!                CC(i)*ten**iC(i)*ta**(-2*i), &
!                LL(i)
!            end do
!            write(35,'(i6,e14.6)') 0, 1.0d0/(2.0d0*d)    *ten**id   *ta**(-r)
!            close(35)
            ! Reciprocal radius mass matrix
            ca = -1; mm = ro(m); nn = ro(n)
            do k = 0, 5
              do j = 0, 5
                do i = 0, 5
                  do l = 0, 5
                    ca = ca + 1
                    if(ca.eq.0) cycle
                    if(TVe(m,ca,typ,n).ne.0) then
                      if(LL(l).lt.-999.0d0) then
                        print*,'ERROR in Element Compute: Attempt to use non-defined logarithm !!!'
                        stop
                      end if
                      cheese = dble(TVe(m,ca,typ,n)) *   AA(i) * BB(j) * CC(k) * LL(l) * d     &
                                              * ( ten ** ( iA(i) + iB(j) + iC(k) - id ) )        &
                                              * (  ta ** ( r - 2 * (i+j+k) ) )
!                      write(34,'(4i6,e14.6)') ca, m, n, TVe(m,ca,typ,n), cheese
                      TV(mm,nn) = TV(mm,nn) + cheese
                    end if
                  end do
                end do
              end do
            end do
          end if

          ! Interpolation Matrix
          if(axisym) then
            W(m,n) = W(m,n)
          else if(disgal) then
            W(m,n) = 0.0D0
          else if(pcored.or.(.not.interpol)) then
            W(m,n) = 0.0D0
          else if(n.eq.m) then
            W(m,n) = 1.0d1
          elseif(n.eq.p(+1)) then
            W(m,n) = -u(+1) * 1.0d1
          elseif(n.eq.p(-1)) then
            W(m,n) = -u(-1) * 1.0d1
          elseif(n.eq.p( 0)) then
            W(m,n) = -u( 0) * 1.0d1
          else
            W(m,n) = 0.0D0
          endif
      
          if(pcored.or.disgal) then
            p(1) = m; p(0) = 0; p(-1) = 0
          end if

          do i = 1, 3
            ! Divergence Matrices
            if(pcored.or.(.not.interpol)) then
              Rx(m,n) = Rx(m,n) + f * b(i) * Re(ac,p(1),i,n) / 2.0d0
              Ry(m,n) = Ry(m,n) + f * c(i) * Re(ac,p(1),i,n) / 2.0d0
              if(dc.eq.1) then
                RQx(m,n) = RQx(m,n) + b(i) * Re(base,p(1),i,n) / 2.0d0
                RQy(m,n) = RQy(m,n) + c(i) * Re(base,p(1),i,n) / 2.0d0
                ! Radius free divergence matrix
!                RRx(m,n) = RRx(m,n) + b(i) * RPe(base,p(1),i,n) / 2.0d0
!                RRy(m,n) = RRy(m,n) + c(i) * RPe(base,p(1),i,n) / 2.0d0
              end if
              RPx(m,n) = RPx(m,n) + f * b(i) * RPe(ac,p(1),i,n) / 2.0d0
              RPy(m,n) = RPy(m,n) + f * c(i) * RPe(ac,p(1),i,n) / 2.0d0
!              if((axisym).and.(dc.eq.1)) then
!                Sxx(m,n) = Sxx(m,n) + b(i) * RPe(base,p(1),i,n) / 2.0d0
!                Syy(m,n) = Syy(m,n) + b(i) * RPe(base,p(1),i,n) / 2.0d0
!              end if
            endif

            if(disgal.and.(dc.eq.1)) then
              Ex(m,i,n) = Ex(m,i,n) + leng(i) * Fe(0,m,i,n) * nx(i)
              Ey(m,i,n) = Ey(m,i,n) + leng(i) * Fe(0,m,i,n) * ny(i)
            endif

            do j = 1, 3
              ! Stress Matrices
              S  (m,n) = S  (m,n) + f * ( b(i)*b(j) + c(i)*c(j) ) * Se(ac,m,i,j,n) / (4.0d0*area)
              Sxx(m,n) = Sxx(m,n) + f * ( b(i)*b(j)             ) * Se(ac,m,i,j,n) / (4.0d0*area)
              Sxy(m,n) = Sxy(m,n) + f * ( b(i)      *      c(j) ) * Se(ac,m,i,j,n) / (4.0d0*area)
              Syx(m,n) = Syx(m,n) + f * (      b(j) * c(i)      ) * Se(ac,m,i,j,n) / (4.0d0*area)
              Syy(m,n) = Syy(m,n) + f * (             c(i)*c(j) ) * Se(ac,m,i,j,n) / (4.0d0*area)
              if((disgal).and.(dc.eq.1)) then
                RRx(m,j,n)= RRx(m,j,n)+(2.0d0*b(i)*nx(j)*tx(j)+c(i)*ny(j)*tx(j)+c(i)*nx(j)*ty(j))*Ge(0,m,i,j,n)*leng(j)/(2.0d0*area)
                RRy(m,j,n)= RRy(m,j,n)+(b(i)*ny(j)*tx(j)+b(i)*nx(j)*ty(j)+2.0d0*c(i)*ny(j)*ty(j))*Ge(0,m,i,j,n)*leng(j)/(2.0d0*area)
                H(m,j,n) = H(m,j,n) + ( b(i)*nx(j) + c(i)*ny(j) ) * Ge(0,m,i,j,n) * leng(j) / (2.0d0*area)
              else if(.not.disgal) then
                do k = 1, 3
                  ! Double Divergence Matrices
                  if(pcored.or.(.not.interpol)) then
                    Ex(m,1,n) = Ex(m,1,n) + f * ( b(i)*b(j) + c(i)*c(j) ) * b(k) * Ee(ac,p(1),i,j,k,n) / (8.0d0*area**2)
                    Ey(m,1,n) = Ey(m,1,n) + f * ( b(i)*b(j) + c(i)*c(j) ) * c(k) * Ee(ac,p(1),i,j,k,n) / (8.0d0*area**2)
                  endif
                  do l = 1, 3
                    ! Double Stress Matrix
                    H(m,j,n) = H(m,j,n) + f * (b(i)*b(j)+c(i)*c(j)) * (b(k)*b(l)+c(k)*c(l)) * He(ac,m,i,j,k,l,n)/(16.0d0*area**3)
                  end do
                end do
              endif
            end do
          end do
        end do
      end do

      end do
      
!      close(34)

      return
      end 

                                
                                                 
      subroutine Catch(A,B,C,D,X,Y,Type,List,k,l,chkroot)

      use CONTROL, only : disgal, axisym, rifsym, tol, fitaa, fitbb
      use PLACE, only : isroot, noderoot, HY, HX, listAlign
      use SIZES, only : maxnod

      implicit none
      integer i, j, k, l, List(maxnod), err
!      integer, allocatable::w(:)
      real X(maxnod), Y(maxnod)
      double precision xcen, ycen, xx, yy
      double precision, allocatable::val(:)
      character*1 Type(maxnod), A, B, C, D
      logical chkroot, alignset, collect, rdone, zdone, origindone, axisim
      data alignset /.false./
      save alignset

      ! Some initializations
      k=0; List=-1; rdone = .false.; zdone = .false.; origindone = .false.; axisim = axisym.or.rifsym

      do 10 i = 1, maxnod

        ! Default is not to collect node
        collect = .false.

        ! Node with label(s) of interest found
        if(index(A//B//C//D,Type(i)).ne.0) then

          ! No node root checking required
          if(.not.chkroot) then
            collect = .true.

          ! Root checking skipped for axisym 'X' and 'Y' nodes as any duplicates from 'J' labelled nodes
          ! will be skipped in the next bit ...
          else if(axisim.and.((A.eq.'J').or.(B.eq.'J')).and.(index('XY',Type(i)).ne.0)) then
            collect = .true.

          ! Root checking skipped for axisym 'X' and 'Y' nodes as any duplicates from 'J' labelled nodes
          ! will be skipped in the next bit ...
          else if(axisim.and.(B.eq.'J').and.(Type(i).eq.'O').and.(.not.origindone)) then
            origindone = .true.
            collect = .true.

          ! Avoid 'J' node collections along the axes in axisym case
          else if(axisim.and.((A.eq.'J').or.(B.eq.'J')).and.((X(i).lt.tol).or.(Y(i).lt.tol))) then
            cycle

          ! Root checking required and satisfied
          else if(isroot(i)) then
            collect = .true.
            
          ! Root checking test passed for certain collections if root node of specified type
          else if(((A.eq.'J').or.(B.eq.'J')).and.(Type(i).eq.'J').and.(Type(noderoot(i)).eq.'Z').and.(.not.zdone)) then
            collect = .true.; zdone = .true.

          ! Root checking test passed for certain collections if root node of specified type
          else if(((A.eq.'J').or.(B.eq.'J')).and.(Type(i).eq.'J').and.(Type(noderoot(i)).eq.'R').and.(.not.rdone)) then
            collect = .true.; rdone = .true.

          end if

        end if

        ! Collect the node if allowed
        if(collect) then
          k=k+1; List(k)=i
        endif
   10 continue      

      if(k.eq.0) return

      xcen = 0.0d0; ycen = 0.0d0
      if((.not.axisim).or.(A.eq.'O')) then
        do i = 1, k
          if(disgal.and.((A.eq.'H').or.(B.eq.'H'))) then
            xx = dble(HX(List(i))); yy = dble(HY(List(i)))
          else
            xx = dble( X(List(i))); yy = dble( Y(List(i)))
          end if
          call fitsmooth(xx,yy)
          xcen = xcen + xx
          ycen = ycen + yy
        end do
        xcen = xcen / dble(k)
        ycen = ycen / dble(k)
        if(axisim.and.(A.eq.'O')) ycen = ycen / 3.0d0
      end if

      allocate(val(k),stat=err)
      if(err.ne.0) then
        print*,'ERRORin Catch: Failed to declare ordering storage !!!'
        stop
      end if

      if(disgal.and.((A.eq.'H').or.(B.eq.'H'))) then
        do i = 1, k
          xx = dble(HX(List(i))); yy = dble(HY(List(i)))
          call fitsmooth(xx,yy)
          val(i) = atan2( yy - ycen , xx - xcen )
        end do
      else
        do i = 1, k
          xx = dble( X(List(i))); yy = dble( Y(List(i)))
          call fitsmooth(xx,yy)
          val(i) = atan2( yy - ycen , xx - xcen )
        end do
      endif
      
      call dheapsort(k,List,val)
      
      ! Ensure list always ends on a node with the first requested letter tag
      if(.not.axisim) then
        do i = 1, k
          if(Type(List(k)).ne.A) then
            ! Shunt all elements up in the list
            l = List(1)
            do j = 1, k-1
              List(j) = List(j+1)
            end do
            List(k) = l
          else
            exit
          endif
       end do
      end if

      ! Last plus one position occupied by the first element for looping neatness
      List(k+1)=List(1)

      ! Shunt elements (clockwise) around by one for 'H' | 'J' disgal list alignments if necessary
      if(disgal.and.(.not.axisim).and.((A.eq.'H').or.(B.eq.'H')).and.alignset) then
        if(listAlign(List(1),0.0,0.0,0.0,0.0,0.0,0.0)) then
          j = List(k)
          do i = k, 2, -1
            List(i) = List(i-1)
          end do
          List(1) = j
        endif
      endif

      ! Last plus one position occupied by the first element for looping neatness
      List(k+1)=List(1)
      l=k

      ! Store (and flag that done) first and last 'J' segment data for disgal list alignments later ...
      if(disgal.and.((A.eq.'J').or.(B.eq.'J')).and.(.not.alignset)) &
        alignset = listAlign(0,X(List(k)),Y(List(k)),X(List(1)),Y(List(1)),X(List(2)),Y(List(2)))

      return
      end



      subroutine Mesh_Retrieve(X,Y,L,Mesh,nomat,sortord,sortcrit)

      use CONTROL, only : disgal, ord, scal, xxxx, yyyy, axisym, inviscid, tol, air, drag, scal, refsym, rifsym
      use SIZES, only : maxnod, maxele

      implicit none
      integer i, j, mat, n(3), err, temp, tenp, tags, nomat, k, pos, neg, offnod, switch(140,4)
      integer dir(2), gam(2), flipposn, pp, qq, qqq, hit, loopcount
      integer sortord(maxele)  !(:), allocatable::
      real Mesh(11,maxele), X(maxnod), Y(maxnod), cross, topy, crossLight
      real sortcrit(maxele)  !(:), allocatable::
      double precision L(3*maxnod)
      character check*5, letter*1, letterlist*10, buffer*120
      logical gmsh, femonly, one, two, thr, rerun, redone, reredone, nospec, flip, offaxis, flipit, top, bot, sorted
      logical special
      data rerun /.false./, redone /.false./, reredone /.false./, nospec /.true./, switch /560*0/, sorted /.false./
      external cross

      loopcount = 0; axisym = .false.

      ! Initialize the rerun flag for reading in the mesh twice for square problems
    1 if(redone) reredone = .true.
      if(rerun) redone = .true.

      i=-2
      nomat=0; letterlist='          '; femonly = .false.; topy = 0.0

      ! Determine the order of decreasing diagonal-y-midpoints for the diagonal switching array
      if(axisym.and.reredone.and.(.not.sorted)) then
        sortord = 0; sortcrit = 1.0
        do j = 1, 140
          sortord(j)  = j

          ! For each switch pair
          if((switch(j,1).ne.0).and.(switch(j,2).ne.0)) then

            ! Special case for bottom right corner diagonal switching only ...
            if(refsym.and.(X(switch(j,1)).gt.tol).and.(X(switch(j,2)).gt.tol)) then

              ! ... has a sort criterion of zero to ensure always appears at the end of the list
              sortcrit(j) = 0.0

            else

              ! ... just use the diagonal mid-point y-coordinates to sort by
              sortcrit(j) = - Y(switch(j,1)) - Y(switch(j,2))
            end if
          end if
        end do

        call heapsort(140,sortord,sortcrit)

        do j = 1, 140
          sortcrit(j) = real(sortord(j))
          sortord(j)  = j
        end do

        call heapsort(140,sortord,sortcrit)

        sorted = .true.
      end if

      ! See if there's a slice mesh to be used for axi-symmetric 3-D experiments
      open(10,file='fem.msh',status='old',err=2)
      femonly = .true.; axisym = .true.
      goto 8

      ! See if there's a slice mesh to be used for axi-symmetric 3-D experiments
    2 open(10,file='slice.msh',status='old',err=3)
      ! Can only request axisymmetry if there is no demand for reflection symmetry in the Y-axis
      if(.not.rifsym) axisym = .true.
      goto 8

      ! See if there's a FEM only mesh to compute first (for "error" comparisons with FEM/BEM results)
    3 open(10,file='femo.msh',status='old',err=5)
      femonly = .true.
      goto 8

      ! Otherwise, as normal ...
    5 open(10,file='mesh.msh',status='old',err=7) 
      goto 8

    7 print*,'No mesh file!'
      stop

    8 pos = 0; neg = 0

      read(10,'(/a5)') check
      gmsh = .true. !(check.eq.'2.2 0')
      if(gmsh) then
        read(10,'(/a5)')check
        read(10,*) temp
      else
        print*,'WRONG GMSH MESH FORMAT !!!'
        close(10)
        stop
      endif
      do 10 i=1, temp
        if(i.le.maxnod) then
          if(gmsh) then
            read(10,*) err, X(i), Y(i), mat
          else
            read(10,'(5x,2E15.7)')X(i),Y(i)
          endif
          X(i) = scal*(X(i)-xxxx); Y(i) = scal*(Y(i)-yyyy)
          topy = max(topy,Y(i))
        else
          read(10,'(5x,2E15.7)')L(i-maxnod)
        endif
   10 continue
 
      do 20 i=1, 600000
        read(10,'(a5)')check
        if((check.eq.'$ELEM').or.(check.eq.'$Elem')) goto 25
   20 continue
 
   25 read(10,*) maxele !'(i5)'
      !if(.not.rerun) allocate(sortcrit(maxele),sortord(maxele),stat=err)

      ! Initialize counter for non-triangular gmsh elements 
      k = 0
    
      do 30 i=1, maxele  
        ! GMSH Format Mesh Input
        if(gmsh) then
          read(10,'(a120)') buffer
          buffer(111:120) = ' 0 0 0 0 0'
          read(buffer,*) err, j, tags, tenp, mat,  n(1), n(3), n(2)

          if(reredone) then
            do flipposn = 1, 140
              ! Non-axi-symmetric problems will have at most two switches required (for the square geometry)
              if((.not.axisym).and.(flipposn.gt.2)) exit
              ! Nothing to do if any of the flip information is missing
              if((switch(flipposn,1).eq.0).or.(switch(flipposn,2).eq.0).or. &
                 (switch(flipposn,3).eq.0).or.(switch(flipposn,4).eq.0)) cycle
              ! Initialize a hit (node match) counter
              hit = 0
              ! Loop the three nodes read-in
              do pp = 1, 3
                ! Loop the four nodes (held at flipposition) to have their diagonal flipped
                do qq = 1, 4
                  ! If a node match ...
                  if(n(pp).eq.switch(flipposn,qq)) then
                    ! ... then keep track of how many
                    hit = hit + 1
                    if(qq.gt.2) qqq = qq
                  endif
                end do
              end do
              ! If no match then look at next ...
              if(hit.ne.3) cycle

              special = .false.

              if(axisym.and.(.not.drag)) then

                ! For bottom right corner special case only ...
                if((X(switch(flipposn,1)).gt.tol).and.(X(switch(flipposn,2)).gt.tol)) then

                  ! ... always flip if present
                  special = .true.; top = .false.; bot = .false.

                else if(X(switch(flipposn,3))-X(switch(flipposn,4)).eq.0.0) then
                  print*,'ERROR in Mesh_Retrieve: Bad Off-Diagonal !!!'
                  stop

                else if(X(switch(flipposn,1))-X(switch(flipposn,2)).eq.0.0) then
                  print*,'ERROR in Mesh_Retrieve: Bad Diagonal !!!'
                  stop

                ! Special case of diagonal and line formed by off-diagonal nodes having gradients of equal sign
                else if( (Y(switch(flipposn,3))-Y(switch(flipposn,4)))/(X(switch(flipposn,3))-X(switch(flipposn,4)))  &
                                                                     .ge.                                             &
                         (Y(switch(flipposn,1))-Y(switch(flipposn,2)))/(X(switch(flipposn,1))-X(switch(flipposn,2))) ) then
                  top = .false.
                ! Determine whether the off diagonal node with the zero x is at the top (or bottom) of the switch pair
                else if(abs(X(switch(flipposn,3))).lt.tol) then
                  top = Y(switch(flipposn,3)).gt.0.5*(Y(switch(flipposn,3))+Y(switch(flipposn,4)))
                else if(abs(X(switch(flipposn,4))).lt.tol) then
                  top = Y(switch(flipposn,4)).gt.0.5*(Y(switch(flipposn,3))+Y(switch(flipposn,4)))
                else
                  print*,"This shouldn't happen."
                  stop
                endif
                ! ... and then what it should be = "bottom" if a multiple of 2 switch pairs from topmost
                bot = mod(sortord(flipposn),2).eq.1
              else
                top = .false.; bot = .false.
              end if
              flipit = axisym.and.((top.and.bot).or.((.not.top).and.(.not.bot)).or.special)
              flipit = flipit.and.(.not.drag)
              ! If found an element to be diagonal flipped ( three node matches ) ...
              if((.not.axisym).or.flipit) then
                ! ... then perform the switches
                if(qqq.eq.3) then
                  n(1) = switch(flipposn,3)
                  n(2) = switch(flipposn,4)
                  n(3) = switch(flipposn,1)
                else
                  n(1) = switch(flipposn,3)
                  n(2) = switch(flipposn,4)
                  n(3) = switch(flipposn,2)
                endif
              endif
            end do
          endif

          ! Correct orientation of input triangle
          if(j.eq.2) then
            if(crossLight(X(n(2))-X(n(1)),Y(n(2))-Y(n(1)),X(n(3))-X(n(1)),Y(n(3))-Y(n(1))).lt.0.0) then
              tenp = n(2)
              n(2) = n(3)
              n(3) = tenp
            
              neg = neg + 1
            else
              pos = pos + 1
            endif
          endif

          ! Skip elements within any given Dirichlet square (Only consider triangular elements)
          if(j.eq.2) call inSquare(j,mat,X(n(1)),Y(n(1)),X(n(2)),Y(n(2)),X(n(3)),Y(n(3)))

          ! Non-triangle gmsh elements - processed to collect node element data for edge coding of triangles later
          if(j.ne.2) then

            ! Count them
            k = k + 1

            ! Boundary Nodes
            if(j.eq.45) then

              ! Flick rerun flag on for squares
              if(.not.rerun) rerun = .true.

              ! Capture Boundary Label info. from Passed-Through User Given Node Numbers (in input "geo" file)
              L( n(1) + (floor(real(mat)/10000.0)-1) * maxnod ) = 1.0d0
              L( n(2) + (floor(real(mat)/10000.0)-1) * maxnod ) = 1.0d0
              L( n(3) + (floor(real(mat)/10000.0)-1) * maxnod ) = 1.0d0

            else if(j.eq.15) then

              ! Capture Boundary Label info. from Passed-Through User Given Node Numbers (in input "geo" file)
              if(mat.ge.10000) L( n(1) + (floor(real(mat)/10000.0)-1) * maxnod ) = 1.0d0

            ! Boundary Segments
            else if(j.eq.1) then

              ! If "new" boundary nodes have been introduced - they derive boundary labels from 
              ! any "old" nodes they may be segment connected to .......
              if(drag) then
                if(( L( n(1)        ).gt.0.5d0 ).and.( L( n(1)+maxnod ).lt. 0.5d0 ).and.(X(n(3)).gt.tol)) L( n(3)          ) = 1.0d0
                if(( L( n(3)        ).gt.0.5d0 ).and.( L( n(3)+maxnod ).lt. 0.5d0 ).and.(X(n(1)).gt.tol)) L( n(1)          ) = 1.0d0
                if(( L( n(1)+maxnod ).gt.0.5d0 ).and.( L( n(1)        ).lt. 0.5d0 )) L( n(3) + maxnod ) = 1.0d0!.and.(X(n(3)).gt.tol)
                if(( L( n(3)+maxnod ).gt.0.5d0 ).and.( L( n(3)        ).lt. 0.5d0 )) L( n(1) + maxnod ) = 1.0d0!.and.(X(n(1)).gt.tol)
              else
                if( L( n(1)          ) .gt. 0.5d0 ) L( n(3)          ) = 1.0d0
                if( L( n(3)          ) .gt. 0.5d0 ) L( n(1)          ) = 1.0d0
                if( L( n(1) + maxnod ) .gt. 0.5d0 ) L( n(3) + maxnod ) = 1.0d0
                if( L( n(3) + maxnod ) .gt. 0.5d0 ) L( n(1) + maxnod ) = 1.0d0
              end if

            endif

            goto 30

          ! True triangular elements
          else
          
            ! Material Number
            mat = modulo(mat,10000)

            ! Determine Dirichlet Boundary Code Number(s)
            one = L( n(1) + 0 * maxnod ) .gt. 0.5d0
            two = L( n(2) + 0 * maxnod ) .gt. 0.5d0
            thr = L( n(3) + 0 * maxnod ) .gt. 0.5d0

            call sides(dir,one,two,thr,X(n(1)),Y(n(1)),X(n(2)),Y(n(2)),X(n(3)),Y(n(3)),rerun,flip,topy)

            ! Determine Gamma Boundary Code Number(s)
            one = L( n(1) + 1 * maxnod ) .gt. 0.5d0
            two = L( n(2) + 1 * maxnod ) .gt. 0.5d0
            thr = L( n(3) + 1 * maxnod ) .gt. 0.5d0

            call sides(gam,one,two,thr,X(n(1)),Y(n(1)),X(n(2)),Y(n(2)),X(n(3)),Y(n(3)),rerun,flip,topy)

            ! If a diagonal has been flagged to be flipped -- the awkward corners of square geometries for the
            ! non-conforming case, and ALL elements with an edge on the rotation axis for axi-symmetric problems  --
            ! then store the respective element's node numbers
            if((disgal.or.axisym).and.(flip.or.redone).and.(.not.reredone)) then
              offnod = 0
              ! 
              do flipposn = 1, 140

                ! New flip data row
                if(flip.and.(.not.redone).and.(switch(flipposn,1).eq.0).and.(switch(flipposn,2).eq.0)) then

                  ! The nodes for the diagonal are held in the first two columns
                  switch(flipposn,1) = n(        gam(1)         )
                  switch(flipposn,2) = n( modulo(gam(2)-1,10)+1 )
                  rerun = .true.
                  exit
                  
                else if(redone.and.(((switch(flipposn,1).eq.n(1)).and.(switch(flipposn,2).eq.n(2))).or.  &
                                    ((switch(flipposn,2).eq.n(1)).and.(switch(flipposn,1).eq.n(2))))) then
                  offnod = n(3)
                  exit

                else if(redone.and.(((switch(flipposn,1).eq.n(1)).and.(switch(flipposn,2).eq.n(3))).or.  &
                                    ((switch(flipposn,2).eq.n(1)).and.(switch(flipposn,1).eq.n(3))))) then
                  offnod = n(2)
                  exit

                else if(redone.and.(((switch(flipposn,1).eq.n(2)).and.(switch(flipposn,2).eq.n(3))).or.  &
                                    ((switch(flipposn,2).eq.n(2)).and.(switch(flipposn,1).eq.n(3))))) then
                  offnod = n(1)
                  exit
                endif
              end do

              if(offnod.ne.0) then
                ! Store the off diagonal node in the appropriate position in the switch array
                if(cross( X(switch(flipposn,1)) - X(offnod)  ,  Y(switch(flipposn,1)) - Y(offnod),        &
                          X(switch(flipposn,2)) - X(offnod)  ,  Y(switch(flipposn,2)) - Y(offnod)         ).gt.0.0) then
                  switch(flipposn,3) = offnod
                else
                  switch(flipposn,4) = offnod
                endif

              endif
            endif
          endif

        endif

        ! Wipe any miss-use of labels for axisym case
        if(gam(2).gt.8) gam(2) = 0; if(dir(2).gt.8) dir(2) = 0

        if(mat.le.10) then
          letter='ABCDEFGHIJ'(mat:mat)
        else
          letter=' '
        endif
        if((index(letterlist,letter).eq.0).and.(mat.le.10)) then
          nomat=nomat+1
          letterlist(nomat:nomat)=letter
        endif

        if(mat.eq.99) nospec = .false.

        if(n(1).le.maxnod) then
          Mesh( 1,i-k)=X(n(1))
          Mesh( 2,i-k)=Y(n(1))
        else
          Mesh( 1,i-k)= L(n(1)-maxnod)
          !Mesh( 2,i-k)=aimag(L(n(1)-maxnod))
        endif
        if(n(2).le.maxnod) then
          Mesh( 3,i-k)=X(n(2))
          Mesh( 4,i-k)=Y(n(2))
        else
          Mesh( 3,i-k)= L(n(2)-maxnod)
        endif
        if(n(3).le.maxnod) then
          Mesh( 5,i-k)=X(n(3))
          Mesh( 6,i-k)=Y(n(3))
        else
          Mesh( 5,i-k)= L(n(3)-maxnod)
        endif
        call decoder(dir,.false.)
        Mesh( 7,i-k)=real(dir(1))
        call decoder(gam,.false.)
        Mesh( 8,i-k)=real(gam(1))
        Mesh( 9,i-k)=real(ord)
        Mesh(10,i-k)=real(mat)         
        Mesh(11,i-k)=min(sqrt((Mesh( 5,i-k)-Mesh( 3,i-k))**2+(Mesh( 6,i-k)-Mesh( 4,i-k))**2), &
                        sqrt((Mesh( 3,i-k)-Mesh( 1,i-k))**2+(Mesh( 4,i-k)-Mesh( 2,i-k))**2), &
                        sqrt((Mesh( 5,i-k)-Mesh( 1,i-k))**2+(Mesh( 6,i-k)-Mesh( 2,i-k))**2)    )/real(ord)
        sortcrit(i-k)=((Mesh( 1,i-k)+Mesh( 3,i-k)+Mesh( 5,i-k))**2+(Mesh( 2,i-k)+Mesh( 4,i-k)+Mesh( 6,i-k))**2)/9.0 
        if(mat.eq.99) sortcrit(i-k)=sortcrit(i-k)+50.0
   30 continue        

      if((.not.rerun).or.((.not.disgal).and.rerun.and.redone).or.(disgal.and.rerun.and.reredone)) then
        close(10)!,status='delete')
      else
        close(10)
      endif
      
      maxele = maxele - k

      ! If no given special region ... then create one ... unless inviscid or not using air tractions ...
      if(nospec.and.(.not.femonly).and.(.not.inviscid).and.air) then

        k = maxele

        ! Loop the collected elements
        do i = 1, k
          
          ! Look-out for an element with an edge along the gamma boundary
          gam(1) = int(Mesh( 8,i))
          call decoder(gam,.true.)

          ! For axi-symmetric only check if element has an edge along a coordinate axis
          if(axisym) then
            offaxis = .not. (  ( ( Mesh(1,i) .eq. 0.0 ) .and. ( Mesh(3,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(1,i) .eq. 0.0 ) .and. ( Mesh(5,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(3,i) .eq. 0.0 ) .and. ( Mesh(5,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(2,i) .eq. 0.0 ) .and. ( Mesh(4,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(2,i) .eq. 0.0 ) .and. ( Mesh(6,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(4,i) .eq. 0.0 ) .and. ( Mesh(6,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(1,i) .eq. 0.0 ) .and. ( Mesh(4,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(2,i) .eq. 0.0 ) .and. ( Mesh(3,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(1,i) .eq. 0.0 ) .and. ( Mesh(6,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(2,i) .eq. 0.0 ) .and. ( Mesh(5,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(3,i) .eq. 0.0 ) .and. ( Mesh(6,i) .eq. 0.0 ) ) .or. &
                               ( ( Mesh(4,i) .eq. 0.0 ) .and. ( Mesh(5,i) .eq. 0.0 ) )      )
          else
            offaxis = .true.
          end if

          if((gam(1).gt.0).and.offaxis) then

            ! Create an element
            maxele = maxele + 1

            if(disgal) then

              if(gam(1).eq.1) then

                Mesh( 1,maxele) = 10.0 * ( Mesh( 3,i) + Mesh( 5,i) - Mesh( 1,i) )
                Mesh( 2,maxele) = 10.0 * ( Mesh( 4,i) + Mesh( 6,i) - Mesh( 2,i) )

                Mesh( 3,maxele) = 10.0 * Mesh( 3,i)
                Mesh( 4,maxele) = 10.0 * Mesh( 4,i)

                Mesh( 5,maxele) = 10.0 * Mesh( 5,i)
                Mesh( 6,maxele) = 10.0 * Mesh( 6,i)

              else if(gam(1).eq.2) then

                Mesh( 1,maxele) = 10.0 * Mesh( 1,i)
                Mesh( 2,maxele) = 10.0 * Mesh( 2,i)

                Mesh( 3,maxele) = 10.0 * ( Mesh( 1,i) + Mesh( 5,i) - Mesh( 3,i) )
                Mesh( 4,maxele) = 10.0 * ( Mesh( 2,i) + Mesh( 6,i) - Mesh( 4,i) )

                Mesh( 5,maxele) = 10.0 * Mesh( 5,i)
                Mesh( 6,maxele) = 10.0 * Mesh( 6,i)

              else if(gam(1).eq.3) then

                Mesh( 1,maxele) = 10.0 * Mesh( 1,i)
                Mesh( 2,maxele) = 10.0 * Mesh( 2,i)

                Mesh( 3,maxele) = 10.0 * Mesh( 3,i)
                Mesh( 4,maxele) = 10.0 * Mesh( 4,i)

                Mesh( 5,maxele) = 10.0 * ( Mesh( 1,i) + Mesh( 3,i) - Mesh( 5,i) )
                Mesh( 6,maxele) = 10.0 * ( Mesh( 2,i) + Mesh( 4,i) - Mesh( 6,i) )

              endif

            else

              Mesh( 1,maxele) = 10.0 * Mesh( 1,i)
              Mesh( 2,maxele) = 10.0 * Mesh( 2,i)

              Mesh( 3,maxele) = 10.0 * Mesh( 3,i)
              Mesh( 4,maxele) = 10.0 * Mesh( 4,i)

              Mesh( 5,maxele) = 10.0 * Mesh( 5,i)
              Mesh( 6,maxele) = 10.0 * Mesh( 6,i)

            endif

            Mesh( 7,maxele) = Mesh( 8,i)
            gam(1) = 0; gam(2) = 0
            call decoder(gam,.false.)
            Mesh( 8,maxele)=real(gam(1))
            
            Mesh( 9,maxele) = Mesh( 9,i)
            Mesh(10,maxele)=real(99)
            Mesh(11,maxele) = 10.0 * Mesh(11,i)
            sortcrit(maxele) = sortcrit(i)+50.0
          endif
        end do
      endif

      if(maxele.le.-25000) then
        !call RSORT@(sortord,sortcrit,maxele)
        do 40 i=1, maxele           
          X(i)=real(sortord(i))
   40   continue
      else if(reredone) then
        do 50 i=1, maxele           
          X(i)=real(i)
   50   continue
      endif

      ! Flag a FEM only mesh with a negative number of materials
      if(femonly) nomat = -nomat

      print*,'  ',pos,' Positive and ',neg,' Negatively Orientated Elements Found.'
      loopcount = loopcount + 1

      if((pos+neg.eq.0).or.(loopcount.gt.7)) then
        call system('touch please.stop')
        stop
      end if

      ! To ensure the redone flag gets tripped on the next round if looping because of an axisym with NO reruns requested ...
      if(axisym) rerun = .true.

      if(((axisym.or.rerun).and.(.not.redone)).or.((axisym.or.disgal).and.rerun.and.(.not.reredone))) goto 1

      ! Clear the Load Vector
      L = 0.0d0

      ! Deallocate sort arrays
      !deallocate(sortord,sortcrit)

      return
      end
               
                                                                  
 
