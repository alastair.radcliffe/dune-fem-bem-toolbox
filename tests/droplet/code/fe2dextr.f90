


      subroutine graph(n,g1,g2,tag)

      implicit none
      integer i, j, m, n!, err
      double precision g1(n), g2(n), top, bot
      character*1 pic(0:43,500)
      character*3 tag

      m = 42

!      allocate(pic(0:m+1,n),stat=err)

!      if(err.ne.0) return

      top = -1000.0; bot = 1000.0

      do j = 1, n
        do i = 0, m+1
          pic(i,j) = ' '
        end do
        if(g1(j).eq.0.0d0) then
          top = max(top,g2(j))
        else if(g2(j).eq.0.0d0) then
          top = max(top,g1(j))
        else
          top = max(top,max(g1(j),g2(j)))
        end if
        if(g1(j).eq.0.0d0) then
          bot = min(bot,g2(j))
        else if(g2(j).eq.0.0d0) then
          bot = min(bot,g1(j))
        else
          bot = min(bot,min(g1(j),g2(j)))
        end if
      end do

      do j = 1, n
        if(abs(top-bot).lt.0.0001) cycle

        ! Plot the Abscissa
        i = min(43,max(0,nint( real(m)*(0.0-bot)/(top-bot) )))
        if(pic(i,j).eq.' ') pic(i,j) = '-'

        ! Plot the First Graph
        if(g1(j).ne.0.0d0) then
          i = nint( real(m)*(g1(j)-bot)/(top-bot) )
          pic(i,j) = '*'
        end if

        ! Plot the Second Graph
        if(g2(j).ne.0.0d0) then
          i = min(43,max(0,nint( real(m)*(g2(j)-bot)/(top-bot) )))
          if(pic(i,j).eq.'*') then
            pic(i,j) = '='
          else
            pic(i,j) = '#'
          endif
        end if
      end do
      
      write(*,'(a6,e13.4)') tag//' <>',top
      do i = m+1, 0, -1
        write(*,'(a3,300a1)') tag,(pic(i,j),j=1,n)
      end do
      write(*,'(a6,e13.4)') tag//' ><',bot

!      deallocate(pic)

      return
      end



      subroutine plot(val)

      implicit none
      integer i, j, n, max, m
      parameter(n=130,m=3)
      double precision val, a, b
      integer d(n), e(n), loc
      character*1 p(n), q(n)
      data d /n*0/, e /n*0/, q /n*' '/, max /0/
      save d, e, p, q, max, a, b

      if(max.eq.0) then
        a = 0.985
        b = 1.015
        q(1) = '|'; q(n) = '|'
        do i = 1, m
          q(nint(real(i*n)/real(m+1))) = '|'
        enddo
      endif

      if(val.ne.0.0) then
        if(val.lt.a) then
          loc = 1
        elseif(val.gt.b) then
          loc = n
        else
          loc = nint(real(n)*(val-a)/(b-a))
        endif
        d(loc) = d(loc) + 1
        if(d(loc).gt.max) max = d(loc)
      else
        p = q
        do i = max, 1, -1
          do j = 1, n
            if(d(j).ge.i) p(j) = '*'
          end do
          write(*,'(200a1)')(p(j),j=1,n)
        end do
        d = e; max = 0
      endif

      return
      end


      subroutine histogram(val)

      implicit none

      real*8, intent(in) :: val

      integer sp, err

      double precision, allocatable :: hi(:)

      data sp /50/
      save sp, hi

      ! Initially create space to store histogram
      if(val.eq.-1.0d0) then
        allocate(hi(-sp:sp),stat=err)
        if(err.ne.0) then
          print*,'ERROR: Insufficient Memory for Element Histogram !!!'
          stop
        else
          hi = 0.0d0
        end if
      else if(val.eq.-2.0d0) then
        call graph(2*sp+1,hi,hi,'[H]')
        deallocate(hi)
      else
        err = floor(log(val))
        if(abs(err).gt.sp) then
          print*,'ERROR: Element Outside Histogram ranges!!!'
          stop
        end if
        hi(err) = hi(err) + 1.0d0
      end if

      return

      end subroutine histogram


      subroutine testGauss(k,x,w)
 
      implicit none
      integer i
      integer, intent(in) :: k
      double precision, intent(in) :: x(-32:32), w(-32:32)
      double precision sum(4)
      
      sum = 0.0d0

      do i = -k, k
        sum(1) = sum(1) + w(i) * 1.0
        sum(2) = sum(2) + w(i) *       ( ( 1.0d0 + x(i) ) / 2.0d0 )
        sum(3) = sum(3) + w(i) *       ( ( 1.0d0 + x(i) ) / 2.0d0 ) ** 2
        sum(4) = sum(4) + w(i) * 1.0 / ( ( 1.0d0 + x(i) ) / 2.0d0 )
      end do

      write(*,'(a14,4f10.4)') 'Gauss tests = ',(sum(i),i=1,4)

      return
      end
 


      double precision function rowsum(n,M)
      
      implicit none
      integer n, i, j
      double precision M(n,n), test, maxi, worst

      worst = 0.0

      do i = 1, n
        test = 0.0d0; maxi = 0.0d0
        do j = 1, n
          maxi = maxi + abs( M(i,j) )
          test = test +      M(i,j)
        end do
        if(maxi.lt.0.1d-8) then
          test = 100.0d0
        else
          test = 100.0 * ( 1.0 - abs(test) / maxi )
        endif
        if(test.lt.worst) worst = test
      end do

      rowsum = worst

      return
      end





      double precision function adj(maxnid,P,Q)
      
      use CONTROL, only : o
      
      implicit none
      integer maxnid, i, j
      double precision P(o:maxnid,o:maxnid), Q(o:maxnid,o:maxnid), test, worst

      worst = 100.0

      do i = o, maxnid
        do j = o, maxnid
          if(i.eq.j) then
            test = 100.0
          else if((abs(Q(j,i)).lt.0.1e-5).and.(abs(P(i,j)).lt.0.1e-5)) then
            test = 100.0
          else if((abs(Q(j,i)).lt.0.1e-5).or.(abs(P(i,j)).lt.0.1e-5)) then
            test = 0.0
          else
            test = 100.0 * min ( P(i,j) / Q(j,i) , Q(j,i) / P(i,j) )
          endif
          if(test.lt.worst) worst = test
        end do
      end do

      adj = worst

      return
      end



      double precision function antisym(maxnid,M)
      
      implicit none
      integer maxnid, i, j, n
      double precision M(maxnid,maxnid), test, worst

      !n = size(M); n = floor(sqrt(real(n)))

      n = maxnid

      worst = 0.0

      do i = 1, n
        do j = 1, n
          if(i.eq.j) then
            test = 100.0
          else if((abs(M(j,i)).lt.0.1e-3).and.(abs(M(i,j)).lt.0.1e-3)) then
            test = 100.0
          else if((abs(M(j,i)).lt.0.1e-3).or.(abs(M(i,j)).lt.0.1e-3)) then
            test = 0.0
          else
            test = 100.0 * min ( -M(i,j) / M(j,i) , -M(j,i) / M(i,j) )
          endif
          if(test.lt.worst) worst = test
        end do
      end do

      antisym = worst

      return
      end



      double precision function simple(m,n,X,Y,List,Px,Py,Z)
      
      implicit none
      integer m, n, i, j, List(n)
      real X(m), Y(m), xn, yn
      double precision Px(n,n), Py(n,n), Z(n,n), test, worst, lhs, rhs

      worst = 0.0

      do i = 1, n
        lhs = 0.0d0; rhs = 0.0d0
        do j = 1, n
          xn = X(List(j))
          yn = Y(List(j))
          call Normal(xn,yn)
          lhs = lhs + Z(i,j)
          rhs = rhs - Px(i,j) * xn - Py(i,j) * yn
        end do
        
        if((abs(lhs).lt.0.1e-5).and.(abs(rhs).lt.0.1e-5)) then
          test = 100.0
        else if((abs(lhs).lt.0.1e-5).or.(abs(rhs).lt.0.1e-5)) then
          test = 0.0
        else
          test = 100.0 * min ( lhs / rhs , rhs / lhs )
        endif
        if(test.lt.worst) worst = test
        
      end do

      simple = worst

      return
      end



      double precision function sym(maxnid,M)
      
      use CONTROL, only : o
      
      implicit none
      integer maxnid, i, j, n
      double precision M(o:maxnid,o:maxnid), test, worst

      !n = size(M); n = floor(sqrt(real(n)))

      n = maxnid

      worst = 100.0

      do i = o, n
        do j = o, n
          if(i.eq.j) then
            test = 100.0
          else if((abs(M(j,i)).lt.0.1e-3).and.(abs(M(i,j)).lt.0.1e-3)) then
            test = 100.0
          else if((abs(M(j,i)).lt.0.1e-3).or.(abs(M(i,j)).lt.0.1e-3)) then
            test = 0.0
          else
            test = 100.0 * min ( M(i,j) / M(j,i) , M(j,i) / M(i,j) )
          endif
          if(test.lt.worst) worst = test
        end do
      end do

      sym = worst

      return
      end



      double precision function sysym(m,n,S,Tipe)
      
      implicit none
      integer*8 n
      integer i, j, m
      double precision S(m,m), test, worst
      character*1 Tipe(m)

      worst = 100.0d0

      do i = 1, n
        do j = 1, n

          ! Matrix elements on the diagonal are automatically symmetric ...
          if(i.eq.j) then
            test = 100.0d0

          ! Dirichlet rows / columns are excused ...
          else if((Tipe(i).eq.'D').or.(Tipe(j).eq.'D').or.(Tipe(i).eq.'E').or.(Tipe(j).eq.'E')) then
            test = 100.0d0

          ! ... as is anything in a row - column corresponding to a pressure special.
          else if( ((Tipe(i).eq.'T').or.(Tipe(j).eq.'T').or.(Tipe(i).eq.'W').or.(Tipe(j).eq.'W')) &
                           .and.((i.gt.2*m/3).or.(j.gt.2*m/3)) ) then
            test = 100.0d0

          ! Very small values don't have to be exactly equal ...
          else if((abs(S(j,i)).lt.0.1e-5).and.(abs(S(i,j)).lt.0.1e-5)) then
            test = 100.0d0

          ! ... but they DO have to be symmetrically placed !
          else if((abs(S(j,i)).lt.0.1e-5).or.(abs(S(i,j)).lt.0.1e-5)) then
            test = 0.0d0

          ! Here the actual testing ...
          else
            test = 100.0d0 * min ( S(i,j) / S(j,i) , S(j,i) / S(i,j) )
          endif

          ! Record the worst case.
          if(test.lt.worst) worst = test
        end do
      end do

      sysym = worst

      return
      end



      subroutine Integral_Test(maxnod,maxnid,List,Jist,Pist,X,Y,Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy,  &
                                                                        Wxx,Wxy,Wyy,T,Px,Py,Qx,Qy,Z,Ex,Ey,L,chk)

      use CONTROL, only : disgal, alph, odv, axisym, rifsym, o
      use PLACE, only : rr

      implicit none
      integer maxnod, maxnid, List(maxnid), Jist(maxnid), Pist(maxnid), err, count
      integer i, j, ii, iii, jj, it, jt
      real X(maxnod), Y(maxnod), len, sx, sy, xs, ys, xd, yd

      real UU_ana, VV_ana, PP_ana, TTx_ana, TTy_ana, TT_ana, xn, yn
    
      ! Integral Matrices
      double precision Vxx(o:maxnid,o:maxnid),Vxy(o:maxnid,o:maxnid),Vyy(o:maxnid,o:maxnid)
      double precision Kxx(o:maxnid,o:maxnid),Kxy(o:maxnid,o:maxnid),Kyy(o:maxnid,o:maxnid)
      double precision Lxx(o:maxnid,o:maxnid),Lxy(o:maxnid,o:maxnid),Lyy(o:maxnid,o:maxnid)
      double precision Wxx(o:maxnid,o:maxnid),Wxy(o:maxnid,o:maxnid),Wyy(o:maxnid,o:maxnid)
      double precision T(o:maxnid,o:maxnid), Z(o:maxnid,o:maxnid), L(3*maxnod)
      double precision Px(o:maxnid,o:maxnid),Py(o:maxnid,o:maxnid),Qx(o:maxnid,o:maxnid),Qy(o:maxnid,o:maxnid)
      double precision Ex(o:maxnid), Ey(o:maxnid), norm(5)
      double precision, allocatable:: lhs(:), rhs(:)
      double precision worst, sym, antisym, adj, ave, glen, simple, rowsum, lig, rig
      logical chk

      norm = 0.0d0; xd = 0.2; yd = 0.2

      allocate(lhs(o:maxnid),rhs(o:maxnid),stat=err)
      if(err.ne.0) then
        print*,'Error: Not enough memory for the BEM matrix tests -no testing done.'
        return
      endif
      print*,' '

      ! Set-up little flag on output
      if(chk) then
        err = 2
      else
        err = 1
      endif

      ! Determine Length of Gamma Boundary from Boundary Mass Matrix Entries
      glen = 0.0
      do i = o, maxnid
        do j = o, maxnid
          glen = glen + T(i,j)
        end do
      end do

      write(*,'(a15,f5.2)') 'Gamma Length = ',glen !,'  Gamma Radius = ',glen/(2.0*atan2(0.0,-1.0))

! TEST 1
      lhs = 0.0d0; rhs = 0.0d0; worst = 0.0d0; ave = 0.0d0; count = 0; lig = 0.0d0; rig = 0.0d0

      do i = o, maxnid
        ii = i; iii = ii + 1
        call qmap(maxnid,ii,sx,sy); call qmap(maxnid,iii,xs,ys)
        len = ((sx*X(List(ii))-xs*X(List(iii)))**2+(sy*Y(List(ii))-ys*Y(List(iii)))**2)
        do j = o, maxnid
          jj = j
          call qmap(maxnid,jj,sx,sy)
          if(chk.and.disgal) then
            lhs(i) = lhs(i) + Vxx(i,j) * L( rr(1,Pist(j)) ) + Vxy(i,j) * L( rr(2,Pist(j)) ) &
                            - Kxx(i,j) * L( rr(1,Jist(j)) ) - Kxy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) - T(i,j) * L( rr(1,Jist(j)) )
          else if(chk) then
            lhs(i) = lhs(i) + Vxx(i,j) * L( rr(1,Jist(j)) ) + Vxy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) + Kxx(i,j) * L( rr(1,List(j)) ) + Kxy(i,j) * L( rr(2,List(j)) )
          !else if(.false.) then
          !  lhs(i) = lhs(i) + Vxx(i,j) * Ex(j)*TT_ana(X(List(j)),Y(List(j))) + Vxy(i,j) * Ey(j)*TT_ana(X(List(j)),Y(List(j))) &
          !                  - Kxx(i,j) *  UU_ana(X(List(j)),Y(List(j))) - Kxy(i,j) *  VV_ana(X(List(j)),Y(List(j)))
          !  rhs(i) = rhs(i) - T(i,j) * UU_ana(X(List(j)),Y(List(j)))
          else
            xs = xd+sx*X(List(jj)); ys = yd+sy*Y(List(jj))
            lhs(i) = lhs(i) + Vxx(i,j) * TTx_ana(xs,ys) + Vxy(i,j) * TTy_ana(xs,ys) &
                            - Kxx(i,j) *  UU_ana(xs,ys) - Kxy(i,j) *  VV_ana(xs,ys)
            rhs(i) = rhs(i) - T(i,j) * UU_ana(xs,ys)
            if(abs(lhs(i)).gt.lig) lig = abs(lhs(i)); if(abs(rhs(i)).gt.rig) rig = abs(rhs(i))
          endif
        end do

        ! Build-up the norm
        norm(1) = norm(1) + len * ( lhs(i) - rhs(i) ) ** 2

        if(modulo(i,odv).eq.0) then
          lhs(i) = (1.0+alph) * lhs(i)
          rhs(i) = (1.0+alph) * rhs(i)
        else
          lhs(i) = (1.0-alph) * lhs(i)
          rhs(i) = (1.0-alph) * rhs(i)
        endif
        if(chk) then
          lhs(i) = L( rr(1,Jist(i)) )
          rhs(i) = UU_ana(sx*X(List(i)),sy*Y(List(i)))
        endif
        !if(disgal) then
        !  lhs(i) = Ex(i)*TT_ana(X(List(i)),Y(List(i)))
        !  rhs(i) =      TTx_ana(X(List(i)),Y(List(i)))
        !endif
        !call stuff(o,maxnid,lhs(i),rhs(i),worst,ave,count,.false.)
      end do

      if(.not.axisym) then
        lig = 1.0d0; rig = 1.0d0
      end if
      call graph(maxnid-o+1,lhs/lig,rhs/rig,'['//'aA'(err:err)//']')
      write(*,98)                               '<'//'aA'(err:err)//'> = ',worst,ave

! TEST 2
      lhs = 0.0d0; rhs = 0.0d0; worst = 0.0d0; ave = 0.0d0; count = 0; lig = 0.0d0; rig = 0.0d0

      do i = o, maxnid
        ii = i; iii = ii + 1
        call qmap(maxnid,ii,sx,sy); call qmap(maxnid,iii,xs,ys)
        len = ((sx*X(List(ii))-xs*X(List(iii)))**2+(sy*Y(List(ii))-ys*Y(List(iii)))**2)
        do j = o, maxnid
          jj = j
          call qmap(maxnid,jj,sx,sy)
          if(chk.and.disgal) then
            lhs(i) = lhs(i) + Vxy(i,j) * L( rr(1,Pist(j)) ) + Vyy(i,j) * L( rr(2,Pist(j)) ) &
                            - Kxy(i,j) * L( rr(1,Jist(j)) ) - Kyy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) - T(i,j) * L( rr(2,Jist(j)) )
          else if(chk) then
            lhs(i) = lhs(i) + Vxy(i,j) * L( rr(1,Jist(j)) ) + Vyy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) + Kxy(i,j) * L( rr(1,List(j)) ) + Kyy(i,j) * L( rr(2,List(j)) )
          !else if(.false.) then
          !  lhs(i) = lhs(i) + Vxy(i,j) * Ex(j)*TT_ana(X(List(j)),Y(List(j))) + Vyy(i,j) * Ey(j)*TT_ana(X(List(j)),Y(List(j))) &
          !                  - Kxy(i,j) *  UU_ana(X(List(j)),Y(List(j))) - Kyy(i,j) *  VV_ana(X(List(j)),Y(List(j)))
          !  rhs(i) = rhs(i) - T(i,j) * VV_ana(X(List(j)),Y(List(j)))
          else
            xs = xd+sx*X(List(jj)); ys = yd+sy*Y(List(jj))
            if(axisym) then
              it = j; jt = i
            else
              it = i; jt = j
            end if
            lhs(i) = lhs(i) + Vxy(it,jt) * TTx_ana(xs,ys) + Vyy(i,j) * TTy_ana(xs,ys) &
                            - Kxy(it,jt) *  UU_ana(xs,ys) - Kyy(i,j) *  VV_ana(xs,ys)
            rhs(i) = rhs(i) - T(i,j) * VV_ana(xs,ys)
            if(abs(lhs(i)).gt.lig) lig = abs(lhs(i)); if(abs(rhs(i)).gt.rig) rig = abs(rhs(i))
          endif
        end do

        ! Build-up the norm
        norm(2) = norm(2) + len * ( lhs(i) - rhs(i) ) ** 2

        if(modulo(i,odv).eq.0) then
          lhs(i) = (1.0+alph) * lhs(i)
          rhs(i) = (1.0+alph) * rhs(i)
        else
          lhs(i) = (1.0-alph) * lhs(i)
          rhs(i) = (1.0-alph) * rhs(i)
        endif
        if(chk) then
          lhs(i) = L( rr(2,Jist(i)) )
          rhs(i) = VV_ana(sx*X(List(i)),sy*Y(List(i)))
        endif
        !if(disgal) then
        !  lhs(i) = Ey(i)*TT_ana(X(List(i)),Y(List(i)))
        !  rhs(i) =      TTy_ana(X(List(i)),Y(List(i)))
        !endif
        !call stuff(maxnid,lhs(i),rhs(i),worst,ave,count,.false.)
      end do

      if(.not.axisym) then
        lig = 1.0d0; rig = 1.0d0
      end if
      call graph(maxnid-o+1,lhs/lig,rhs/rig,'['//'bB'(err:err)//']')
      write(*,98)                               '<'//'bB'(err:err)//'> = ',worst,ave

! TEST 3
      lhs = 0.0d0; rhs = 0.0d0; worst = 0.0d0; ave = 0.0d0; count = 0; lig = 0.0d0; rig = 0.0d0

      do i = o, maxnid
        ii = i; iii = ii + 1
        call qmap(maxnid,ii,sx,sy); call qmap(maxnid,iii,xs,ys)
        len = ((sx*X(List(ii))-xs*X(List(iii)))**2+(sy*Y(List(ii))-ys*Y(List(iii)))**2)
        do j = o, maxnid
          jj = j
          call qmap(maxnid,jj,sx,sy)
          if(chk.and.disgal) then
            lhs(i) = lhs(i) - Lxx(i,j) * L( rr(1,Pist(j)) ) - Lxy(i,j) * L( rr(2,Pist(j)) ) &
                            + Wxx(i,j) * L( rr(1,Jist(j)) ) + Wxy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) + T(j,i) * L( rr(1,Pist(j)) )
          else if(chk) then
            lhs(i) = lhs(i) - Lxx(i,j) * L( rr(1,Jist(j)) ) + Lxy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) + Wxx(i,j) * L( rr(1,List(j)) ) + Wxy(i,j) * L( rr(2,List(j)) )
          else if(.false.) then
            lhs(i) = lhs(i) - Lxx(i,j) * Ex(j)*TT_ana(X(List(j)),Y(List(j))) - Lxy(i,j) * Ey(j)*TT_ana(X(List(j)),Y(List(j))) &
                            + Wxx(i,j) *  UU_ana(X(List(j)),Y(List(j))) + Wxy(i,j) *  VV_ana(X(List(j)),Y(List(j)))
            rhs(i) = rhs(i) + T(j,i) * Ex(j)*TT_ana(X(List(j)),Y(List(j)))
          else
            xs = xd+sx*X(List(jj)); ys = yd+sy*Y(List(jj))
            lhs(i) = lhs(i) - Lxx(i,j) * TTx_ana(xs,ys) - Lxy(i,j) * TTy_ana(xs,ys) &
                            + Wxx(i,j) *  UU_ana(xs,ys) + Wxy(i,j) *  VV_ana(xs,ys)
            rhs(i) = rhs(i) + T(j,i) * TTx_ana(xs,ys)
            !lhs(i) = lhs(i) + Wxx(i,j) + Wxy(i,j)
            !rhs(i) = rhs(i) + Kxx(i,j) + T(i,j) + Kxy(i,j)
            if(abs(lhs(i)).gt.lig) lig = abs(lhs(i)); if(abs(rhs(i)).gt.rig) rig = abs(rhs(i))
          endif
        end do

        ! Build-up the norm
        norm(3) = norm(3) + len * ( lhs(i) - rhs(i) ) ** 2

        if(modulo(i,odv).eq.0) then
          lhs(i) = (1.0+alph) * lhs(i)
          rhs(i) = (1.0+alph) * rhs(i)
        else
          lhs(i) = (1.0-alph) * lhs(i)
          rhs(i) = (1.0-alph) * rhs(i)
        endif
        if(chk) then
          lhs(i) = L( rr(1,Pist(i)) )
          rhs(i) = TTx_ana(sx*X(List(i)),sy*Y(List(i)))
        endif
        !call stuff(maxnid,lhs(i),rhs(i),worst,ave,count,.false.)
      end do

      if(.not.axisym) then
        lig = 1.0d0; rig = 1.0d0
      end if
      call graph(maxnid-o+1,lhs/lig,rhs/rig,'['//'cC'(err:err)//']')
      write(*,98)                               '<'//'cC'(err:err)//'> = ',worst,ave

! TEST 4
      lhs = 0.0d0; rhs = 0.0d0; worst = 0.0d0; ave = 0.0d0; count = 0; lig = 0.0d0; rig = 0.0d0

      do i = o, maxnid
        ii = i; iii = ii + 1
        call qmap(maxnid,ii,sx,sy); call qmap(maxnid,iii,xs,ys)
        len = ((sx*X(List(ii))-xs*X(List(iii)))**2+(sy*Y(List(ii))-ys*Y(List(iii)))**2)
        do j = o, maxnid
          jj = j
          call qmap(maxnid,jj,sx,sy)
          if(chk.and.disgal) then
            lhs(i) = lhs(i) - Lxy(i,j) * L( rr(1,Pist(j)) ) - Lyy(i,j) * L( rr(2,Pist(j)) ) &
                            + Wxy(i,j) * L( rr(1,Jist(j)) ) + Wyy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) + T(j,i) * L( rr(2,Pist(j)) )
          else if(chk) then
            lhs(i) = lhs(i) + Lxy(i,j) * L( rr(1,Jist(j)) ) + Lyy(i,j) * L( rr(2,Jist(j)) )
            rhs(i) = rhs(i) + Wxy(i,j) * L( rr(1,List(j)) ) + Wyy(i,j) * L( rr(2,List(j)) )
          else if(.false.) then
            lhs(i) = lhs(i) - Lxy(i,j) * Ex(j)*TT_ana(X(List(j)),Y(List(j))) - Lyy(i,j) * Ey(j)*TT_ana(X(List(j)),Y(List(j))) &
                            + Wxy(i,j) *  UU_ana(X(List(j)),Y(List(j))) + Wyy(i,j) *  VV_ana(X(List(j)),Y(List(j)))
            rhs(i) = rhs(i) + T(j,i) * Ey(j)*TT_ana(X(List(j)),Y(List(j)))
          else
            xs = xd+sx*X(List(jj)); ys = yd+sy*Y(List(jj))
            if(axisym) then
              it = j; jt = i
            else
              it = i; jt = j
            end if
            lhs(i) = lhs(i) - Lxy(it,jt) * TTx_ana(xs,ys) - Lyy(i,j) * TTy_ana(xs,ys) &
                            + Wxy(it,jt) *  UU_ana(xs,ys) + Wyy(i,j) *  VV_ana(xs,ys)
            rhs(i) = rhs(i) + T(j,i) * TTy_ana(xs,ys)
            !lhs(i) = lhs(i) + Wxy(i,j) + Wyy(i,j)
            !rhs(i) = rhs(i) + Kxy(i,j) + Kyy(i,j) + T(i,j)
            if(abs(lhs(i)).gt.lig) lig = abs(lhs(i)); if(abs(rhs(i)).gt.rig) rig = abs(rhs(i))
          endif
        end do

        ! Build-up the norm
        norm(4) = norm(4) + len * ( lhs(i) - rhs(i) ) ** 2

        if(modulo(i,odv).eq.0) then
          lhs(i) = (1.0+alph) * lhs(i)
          rhs(i) = (1.0+alph) * rhs(i)
        else
          lhs(i) = (1.0-alph) * lhs(i)
          rhs(i) = (1.0-alph) * rhs(i)
        endif
        if(chk) then
          lhs(i) = L( rr(2,Pist(i)) )
          rhs(i) = TTy_ana(sx*X(List(i)),sy*Y(List(i)))
        endif
        !call stuff(maxnid,lhs(i),rhs(i),worst,ave,count,.false.)
      end do

      if(.not.axisym) then
        lig = 1.0d0; rig = 1.0d0
      end if
      call graph(maxnid-o+1,lhs/lig,rhs/rig,'['//'dD'(err:err)//']')
      write(*,98)                               '<'//'dD'(err:err)//'> = ',worst,ave

      if(axisym.or.rifsym) goto 88

! TEST 5
      lhs = 0.0; rhs = 0.0; worst = 0.0; ave = 0.0; count = 0!; ordv = 0
      do i = o, maxnid
        ii = i; iii = ii + 1
        call qmap(maxnid,ii,sx,sy); call qmap(maxnid,iii,xs,ys)
        len = ((sx*X(List(ii))-xs*X(List(iii)))**2+(sy*Y(List(ii))-ys*Y(List(iii)))**2)
        if(Z(i,i).eq.0.0) then
         ! ordv = ordv + 1
          cycle
        endif
        do j = o, maxnid
          jj = j
          call qmap(maxnid,jj,sx,sy)
          if(chk.and.disgal) then
            lhs(i) = lhs(i) - T(j,i)  * L( rr(3,List(j)) )
            rhs(i) = rhs(i) - Qx(i,j) * L( rr(1,List(j)) ) - Qy(i,j) * L( rr(2,List(j)) ) &
                            + Px(i,j) * L( rr(1,Jist(j)) ) + Py(i,j) * L( rr(2,Jist(j)) )
          else if(chk) then

          else if(disgal) then
            lhs(i) = lhs(i) -  T(j,i) *  PP_ana(X(List(j)),Y(List(j))) &
                            - Px(i,j) * TTx_ana(X(List(j)),Y(List(j))) - Py(i,j) * TTy_ana(X(List(j)),Y(List(j)))
            rhs(i) = rhs(i) + Qx(i,j) *  UU_ana(X(List(j)),Y(List(j))) + Qy(i,j) *  VV_ana(X(List(j)),Y(List(j)))
          else
            xs = xd+sx*X(List(jj)); ys = yd+sy*Y(List(jj))
            lhs(i) = lhs(i) -  Z(i,j) *  PP_ana(xs,ys) &
                            - Px(i,j) * TTx_ana(xs,ys) - Py(i,j) * TTy_ana(xs,ys)
            rhs(i) = rhs(i) + Qx(i,j) *  UU_ana(xs,ys) + Qy(i,j) *  VV_ana(xs,ys)
          endif
        end do

        ! Build-up the norm
        norm(5) = norm(5) + len * ( lhs(i) - rhs(i) ) ** 2

      !  call stuff(maxnid-ordv,lhs(i),rhs(i),worst,ave,count,chk)
      end do

      call graph(maxnid-o+1,lhs,rhs,'['//'pP'(err:err)//']')
      write(*,98)                               '<'//'pP'(err:err)//'> = ',worst,ave

      if(.not.chk) goto 88

! TEST 5
      lhs = 0.0; rhs = 0.0; worst = 0.0; ave = 0.0; count = 0!; ordv = 0
      do i = o, maxnid
        if(Z(i,i).eq.0.0) then
  !        ordv = ordv + 1
          cycle
        endif
        lhs(i) = L( rr(3,List(i)) )
        rhs(i) = L( rr(3,Jist(i)) ) 
    !    call stuff(maxnid-ordv,lhs(i),rhs(i),worst,ave,count,chk)
      end do

      call graph(maxnid-o+1,lhs,rhs,'[S]')
      write(*,98)                               '<S> = ',worst,ave

! TEST 6

      lhs = 0.0; rhs = 0.0; worst = 0.0; ave = 0.0; count = 0; !ordv = 0
      do i = o, maxnid
        if(Z(i,i).eq.0.0) then
 !         ordv = ordv + 1
          cycle
        endif
        do j = o, maxnid
          jj = j
          call qmap(maxnid,jj,sx,sy)
          xn = xs; yn = ys
          call Normal(xn,yn)
          lhs(i) = lhs(i) + Z(i,j)
          rhs(i) = rhs(i) - Px(i,j) * xn - Py(i,j) * yn
        end do
!        call stuff(maxnid-ordv,lhs(i),rhs(i),worst,ave,count,.false.)
      end do

      call graph(maxnid-o+1,lhs,rhs,'['//'zZ'(err:err)//']')
      write(*,98)                               '<'//'zZ'(err:err)//'> = ',worst,ave

   88 if(chk) return

      write(*,99) 'Matrix Vxx is   ',sym(maxnid,Vxx),' % Symmetric.'
      if(axisym) &
      write(*,99) 'Matrix Vxy is   ',adj(maxnid,Vxy,Px),' % the Adjoint of Vyx.'
      write(*,99) 'Matrix Vxy is   ',sym(maxnid,Vxy),' % Symmetric.'
      write(*,99) 'Matrix Vyy is   ',sym(maxnid,Vyy),' % Symmetric.'
      print*,' '

      write(*,99) 'Matrix Wxx is   ',sym(maxnid,Wxx),' % Symmetric.'
      if(axisym) &
      write(*,99) 'Matrix Wxy is   ',adj(maxnid,Wxy,Qy),' % the Adjoint of Wyx.'
      write(*,99) 'Matrix Wxy is   ',sym(maxnid,Wxy),' % Symmetric.'
      write(*,99) 'Matrix Wyy is   ',sym(maxnid,Wyy),' % Symmetric.'
      print*,' '

      write(*,99) 'Matrix T is   ',sym(maxnid,T),' % Symmetric.'
      write(*,99) 'Matrix Z is   ',sym(maxnid,Z),' % Symmetric.'
      print*,' '

      !write(*,99) 'Matrix Qx is   ',sym(maxnid,Qx),' % Symmetric.'
      !write(*,99) 'Matrix Qy is   ',sym(maxnid,Qy),' % Symmetric.'
      !print*,' '
      !write(*,99) 'Matrix Qx is   ',antisym(maxnid,Qx),' % Anti-Symmetric.'
      !write(*,99) 'Matrix Qy is   ',antisym(maxnid,Qy),' % Anti-Symmetric.'
      !print*,' '

      ! Discontinuous Galerkin requires a Transpose
      if(disgal) then

        ! Store original Z
        do i = 1, maxnid
          lhs(i) = Z(i,i)
        end do

        ! Transpose T into Z
        do i = 1, maxnid
          do j = 1, maxnid
            Z(i,j) = T(j,i)
          end do
        end do

        ! Do Adjoint Tests with Z instead of T ( ie: with transpose T )
        write(*,99) 'Matrix Kxx-I is ',adj(maxnid,Kxx-T,Lxx-Z),' % the Adjoint of Lxx-I.'
        write(*,99) 'Matrix Kxy is   ',adj(maxnid,Kxy,Lxy),' % the Adjoint of Lxy.'
        write(*,99) 'Matrix Kyy-I is ',adj(maxnid,Kyy-T,Lyy-Z),' % the Adjoint of Lyy-I.'
        print*,' '

        ! Restore original Z values
        Z = 0.0D0
        do i = 1, maxnid
          Z(i,i) = lhs(i)
        end do

      else

        if(axisym) then
          write(*,99) 'Matrix Kxy is ',adj(maxnid,Kxy,Qx),' % the Adjoint of Lyx.'
          write(*,99) 'Matrix Kyx is ',adj(maxnid,Py,Lxy),' % the Adjoint of Lxy.'
        else
          write(*,99) 'Matrix Kxx-I is ',adj(maxnid,Kxx-T,Lxx-T),' % the Adjoint of Lxx-I.'
          write(*,99) 'Matrix Kxy is   ',adj(maxnid,Kxy,Lxy),' % the Adjoint of Lxy.'
          write(*,99) 'Matrix Kyy-I is ',adj(maxnid,Kyy-T,Lyy-T),' % the Adjoint of Lyy-I.'
        end if
        print*,' '

      endif

      !write(*,99) 'Matrix Px is   ',antisym(maxnid,Px),' % Anti-Symmetric.'
      !write(*,99) 'Matrix Py is   ',antisym(maxnid,Py),' % Anti-Symmetric.'
      !write(*,99) 'Matrix Qx is   ',sym(maxnid,Qx),' % Symmetric.'
      !write(*,99) 'Matrix Qy is   ',sym(maxnid,Qy),' % Symmetric.'
      !write(*,99) 'Matrix Qx is   ',rowsum(maxnid,Qx),' % Row sum zero.'
      !write(*,99) 'Matrix Qy is   ',rowsum(maxnid,Qy),' % Row sum zero.'

      !write(*,99) 'P test is      ',simple(maxnod,maxnid,X,Y,List,Px,Py,Z),' % satisfied.'

      do i = 1, 5
        if(norm(i).gt.0.0) then
          norm(i) = sqrt(norm(i))
        else
          norm(i) = 0.0
        endif
      end do

      write(*,'(a13,5f15.8)') 'Test Nurms = ',norm

      print*,' '

   98 format(a6,2f7.3)
   99 format(a16,f8.3,a25)

      return
      end




      subroutine stuff(maxnid,lhs,rhs,worst,ave,count,output)

      implicit none
      integer maxnid, count
      double precision lhs, rhs, worst, ave, test, maxi, mini
      logical output
      save maxi, mini

      if(count.eq.0) maxi = -999.9d9; mini = -maxi

      ! Establish the extreme values
      maxi = max(maxi,lhs,rhs)
      mini = min(mini,lhs,rhs)

      ! Test error specification
      test = abs( lhs - rhs )

      ! Accumulate for average calculation
      ave = ave + test
      count = count + 1

      ! Keep track of the worst offender
      if(test.gt.worst) worst = test

      ! Print out values if requested
      if(output) write(*,'(4(e12.4,a5))') lhs,'     ',rhs,'     ',lhs/rhs,'     ',rhs/lhs

      ! Finallize the average and worst values on loop completion
      if(count.eq.maxnid) then
        ave   = 100.0 * ( 1.0 - (ave/count) / ( maxi - mini ) )
        worst = 100.0 * ( 1.0 -    worst    / ( maxi - mini ) )
      endif

      return
      end



      subroutine Template_Write(R,S,T)

      integer i, j, k, l
      double precision S(10,3,3,10), T(10,10), R(10,3,10)

      print*,"T"
      write(*,'(10(10(f10.5,3x),/))') ((T(i,j),j=1,10),i=1,10)

      do 10 k=1,3
        do 5 l=1,3
            print*,"S",k,l
            write(*,'(10(10(f10.5,3x),/))') ((S(i,k,l,j),j=1,10),i=1,10)
    5   continue
   10 continue
   
      do 15 k=1,3
        print*,"R",k
        write(*,'(10(10(f10.5,3x),/))') ((R(i,k,j),j=1,10),i=1,10)
   15 continue

      return
      end



      subroutine Matrix_Write(maxnod,Type,S)!,P)         
 
      implicit none
      integer maxnod, i, j
      double precision S(3*maxnod,3*maxnod)!, P(3*maxnod)
      character*1 Type(maxnod)
      character*2 ngis

      open(55,file='full.dat')
 
      ! Type Line
      write(55,'(900(a2))') &
      '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '# '

      ! Data Lines
      do i = 1, maxnod
      write(55,'(900(a2))') &
      Type(i)//',' , (ngis(S(i,j)),j=1,maxnod) , Type(i)//',' , (ngis(S(i,j)),j=maxnod+1,2*maxnod) , &
                                                   Type(i)//',' , (ngis(S(i,j)),j=2*maxnod+1,3*maxnod) , Type(i)//' '
      end do

      ! Type Line
      write(55,'(900(a2))') &
      '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '# '

      ! Data Lines
      do i = 1, maxnod
      write(55,'(900(a2))') &
      Type(i)//',' , (ngis(S(maxnod+i,j)),j=1,maxnod) , Type(i)//',' , (ngis(S(maxnod+i,j)),j=maxnod+1,2*maxnod) , &
                                                   Type(i)//',' , (ngis(S(maxnod+i,j)),j=2*maxnod+1,3*maxnod) , Type(i)//','
      end do

      ! Type Line
      write(55,'(900(a2))') &
      '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '# '

      ! Data Lines
      do i = 1, maxnod
      write(55,'(900(a2))') &
      Type(i)//',' , (ngis(S(2*maxnod+i,j)),j=1,maxnod) , Type(i)//',' , (ngis(S(2*maxnod+i,j)),j=maxnod+1,2*maxnod) , &
                                                   Type(i)//',' , (ngis(S(2*maxnod+i,j)),j=2*maxnod+1,3*maxnod) , Type(i)//','
      end do

      ! Type Line
      write(55,'(900(a2))') &
      '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '#,' , (Type(j)//',',j=1,maxnod) , '# '

      close(55)
 
      return
      end



      character*2 function ngis(r)

      double precision r

      if(abs(r).gt.0.0d0) then
        ngis = 'X,'
      else
        ngis = ' ,'
      endif

      return
      end


      real function stepfn(y)
      ! Gives a graduated freezing of the mesh nodes towards the tip of the droplet during explosions

      use CONTROL, only : step, fitaa, meshfreeze

      implicit none

      real y

      if(meshfreeze) then
        stepfn = step * ( 1.0 - min( 1.0 , y / fitaa ) ** 4 )
      else
        stepfn = step
      end if

      end function stepfn



      subroutine Nudge(Connect,List,Jist,Pist,X,Y,L,P,Q,Type,R,totvolu)

      use CONTROL, only : rho, disgal, axisym, step, tol, omeg, coun, bres, prefix, fish, onedekink, clipvel, sphereish
      use CONTROL, only : relaxing, voluhold, inflex, inflexion, transform
      use PLACE, only : oor, rr, rrr, oc, noderoot, isroot, isvroot, HX, HY
      use SIZES, only : maxnod, maxsid, maxele

      implicit none

      integer i, j, jj, jjj
      integer Connect(maxele,10)
      integer List(maxnod), Jist(maxnod), Pist(maxnod)
      real X(-maxsid+1:maxnod), Y(-maxsid+1:maxnod), stepfn
      double precision P(-3*maxsid+1:3*maxnod), Q(2*maxnod), L(2*maxnod), R(3,maxele), valx, valy, vx, vy, totvolu
      character*1 Type(maxnod)
      logical set
      external stepfn

      ! Dis-continuous Galerkin (DG) mesh advancements
      if(disgal) then

        ! Preliminary loop to set-up default positions and initialize the box counters
        do i = 1, maxnod
          List(i) = 0; Jist(i) = i; Pist = 0
        end do

        ! Initialize buddy node counter
        jj = 0

        ! Sync. loop - for disgal only, need to establish "R" / "J" buddy pairs before calculating the actual node nudges
        do i = 1, maxnod

          ! Keep y-nudges of R nodes and their J buddies in sync
          ! ( whichever of a pair is arrived at first gets to set the nudge ... )
          if((index('JR',Type(i)).ne.0).and.(abs(X(i)).lt.1.3*bres)) then
            set = .false.
            do j = 1, jj
              if(abs(Y(i)-L(j)).lt.tol) then
                ! ... too late, use the other node's nudge
                Jist(noderoot(i)) = Pist(j)
                set = .true.
              end if
            end do
            if(set) then
              cycle
            else
              ! The first to arrive, can thus set the nudge to be used by the buddy node ...
              jj = jj + 1
              L(jj) = Y(i)
              Pist(jj) = noderoot(i)
            end if
          end if

          ! Store flags for edge nodes (and 'honoury' edge nodes)
          if((index('XY',Type(i)).ne.0).or.((Type(i).eq.'J').and.(X(i).gt.tol).and.(Y(i).gt.tol))) List(noderoot(i)) = 1
          !if(((Type(i).eq.'J').and.(Jist(i).eq.i)).or.((Type(i).eq.'R').and.(Jist(i).ne.i))) List(noderoot(i)) = 1

        ! End of sync. loop
        end do

        ! Initialize nudges and their counts
        Pist = 0; Q = 0.0d0

        ! Loop the elements
        do i = 1, maxele

          ! Don't bother nudging "specials"
          if(R(1,i).gt.599) cycle

          ! Loop the nodes
          do j = 1, 3

            ! "Other" nodes of element
            jj = modulo(j+1-1,3)+1; jjj = modulo(j+2-1,3)+1

            ! Edge nodes require an average velocity
            if( List(  noderoot(Connect(i,j)) ) .ne. 0 ) then

              ! Value of velocity at Node's position
              valx = P(rr(1,Connect(i,jj))) + P(rr(1,Connect(i,jjj))) - P(rr(1,Connect(i,j)))
              valy = P(rr(2,Connect(i,jj))) + P(rr(2,Connect(i,jjj))) - P(rr(2,Connect(i,j)))

            ! Interior nodes require an average position
            else

              ! Value of mid-point of the side opposite the node concerned
              valx = ( X(noderoot(Connect(i,jj))) + X(noderoot(Connect(i,jjj))) ) / 2.0d0
              valy = ( Y(noderoot(Connect(i,jj))) + Y(noderoot(Connect(i,jjj))) ) / 2.0d0

            end if

            !call gmshSP(fil,X(noderoot(Connect(i,j))),Y(noderoot(Connect(i,j))),valy/100.0d0)

            ! x and y nudges for the element's j th node
            Q(rrr(1,noderoot(Connect(i,j)))) = Q(rrr(1,noderoot(Connect(i,j)))) + valx
            Q(rrr(2,noderoot(Connect(i,j)))) = Q(rrr(2,noderoot(Connect(i,j)))) + valy

            ! Count the additions to do an average at the end
            Pist(  noderoot(Connect(i,j)) ) = Pist(  noderoot(Connect(i,j)) ) + 1
          end do
        enddo

        ! Initialize mesh NODE velocity vector
        L = 0.0d0
        
        ! Compute the displacements
        do i = 1, maxnod

          ! Compute the averages (xval's NEVER aliased)
          j = noderoot(i)
          valx = Q(rrr(1,j)) / dble(Pist(j))
          j = Jist(j)
          valy = Q(rrr(2,j)) / dble(Pist(j))

          ! Check what's been averaged
!          if((rho.gt.0.0).and.isroot(i)) call gmshSP(fil,X(i),Y(i),valy/100.0d0)

          ! Record the initial node position
          L(rrr(1,i)) = dble( X(i) )
          L(rrr(2,i)) = dble( Y(i) )

          ! Edge nodes are nudged by the average velocity
          if(List(j).ne.0) then
            if(sphereish.and.fish.and.relaxing) then
              vx = dble(X(i)); vy = dble(Y(i))
              call fitsmooth(vx,vy)
              if(voluhold.lt.0.0d0) voluhold = totvolu
              vx = vx * ( 1.0d0 + voluhold / totvolu ) / 2.0d0
              vy = vy * ( 1.0d0 + voluhold / totvolu ) / 2.0d0
              X(i) = real(vx); Y(i) = real(vy)
            end if
            if(valx*valx+valy*valy.ge.clipvel*clipvel) then
            ! Advancements found from just a straight step on the velocities
            if((.not.axisym).or.(X(i).gt.tol)) X(i) =  X(i) + stepfn(Y(i)) * valx
            if((.not.axisym).or.(Y(i).gt.tol)) Y(i) =  Y(i) + stepfn(Y(i)) * valy
            end if
          ! Interior nodes ...
          else

            ! ... just assume the averaged position
            if((.not.axisym).or.(X(i).gt.tol)) X(i) =  valx
            if((.not.axisym).or.(Y(i).gt.tol)) Y(i) =  valy

          end if

        end do

        if(fish.or.onedekink) then
          if(onedekink) onedekink = .false.
          ! Check for (and remove) any kinks in the free surface
          List = -1
          !call Catch('O','J','X','Y',X,Y,Type,List,nods,eles,.true.)
          call Catch('J','K','X','Y',X(1),Y(1),Type(1),List,i,j,.true.)
          call dekinker(i,X(1),Y(1),List)
        end if

        do i = 1, maxnod

          if(inflex.and.inflexion.and.(.not.relaxing)) then
            call transform(X(i),Y(i))
          else

          ! Determine the mesh node velocity
          L(rrr(1,i)) = ( dble(X(i)) - L(rrr(1,i)) ) / dble(stepfn(Y(i)))
          L(rrr(2,i)) = ( dble(Y(i)) - L(rrr(2,i)) ) / dble(stepfn(Y(i)))

          end if
            
        end do

        ! Initialize the mesh EDGE velocity vector
        Q = 0.0d0

        if(.not.(inflex.and.inflexion.and.(.not.relaxing))) then

        ! Loop the elements
        do i = 1, maxele

          ! Don't bother with "specials"
          if(R(1,i).gt.599) cycle

          ! Loop the nodes
          do j = 1, 3

            ! "Other" nodes of element
            jj = modulo(j+1-1,3)+1; jjj = modulo(j+2-1,3)+1

            ! Mesh velocity at the edge mid-points the average of that at the nodes
            Q(rr(1,Connect(i,j))) = ( L(rrr(1,Connect(i,jj))) + L(rrr(1,Connect(i,jjj))) ) / 2.0d0
            Q(rr(2,Connect(i,j))) = ( L(rrr(2,Connect(i,jj))) + L(rrr(2,Connect(i,jjj))) ) / 2.0d0

          end do
        end do

        end if

      ! Continuous Galerkin (CG) mesh advancements
      else

        jj = 0

        ! Loop ALL the nodes to be advanced - including the negatively numbered "specials"
        do i = -maxsid+1, maxnod

          ! Don't nudge "T" nodes ...
          if(index('TWOS',Type(i)).ne.0) cycle

          ! Advancements  found from just a straight step on the velocities
          X(i) = X(i) + stepfn(Y(i)) * P( rr(1,i) )

          ! Keep y-nudges of R nodes and their J buddies in sync
          ! ( whichever of a pair is arrived at first gets to set the nudge ... )
          if((index('JR',Type(i)).ne.0).and.(abs(X(i)).lt.1.3*bres)) then
            set = .false.
            do j = 1, jj
              if(abs(Y(i)-L(j)).lt.tol) then
                ! ... too late, use the other node's nudge
                Y(i) = L(maxnod+j)
                set = .true.
              end if
            end do
            if(set) then
              cycle
            else
              ! The first to arrive, can thus set the nudge to be used by the buddy node ...
              jj = jj + 1
              L(jj) = Y(i)
              L(maxnod+jj) = Y(i) + stepfn(Y(i)) * P( rr(2,i) )
            end if
          end if

          Y(i) = Y(i) + stepfn(Y(i)) * P( rr(2,i) )

        end do

      ! End of if block on choice of conforming / non-conforming scheme
      endif

!      call gmshClose(fil)

      return
      end subroutine Nudge



      subroutine Output(X,Y,P,Connect,Type,List,Jist,nids,Pist,beg,fin,R)

      use CONTROL, only : ord, disgal, spec, troc, anal, prefix, fish, heat, drag
      use PLACE, only : rr, noderoot
      use SIZES, only : maxnod, maxsid, maxele

      implicit none
      integer z1, z2, z3, Connect(maxele,10), nids, Pist(nids)
      integer i, j, k, ii, jj, kk, iii, jjj, kkk, List(maxnod), Jist(maxnod), beg, fin, fil, sygn
      integer iiieq, jjjeq, kkkeq
      real X(-maxsid+1:maxnod), Y(-maxsid+1:maxnod), xx, yy, flat
      double precision P(-3*maxsid+1:3*maxnod), R(3,maxele)
      real scal(3), vect(3,2), P_ana, U_ana, V_ana, Tx_ana, Ty_ana
      character Type(-maxsid+1:maxnod)*1, label*420, filnam*120
      logical iiiIsTractionNode, jjjIsTractionNode, kkkIsTractionNode

      if((.not.fish).and.(fin.ne.40).and.(.not.drag)) return

      label = 'Diagnostics         '// & ! 40
              'Nummeric U Velocity '// & ! 41
              'Nummeric V Velocity '// & ! 42
              'Nummeric Pressure   '// & ! 43
              'Nummeric Temperature'// & ! 44
              'Analytic U Velocity '// & ! 45
              'Analytic V Velocity '// & ! 46
              'Analytic Pressure   '// & ! 47
              'Analytic Temperature'// & ! 48
              'U Velocity Error (%)'// & ! 49
              'V Velocity Error (%)'// & ! 50
              'Pressure Error (%)  '// & ! 51              
              'Everything          '// & ! 52
              'Outline             '// & ! 59
              'Divergence          '     ! 60

      filnam = 'diagX'//'nummU'//'nummV'//'nummP'//'nummT'//'analU'//'analV'//'analP'//'analT'//'diffU'//'diffV'//'diffP' &
               //'diffT'//'every'//'uxder'//'uyder'//'vxder'//'vyder'//'pxder'//'pyder'//'diver'//'norms'//'outln'//'diver'

      ! Loop over All Results, Nummeric, Analytic, Errors
      do fil = beg, fin

        ! No temperature output if no heating !!!
        if((.not.heat).and.((fil.eq.44).or.(fil.eq.48))) cycle

        open(fil,file=prefix//filnam(5*(fil-40)+1: 5*(fil-40+1))//'.pos')
        write(fil,'(a49)')'View "'//prefix//label(20*(fil-40)+1:20*(fil-40+1))//'" {'
 
        if(fil.eq.59) goto 99

        do i = 1, maxele
          if((spec.eq.0.0).and.(R(1,i).gt.599)) cycle
          do sygn = -1, 1, 2
            do z1 = ord -(sygn+1), (sygn-1)/(-2), -1
              do z3 = (sygn+1)/2, ord-z1-(sygn+1)/2
                z2=ord-z1-z3
                call Simplex(ord,ii,z1,z2,z3,.True.)          
                call Simplex(ord,jj,z1+sygn,z2-sygn,z3,.True.)       
                call Simplex(ord,kk,z1+sygn,z2,z3-sygn,.True.)
                iii=Connect(i,ii); jjj=Connect(i,jj); kkk=Connect(i,kk)
                iiiIsTractionNode = .false.; jjjIsTractionNode = .false.; kkkIsTractionNode = .false.
                iiieq = iii; jjjeq = jjj; kkkeq = kkk
                do j = 1, maxnod
                  k = Jist(j)
                  if(k.lt.0) exit
                  if(disgal.and.(Type(k).ne.'G')) cycle
                  if((troc.ne.0.0).and.(iii.eq.k)) then
                    iiiIsTractionNode = .true.
                    iiieq = List(j)
                  endif
                  if((troc.ne.0.0).and.(jjj.eq.k)) then
                    jjjIsTractionNode = .true.
                    jjjeq = List(j)
                  endif
                  if((troc.ne.0.0).and.(kkk.eq.k)) then
                    kkkIsTractionNode = .true.                  
                    kkkeq = List(j)
                  endif
                end do

                ! Initialise to Flat Plots with No Vectors
                scal = 0.0; flat = 0.0; vect = 0.0

                ! Set-Up the Appropriate Data to be Output
                if(fil.eq.40) then

                  ! Diagnostics Output Settings
                  !                              11111111112222222
                  !                     12345678901234567890123456
                  scal(1) = real(index('USXFKZBTQDYJWMRNCVLPIAEHGO',Type(iii)))
                  scal(2) = real(index('USXFKZBTQDYJWMRNCVLPIAEHGO',Type(jjj)))
                  scal(3) = real(index('USXFKZBTQDYJWMRNCVLPIAEHGO',Type(kkk)))
                  xx = X(noderoot(iii)); yy = Y(noderoot(iii)); call Normal(xx,yy); vect(1,1) = xx; vect(1,2) = yy
                  xx = X(noderoot(jjj)); yy = Y(noderoot(jjj)); call Normal(xx,yy); vect(2,1) = xx; vect(2,2) = yy
                  xx = X(noderoot(kkk)); yy = Y(noderoot(kkk)); call Normal(xx,yy); vect(3,1) = xx; vect(3,2) = yy

                else if(fil.eq.47) then

                  ! Analytic Pressure Output Settings
                  scal(1) = P_ana(X(iiieq),Y(iiieq))
                  scal(2) = P_ana(X(jjjeq),Y(jjjeq))
                  scal(3) = P_ana(X(kkkeq),Y(kkkeq))
                  flat = 1.0
                  vect(1,1) = U_ana(X(iiieq),Y(iiieq))
                  vect(1,2) = V_ana(X(iiieq),Y(iiieq))
                  vect(2,1) = U_ana(X(jjjeq),Y(jjjeq))
                  vect(2,2) = V_ana(X(jjjeq),Y(jjjeq))
                  vect(3,1) = U_ana(X(kkkeq),Y(kkkeq))
                  vect(3,2) = V_ana(X(kkkeq),Y(kkkeq))

                else if(fil.eq.45) then

                  ! Analytic U Velocity Output Settings
                  if(disgal.and.(R(1,i).lt.599)) then
                    vect(1,1) = 0.5 * ( X(jjjeq) + X(kkkeq) ); vect(1,2) = 0.5 * ( Y(jjjeq) + Y(kkkeq) )
                    vect(2,1) = 0.5 * ( X(kkkeq) + X(iiieq) ); vect(2,2) = 0.5 * ( Y(kkkeq) + Y(iiieq) )
                    vect(3,1) = 0.5 * ( X(iiieq) + X(jjjeq) ); vect(3,2) = 0.5 * ( Y(iiieq) + Y(jjjeq) )

                    vect(1,1) = U_ana(vect(1,1),vect(1,2))
                    vect(2,1) = U_ana(vect(2,1),vect(2,2))
                    vect(3,1) = U_ana(vect(3,1),vect(3,2))

                    scal(1) = vect(2,1) + vect(3,1) - vect(1,1)
                    scal(2) = vect(3,1) + vect(1,1) - vect(2,1)
                    scal(3) = vect(1,1) + vect(2,1) - vect(3,1)

                    vect = 0.0
                  else
                    scal(1) = U_ana(X(iiieq),Y(iiieq))
                    scal(2) = U_ana(X(jjjeq),Y(jjjeq))
                    scal(3) = U_ana(X(kkkeq),Y(kkkeq))
                  endif
                  if(iiiIsTractionNode) scal(1) = Tx_ana(X(iiieq),Y(iiieq))
                  if(jjjIsTractionNode) scal(2) = Tx_ana(X(jjjeq),Y(jjjeq))
                  if(kkkIsTractionNode) scal(3) = Tx_ana(X(kkkeq),Y(kkkeq))
                  flat = 1.0

                else if(fil.eq.46) then

                  ! Analytic V Velocity Output Settings
                  if(disgal.and.(R(1,i).lt.599)) then
                    vect(1,1) = 0.5 * ( X(jjjeq) + X(kkkeq) ); vect(1,2) = 0.5 * ( Y(jjjeq) + Y(kkkeq) )
                    vect(2,1) = 0.5 * ( X(kkkeq) + X(iiieq) ); vect(2,2) = 0.5 * ( Y(kkkeq) + Y(iiieq) )
                    vect(3,1) = 0.5 * ( X(iiieq) + X(jjjeq) ); vect(3,2) = 0.5 * ( Y(iiieq) + Y(jjjeq) )

                    vect(1,1) = V_ana(vect(1,1),vect(1,2))
                    vect(2,1) = V_ana(vect(2,1),vect(2,2))
                    vect(3,1) = V_ana(vect(3,1),vect(3,2))

                    scal(1) = vect(2,1) + vect(3,1) - vect(1,1)
                    scal(2) = vect(3,1) + vect(1,1) - vect(2,1)
                    scal(3) = vect(1,1) + vect(2,1) - vect(3,1)

                    vect = 0.0
                  else
                    scal(1) = V_ana(X(iiieq),Y(iiieq))
                    scal(2) = V_ana(X(jjjeq),Y(jjjeq))
                    scal(3) = V_ana(X(kkkeq),Y(kkkeq))
                  endif
                  if(iiiIsTractionNode) scal(1) = Ty_ana(X(iiieq),Y(iiieq))
                  if(jjjIsTractionNode) scal(2) = Ty_ana(X(jjjeq),Y(jjjeq))
                  if(kkkIsTractionNode) scal(3) = Ty_ana(X(kkkeq),Y(kkkeq))
                  flat = 1.0

                else if((fil.eq.43).or.(fil.eq.60)) then

                  ! Nummeric Pressure Output Settings (also used for divergence)
                  scal(1) = P( rr(3,iii) )
                  scal(2) = P( rr(3,jjj) )
                  scal(3) = P( rr(3,kkk) )
                  flat = 1.0
                  if(fil.ne.60) then
                  vect(1,1) = P( rr(1,iii) )
                  vect(1,2) = P( rr(2,iii) )
                  vect(2,1) = P( rr(1,jjj) )
                  vect(2,2) = P( rr(2,jjj) )
                  vect(3,1) = P( rr(1,kkk) )
                  vect(3,2) = P( rr(2,kkk) )
                  end if

!                  if(anal.eq.5.0) scal = scal / 100.0

                else if(fil.eq.44) then

                  ! Nummeric Temperature Output Settings
                  if(disgal.and.(R(1,i).lt.599)) then
                    scal(1) = P( rr(4,jjj) ) + P( rr(4,kkk) ) - P( rr(4,iii) )
                    scal(2) = P( rr(4,kkk) ) + P( rr(4,iii) ) - P( rr(4,jjj) )
                    scal(3) = P( rr(4,iii) ) + P( rr(4,jjj) ) - P( rr(4,kkk) )
                  else
                    scal(1) = P( rr(4,iii) )
                    scal(2) = P( rr(4,jjj) )
                    scal(3) = P( rr(4,kkk) )
                  endif
                  flat = 1.0
                  vect(1,1) = P( rr(1,iii) )
                  vect(1,2) = P( rr(2,iii) )
                  vect(2,1) = P( rr(1,jjj) )
                  vect(2,2) = P( rr(2,jjj) )
                  vect(3,1) = P( rr(1,kkk) )
                  vect(3,2) = P( rr(2,kkk) )

!                  if(anal.eq.5.0) scal = scal / 100.0

                else if(fil.eq.41) then

                  ! Nummeric U Velocity Output Settings
                  if(disgal.and.(R(1,i).lt.599)) then
                    scal(1) = P( rr(1,jjj) ) + P( rr(1,kkk) ) - P( rr(1,iii) )
                    scal(2) = P( rr(1,kkk) ) + P( rr(1,iii) ) - P( rr(1,jjj) )
                    scal(3) = P( rr(1,iii) ) + P( rr(1,jjj) ) - P( rr(1,kkk) )
                  else
                    scal(1) = P( rr(1,iii) )
                    scal(2) = P( rr(1,jjj) )
                    scal(3) = P( rr(1,kkk) )
                  endif
                  flat = 1.0

!                  if(anal.eq.5.0) scal = scal / 100.0

                else if(fil.eq.42) then

                  ! Nummeric V Velocity Output Settings
                  if(disgal.and.(R(1,i).lt.599)) then
                    scal(1) = P( rr(2,jjj) ) + P( rr(2,kkk) ) - P( rr(2,iii) )
                    scal(2) = P( rr(2,kkk) ) + P( rr(2,iii) ) - P( rr(2,jjj) )
                    scal(3) = P( rr(2,iii) ) + P( rr(2,jjj) ) - P( rr(2,kkk) )
                  else
                    scal(1) = P( rr(2,iii) )
                    scal(2) = P( rr(2,jjj) )
                    scal(3) = P( rr(2,kkk) )
                  endif
                  flat = 1.0

!                  if(anal.eq.5.0) scal = scal / 100.0
                  
                else if(fil.eq.51) then

                  ! Pressure Error (%) Output Settings
                  scal(1) = P(rr(3,iii))-P_ana(X(iii),Y(iii))
                  scal(2) = P(rr(3,jjj))-P_ana(X(jjj),Y(jjj))
                  scal(3) = P(rr(3,kkk))-P_ana(X(kkk),Y(kkk))

                else if(fil.eq.49) then

                  ! U Velocity Error (%) Output Settings
                  scal(1) = P(rr(1,iii))-U_ana(X(iii),Y(iii))
                  scal(2) = P(rr(1,jjj))-U_ana(X(jjj),Y(jjj))
                  scal(3) = P(rr(1,kkk))-U_ana(X(kkk),Y(kkk))

                else if(fil.eq.50) then

                  ! V Velocity Error (%) Output Settings
                  scal(1) = P(rr(2,iii))-V_ana(X(iii),Y(iii))
                  scal(2) = P(rr(2,jjj))-V_ana(X(jjj),Y(jjj))
                  scal(3) = P(rr(2,kkk))-V_ana(X(kkk),Y(kkk))

                endif

                ! Write Scalar Output Down in GMSH Format
                write(fil,'(a2)')'ST'
                write(fil,'(a1)')'('
                write(fil,'(e18.10,2(a1,e19.10))') X(iii),',', Y(iii),',',flat*scal(1)
                write(fil,'(a1)')','            
                write(fil,'(e18.10,2(a1,e19.10))') X(jjj),',', Y(jjj),',',flat*scal(2)
                write(fil,'(a1)')','            
                write(fil,'(e18.10,2(a1,e19.10))') X(kkk),',', Y(kkk),',',flat*scal(3)
                write(fil,'(a1)')')'
                write(fil,'(a1)')'{'
                write(fil,'(e18.10)') scal(1)
                write(fil,'(a1)')','
                write(fil,'(e18.10)') scal(2)
                write(fil,'(a1)')','
                write(fil,'(e18.10)') scal(3)
                write(fil,'(a2)')'};'

                ! Write Vector Output Down in GMSH Format (if present)
                if(abs(vect(1,1))+abs(vect(1,2))+abs(vect(2,1))+abs(vect(2,2))+abs(vect(3,1))+abs(vect(3,2)).ne.0.0) then
                  write(fil,'(a2)')'VT'
                  write(fil,'(a1)')'('
                  write(fil,'(e18.10,2(a1,e19.10))') X(iii),',', Y(iii),',',10.0 !flat*scal(1)
                  write(fil,'(a1)')','
                  write(fil,'(e18.10,2(a1,e19.10))') X(jjj),',', Y(jjj),',',10.0 !flat*scal(2)
                  write(fil,'(a1)')','
                  write(fil,'(e18.10,2(a1,e19.10))') X(kkk),',', Y(kkk),',',10.0 !flat*scal(3)
                  write(fil,'(a1)')')'
                  write(fil,'(a1)')'{'
                  write(fil,'(e18.10,2(a1,e19.10))') vect(1,1),',', vect(1,2),',', 0.0
                  write(fil,'(a1)')','
                  write(fil,'(e18.10,2(a1,e19.10))') vect(2,1),',', vect(2,2),',', 0.0
                  write(fil,'(a1)')','
                  write(fil,'(e18.10,2(a1,e19.10))') vect(3,1),',', vect(3,2),',', 0.0
                  write(fil,'(a2)')'};'
                endif

              end do
            end do
          end do
        end do

        ! Create Blob Outline
   99   if(Pist(1).ne.-1) call Outline(maxnod,maxsid,X,Y,fil,nids,Pist,R)
        
        write(fil,'(a2)')'};'
        write(fil,'(a1)')' '
        close(fil)

        !print*,label(20*(fil-40)+1:20*(fil-40+1))//' Output Called.'
      end do

      ! Indicate to screen that output has been generated
      write (*, 999, advance='no')
  999 format (' X ')
     
      return
      end




      subroutine Outline(maxnod,maxsid,X,Y,unit,nids,Pist,Ex)

      use CONTROL, only : twopi

      implicit none
      integer maxnod, maxsid, i, j, k, unit, nids, Pist(nids), n
      real X(-maxsid+1:maxnod), Y(-maxsid+1:maxnod), maxy, ang
      double precision Ex(nids)

      maxy = 0.0; n = 50

      do j = 0, n-1, 1
        do k = -1, 1, 2
          do i = 1, nids-1

            ! Keep track of the biggest y node value
            if((j+k.eq.-2).and.(Y(Pist(i)).gt.maxy)) maxy = Y(Pist(i))

            write(unit,'(a2)') 'SQ'
            write(unit,'(a1)') '('

            ang = twopi*real(j)/real(n)

            write(unit,'(e18.10,2(a1,e19.10))') cos(ang)*X(Pist(i  )),',',real(k)*Y(Pist(i  )),',',sin(ang)*X(Pist(i  ))
            write(unit,'(a1)') ','
            write(unit,'(e18.10,2(a1,e19.10))') cos(ang)*X(Pist(i+1)),',',real(k)*Y(Pist(i+1)),',',sin(ang)*X(Pist(i+1))
            write(unit,'(a1)') ','

            ang = twopi*real(j+1)/real(n)

            write(unit,'(e18.10,2(a1,e19.10))') cos(ang)*X(Pist(i+1)),',',real(k)*Y(Pist(i+1)),',',sin(ang)*X(Pist(i+1))
            write(unit,'(a1)') ','
            write(unit,'(e18.10,2(a1,e19.10))') cos(ang)*X(Pist(i  )),',',real(k)*Y(Pist(i  )),',',sin(ang)*X(Pist(i  ))
            write(unit,'(a1)') ')'
            write(unit,'(a1)') '{'
            write(unit,'(e18.10)') sqrt( 2.0d0 * Ex(max(2,i)) )
            write(unit,'(a1)') ','
            write(unit,'(e18.10)') sqrt( 2.0d0 * Ex(i+1)      )
            write(unit,'(a1)') ','
            write(unit,'(e18.10)') sqrt( 2.0d0 * Ex(i+1)      )
            write(unit,'(a1)') ','
            write(unit,'(e18.10)') sqrt( 2.0d0 * Ex(max(2,i)) )
            write(unit,'(a2)') '};'

            ! Draw a reference circle
!            write(unit,'(a2)') 'SL'
!            write(unit,'(a1)') '('
!            yy = atan2(Y(Pist(i)),X(Pist(i)))
!            xx = 1.0 * cos(yy)
!            yy = 1.0 * sin(yy)
!            write(unit,'(e18.10,2(a1,e19.10))') real(j)*xx,',',real(k)*yy,',',0.0 !P( rr(3,Pist(i)) )
!            write(unit,'(a1)') ','
!            yy = atan2(Y(Pist(i+1)),X(Pist(i+1)))
!            xx = 1.0 * cos(yy)
!            yy = 1.0 * sin(yy)
!            write(unit,'(e18.10,2(a1,e19.10))') real(j)*xx,',',real(k)*yy,',',0.0 !P( rr(3,Pist(i+1)) )
!            write(unit,'(a1)') ')'
!            write(unit,'(a1)') '{'
!            write(unit,'(e18.10)') 0.0
!            write(unit,'(a1)') ','
!            write(unit,'(e18.10)') 0.0
!            write(unit,'(a2)') '};'

          end do
        end do

        ! Add an illustration of the explosion (at either end) when present
!        if(momo.gt.-1.0) then
!          write(unit,'(a2)') 'ST'
!          write(unit,'(a1)') '('
!          write(unit,'(e18.10,2(a1,e19.10))') - maxy / 10.0, ',' , 2.0  * real(j) * maxy, ',' ,0.0
!          write(unit,'(a1)') ','
!          write(unit,'(e18.10,2(a1,e19.10))')     0.0     ,  ',' , 1.3 * real(j) * maxy, ',' ,0.0
!          write(unit,'(a1)') ','
!          write(unit,'(e18.10,2(a1,e19.10))') + maxy / 10.0, ',' , 2.0  * real(j) * maxy, ',' ,0.0
!          write(unit,'(a1)') ')'
!          write(unit,'(a1)') '{'
!          write(unit,'(e18.10)') 0.0
!          write(unit,'(a1)') ','
!          write(unit,'(e18.10)') 0.0
!          write(unit,'(a1)') ','
!          write(unit,'(e18.10)') 0.0
!          write(unit,'(a2)') '};'
!        end if

      end do

      return
      end







