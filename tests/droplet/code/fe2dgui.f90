
 
      subroutine Integral_Element(maxnid,X,Y,Type,L,List,Jist,Pist,Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy,Wxx,Wxy,Wyy,T,&
                                                                                                     Px,Py,Qx,Qy,Z,CP,Ex,Ey,filling)

      use PLACE, only : rr
      use CONTROL, only : disgal, troc, axisym, inviscid, refsym, rifsym, bemo, o
      use SIZES, only : maxnod, maxsid

      implicit none
      integer maxnid, List(maxnid), Jist(maxnid), Pist(maxnid)
      integer i, ii, j, jj, m, n, loopstart, loopend, flag
      real X(maxnod), Y(maxnod), p(13,2), q(13,2), flip, nudgeX, nudgeY, sx, sy

      real U_ana, V_ana, Tx_ana, Ty_ana, U_inf, V_inf
    
      ! Integral Matrices
      double precision Vxx(o:maxnid,o:maxnid),Vxy(o:maxnid,o:maxnid),Vyy(o:maxnid,o:maxnid)
      double precision Kxx(o:maxnid,o:maxnid),Kxy(o:maxnid,o:maxnid),Kyy(o:maxnid,o:maxnid)
      double precision Lxx(o:maxnid,o:maxnid),Lxy(o:maxnid,o:maxnid),Lyy(o:maxnid,o:maxnid)
      double precision Wxx(o:maxnid,o:maxnid),Wxy(o:maxnid,o:maxnid),Wyy(o:maxnid,o:maxnid)
      double precision T(o:maxnid,o:maxnid), Z(o:maxnid,o:maxnid), CP(o:maxnid,o:maxnid), tt, zz, cpp, L(3*maxnod)
      double precision Ex(o:maxnid), Ey(o:maxnid)
      double precision Px(o:maxnid,o:maxnid),Py(o:maxnid,o:maxnid),Qx(o:maxnid,o:maxnid),Qy(o:maxnid,o:maxnid)
      double precision vvxx, vvxy, vvyy, kkxx, kkxy, kkyy, llxx, llxy, llyy, wwxx, wwxy, wwyy, ppxx, ppyy, ooxx, ooyy, eexx, eeyy
      double precision qqxx, qqyy
      double precision anna, numa
      character Type(maxnod)*1
      logical filling, trac, obs, int
      logical integratetest
      save                  

      if((disgal.or.inviscid).and.filling) return
      
      if(maxnid.lt.1) return

      !call testElliptic()

      trac = troc.ne.0.0; flip = 0.0

      !if(.false.) then
      !  anna = 1.0d0; numa = 0.0d0
      !else
        anna = 0.0d0; numa = 1.0d0
      !endif

      if(filling) then
        ! Need to Store Traction Values First (use first rows of left-over BEM Matrices...)
        do j = 1, maxnid
          ! Swap values over (rather than replace) so that BEM matrices can be reconstructed afterwards when 'trac' is on for checking
          tt = Vxx(1,j)
          Vxx(1,j) = L( rr(1,List(j)) ) - U_inf(X(List(j)),Y(List(j)))
          if(trac) L( rr(1,List(j)) ) = tt

          tt = Kxx(1,j)
          Kxx(1,j) = L( rr(2,List(j)) ) - V_inf(X(List(j)),Y(List(j)))
          if(trac) L( rr(2,List(j)) ) = tt

          tt = Lxx(1,j)
          Lxx(1,j) = L( rr(1,Jist(j)) )
          if(trac) L( rr(1,Jist(j)) ) = tt

          tt = Wxx(1,j)
          Wxx(1,j) = L( rr(2,Jist(j)) )
          if(trac) L( rr(2,Jist(j)) ) = tt
        end do

        if(trac) then
          ! Plot values of Boundary Traction at "T" & "W" nodes if requested (Values there already).
          loopend = 0
        else
          ! Overwrite the Traction Values with those for the Solution
          loopend=maxnid
        endif
        loopstart=-maxsid+1; flag = 0

      else if(filling.and.disgal) then
        loopend=-1
        loopstart=0; flag = 0
      ! For axisym disgal first row + column of Coulomb Potential matrix will be empty ...
      else if(disgal.and.axisym) then
        loopend=maxnid
        loopstart=2; flag = 1
      else if(rifsym) then
        loopend=maxnid
        loopstart=3-maxnid; flag = 1
      else
        loopend=maxnid
        loopstart=1; flag = 1
      endif
    
      ! Outer Loop -either over the Boundary Elements for Calculating the Integral Matrices
      !            -or over the "S" Special Nodes for Integral Solution Visualization
      ! Variable categorization once again:
      ! filling                 flag, readonly                  shared
      ! Jist                    flag, readonly                  shared
      ! maxnod, maxnid          flag, readonly                  shared
      ! List, Type              flag, readonly                  shared
      ! X,Y                     readonly                        shared
      ! numa, anna              readonly                        shared
      ! flag                    flag, readonly                  shared
      ! 
      ! Where does the variable rr come from???
      ! I'm now trying to understand on which variables inside the loop the result is stored.
      ! These should be at least shared or reduction
      ! 
      ! What about V**, L**, K** and T**? These seem to be of the type reduction but shared by 
      ! default.

      !$omp parallel do&
      !$omp shared(filling,Jist,maxnod,maxnid,List,Type,X,Y,numa,anna,flag)&
      !!!$omp firstprivate(filling,Jist,maxnod,maxnid,List,Type,X,Y,numa,anna,flag)&
      !!!$omp firstprivate(Vfirstxx,Vfirstxy,Vfirstyy)&
      !!!$omp reduction(+:Vxx,Vxy,Vyy)&
      !$omp private(p,q,ii,j,m,n,nudgeX,nudgeY,&
      !$omp    qqxx,qqyy,ooxx,ooyy,ppxx,ppyy,wwxx,wwxy,wwyy,&
      !$omp    llxx,llxy,llyy,kkxx,kkxy,kkyy,vvxx,vvxy,vvyy,eexx,eeyy,tt,zz,cpp,obs,int)
      do i = loopstart, loopend
        
        ! Initialize Segment Data holders to some Extreme Values to Catch any Bugs ...
        p = 999999.0; q = 999999.0

        if(filling) then
          if(i.gt.0) then
            ii = Jist(i)
          else
            ii = i
          endif
          if(((Type(ii).ne.'S').and.(Type(ii).ne.'G')).and.((Type(ii).ne.'T').and.(Type(ii).ne.'W'))) then
            print*,'Attempt to fill in Non-Specials ...'
          end if
          L(rr(1,ii)) = U_inf(X(ii),Y(ii))
          L(rr(2,ii)) = V_inf(X(ii),Y(ii))
          L(rr(3,ii)) = 0.0
          if(i.gt.0) then
            nudgeX = X(ii); nudgeY = Y(ii)
            call Normal(nudgeX,nudgeY)
            nudgeX = nudgeX * 0.02
            nudgeY = nudgeY * 0.02
          else
            nudgeX = 0.0
            nudgeY = 0.0
          endif
          p(3,1) = X(ii)+nudgeX
          p(4,1) = Y(ii)+nudgeY
          p(5,1) = X(ii)+nudgeX
          p(6,1) = Y(ii)+nudgeY
          p(12,1) = 1.0
        else               
          ! Get the Observation Segment Details
          call Segment(maxnod,maxnid,List,Type,X,Y,i,p,.true.)
        endif

        ! Inner Loop over the Boundary Elements
        do j = 2-maxnid, maxnid

          ! If using reflection symmetry in the Y-axis (which excludes axisymmetry) then should really start from 3-maxnid
          if(rifsym.and.(j.eq.2-maxnid)) cycle

          ! Axisymmetry with NO reflection symmetry in the X-axis should avoid ALL negative dummies
          if(axisym.and.(.not.refsym).and.(j.lt.1)) cycle

          ! Discontinuous Galerkin (non-conforming) discretizations should avoid the first number in the list
          if(axisym.and.disgal.and.(j.eq.1)) cycle

          ! 'True' integrations going all the way round the boundary should also avoid the dummy nodes
          if((.not.axisym).and.(.not.rifsym).and.(j.lt.1)) cycle

          !if(i.eq.0) write(*,'(8(f8.4,a5))')  &
          !  L(rr(1,List(j))),'  =  ',U_ana(X(List(j)),Y(List(j))), &
          !                   '  (  ',L(rr(1,List(j)))/(0.1e-5+U_ana(X(List(j)),Y(List(j)))), &
          !                   '  ,  ',U_ana(X(List(j)),Y(List(j)))/(0.1e-5+real(L(rr(1,List(j))))), &
          !                   '  )  ', &
          !  L(rr(2,List(j))),'  =  ',V_ana(X(List(j)),Y(List(j))), &
          !                   '  (  ',L(rr(2,List(j)))/(0.1e-5+V_ana(X(List(j)),Y(List(j)))), &
          !                   '  ,  ',V_ana(X(List(j)),Y(List(j)))/(0.1e-5+real(L(rr(2,List(j))))), &
          !                   '  )  '

          ! Get the Integration Segment Details
          call Segment(maxnod,maxnid,List,Type,X,Y,j,q,.false.)

          ! Double Loop for All the Possible Segment Combinations
          do 20 m = 1, 1 + flag
            do 10 n = 1, 2

              obs = m .eq. 1
              if(j.gt.1) then
                int = n .eq. 1
              else
                int = n .eq. 2
              end if

              ! Perform the Actual Numerical Integrations
              call NI(p(:,m),q(:,n),vvxx,vvxy,vvyy,kkxx,kkxy,kkyy,llxx,llxy,llyy,wwxx,wwxy,wwyy,tt,  &
                                                              ppxx,ppyy,qqxx,qqyy,ooxx,ooyy,zz,cpp,eexx,eeyy,flag,obs,int)
              if(filling) then
                ! Pressure at Specials
                L(rr(3,ii)) = L(rr(3,ii)) - 0.5 * qqxx * ( Vxx(1,j) * numa +  U_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                          - 0.5 * qqyy * ( Kxx(1,j) * numa +  V_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                          + 0.5 * ooxx * ( Vxx(1,j) * numa +  U_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                          + 0.5 * ooyy * ( Kxx(1,j) * numa +  V_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                          - 0.5 * ppxx * ( Lxx(1,j) * numa + Tx_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                          - 0.5 * ppyy * ( Wxx(1,j) * numa + Ty_ana( X( List(j) ), Y ( List(j) ) ) * anna )
                
                if(flip.ne.0.0) then
                  ! X Traction at Specials
                  L(rr(1,ii)) = L(rr(1,ii)) + 0.5 * wwxx * ( Vxx(1,j) * numa +  U_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            + 0.5 * wwxy * ( Kxx(1,j) * numa +  V_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * llxx * ( Lxx(1,j) * numa + Tx_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * llxy * ( Wxx(1,j) * numa + Ty_ana( X( List(j) ), Y ( List(j) ) ) * anna )
                  ! Y Traction at Specials
                  L(rr(2,ii)) = L(rr(2,ii)) + 0.5 * wwxy * ( Vxx(1,j) * numa +  U_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            + 0.5 * wwyy * ( Kxx(1,j) * numa +  V_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * llxy * ( Lxx(1,j) * numa + Tx_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * llyy * ( Wxx(1,j) * numa + Ty_ana( X( List(j) ), Y ( List(j) ) ) * anna )
                else
                  ! U Velocity at Specials
                  L(rr(1,ii)) = L(rr(1,ii)) + 0.5 * kkxx * ( Vxx(1,j) * numa +  U_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            + 0.5 * kkxy * ( Kxx(1,j) * numa +  V_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * vvxx * ( Lxx(1,j) * numa + Tx_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * vvxy * ( Wxx(1,j) * numa + Ty_ana( X( List(j) ), Y ( List(j) ) ) * anna )
                  ! V Velocity at Specials
                  L(rr(2,ii)) = L(rr(2,ii)) + 0.5 * kkxy * ( Vxx(1,j) * numa +  U_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            + 0.5 * kkyy * ( Kxx(1,j) * numa +  V_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * vvxy * ( Lxx(1,j) * numa + Tx_ana( X( List(j) ), Y ( List(j) ) ) * anna ) &
                                            - 0.5 * vvyy * ( Wxx(1,j) * numa + Ty_ana( X( List(j) ), Y ( List(j) ) ) * anna )
                endif

              else

                jj = j

                if(rifsym) then
                  sx = 1.0; sy = 1.0
                else
                call qmap(maxnid,jj,sx,sy)
                end if

                ! Fill in the Pressure Compatibility Row Vectors
                Ex(i) = Ex(i) + eexx
                Ey(i) = Ey(i) + eeyy

                ! Velocity ( & Traction ) Boundary Integral Equation (VBIE) BEM Matrices Fill-in
                Vxx(i,jj) = Vxx(i,jj) + sx * vvxx
                Kxx(i,jj) = Kxx(i,jj) + sx * kkxx
                Lxx(i,jj) = Lxx(i,jj) + sx * llxx
                Wxx(i,jj) = Wxx(i,jj) + sx * wwxx

                Vxy(i,jj) = Vxy(i,jj) + sy * vvxy
                Kxy(i,jj) = Kxy(i,jj) + sy * kkxy
                Lxy(i,jj) = Lxy(i,jj) + sy * llxy
                Wxy(i,jj) = Wxy(i,jj) + sy * wwxy

                if(axisym) then
                Qx(i,jj) = Qx(i,jj) + qqxx
                Qy(i,jj) = Qy(i,jj) + qqyy

                Px(i,jj) = Px(i,jj) + sy * ppxx
                Py(i,jj) = Py(i,jj) + sy * ppyy
                end if

                Vyy(i,jj) = Vyy(i,jj) + sy * vvyy
                Kyy(i,jj) = Kyy(i,jj) + sy * kkyy
                Lyy(i,jj) = Lyy(i,jj) + sy * llyy
                Wyy(i,jj) = Wyy(i,jj) + sy * wwyy

                if(.not.axisym) then
                ! Pressure Boundary Integral Equation (PBIE) BEM Matrices Fill-in
                Qx(i,jj) = Qx(i,jj) + sx * qqxx - sx * ooxx
                Qy(i,jj) = Qy(i,jj) + sy * qqyy - sy * ooyy

                Px(i,jj) = Px(i,jj) + sx * ppxx
                Py(i,jj) = Py(i,jj) + sy * ppyy
                end if

                ! Boundary Velocity & Pressure Identity Matrices Fill-in
                T(i,jj) = T(i,jj) + tt
                Z(i,jj) = Z(i,jj) + zz

                ! Fill in the Coulomb Potential Matrix
                CP(i,jj) = CP(i,jj) + cpp

              endif

            ! End Loops over Combinations
   10       continue
   20     continue

        ! End Loops over Elements
        enddo ! j = 1, maxnid
      enddo ! i = loopstart, loopend 
      !$omp end parallel do

      integratetest = .false.
      if (integratetest) then
      ! in order to test that the parallelisation works, write the
      ! variables calculated in the above loops out to file
      open(3141, file='integration_vars.dat', status='unknown', position='append')

      write(3141, '(a, a, a)') 'Vxx', 'Vxy', 'Vyy'
      do i = loopstart, loopend
        !write(3141, '(g, g, g)'), (Vxx(i,j), Vxy(i,j), Vyy(i,j), j=1,maxnid)
      enddo

      write(3141, '(a, a, a)') 'Kxx', 'Kxy', 'Kyy'
      do i = loopstart, loopend
        !write(3141, '(g, g, g)'), (Kxx(i,j), Kxy(i,j), Kyy(i,j), j=1,maxnid)
      enddo

      write(3141, '(a, a, a)') 'Lxx', 'Lxy', 'Lyy'
      do i = loopstart, loopend
        !write(3141, '(g, g, g)'), (Lxx(i,j), Lxy(i,j), Lyy(i,j), j=1,maxnid)
      enddo

      write(3141, '(a, a, a)') 'Wxx', 'Wxy', 'Wyy'
      do i = loopstart, loopend
        !write(3141, '(g, g, g)'), (Wxx(i,j), Wxy(i,j), Wyy(i,j), j=1,maxnid)
      enddo

      write(3141, '(a, a)') 'Qx', 'Qy'
      do i = loopstart, loopend
        !write(3141, '(g, g)'), (Qx(i,j), Qy(i,j), j=1,maxnid)
      enddo

      write(3141, '(a, a)') 'Px', 'Py'
      do i = loopstart, loopend
        !write(3141, '(g, g)'), (Px(i,j), Py(i,j), j=1,maxnid)
      enddo

      write(3141, '(a, a)') 'T', 'Z'
      do i = loopstart, loopend
        !write(3141, '(g, g)'), (T(i,j), Z(i,j), j=1,maxnid)
      enddo

      close(3141)

      endif ! integratetest
      !!! 


      if(filling) then
        ! Swapping back to reconstruct the BEM matrices for Solution Testing (only if tractions are being plotted)
        if(trac) then
          do j = 1, maxnid
            
            tt = L( rr(1,List(j)) )
            L( rr(1,List(j)) ) = Vxx(1,j)
            Vxx(1,j) = tt
            
            tt = L( rr(2,List(j)) )
            L( rr(2,List(j)) ) = Kxx(1,j)
            Kxx(1,j) = tt
            
            tt = L( rr(1,Jist(j)) )
            L( rr(1,Jist(j)) ) = Lxx(1,j)
            Lxx(1,j) = tt

            tt = L( rr(2,Jist(j)) )
            L( rr(2,Jist(j)) ) = Wxx(1,j)
            Wxx(1,j) = tt
          end do
        else
          return
        endif
      endif

      if((.not.(axisym.and.disgal)).and.(.not.inviscid)) &
      call Integral_Test(maxnod,maxnid,List,Jist,Pist,X,Y,Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy,Wxx,Wxy,Wyy,T,Px,Py,Qx,Qy,Z,Ex,Ey, &
                                                                                                                          L,filling)
      if(bemo) stop
      return
      end
          


      subroutine NI(p,q,Vxx,Vxy,Vyy,Kxx,Kxy,Kyy,Lxx,Lxy,Lyy,Wxx,Wxy,Wyy,T,Px,Py,Qx,Qy,Ox,Oy,Z,CP,Ex,Ey,flag,obs,int)

      use CONTROL, only : disgal, axisym, pi, twoonpi, twopi, inviscid, ccaa, omeg, air, tol, rifsym, bemo, perfect

      implicit none
      integer m, n, kp, kq, s
      integer, intent(in) :: flag
      real regbit
      logical, intent(in) :: obs, int

      ! Integration Segment Details
      real, intent(inout) :: p(13), q(13)
      real cauchy, plen, qlen, xm, ym, xn, yn, xx, yy, xxyy, sxxyy

      ! Gaussian Quadrature Integration Points and Weights
      double precision xp(-32:32), wp(-32:32), xq(-32:32), wq(-32:32)

      ! Return Arguments
      double precision, intent(out) :: Vxx,Vxy,Vyy, Kxx,Kxy,Kyy, Lxx,Lxy,Lyy, Wxx,Wxy,Wyy, T, Px,Py, Qx,Qy, Ox,Oy, Z, Ex,Ey, CP

      ! Local Variables
      double precision alpha, dalphads, presalpha, va, ca, ka, la, wa, fi, fe, po, qo, ker(20)
      double precision presalpha_p, presalpha_q, alpha_p, alpha_q, dalphads_p, dalphads_q

      double precision fact
      ! Flag for identical observation and integration segment
      logical self, ptop, pbot, qtop, qbot

      ! Set Cauchy Tolerance
      cauchy = min(p(12),q(12)); presalpha_p = 0.0d0; presalpha_q = 0.0d0

      ! Initialize Accumulators
      Vxx = 0.0d0; Vxy = 0.0d0; Vyy = 0.0d0
      Kxx = 0.0d0; Kxy = 0.0d0; Kyy = 0.0d0
      T = 0.0d0; Z = 0.0d0; CP = 0.0d0
      Ex = 0.0d0; Ey = 0.0d0
      Lxx = 0.0d0; Lxy = 0.0d0; Lyy = 0.0d0
      Wxx = 0.0d0; Wxy = 0.0d0; Wyy = 0.0d0
      Px  = 0.0d0; Py  = 0.0d0
      Qx  = 0.0d0; Qy  = 0.0d0
      Ox  = 0.0d0; Oy  = 0.0d0

      ! Return if Tolerance is Zero -a Flag that Segment is not to be Integrated
      if(cauchy.eq.0.0) return

      ! Flag if either segment is against the rotation axis
      ptop=axisym.and.(p(5).lt.tol);qtop=axisym.and.(q(5).lt.tol);pbot=axisym.and.(p(3).lt.tol);qbot=axisym.and.(q(3).lt.tol)

      ! Observation & Integration Segment Lengths
      plen = sqrt( (p(5)-p(3))**2 + (p(6)-p(4))**2 )
      qlen = sqrt( (q(5)-q(3))**2 + (q(6)-q(4))**2 )

      ! Determine the order of Gaussian quadrature to use for the integration
      kp = 10 - 5*floor( sqrt( min(  (q(3)-p(3))**2 + (q(4)-p(4))**2, &
                                     (q(5)-p(5))**2 + (q(6)-p(6))**2, &
                                     (q(5)-p(3))**2 + (q(6)-p(4))**2, &
                                     (q(3)-p(5))**2 + (q(4)-p(6))**2  ) ) / sqrt( (q(5)-q(3))**2 + (q(6)-q(4))**2 ) )
      kq = kp
      ! Axis segments get extra Gauss points
      if(ptop.or.pbot) kp = 10; if(qtop.or.qbot) kq = 10
      ! Bound requested values to what is actually available
      kp = min(max(kp,4),64); kq = min(max(kq,4),64)
      ! Get Gauss Points and Weights
      call Gauss(kp,xp,wp); call Gauss(kq,xq,wq)

!      call testGauss(k,x,w)

      ptop = .false.; pbot = .false.
      qtop = .false.; qbot = .false.

      ! Determine Normal at Observation Point (P)
      xx = p(6) - p(4)
      yy = p(3) - p(5)

      xxyy = xx*xx + yy*yy
      sxxyy = sqrt(xxyy)
      if(xxyy.lt.0.00001**2) then
        ! Dummey "Specials" Filling Observation Element, have to use interpolated "Normal" ...
        xm = p(3)
        ym = p(4)
        call Normal(xm,ym)
        !xm = 0.0
        !ym = 0.0
      else
        xm = xx / sxxyy
        ym = yy / sxxyy
      endif

      ! Determine Normal at Integration Point (Q)
      xx = q(6) - q(4)
      yy = q(3) - q(5)
      xxyy = xx*xx + yy*yy
      sxxyy = sqrt(xxyy)
      xn = xx / sxxyy
      yn = yy / sxxyy

      ! Outer Numerical Integration Loop over "Observation" Segment
      do m = -flag*kp, flag*kp

        ! Set Observation Point (p)
        if(ptop) then
          ! Cluster points around the end for top-most boundary segment abutting the rotational symmetry axis
          fact =  1.0d0 + ( -1.0d0  - 1.0d0 ) * ( (1.0d0 + xp(m)) / 2.0d0 ) ** 2
        else if(pbot) then
          ! Cluster points around the end for bottom-most boundary segment abutting the rotational symmetry axis
          fact = -1.0d0 + (  1.0d0  + 1.0d0 ) * ( (1.0d0 + xp(m)) / 2.0d0 ) ** 2
        else
          ! Well separated boundary elements (away from rotation axis) are done as normal, i.e.: no clustering
          fact = xp(m)
        end if

        ! Convert Gauss point from given [ -1 : 1 ] interval to required [ 0 : 1 ]
        p(2) = real( ( 1.0d0 + fact ) / 2.0d0 )

        ! Find corresponding coordinates
        p(1) = p(3) + p(2) * (p(5)-p(3))
        p(2) = p(4) + p(2) * (p(6)-p(4))

        ! Set Integration Point to Observation Point
        q(1) = p(1); q(2) = p(2)

        ! Calculate Shape Functions here for efficiency
        if(flag.ne.0) then
          alpha_p = alpha(p)
          if(.not.disgal) presalpha_p = presalpha(p)
          dalphads_p = dalphads(p)
        else
          alpha_p = 1.0D0
          if(.not.disgal) presalpha_p = 1.0D0
          dalphads_p = 1.0D0
        endif
        alpha_q = alpha(q)
        if(.not.disgal) presalpha_q = presalpha(q)

        ! Set observation weights
        if(ptop.or.pbot) wp(m) = wp(m) * ( 1.0d0 + xp(m) )
                
        ! Initialize Flags
        regbit = 0.0; self = .false.

        ! Mass Matrix Entries non-zero only for same element case
        if((p(3)-q(3))**2+(p(4)-q(4))**2+(p(5)-q(5))**2+(p(6)-q(6))**2.lt.cauchy) then

          ! Flag for same-element integration
          self = .true.

          ! Pressure Compatibility Condition Vectors
          if(disgal.and.obs) then
            Ex = Ex + wp(m) * xm * plen
            Ey = Ey + wp(m) * ym * plen
          else if(.not.disgal) then
            if(axisym) then
              Ex = Ex + wp(m) * alpha_p * p(1) * plen
              Ey = Ey + wp(m) * alpha_p * p(1) * plen
            else
              Ex = Ex + wp(m) * alpha_p * plen  ! * xm
              Ey = Ey + wp(m) * alpha_p * plen  ! * ym
            end if
          endif

          if(disgal.and.obs.and.int) then

            ! Boundary Velocity Mass Matrix (T)
            if(axisym) then
              T = T + wp(m) * alpha_q * plen
            else
              T = T + wp(m) * alpha_q * plen
            end if

            ! Non-Conforming / Constant Boundary Mass Matrix (Z)
            if(axisym) then
              Z = Z + wp(m) * p(1) * plen
            else
              Z = Z + wp(m) * plen
            end if

          else if(disgal.and.obs) then

            ! Boundary Velocity Mass Matrix (T)
            if(axisym) then
              T = T + wp(m) * p(1) * alpha_q * plen
            else
              T = T + wp(m) * alpha_q * plen
            end if

          else if(.not.disgal) then

            ! Boundary Velocity Mass Matrix (T)
            if(axisym) then
              T = T + wp(m) * alpha_p * p(1) * alpha_q * plen
            else
              T = T + wp(m) * alpha_p * alpha_q * plen
            end if

            ! Boundary Pressure Mass Matrix (Z)
            Z = Z + wp(m) * presalpha_p * presalpha_q * plen

          endif

          ! Set Flags
          regbit = 1.0

        else if(disgal.or.axisym) then
          ! To avoid wasting time with next two options if disgal

        else if((p(3)-q(5))**2+(p(4)-q(6))**2.lt.cauchy) then

          ! Set Integration Point to Beginning of Interval
          q(1) = q(3); q(2) = q(4)

          ! 3rd Hyper-Singular Pressure Corrector
          fact = ( plen / sqrt( (p(1)-q(1))**2 + (p(2)-q(2))**2 ) ) &
                                * presalpha_p * alpha(q) * ( twoonpi )
          Qx = Qx - wp(m) * xm * fact
          Qy = Qy - wp(m) * ym * fact

          ! Set Flags
          regbit = + 3.0
        
        else if((p(5)-q(3))**2+(p(6)-q(4))**2.lt.cauchy) then

          ! Set Integration Point to End of Interval
          q(1) = q(5); q(2) = q(6)

          ! 4th Hyper-Singular Pressure Corrector
          fact = ( plen / sqrt( (p(1)-q(1))**2 + (p(2)-q(2))**2 ) ) &
                                * presalpha_p * alpha(q) * ( twoonpi )
          Qx = Qx - wp(m) * xm * fact
          Qy = Qy - wp(m) * ym * fact

          ! Set Flags
          regbit = - 3.0

        endif

        ! Don't bother with other integrals for the inviscid case if no surface charge either
        if(inviscid.and.(ccaa.eq.0.0).and.(omeg.eq.0.0)) cycle

        ! Inner Numerical Integration Loop over "Integration" Segment
        do n = -kq, kq

          ! Extra loop for the two terms of the self-element integration
          do s = -1, 1, 2

          if((.not.self).and.(s.eq.1)) cycle

          ! Initialize Basis Function containing factors
          va = 0.0d0; ca = 0.0d0; ka = 0.0d0; la = 0.0d0; wa = 0.0d0; fi = 0.0d0; fe = 0.0d0; po = 0.0d0; qo = 0.0d0

          ! Set Integration Point (q)
          if(self) then
            ! Always cluster Gauss points around singularity at x(m) for self-element integrations
            fact =  xp(m) + ( dble(s) - xp(m) ) * ( (1.0d0 + xq(n)) / 2.0d0 ) ** 2
          else if(qtop) then
            ! Cluster points around the end for top-most segment abutting the rotational symmetry axis xp(m) = 1.0d0, s = -1
            fact =  1.0d0 + ( -1.0d0  - 1.0d0 ) * ( (1.0d0 + xq(n)) / 2.0d0 ) ** 2
          else if(qbot) then
            ! Cluster points around the end for bottom-most segment abutting the rotational symmetry axis xp(m) = -1.0d0, s = +1
            fact = -1.0d0 + (  1.0d0  + 1.0d0 ) * ( (1.0d0 + xq(n)) / 2.0d0 ) ** 2
          else
            ! Well separated boundary elements (away from rotation axis) are done as normal, i.e.: no clustering
            fact = xq(n)
          end if

          ! Convert Gauss point from given [ -1 : 1 ] interval to required [ 0 : 1 ]
          q(2) = real( ( 1.0d0 + fact ) / 2.0d0 )

          ! Find corresponding coordinates
          q(1) = q(3) + q(2) * (q(5)-q(3))
          q(2) = q(4) + q(2) * (q(6)-q(4))

          if((abs(regbit).gt.0.5).and.(abs(regbit).lt.1.5)) regbit = sign(1.0d0, xp(m) - xq(n) )

          ! Calculate Shape Functions here for efficiency
          alpha_q = alpha(q)
          dalphads_q = dalphads(q)

          ! Common Factors (for Velocity & Mixed Velocity/Pressure Integrals) with Integration Segment Contributions
          if(disgal.and.obs.and.int) then
!            va = wq(n) * qlen
!            ka = wq(n) * alpha_q * qlen
            if(self) then
              ca = wq(n) * qlen * ( 1.0d0 - dble(s) *   xp(m)  ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else if(qtop) then
              ca = wq(n) * qlen * ( 1.0d0 + 1.0d0   *   1.0d0  ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else if(qbot) then
              ca = wq(n) * qlen * ( 1.0d0 - 1.0d0   * (-1.0d0) ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else
              ca = wq(n) * qlen
            end if
!            la = wq(n) * qlen
!            po = wq(n) * qlen
!          else if(disgal.and.obs) then
!            ka = wq(n) * alpha_q * qlen
!          else if(disgal.and.int) then
!            la = wq(n) * qlen
!            po = wq(n) * qlen
          else if(.not.disgal) then
            if(self) then
              va = wq(n) * alpha_q * qlen * ( 1.0d0 - dble(s) *   xp(m)  ) * ( 1.0d0 + xq(n) ) / 2.0d0
              ka = wq(n) * alpha_q * qlen * ( 1.0d0 - dble(s) *   xp(m)  ) * ( 1.0d0 + xq(n) ) / 2.0d0
              la = wq(n) * alpha_q * qlen * ( 1.0d0 - dble(s) *   xp(m)  ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else if(qtop) then
              va = wq(n) * alpha_q * qlen * ( 1.0d0 + 1.0d0   *   1.0d0  ) * ( 1.0d0 + xq(n) ) / 2.0d0
              ka = wq(n) * alpha_q * qlen * ( 1.0d0 + 1.0d0   *   1.0d0  ) * ( 1.0d0 + xq(n) ) / 2.0d0
              la = wq(n) * alpha_q * qlen * ( 1.0d0 + 1.0d0   *   1.0d0  ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else if(qbot) then
              va = wq(n) * alpha_q * qlen * ( 1.0d0 - 1.0d0   * (-1.0d0) ) * ( 1.0d0 + xq(n) ) / 2.0d0
              ka = wq(n) * alpha_q * qlen * ( 1.0d0 - 1.0d0   * (-1.0d0) ) * ( 1.0d0 + xq(n) ) / 2.0d0
              la = wq(n) * alpha_q * qlen * ( 1.0d0 - 1.0d0   * (-1.0d0) ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else
            va = wq(n) * alpha_q * qlen
            ka = wq(n) * alpha_q * qlen
            la = wq(n) * alpha_q * qlen
!            po = wq(n) * alpha_q * qlen
!            ca = wq(n) * alpha_q * qlen
            end if
          endif
          wa = wq(n) * alpha_q * qlen
!          qo = wq(n) * alpha_q * qlen

          ! Different Factor Formation for BEM Matrix Construction and "Specials" Filling
          if(flag.eq.1) then

            ! Special Factor for Hyper-Singular Velocity Operator uses Arclength derivatives of Shape Functions
            if(self) then
              fi = wq(n) * dalphads_q * qlen * ( 1.0d0 - dble(s) *   xp(m)  ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else if(qtop) then
              fi = wq(n) * dalphads_q * qlen * ( 1.0d0 + 1.0d0   *   1.0d0  ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else if(qbot) then
              fi = wq(n) * dalphads_q * qlen * ( 1.0d0 - 1.0d0   * (-1.0d0) ) * ( 1.0d0 + xq(n) ) / 2.0d0
            else
            fi = wq(n) * dalphads_q * qlen
            end if

            ! Special Factors for Hyper-Singular Pressure Operator Regularization (both Shape functions evaluated at obs. point)
!            fe = wq(n) * dalphads_q * qlen

            ! Add Contributions of the Observation Segment to the Common Factors for BEM Matrix Construction Only
            if(disgal) then
!              va = va * wp(m)               * plen
!              ka = ka * wp(m)               * plen
              ca = ca * wp(m)               * plen
            else
              va = va * wp(m) *  alpha_p    * plen
              ka = ka * wp(m) *  alpha_p    * plen
!              ca = ca * wp(m) *  alpha_p    * plen
            endif
            la = la * wp(m) *  alpha_p    * plen
            wa = wa * wp(m) *  alpha_p    * plen
            fi = fi * wp(m) * dalphads_p  * plen
!            if(.not.disgal) then
!            fe = fe * wp(m) * presalpha_p * plen
!            po = po * wp(m) * presalpha_p * plen
!            qo = qo * wp(m) * presalpha_p * plen
!            end if
          endif

          ! Determine Integral Operator Kernal Function Values
          if(axisym) then
            call axikernal(ker,dble(p(1)),dble(p(2)),dble(q(1)),dble(q(2)),dble(xm),dble(ym),dble(xn),dble(yn),self)
          else
            call kernal(ker,p,q,xm,ym,xn,yn,cauchy)
          end if

          ! Coulomb Potential Operator (CP)
          if(axisym) CP  = CP  + ca*ker(20) / ( twopi )

          ! Need to regularize electric field operators if using non-perfect conductor
          if(.not.perfect) then
            Qx  = Qx - ca*ker(18) / ( pi )
            Qy  = Qy - ca*ker(19) / ( pi )
          end if

          ! Don't bother with other integrals for the inviscid case
          if((inviscid.or.(.not.air)).and.(.not.bemo)) cycle

          ! Single Layer Velocity Operator (V)
          Vxx = Vxx + va*ker( 1)
          Vxy = Vxy + va*ker( 2)
          if(axisym) & ! Px holds Vyx
          Px  = Px  + va*ker(13)
          Vyy = Vyy + va*ker( 3)

          ! Double Layer Velocity Operator (K)
          Kxx = Kxx + ka*ker( 4)
          Kxy = Kxy + ka*ker( 5)
          if(axisym) & ! Py holds Kyx
          Py  = Py  + ka*ker(14)
          Kyy = Kyy + ka*ker( 6)

          ! Adjoint Double Layer Velocity Operator (K' a.k.a. L)
          Lxx = Lxx + la*ker( 7)
          Lxy = Lxy + la*ker( 8)
          if(axisym) & ! Qx holds Lyx
          Qx  = Qx  + la*ker(15)
          Lyy = Lyy + la*ker( 9)

          ! Hyper-Singular Velocity Operator (W)
          if(flag.ne.0) then
            ! BEM Matrix Structure
            Wxx = Wxx - fi*ker(10)
            Wxy = Wxy - fi*ker(11)
            if(axisym) & ! Qy holds Wyx
            Qy  = Qy  - fi*ker(16)
            Wyy = Wyy - fi*ker(12)
          else
            ! Exterior Representation Formula
            Wxx = Wxx - wa*ker(10) / ( twopi )
            Wxy = Wxy - wa*ker(11) / ( twopi )
            Wyy = Wyy - wa*ker(12) / ( twopi )
          endif

          ! No pressure integral equation discretized for disgal
          if(disgal.or.axisym.or.rifsym) cycle

          ! Single Layer Pressure Operator (P)
          Px = Px + po*ker(13) / ( pi )
          Py = Py + po*ker(14) / ( pi )

          ! Regularize Hyper-singular Pressure Operators for same element case only
          if(abs(regbit).gt.0.5) then

            ! Lower Adjacent Derivative Contribution to Hyper-Singular Pressure Operator (Q)
            Qx = Qx - fe*ker(19) * ( twoonpi ) * min(max(regbit,-1.0),1.0)
            Qy = Qy - fe*ker(20) * ( twoonpi ) * min(max(regbit,-1.0),1.0)

          else
          
            ! Hyper-Singular Pressure Operator - Part One (Q)
            Qx = Qx + qo*ker(15) * ( twoonpi )
            Qy = Qy + qo*ker(16) * ( twoonpi )

            ! Hyper-Singular Pressure Operator - Part Two (O)
            Ox = Ox + qo*ker(17) * ( twoonpi )
            Oy = Oy + qo*ker(18) * ( twoonpi )

          endif

          ! End extra self-element loop
          end do

        ! End Inner Numerical Integration Loop
        end do
      ! End Outer Numerical Integration Loop
      end do

      return
      end                         
                               


      double precision function alpha(r)

      implicit none
      integer i, j, n, s
      real r(13), x, y, x1, y1, x2, y2, z1, z2, length, tol
 
      ! Retrieve "Hidden" Data
      x = r(1); x1 = r(3); x2 = r(5)
      y = r(2); y1 = r(4); y2 = r(6)
      tol = r(12)
 
      ! Determine Length of Element
      length=sqrt( (x2-x1)**2 + (y2-y1)**2 )

      ! Determine Simplex Coordinates (z1,z2) of Input Position (x,y)
      z1 = sqrt( (x-x2)**2 + (y-y2)**2 ) / length
      z2 = sqrt( (x-x1)**2 + (y-y1)**2 ) / length
 
      ! Check for Positions Outside the Element
      if((z1.gt.1.0+tol).or.(z1.lt.0.0-tol)) then
        alpha = 0.0d0
        goto 40
      elseif((z2.gt.1.0+tol).or.(z2.lt.0.0-tol)) then
        alpha = 0.0d0
        goto 40
      !elseif((z3.gt.1.0+tol).or.(z3.lt.0.0-tol)) then
      !  alpha= 0.0d0
      !  goto 40
      endif
 
      ! Retrieve the Simplex Identity for the Shape Function being Integrated
      i = int( r( 9) )
      j = int( r(10) )
 
      ! Determine the Interpolation Function Order
      n = i + j
 
      ! Initialize the Multiplicative Counter
      alpha = 1.0d0
 
      ! Build up the Shape Function
      do 10 s = 0, i-1
        alpha = alpha * ( real(n)*z1 - real(s) ) / real(i-s)
   10 continue
 
      do 20 s = 0, j-1
        alpha = alpha * ( real(n)*z2 - real(s) ) / real(j-s)
   20 continue
 
   40 return
      end




      
      
      subroutine Bitty(x1,y1,x2,y2,x3,y3)
        
      real, intent(in) :: x1, y1, x2, y2
      real, intent(out) :: x3, y3
      real xmid, ymid, length, gradient  
                  
      xmid=(x1+x2)/2.0
      ymid=(y1+y2)/2.0  
      length=sqrt((x2-x1)**2+(y2-y1)**2)
      if(abs(y2-y1).gt.0.0001) then
        gradient=(x1-x2)/(y2-y1)
        x3=xmid+sqrt(length**2/(1.0+gradient**2))
        y3=ymid+gradient*(x3-xmid)
      else
        x3=xmid
        y3=ymid+length
      endif
      return
      end       
 


      subroutine qmap(m,i,sx,sy)

      use CONTROL, only : axisym, refsym, rifsym

      ! Determines existing boundary nodes to use in "ficticious" segment data for axi-symmetric case

      implicit none

      integer, intent(in)    :: m  ! Number of boundary nodes
      integer, intent(inout) :: i  ! Index of boundary node in non-symmetric sense

      real, intent(out) :: sx, sy  ! Sign alternators to be used by calling routine

      ! Quarter Case: Only nodes in the top right quarter actually exist
      if(axisym.and.refsym) then

        ! Quadrant 1 (++)
        if((i.ge.1).and.(i.le.m)) then
          ! Index can be used as is ...
          sx =  1.0; sy =  1.0

        ! Quadrant 3 (--)
        else if(i.lt.2-m) then
          ! Use reflected points in the x & y -axes -- hence with opposite x & y values
          sx = -1.0; sy = -1.0; i = i + 2 * ( m - 1 )

        ! Quadrant 4 (+-)
        else if(i.lt.1) then
          ! Use reflected points in the x-axis -- hence with opposite y values
          sx = +1.0; sy = -1.0; i = 2 - i

        ! Quadrant 3 (--) again -- reached in other direction
        else if(i.gt.2*m-1) then
          ! Use reflected points in the x & y -axes -- hence with opposite x & y values
          sx = -1.0; sy = -1.0; i = i - 2 * (m - 1 )

        ! Quadrant 2 (-+)
        else if(i.gt.m) then
          ! Use reflected points in the y-axis -- hence with opposite x values
          sx = -1.0; sy = +1.0; i = 2 * m - i

        ! Unknown quadrant ...........
        else
          print*,'ERROR in qmap: Bad quadrant !!!'
          stop
        end if

      ! Half Case: Only nodes on or to the right of the Y-axis actually exist
      else if(axisym.or.rifsym) then

        ! Positive x hand side
        if((i.ge.1).and.(i.le.m)) then
          ! Index can be used as is ...
          sx =  1.0; sy =  1.0

        ! Negative x hand side
        else if(i.lt.1) then
          ! Use reflected points in the y-axis -- hence with opposite y values
          sx = -1.0; sy = +1.0; i = 2 - i

        ! Negative x hand side again -- reached in other direction
        else if(i.gt.m) then
          ! Use reflected points in the y-axis again -- hence with opposite x values
          sx = -1.0; sy = +1.0; i = 2 * m - i

        ! Unknown quadrant ...........
        else
          print*,'ERROR in qmap: Bad quadrant !!!'
          stop
        end if

      ! Full Case: All nodes actually exist
      else
          
        ! If not axi-symmetric, just do a regular modulo thing ...
        i = modulo( i - 1 , m ) + 1
        sx = 1.0; sy = 1.0

      end if

      return

      end subroutine qmap



      subroutine Segment(maxnod,maxnid,List,Type,X,Y,indx,r,obseg)

      use CONTROL, only : dg, axisym, inviscid, sphereish, splines, legendre, refsym
      use BOUNDARY, only : splinesmooth

      implicit none
      integer, intent(in) :: maxnod, maxnid, List(maxnid), indx
      real, intent(in) :: X(maxnod), Y(maxnod)
      real, intent(inout) :: r(13,2)
      character, intent(in) :: Type(maxnod)*1
      logical, intent(in) :: obseg

      ! Local Variables
      integer i, ii, iii, z1, z2
      real tol, sxi, syi, sxii, syii, sxiii, syiii
      double precision xx, yy

      tol = 0.00001

      ! Initialize the Cauchy Tolerance (flags whether segment is to be used)
      r(12,:) = 0.0

      ! First Establish the Simplex Coordinates for the Indexed Node
      do 10 z2=1, 20 
        ii=indx-z2
        call qmap(maxnid,ii,sxii,syii)
        ii=List(ii)
        if((Type(ii).eq.'HJ'(dg:dg)).or.(index('TDXY',Type(ii)).ne.0)) goto 15
   10 continue
      
      ! This should never be reached
      print*,'Error in Segment (1)'
      stop

   15 do 20 z1=1, 20     
        iii=indx+z1
        call qmap(maxnid,iii,sxiii,syiii)
        iii=List(iii)
        if((Type(iii).eq.'HJ'(dg:dg)).or.(index('TDXY',Type(iii)).ne.0)) goto 25
   20 continue

      ! This should never be reached
      print*,'Error in Segment (2)'
      stop

   25 i = indx
      call qmap(maxnid,i,sxi,syi)
      i=List(i)

      ! If The Node is at a Vertex, then its corresponding Shape Function
      ! will have its Support over TWO Boundary Segments
      if((Type(i).eq.'HJ'(dg:dg)).or.(index('TDXY',Type(i)).ne.0)) then

        ! The First Segment ! Coordinates of Segment's End-Points
        if(sphereish.or.splines.or.legendre) then
          xx = dble(sxii*X(ii))
          yy = dble(syii*Y(ii))
          if(legendre) then
            call legfit(xx,yy)
          else if(sphereish) then
            call fitsmooth(xx,yy)
          else
            call splinesmooth(xx,yy)
          end if
          r(3,1) = real(xx)
          r(4,1) = real(yy)
        else
          r(3,1) = sxii*X(ii)
          r(4,1) = syii*Y(ii)
        end if

        if(sphereish.or.splines.or.legendre) then
          xx = dble(sxi *X(i))
          yy = dble(syi *Y(i))
          if(legendre) then
            call legfit(xx,yy)
          else if(sphereish) then
            call fitsmooth(xx,yy)
          else
            call splinesmooth(xx,yy)
          end if
          r(5,1) = real(xx)
          r(6,1) = real(yy)
        else
          r(5,1) = sxi *X(i)
          r(6,1) = syi *Y(i)
        end if
        
        call Bitty(r(3,1),r(4,1),r(5,1),r(6,1),r(7,1),r(8,1))     

        ! Simplex Coordinates for Node
        r( 9,1)=0.0
        r(10,1)=real(z2)
        r(11,1)=0.0    
        
        ! Cauchy Tolerance
        if(axisym.and.((r(5,1).lt.0.0).or.(r(3,1).lt.0.0))) then
          r(12,1)=0.0
        else if(axisym.and.((r(6,1).lt.0.0).or.(r(4,1).lt.0.0)).and.obseg.and.refsym) then
          r(12,1)=0.0
        else
          r(12,1)=tol * sqrt( (r(5,1)-r(3,1))**2 + (r(6,1)-r(4,1))**2 )
        end if

        ! Jump Condition Value
        r(13,1)=0.5

        ! The Second Segment ! Coordinates of Segment's End-Points
        if(sphereish.or.splines.or.legendre) then
          xx = dble(sxi*X(i))
          yy = dble(syi*Y(i))
          if(legendre) then
            call legfit(xx,yy)
          else if(sphereish) then
            call fitsmooth(xx,yy)
          else
            call splinesmooth(xx,yy)
          end if
          r(3,2) = real(xx)
          r(4,2) = real(yy)
        else
          r(3,2) = sxi*X(i)
          r(4,2) = syi*Y(i)
        end if

        if(sphereish.or.splines.or.legendre) then
          xx = dble(sxiii*X(iii))
          yy = dble(syiii*Y(iii))
          if(legendre) then
            call legfit(xx,yy)
          else if(sphereish) then
            call fitsmooth(xx,yy)
          else
            call splinesmooth(xx,yy)
          end if
          r(5,2) = real(xx)
          r(6,2) = real(yy)
        else
          r(5,2) = sxiii*X(iii)
          r(6,2) = syiii*Y(iii)
        end if
        
        call Bitty(r(3,2),r(4,2),r(5,2),r(6,2),r(7,2),r(8,2))     

        ! Simplex Coordinates for Node
        r( 9,2)=real(z1)
        r(10,2)=0.0
        r(11,2)=0.0              
        
        ! Cauchy Tolerance
        if(axisym.and.((r(3,2).lt.0.0).or.(r(5,2).lt.0.0))) then
          r(12,2)=0.0
        else if(axisym.and.((r(4,2).lt.0.0).or.(r(6,2).lt.0.0)).and.obseg.and.refsym) then
          r(12,2)=0.0
        else
          r(12,2)=tol * sqrt( (r(3,2)-r(5,2))**2 + (r(4,2)-r(6,2))**2 )
        end if

        ! Jump Value
        r(13,2)=0.5

      ! Otherwise the Support is confined to just ONE Segment
      else          
        ! Coordinates of Segment's End-Points
        if(sphereish.or.splines.or.legendre) then
          xx = dble(sxii *X(ii))
          yy = dble(syii *Y(ii))
          if(legendre) then
            call legfit(xx,yy)
          else if(sphereish) then
            call fitsmooth(xx,yy)
          else
            call splinesmooth(xx,yy)
          end if
          r( 3,1) = real(xx)
          r( 4,1) = real(yy)
        else
          r( 3,1) = sxii *X(ii)
          r( 4,1) = syii *Y(ii)
        end if

        if(sphereish.or.splines.or.legendre) then
          xx = dble(sxiii*X(iii))
          yy = dble(syiii*Y(iii))
          if(legendre) then
            call legfit(xx,yy)
          else if(sphereish) then
            call fitsmooth(xx,yy)
          else
            call splinesmooth(xx,yy)
          end if
          r( 5,1) = real(xx)
          r( 6,1) = real(yy)
        else
          r( 5,1) = sxiii*X(iii)
          r( 6,1) = syiii*Y(iii)
        end if

        call Bitty(r(3,1),r(4,1),r(5,1),r(6,1),r(7,1),r(8,1))     

        ! Simplex Coordinates for Node
        r( 9,1)=real(z1)
        r(10,1)=real(z2)
        r(11,1)=0.0        

        ! Cauchy Tolerance
        if(axisym.and.((r(3,1).lt.0.0).or.(r(5,1).lt.0.0))) then
          r(12,1)=0.0
        else if(axisym.and.((r(4,1).lt.0.0).or.(r(6,1).lt.0.0)).and.obseg.and.refsym) then
          r(12,1)=0.0
        else
          r(12,1)=tol * sqrt( (r(3,1)-r(5,1))**2 + (r(4,1)-r(6,1))**2 )
        end if

        ! Jump Value
        r(13,1)=0.5       
      endif   
      
      return
      end



      double precision function dalphads(r)
 
      implicit none
      integer i, j, k, n
      real r(13), x, y, x1, y1, x2, y2, z1, z2, length, tol
      double precision two_length
 
      ! Retrieve "Hidden" Data
      x = r(1); x1 = r(3); x2 = r(5)
      y = r(2); y1 = r(4); y2 = r(6)
      tol = r(12)
 
      ! Determine Length of Element
      length=sqrt( (x2-x1)**2 + (y2-y1)**2 )

      ! Determine Simplex Coordinates (z1,z2) of Input Position (x,y)
      z1 = sqrt( (x-x2)**2 + (y-y2)**2 ) / length
      z2 = sqrt( (x-x1)**2 + (y-y1)**2 ) / length
 
      ! Check for Positions Outside the Element
      if((z1.gt.1.0+tol).or.(z1.lt.0.0-tol)) then
        dalphads = 0.0d0
        goto 40
      elseif((z2.gt.1.0+tol).or.(z2.lt.0.0-tol)) then
        dalphads = 0.0d0
        goto 40
      endif
 
      ! Retrieve the Simplex Identity for the Shape Function being Integrated
      i = int( r( 9) )
      j = int( r(10) )
      k = int( r(11) )
      
      ! Determine the Interpolation Function Order
      n = i + j
 
      ! Initialize the Multiplicative Counter
      dalphads=1.0d0
 
      ! Build up the Shape Function
      if(k.ne.0) then
        print*,'Error: Non-boundary Shape Function request for dalphads function!!!'
        stop
      else if((n.ne.1).and.(n.ne.2).and.(n.ne.3)) then
        print*,'Error: Only linears, quadratics and cubics implemented for dalphads function!!!'
        stop      
      else if(n.eq.1) then
        ! Linear Shape Functions
        if(i.eq.1) then
          dalphads = -1.0 / length
        else if(j.eq.1) then
          dalphads = 1.0 / length
        else
          print*,'Error: Neither i nor j equal to 1 in dalphads function for linears!!!'
          stop
        endif
      elseif(n.eq.2) then
        ! Quadratic Shape Functions
        if(i.eq.n) then
          dalphads = ( 4.0 * z2 - 3.0 ) / length
        else if(j.eq.n) then
          dalphads = ( 4.0 * z2 - 1.0 ) / length
        else 
          dalphads = (4.0 - 8.0 * z2) / length
        endif
      else
        two_length = 2.0d0*length
        ! Cubic Shape Functions
        if(i.eq.n) then
          dalphads = ( -11.0 +36.0*z2 -27.0*z2*z2 ) / (two_length)
        else if(j.eq.n) then
          dalphads = ( 27.0*z2*z2 -18.0*z2 +2.0 ) / (two_length)
        else if(i.eq.j+1) then
          dalphads = ( 18.0 -90.0*z2 +81.0*z2*z2 ) / (two_length)
        else if(j.eq.i+1) then
          dalphads = ( 72.0*z2 -81.0*z2*z2 -9.0 ) / (two_length)
        else
          print*,'This should not happen!'
          stop
        endif
      end if
 
   40 return
      end






      double precision function presalpha(r)

      use CONTROL, only : ordv, ordp

      implicit none
      integer i, j, k, n, s
      real r(13), x, y, x1, y1, x2, y2, x3, y3, z1, z2, length, tol

      
      logical firsttime, sameorder
      data firsttime /.true./
      save firsttime, sameorder

      if(firsttime) then
        firsttime = .false.
        sameorder = ordv.eq.ordp
      endif
 
      ! Retrieve "Hidden" Data
      x = r(1); x1 = r(3); x2 = r(5); x3 = r(7)
      y = r(2); y1 = r(4); y2 = r(6); y3 = r(8)
      tol = r(12)
 
      ! Determine Area of Element
      ! area=x2*y3-x3*y2+x3*y1-x1*y3+x1*y2-x2*y1

      ! Determine Length of Element
      length=sqrt( (x2-x1)**2 + (y2-y1)**2 )

      ! Determine Simplex Coordinates (z1,z2) of Input Position (x,y)
      z1 = sqrt( (x-x2)**2 + (y-y2)**2 ) / length
      z2 = sqrt( (x-x1)**2 + (y-y1)**2 ) / length
 
      ! Check for Positions Outside the Element
      if((z1.gt.1.0+tol).or.(z1.lt.0.0-tol)) then
        presalpha = 0.0d0
        goto 40
      elseif((z2.gt.1.0+tol).or.(z2.lt.0.0-tol)) then
        presalpha = 0.0d0
        goto 40
      endif
 
      ! Retrieve the Simplex Identity for the Shape Function being Integrated
      i = int( r( 9) )
      j = int( r(10) )
      k = int( r(11) )
 
      ! Determine the Interpolation Function Order
      n = i + j !+ k
 
      if(sameorder) then
        n = n
      else if(i.eq.n) then
        i = 1; n = 1
      else if(j.eq.n) then
        j = 1; n = 1
      else
        presalpha = 0.0d0
        return
      endif

      ! Initialize the Multiplicative Counter
      presalpha=1.0d0
 
      ! Build up the Shape Function
      do 10 s = 0, i-1
        presalpha = presalpha * ( real(n)*z1 - real(s) ) / real(i-s)
   10 continue
 
      do 20 s = 0, j-1
        presalpha = presalpha * ( real(n)*z2 - real(s) ) / real(j-s)
   20 continue

   40 return
      end
