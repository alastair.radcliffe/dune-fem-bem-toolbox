
      subroutine dekinker(nods,X,Y,List)

      use CONTROL, only : bres, tol, heat, RealEquilibriumCircle
      use PLACE, only : noderoot
      use SIZES, only : maxnod

      !include "fe2dnag.inc"

      implicit none
      
      integer i, j, k, ii
      integer List(maxnod), nods
      real X(maxnod), Y(maxnod), smX(5),smY(5), reflevel
      logical changed, reverse
     
      changed = .false.

      ! Loop the nodes to be smoothed starting from the top
      do i = nods + 3, 4, -1
        ! Set-up smoothing stencil node coordinates
        do k = 1, 5
          if(i-k.eq.nods+2) then
            smX(k) = -2.0*bres;                  smY(k) = Y(List(nods-1))
          else if(i-k.eq.nods+1) then
            smX(k) = -bres;                      smY(k) = Y(List(nods-1))
          else if(i-k.lt.1) then
            smX(k) = max(X(List(1)),X(List(2))); smY(k) = -real(1+k-i) * bres
          else
            smX(k) = X(List(i-k));               smY(k) = Y(List(i-k))
          end if
          call RealEquilibriumCircle(smX(k),smY(k))
        end do
        ! For substrate problems, determine whether stencil overhangs or not
        reverse = heat .and. ( smX(2) .ge. smX(4) )
        ! Only allow i=4 case if stencil will be reversed
        if((i.eq.4).and.(.not.reverse)) cycle
        ! Flip under stencils over
        if(reverse) then
          
          !call gmshOpen(76,'reverse ')
          
          !do k = 1, 4
          !  call gmshSLd(76,smX(k),smY(k),smX(k+1),smY(k+1),2.0d0)    
          !end do
          !call gmshSP(76,smX(5),smY(5),1.0d0)
          
          reflevel = 2.0 * smY(1)
          do k = 1, 5
            smY(k) = reflevel - smY(k)
          end do

          !do k = 1, 4
          !  call gmshSLd(76,smX(k),smY(k),smX(k+1),smY(k+1),1.0d0)    
          !end do
          !call gmshSP(76,smX(5),smY(5),1.0d0)
          
        end if
        ! Perform the smoothing
        call smooth(smX,smY,reverse,changed)
        ! Flip reversed stencils back
        if(reverse) then

          !do k = 1, 4
          !  call gmshSLd(76,smX(k),smY(k),smX(k+1),smY(k+1),-1.0d0)    
          !end do
          !call gmshSP(76,smX(5),smY(5),1.0d0)
          
          do k = 1, 5
            smY(k) = reflevel - smY(k)
          end do

          !do k = 1, 4
          !  call gmshSLd(76,smX(k),smY(k),smX(k+1),smY(k+1),-2.0d0)    
          !end do
          !call gmshSP(76,smX(5),smY(5),1.0d0)
          
          !call gmshClose(76)
        end if
        ! Place back the smoothed coordinates if changed
        if(.not.changed) cycle
        do k = 1, 5
          if((i-k.le.0).or.(i-k.gt.nods)) then
            cycle
          else
            do j = 1, maxnod
              if(noderoot(j).eq.noderoot(List(i-k))) then
                if(X(j).gt.tol) X(j) = smX(k); if(Y(j).gt.tol) Y(j) = smY(k)
              end if
            end do
          end if
        end do
      end do
      
      end subroutine dekinker




      subroutine smooth(x,y,rev,changed)

      use CONTROL, only : zero, sphereish, tol, fita, fitb, relaxing, monocharge, heat!, fitan, fitbn
      use CONTROL, only : inflex, inflexion, ejectP, ejectG, ejectK, ejectC

      implicit none

      ! Each call of this subroutine will effectively push the left-most kink right-wards

      !  Works on a five       Y                         3
      !  node stencil as       |         1_______2______/ \_____4
      !  illustrated ...       |                                 \
      !  (kink at node 3)      |                                  5
      !                        O_______X

      integer i
      real x(5), y(5), cr1, cr2, crossLight, stenVol, xt3, yt3, xt4, yt4
      double precision s, t, x1,x2,x3,x4,x5,y1,y2,y3,y4,y5, r
      logical changed, rev
      external crossLight, stencilVolume

      changed = .false.

      ! Only even-out boundary node distances if REALLY necessary ...
      cr1 = ( x(2) - x(3) ) ** 2 + ( y(2) - y(3) ) ** 2
      cr2 = ( x(4) - x(3) ) ** 2 + ( y(4) - y(3) ) ** 2
      ! Mathematica formulae don't work for reversed stencils
      if((max(cr1/cr2,cr2/cr1).lt.1.5).or.(x(3).lt.tol).or.rev) goto 10

      ! Make a note of the initial volume under the stencil
      ! cr2 = stenVol(x,y,'before ')

      ! Near-vertical stencils done differently
      if(abs(x(2)-x(4)).lt.100.0*tol) then
        !x(3) = ( x(2) + x(4) ) / 2.0
        !y(3) = ( y(2) + y(4) ) / 2.0
      else

      ! To use Mathematicaed output directly
      x1 = dble(x(1)); x2 = dble(x(2)); x3 = dble(x(3)); x4 = dble(x(4)); x5 = dble(x(5))
      y1 = dble(y(1)); y2 = dble(y(2)); y3 = dble(y(3)); y4 = dble(y(4)); y5 = dble(y(5))

      r  = x3 &
              -(x2**3 + 4*x2**2*x3 - x2**2*x4 - 8*x2*x3*x4 - x2*x4**2 +    &
          4*x3*x4**2 + x4**3 + x2*y2**2 + 4*x3*y2**2 +    &
          x4*y2**2 - 2*x2*y2*y4 - 8*x3*y2*y4 - 2*x4*y2*y4 +    &
          x2*y4**2 + 4*x3*y4**2 + x4*y4**2 -    &
          Sqrt(x2**2 - 2*x2*x4 + x4**2 + (y2 - y4)**2)*    &
           Sqrt(9*x2**4 + 9*x4**4 +    &
             2*x2*(8*x3*(y2 - y3) + x4*(y2 - y4))*(y2 - y4) +    &
             16*x3**2*(y2 - y4)**2 +    &
             16*x3*x4*(y2 - y4)*(y3 - y4) -    &
             x4**2*(y2 - y4)*(7*y2 - 16*y3 + 9*y4) -    &
             x2**2*(18*x4**2 - (y2 - y4)*(9*y2 - 16*y3 + 7*y4))))    &
        /(4.*(x2**2 - 2*x2*x4 + x4**2 + (y2 - y4)**2))

      y3 = y3 +    &
        (3*x2**4 - 6*x2**3*x4 + 6*x2*x4**3 - 3*x4**4 +    &
         5*x2**2*y2**2 - 4*x2*x4*y2**2 - x4**2*y2**2 + 2*y2**4 -    &
         4*x2**2*y2*y3 + 8*x2*x4*y2*y3 - 4*x4**2*y2*y3 -    &
         4*y2**3*y3 - 6*x2**2*y2*y4 + 6*x4**2*y2*y4 -    &
         4*y2**3*y4 + 4*x2**2*y3*y4 - 8*x2*x4*y3*y4 +    &
         4*x4**2*y3*y4 + 12*y2**2*y3*y4 + x2**2*y4**2 +    &
         4*x2*x4*y4**2 - 5*x4**2*y4**2 - 12*y2*y3*y4**2 +    &
         4*y2*y4**3 + 4*y3*y4**3 - 2*y4**4 +    &
         Sqrt((x2 - x4)**2*    &
           (x2**2 - 2*x2*x4 + x4**2 + (y2 - y4)**2)*    &
           (9*x2**4 + 9*x4**4 +    &
             2*x2*(8*x3*(y2 - y3) + x4*(y2 - y4))*(y2 - y4) +    &
             16*x3**2*(y2 - y4)**2 +    &
             16*x3*x4*(y2 - y4)*(y3 - y4) -    &
             x4**2*(y2 - y4)*(7*y2 - 16*y3 + 9*y4) -    &
             x2**2*(18*x4**2 - (y2 - y4)*(9*y2 - 16*y3 + 7*y4))))    &
         )/(4.*(x2**2 - 2*x2*x4 + x4**2 + (y2 - y4)**2)*    &
         (y2 - y4))

      x(3) = real(r); y(3) = real(y3)

      end if

      ! Check the stencil volume has not varied
      ! cr1 = stenVol(x,y,'after  ')

      !if(abs(cr1-cr2).gt.tol) then
      !  print*,'ERROR in smooth: Changed stencil area !!!', cr2, ' ne ',cr1
      !  !stop
      !end if

      changed = .true.

      ! Only allow dekinking for sphereish drops  .and.(.not.sphereish).and. (x(3).gt.tol))
   !10 if(.not.(relaxing)) return
   10 cr1 = cr2

      ! Make a note of the initial volume under the stencil
      ! cr2 = stenVol(x,y,'before ')

      ! Negligible amount of liquid below the top node so just move at will ... (!)
      if(x(3).lt.tol) then

        ! Heated droplets should always have a flat top
        if(heat) then
          y(3) = y(4)
          changed = .true.
        ! Less than critically charged drops can just keep a level head
        else if(relaxing) then
          if((fita.lt.2.6*fitb).and.(fita.gt.-1.6*fitb)) then
            x4 = dble(x(4)); y4 = dble(y(4))
            call fitsmooth(x4,y4)
            y3 = dble(y(4)) + dble(x(4)) * ( dble(fita) - y4 ) / x4
            !if(y(3).lt.real(y3)) then
              y(3) = real(y3)
              changed = .true.
            !end if
          end if
        else if(monocharge) then
          if(y(3).lt.y(4)) then
            call rswap(y(3),y(4))
            changed = .true.
          ! Check for a surface inflexion here for triggering the explosions of prolates ...
          else if(crossLight(x(3)-x(4),y(3)-y(4),x(5)-x(4),y(5)-y(4)).lt.tol) then
            inflexion = inflex
            ! Set-up the transformation parameters to the canonical tip geometry
            ejectG = dble( y(5) - y(3) ) / dble(x(5))
            ejectK = 0.25d0 * ( ejectG * ejectG * ejectG * ejectG / dble(ejectP) ) ** (1.0d0/3.0d0)
            ejectC = dble(y(3)) * ejectK - ejectG * ejectG / 2.0d0
          end if
        end if

      else

      ! First establish if there is a kink in the first place
      if(rev) then
        cr1 = crossLight(x(4)-x(3),y(4)-y(3),x(2)-x(3),y(2)-y(3))
      else
        cr1 = crossLight(x(2)-x(3),y(2)-y(3),x(4)-x(3),y(4)-y(3))
      end if

      if(cr1.ge.0.0) return

      changed = .true.

      ! To use Mathematicaed output directly
      x1 = dble(x(1)); x2 = dble(x(2)); x3 = dble(x(3)); x4 = dble(x(4)); x5 = dble(x(5))
      y1 = dble(y(1)); y2 = dble(y(2)); y3 = dble(y(3)); y4 = dble(y(4)); y5 = dble(y(5))

      if(abs(x4-x2).le.dble(10.0*tol)) then
        ! Near-vertical boundary - assume a negligible variation of the radial coordinate over the stencil

        !   x3 + s - x2   x4 - s  - x2
        !   ----------- = ------------
        !     y3 - y2       y4 - y2

        s = 1.0d0 / (y3-y2) + 1.0d0 / (y4-y2)
        s = ( (x4-x2)/(y4-y2) - (x3-x2)/(y3-y2) ) / s

        x(3) = real( x3 + s )
        x(4) = real( x4 - s )

      else

        ! Define the gradient required to remove the kink
        r = ( y4 - y2 ) / ( x4 - x2 )

        ! Calculate the displacement needed for node 3 to achieve this gradient of r
        s = (r*x2 - r*x3 - y2 + y3)/(x2 - x5 + r*y2 - r*y5)

        ! How far to push node 4 out along the construction normal to keep the VOLUME below the whole stencil unchanged
        t = (2.*(y2 - y5)*(x3*x5 - x5**2 + x2*(-x3 + x5) - y2*y3 + y2*y5 + y3*y5 - y5**2))

        ! Check the denominator to be used for t is not zero
        if(abs(t).lt.zero) then
          print*,'ERROR in smooth: Zero t parameter denominator !!!'
          stop
        else
          ! Non-Linear Correction
          t =  (-(x2*x3**2) - x2*x3*x4 + x3**2*x5 + x2*x4*x5 +     &
           x3*x4*x5 + x2*x5**2 - x4*x5**2 - x5**3 - s*x2*x3*y2 +     &
           s*x2*x4*y2 + s*x3*x5*y2 - s*x4*x5*y2 - x3*y2*y3 -     &
           2*x4*y2*y3 - s*y2**2*y3 + x3*y2*y4 - x5*y2*y4 +     &
           s*y2**2*y4 + s*x2*x3*y5 - s*x2*x4*y5 - s*x3*x5*y5 +     &
           s*x4*x5*y5 + 2*x4*y2*y5 + x5*y2*y5 + x3*y3*y5 +     &
           2*x4*y3*y5 + 2*s*y2*y3*y5 - x3*y4*y5 + x5*y4*y5 -     &
           2*s*y2*y4*y5 - 2*x4*y5**2 - x5*y5**2 - s*y3*y5**2 +     &
           s*y4*y5**2 + Sqrt(-4*s*(y2 - y5)*     &
              (x2*(x3 - x5) - x3*x5 + x5**2 + y2*y3 - y2*y5 -     &
                y3*y5 + y5**2)*     &
              (x2**3 + x4**2*x5 + s*x4*x5*y2 + s*y2**3 +     &
                x4*y2*y3 - x4*y2*y4 - s*y2**2*y4 -     &
                x2*(x4**2 + x3*(x4 + x5) + s*x4*(y2 - y5) +     &
                   (s*x5 - y2 + y3)*(y2 - y5)) +     &
                x3*(x4*x5 + 2*(y2 - y4)*(y2 - y5)) - s*x4*x5*y5 -     &
                2*s*y2**2*y5 - x4*y3*y5 + x4*y4*y5 +     &
                2*s*y2*y4*y5 + s*y2*y5**2 - s*y4*y5**2 +     &
                x2**2*(x3 - x5 + s*y2 - s*y5)) +     &
             (-(x3**2*x5) + x4*x5**2 + x5**3 + s*x4*x5*y2 +     &
                2*x4*y2*y3 + s*y2**2*y3 + x5*y2*y4 - s*y2**2*y4 -     &
                x3*(x4*x5 + (s*x5 - y3 + y4)*(y2 - y5)) -     &
                s*x4*x5*y5 - 2*x4*y2*y5 - x5*y2*y5 - 2*x4*y3*y5 -     &
                2*s*y2*y3*y5 - x5*y4*y5 + 2*s*y2*y4*y5 +     &
                2*x4*y5**2 + x5*y5**2 + s*y3*y5**2 - s*y4*y5**2 +     &
                x2*(x3**2 - x4*x5 - x5**2 - s*x4*y2 + s*x4*y5 +     &
                   x3*(x4 + s*y2 - s*y5)))**2))/t
        end if

        ! Dekink
        x(3) = x(3) + real(s * ( y2 - y5 )); y(3) = y(3) + real(s * ( x5 - x2 ))
        x(4) = x(4) - real(t * ( y2 - y5 )); y(4) = y(4) - real(t * ( x5 - x2 ))

      end if
      end if

      ! Check the stencil volume has not varied
      !cr1 = stenVol(x,y,'after  ')

      !if(abs(cr1-cr2).gt.tol) then
      !  print*,'ERROR in smooth: Changed stencil area !!!', cr2, ' ne ',cr1
      !  !stop
      !end if

      ! cr1 = crossLight(x(2)-x(3),y(2)-y(3),x(4)-x(3),y(4)-y(3))

      end subroutine smooth



      real function stenVol(x,y,char)

      implicit none
      integer i
      real x(5), y(5), stencilVolume
      character*7 char

      ! Make a note of the initial volume under the stencil
      stenVol = 0.0
      ! call gmshOpen(91,char)
      do i = 1, 4
        stenVol = stenVol + stencilVolume(x(i),y(i),x(i+1),y(i+1))
        ! call gmshSL(91,x(i),y(i),x(i+1),y(i+1))
      end do
      ! call gmshClose(91)

      end function stenVol


      ! Volume (area with x weighting) under a given stencil
      real function stencilVolume(a,b,c,d)

      implicit none

      real a, b, c, d

      stencilVolume = -(a-c)*(a*(2.0*b+d)+c*(b+2.0*d))/6.0d0

      end function stencilVolume



      ! Determine neighbour nodes for linear interpolation of pressure
      subroutine linint(m,n,i,p,u,interpol)

      use CONTROL, only : disgal

      implicit none
      integer m,n,i,p(-1:1), a(3), x(3,-1:1), k, j, s, fn, gn, r
      double precision u(-1:1)
      logical test, interpol

      if(disgal) then
        interpol = .false.
        return
      endif

      p = 0; u = 0.0D0; r = m

      ! Find simplex coordinates of nodee
      call Simplex(m,i,a(1),a(2),a(3),.False.)

      ! Set start positions for searches
      x(1,:) = a(1); x(2,:) = a(2); x(3,:) = a(3)
      
      ! Check for a central node (done specially)
      if( (a(1).eq.a(2)).and.(a(2).eq.a(3)) ) then
	! Set Positions and weights for central node case
       	p(-1) = 1; p(0) = 1; p(+1) = 1; u = 1.0D0/3.0D0
        x = 0; x(1,-1) = m; x(2,0) = m; x(3,+1) = m
      else 
        ! Loop over simplex coords to ...
        do 30 j = 1, 3
          ! ... find which side to start from (which coord. does not change)
          if( (a(j).eq.0).or. &
            ((a(1)*a(2)*a(3).ne.0).and.(a(j).gt.a(fn(j))).and.(a(j).gt.a(gn(j))))) then
            ! Loop over search directions ("up"-> s=+1, and "down" -> s=-1)
            do 20 s = 1, -1, -2
            ! Search loop
              do 10 k = 1, 4
                ! Check if found a low order node
                if(test(m,n,x(1,s),x(2,s),x(3,s))) exit
                ! Step along other (than "j") simplex coordinates
                x(fn(j),s) = x(fn(j),s)+s; x(gn(j),s) = x(gn(j),s)-s; p(s) = p(s) + abs(s)
                !print*,'step ',m,n,i,'(',a,')',j,s,k,p
   10         continue
   20       continue
	    exit
          endif
   30   continue
      endif
      
      ! Check to see whether any stepping has been performed ...
      if(p(-1)+p(0)+p(+1).eq.0) then
        ! ... if NOT, then the high order node corresponds with a low order one,
        ! linear interpolation is not required, so just return the matching LOW ORDER node number.
        r = n
      else if( ( (p(-1).ne.0).or.(p(+1).ne.0) ).and.(p(0).eq.0)) then
        ! Otherwise, return the HIGH ORDER node numbers between which linear interpolation is required,
        ! and set-up the weights in accordance with the amount of stepping done.
        u(-1) = real( p(+1) + p( 0) ) / real( p(+1) + p(0) + p(-1) )
        u( 0) = real( p(-1) + p(+1) ) / real( p(+1) + p(0) + p(-1) )
        u(+1) = real( p(-1) + p( 0) ) / real( p(+1) + p(0) + p(-1) )
      endif

      call Simplex(r, p(-1), x(1,-1)*r/m, x(2,-1)*r/m, x(3,-1)*r/m, .True.)
      call Simplex(r, p(+1), x(1,+1)*r/m, x(2,+1)*r/m, x(3,+1)*r/m, .True.)
      call Simplex(r, p( 0), x(1, 0)*r/m, x(2, 0)*r/m, x(3, 0)*r/m, .True.)

      if(u(0).gt.0.99) then
        p(0) = 0
        u(0) = 0.0
      endif

      if((abs(u(-1)+u(0)+u(+1)).lt.0.99)) then
        if(.not.((p(1).eq.p(0)).and.(p(0).eq.p(-1)))) then
          print*,'Bad p ssssss'
          stop
        endif
        interpol = .false.
      else
        interpol = .true.
      endif

      return
      end



      logical function test(m,n,x,y,z)
      
      implicit none
      integer m,n,x,y,z

      test = int(real(x*n)/real(m)) &
           + int(real(y*n)/real(m)) &
           + int(real(z*n)/real(m)) &
           .eq.n
      return
      end


      integer function fn(i)

      integer i
      
      if(i.eq.1) then
        fn = 2
      else if(i.eq.2) then
        fn = 3
      else if(i.eq.3) then
        fn = 1
      else
        print*,'Error in fn.'
        stop
      endif

      return
      end



      integer function gn(i)

      integer i
      
      if(i.eq.1) then
        gn = 3
      else if(i.eq.2) then
        gn = 1
      else if(i.eq.3) then
        gn = 2
      else
        print*,'Error in gn.'
        stop
      endif

      return
      end
