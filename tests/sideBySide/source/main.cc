#include <config.h>

// iostream includes
#include <iostream>

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

// include header of elliptic solver
#include "../../common/source/femscheme.hh"
#include "../../common/source/poisson.hh"

// extra includes needed for gluing
#include <dune/common/parallel/mpihelper.hh>
#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/grid/sgrid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/geometrygrid.hh>
#include <dune/geometry/quadraturerules.hh>


#include <dune/common/version.hh>

#include <dune/grid/utility/structuredgridfactory.hh>
#include <doc/grids/gridfactory/hybridtestgrids.hh>

#include <dune/fem_fem_coupling/surfacegridclass.hh>

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( HGridType &grid, HGridType &gryd, int step )
{
  // we want to solve the problem on the leaf elements of the grid
  typedef typename Dune::FemFemCoupling::Glue<HGridType>::GridPartType GridPartType;
  GridPartType gridPart(grid);
  GridPartType grydPart(gryd);

  typedef typename Dune::FemFemCoupling::Glue<HGridType>::GlueType GlueType;

  GlueType* bob = 0;

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType>::FunctionSpaceType FunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< FunctionSpaceType, GridPartType > ModelType;

  typedef typename ModelType::ProblemType ProblemType ;
  ProblemType* problemPtr = 0 ;
  const std::string problemNames [] = { "cos", "sphere", "sin", "corner", "curvedridges" };
  const int problemNumber = Dune::Fem::Parameter::getEnum("poisson.problem", problemNames, 0 );
  switch ( problemNumber )
  {
    case 0:
      problemPtr = new CosinusProduct< FunctionSpaceType > ();
      break ;
    case 1:
      problemPtr = new SphereProblem< FunctionSpaceType > ();
      break ;
    case 2:
      problemPtr = new SinusProduct< FunctionSpaceType > ();
      break ;
    case 3:
      problemPtr = new BulkProblem< FunctionSpaceType > ();
      break ;
    case 4:
      problemPtr = new SurfaceProblem< FunctionSpaceType > ();
      break ;
    default:
      problemPtr = new CosinusProduct< FunctionSpaceType > ();
  }
  assert( problemPtr );
  ProblemType& problem = *problemPtr ;

  // implicit model for left hand side
  ModelType implicitModel( problem, gridPart );
  ModelType implycitModel( problem, grydPart );

  // poisson solver
  typedef FemScheme< ModelType, istl > SchemeType;
  SchemeType scheme( gridPart, implicitModel, "left" );
  SchemeType schyme( grydPart, implycitModel, "right" );

  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GridPartType > GridExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, gridPart, 5 );
  GridExactSolutionType grydExactSolution("exact solution", problem, grydPart, 5 );
  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *, GridExactSolutionType * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution) ; // tuple with pointers
  IOTupleType ioTople( &(schyme.solution()), &grydExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step , "left" ) );
  DataOutputType detaOutput( gryd, ioTople, DataOutputParameters( step , "right" ) );

  // typedef CouplingConstraintsStore<DiscreteFunctionSpaceType> CStore;

  // CStore::glueHome( CStore::makeGlue( &gridPart, &grydPart ) );
  // CStore::checkGlue();

  // calculate error
  double error = 0;
  double errer = 0;

  // iterate a certain number of times
  for( int n = 0; n <= 11; ++n )
  {
    // set-up the coupling
    scheme.setup(schyme.solution());
    schyme.setup(scheme.solution());

    // setup the right hand side
    scheme.prepare();
    schyme.prepare();

    // apply the coupling
    scheme.couple(schyme.solution());
    schyme.couple(scheme.solution());

    // solve once
    scheme.solve( true );
    schyme.solve( true );

    // calculate standard error
    // select norm for error computation
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    NormType norm( gridPart );
    NormType nurm( grydPart );
    error = norm.distance( gridExactSolution, scheme.solution() );
    errer = nurm.distance( grydExactSolution, schyme.solution() );
    std::cout << "                                                   Error at iteration " << n << " = " << error << " + " << errer << " = " << error + errer << std::endl;

    // check for convergence
    bool done = scheme.stop(); bool dune = schyme.stop();
    if ( ( n > 3 ) && done && dune )
    {
      std::cout << "Converged at iteration " << n << " to an error of " << error+errer << std::endl;
      break;
    }

    // only write output (and continue iterating) if solution still converging
    else
    {
      // write initial solve
      dataOutput.write();
      detaOutput.write();

      // reset the problem
      scheme.reset();
      schyme.reset();
    }
  }
  return error + errer;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // set slice value to be used for the gluing
  SliceStore::sliceHome(0.6);
  SliceStore::x(0); SliceStore::y(1); SliceStore::z(1);

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string grydkey = Dune::Fem::IOInterface::defaultGridKey( "fem.io.macroGrydFile", HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );
  const std::string grydfile = Dune::Fem::Parameter::getValue< std::string >( grydkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grids: " << gridfile << " and " << grydfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  Dune::GridPtr< HGridType > grydPtr( grydfile );
  HGridType& grid = *gridPtr ;
  HGridType& gryd = *grydPtr ;

  // do initial load balance
  grid.loadBalance();
  gryd.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "poisson.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );
  Dune::Fem::GlobalRefine::apply( gryd, level * refineStepsForHalf );

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "poisson.repeats", 0 );

  // calculate first step
  double oldError = algorithm<HGridType>( grid, gryd, (repeats > 0) ? 0 : -1 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );
    Dune::Fem::GlobalRefine::apply( gryd, refineStepsForHalf );

    const double newError = algorithm( grid, gryd, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
