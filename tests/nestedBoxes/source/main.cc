#include <config.h>

// iostream includes
#include <iostream>
#include <complex>

// include grid part
#include <dune/fem/gridpart/filteredgridpart.hh>
#include "function.hh"
#include "../../common/source/radialfilter.hh"
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

// include header of elliptic solver
#include "../../common/source/femscheme.hh"
#include "../../common/source/poisson.hh"

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( HGridType &grid, int step )
{
  // create host grid part consisting of leaf level elements
  typedef typename Dune::FemFemCoupling::Glue<HGridType>::HostGridPartType HostGridPartType;
  HostGridPartType hostGridPart( grid );

  // create filter
  typedef typename Dune::FemFemCoupling::Glue<HGridType>::FilterType FilterType;
  typename FilterType::GlobalCoordinateType center( 0.5 );
  // outer overlapping mesh setup
  typename FilterType::ctype radius( -SliceStore::sliceHome(0) );
  FilterType Filter( hostGridPart, center, radius, true );
  FilterType fylter( hostGridPart, center, radius, false );
  // inner overlapping mesh setup
  typename FilterType::ctype radias( -SliceStore::sliceHome(0,true) );
  FilterType filter( hostGridPart, center, radias, true );
  FilterType Fylter( hostGridPart, center, radias, false );

  // create filtered grid part, only a subset of the host grid part's entities are contained
  typedef typename Dune::FemFemCoupling::Glue<HGridType>::GridPartType GridPartType;
  GridPartType GridPart( hostGridPart, Filter );
  GridPartType gridPart( hostGridPart, filter );
  GridPartType grydPart( hostGridPart, fylter );
  GridPartType GrydPart( hostGridPart, Fylter );

  // use a scalar function space
  typedef typename Dune::FemFemCoupling::Glue<HGridType>::FunctionSpaceType FunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< FunctionSpaceType, GridPartType > ModelType;

  typedef typename ModelType::ProblemType ProblemType ;
  ProblemType* problemPtr = 0 ;
  const std::string problemNames [] = { "cos", "sphere", "sin", "corner", "curvedridges" };
  const int problemNumber = Dune::Fem::Parameter::getEnum("poisson.problem", problemNames, 0 );
  switch ( problemNumber )
  {
    case 0:
      problemPtr = new CosinusProduct< FunctionSpaceType > ();
      break ;
    case 1:
      problemPtr = new SphereProblem< FunctionSpaceType > ();
      break ;
    case 2:
      problemPtr = new SinusProduct< FunctionSpaceType > ();
      break ;
    case 3:
      problemPtr = new BulkProblem< FunctionSpaceType > ();
      break ;
    case 4:
      problemPtr = new SurfaceProblem< FunctionSpaceType > ();
      break ;
    default:
      problemPtr = new CosinusProduct< FunctionSpaceType > ();
  }
  assert( problemPtr );
  ProblemType& problem = *problemPtr ;

#if DIAG_PLOT
  // construct an instance of the problem
  typedef School::NullFunction< FunctionSpaceType > FunctionType;
  FunctionType function;
#endif

  // implicit model for left hand side
  ModelType ImplicitModel( problem, GridPart );
  ModelType implicitModel( problem, gridPart );
  ModelType implycitModel( problem, grydPart );
  ModelType ImplycitModel( problem, GrydPart );

  // poisson solver
  typedef FemScheme< ModelType, istl > SchemeType;
  SchemeType Scheme( GridPart, ImplicitModel, "junkinside" );
  SchemeType schyme( grydPart, implycitModel, "outside" );
  SchemeType scheme( gridPart, implicitModel, "inside" );
  SchemeType Schyme( GrydPart, ImplycitModel, "junkoutside" );

#if DIAG_PLOT
  typedef Dune::Fem::GridFunctionAdapter< FunctionType, GridPartType > GridExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", function, gridPart, 5 );
  GridExactSolutionType grydExactSolution("exact solution", function, grydPart, 5 );
#else
  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GridPartType > GridExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, gridPart, 5 );
  GridExactSolutionType grydExactSolution("exact solution", problem, grydPart, 5 );
#endif

  //! input/output tuple and setup datawritter
#if DIAG_PLOT
  typedef Dune::tuple< GridExactSolutionType * > IOTupleType;
#else
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *, GridExactSolutionType * > IOTupleType;
#endif
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
#if DIAG_PLOT
  IOTupleType ioTuple( &gridExactSolution) ; // tuple with pointers
  IOTupleType ioTople( &grydExactSolution) ; // tuple with pointers
#else
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution) ; // tuple with pointers
  IOTupleType ioTople( &(schyme.solution()), &grydExactSolution) ; // tuple with pointers
#endif
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step , "inner" ) );
  DataOutputType detaOutput( grid, ioTople, DataOutputParameters( step , "outer" ) );
#if DIAG_PLOT
  dataOutput.write();
  detaOutput.write();
#endif

  // calculate error
  double error = 0;
  double errer = 0;

#if !DIAG_PLOT

  // iterate a certain number of times
  for( int n = 0; n <= 101; ++n )
  {
    // set-up the coupling
    scheme.setup(schyme.solution());
    schyme.setup(scheme.solution());

    // setup the right hand side
    scheme.prepare();
    schyme.prepare();

    // apply the coupling
    scheme.couple(schyme.solution());
    schyme.couple(scheme.solution());

    // solve once
    scheme.solve( true );
    schyme.solve( true );

    // calculate standard error
    // select norm for error computation
    typedef Dune::Fem::L2Norm< GridPartType > NormType;
    NormType norm( gridPart );
    NormType nurm( grydPart );
    error = std::real(norm.distance( gridExactSolution, scheme.solution() ));
    errer = std::real(nurm.distance( grydExactSolution, schyme.solution() ));
    std::cout << "                                                   Error at iteration " << n << " = " << error << " + " << errer << " = " << error + errer << std::endl;

    // check for convergence
    bool done = scheme.stop(); bool dune = schyme.stop();
    if ( ( n > 3 ) && ( error + errer > 2.0079 ) )
    {
      break;
    }

    else if ( ( n > 3 ) && done && dune )
    {
      std::cout << "Converged at iteration " << n << " to an error of " << error+errer << std::endl;
      break;
    }

    // only write output (and continue iterating) if solution still converging
    else
    {
      // write initial solve
      dataOutput.write();
      detaOutput.write();

      // reset the problem
      scheme.reset();
      schyme.reset();
    }
  }
#endif
  return error + errer ;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // set slice value to be used for the gluing
  SliceStore::sliceHome(-Dune::Fem::Parameter::getValue< double >( "coupling.inner" ));
  SliceStore::x(1); SliceStore::y(1);
  if( HGridType::dimension < 3 )
  {
    SliceStore::z(0);
  }
  else
  {
    SliceStore::z(1);
  }
  SliceStore::sliceHome(-Dune::Fem::Parameter::getValue< double >( "coupling.outer" ),true);

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "poisson.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "poisson.repeats", 0 );

  // calculate first step
  double oldError = algorithm( grid, (repeats > 0) ? 0 : -1 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    const double newError = algorithm( grid, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
