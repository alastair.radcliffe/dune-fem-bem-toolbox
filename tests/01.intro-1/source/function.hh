#ifndef DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH
#define DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH

// dune-common includes
#include <dune/common/documentation.hh>

namespace School
{

  // Function
  // --------

  /*
   * A class simply documenting the interface for a function
   */
  struct Function
  {
    // domain type
    typedef Dune::ImplementationDefined DomainType;
    // range type
    typedef Dune::ImplementationDefined RangeType;

    // evaluate function in global coordinate x
    void evaluate ( const DomainType &x, RangeType &value ) const;

  };

  // QuadraticFunction
  // -----------------

  /*
   * A simple example function
   */
  template< class FunctionSpace >
  class QuadraticFunction
  {
    typedef QuadraticFunction< FunctionSpace > ThisType;

    static const int dimDomain = FunctionSpace::dimDomain;
    static const int dimRange = FunctionSpace::dimRange;

  public:
    typedef FunctionSpace FunctionSpaceType;
    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static void evaluate ( const DomainType &x, RangeType &value )
    {
      value = RangeType( 0 );

      value[ 0 ] = 1;
      for( int i = 0; i < dimDomain; ++i )
        value[ 0 ] *= x[ i ]*x[ i ];
    }

  };

} // namespace School

#endif // #ifndef DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH
